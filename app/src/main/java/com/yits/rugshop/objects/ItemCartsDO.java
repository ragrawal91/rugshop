package com.yits.rugshop.objects;

import java.util.ArrayList;

public class ItemCartsDO {
	
	public String ItemProductId="",ItemName="",ItemId="",ItemBasePrice="",ItemDsicountedPrice="",ItemFinalPrice="",ItemPerSaved="";
	public String ItemSize="",ItemColor="",ItemPattern="",ItemMaterial="",ItemSmallImgUrl="",ItemMediumImgUrl="",ItemBigImgUrl="";
	public String ItemRating="";
	public String categoryId="";
	public int ItemQuantity=1,ItemStatus=1;



	public ArrayList<ItemsSizeDO> itemSizeList=new ArrayList<ItemsSizeDO>();
	public ArrayList<SizeListDO> sizeListDOs =new ArrayList<SizeListDO>();
	public ArrayList<String> itemColorList=new ArrayList<String>();


	public ItemCartsDO(String itemId, String productID, String itemName, int ItemQuantity, String basePrice, String discountedPrice, String finalPrice, String itemSize, String itemColor, String itemPattern,String ItemMaterial
	,String ItemSmallImgUrl,String ItemMediumImgUrl,String ItemBigImgUrl,int ItemStatus,String ItemRating,String ItemPerSaved,String Catogiryid)
	{
		this.ItemId=itemId;
		this.ItemProductId=productID;
		this.ItemName=itemName;
		this.ItemBasePrice=basePrice;
		this.ItemDsicountedPrice=discountedPrice;
		this.ItemFinalPrice=finalPrice;
		this.ItemSize=itemSize;
		this.ItemQuantity=ItemQuantity;
		this.ItemColor=itemColor;
		this.ItemPattern=itemPattern;
		this.ItemMaterial=ItemMaterial;
		this.ItemBigImgUrl=ItemBigImgUrl;
		this.ItemSmallImgUrl=ItemSmallImgUrl;
		this.ItemMediumImgUrl=ItemMediumImgUrl;
		this.ItemStatus=ItemStatus;
		this.ItemRating=ItemRating;
		this.ItemPerSaved=ItemPerSaved;
		this.categoryId =Catogiryid;
	}

	public ItemCartsDO() {

	}


}
