package com.yits.rugshop.objects;

import java.util.ArrayList;

public class ItemsDO {


    public String ItemName = "", ProductID = "", ItemId = "", CategoryId = "", ItemSizeGroup = "", ItemPerSaved = "", ItemMaterial = "", ItemDesc = "";
    public String ItemPattern = "", ItemRating = "", ItemProDesc = "", ItemSmallImgUrl = "", ItemMediumImgUrl = "", ItemBigImgUrl = "";
    public String Status = "", ErrorMessage = "";
    public int ItemQuantity = 1, ItemStatus = 1, viewcount = 0;
    public String ItemBasePrice = "", ItemDsicountedPrice = "", ItemFinalPrice = "", ItemSize = "", ItemColor = "";
    public boolean cartIconChecked;
    public boolean favIconChecked;
    public boolean positionSelected;
    public String catogiryname = "";
    public int selectedSizePosition = 0;
    public ArrayList<String> urls = new ArrayList<>();

    public ItemsDO(String itemId, String productID, String itemName, int ItemQuantity, String basePrice,
                   String discountedPrice, String finalPrice, String itemSize, String ItemColor, String itemPattern, String
                           ItemMaterial, String ItemSmallImgUrl, String ItemMediumImgUrl, String ItemBigImgUrl,
                   int ItemStatus, String ItemRating, String ItemPerSaved) {

        this.ItemId = itemId;
        this.ProductID = productID;
        this.ItemName = itemName;
        this.ItemBasePrice = basePrice;
        this.ItemDsicountedPrice = discountedPrice;
        this.ItemFinalPrice = finalPrice;
        this.ItemSize = itemSize;
        this.ItemQuantity = ItemQuantity;
        this.ItemColor = ItemColor;
        this.ItemPattern = itemPattern;
        this.ItemMaterial = ItemMaterial;
        this.ItemBigImgUrl = ItemBigImgUrl;
        this.ItemSmallImgUrl = ItemSmallImgUrl;
        this.ItemMediumImgUrl = ItemMediumImgUrl;
        this.ItemStatus = ItemStatus;
        this.ItemRating = ItemRating;
        this.ItemPerSaved = ItemPerSaved;
    }

    public ItemsDO() {

    }

    public boolean isPositionSelected() {
        return positionSelected;
    }

    public void setPositionSelected(boolean positionSelected) {
        this.positionSelected = positionSelected;
    }


    public ArrayList<ItemsSizeDO> itemSizeList = new ArrayList<ItemsSizeDO>();

    public ArrayList<String> itemColorList = new ArrayList<String>();


}
