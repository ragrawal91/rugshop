package com.yits.rugshop.objects;

public class ReviewSizeListDO {

	public String ItemId="",SizeName="",BasePrice="",DiscountPrice="",FinalPrice="";

	public ReviewSizeListDO(String itemId, String itemSize, String basePrice, String discountedPrice, String finalPrice)
	{
		this.ItemId=itemId;
		this.SizeName=itemSize;
		this.BasePrice=basePrice;
		this.DiscountPrice=discountedPrice;
		this.FinalPrice=finalPrice;
	}

	public ReviewSizeListDO() {

	}
}
