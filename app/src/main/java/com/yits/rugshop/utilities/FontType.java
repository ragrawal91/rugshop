package com.yits.rugshop.utilities;

import android.content.Context;
import android.graphics.Typeface;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

public class FontType {

    public Typeface typeFace_AmTypeWriter;
    public static final String ROBOTOBOLD = "roboto bold";
    public static final String ROBOTOMEDIUM = "roboto medium";
    public static final String ROBOTOREGULAR = "roboto regular";
    public static final String ROBOTOLIGHT = "roboto light";


    Context context;

    public FontType(Context con, ViewGroup root) {

        this.context = con;
        typeFace_AmTypeWriter = Typeface.createFromAsset(con.getAssets(), "CalibriL.ttf");
        setFont(root, typeFace_AmTypeWriter);

    }

    public FontType(Context context) {
        this.context = context;

    }

    public void setFont(ViewGroup group, Typeface font) {
        int count = group.getChildCount();
        View v;
        for (int i = 0; i < count; i++) {
            v = group.getChildAt(i);
            if (v instanceof TextView || v instanceof Button) {
                ((TextView) v).setTypeface(font);
            } else if (v instanceof ViewGroup)
                setFont((ViewGroup) v, font);
        }
    }

    public void applyfonttoView(View view, String fontname) {
        switch (fontname) {
            case AppConstants.TRAJANPRO:
                typeFace_AmTypeWriter = Typeface.createFromAsset(context.getAssets(), "TRAJANPRO-REGULAR.OTF");
                break;
            case AppConstants.GOTHIC:
                typeFace_AmTypeWriter = Typeface.createFromAsset(context.getAssets(), "CalibriL.ttf");
                break;
            case ROBOTOBOLD:
                typeFace_AmTypeWriter = Typeface.createFromAsset(context.getAssets(), "font/Roboto-Bold.ttf");
                break;
            case ROBOTOMEDIUM:
                typeFace_AmTypeWriter = Typeface.createFromAsset(context.getAssets(), "font/Roboto-Medium.ttf");
                break;
            case ROBOTOREGULAR:
                typeFace_AmTypeWriter = Typeface.createFromAsset(context.getAssets(), "font/Roboto-Regular.ttf");
                break;
            case ROBOTOLIGHT:
                typeFace_AmTypeWriter = Typeface.createFromAsset(context.getAssets(), "font/Roboto-Light.ttf");
                break;
        }
        ((TextView) view).setTypeface(typeFace_AmTypeWriter);

    }

    public void applyfonttoViewGroup(ViewGroup view, String fontname) {
        switch (fontname) {
            case AppConstants.TRAJANPRO:
                typeFace_AmTypeWriter = Typeface.createFromAsset(context.getAssets(), "TRAJANPRO-REGULAR.OTF");
                break;
            case AppConstants.GOTHIC:
                typeFace_AmTypeWriter = Typeface.createFromAsset(context.getAssets(), "CalibriL.ttf");
                break;
            case ROBOTOBOLD:
                typeFace_AmTypeWriter = Typeface.createFromAsset(context.getAssets(), "font/Roboto-Bold.ttf");
                break;
            case ROBOTOMEDIUM:
                typeFace_AmTypeWriter = Typeface.createFromAsset(context.getAssets(), "font/Roboto-Medium.ttf");
                break;
            case ROBOTOREGULAR:
                typeFace_AmTypeWriter = Typeface.createFromAsset(context.getAssets(), "font/Roboto-Regular.ttf");
                break;
            case ROBOTOLIGHT:
                typeFace_AmTypeWriter = Typeface.createFromAsset(context.getAssets(), "font/Roboto-Light.ttf");
                break;

        }
        setFont(view, typeFace_AmTypeWriter);
    }


}
