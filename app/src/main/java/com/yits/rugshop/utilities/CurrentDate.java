package com.yits.rugshop.utilities;

import android.annotation.SuppressLint;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

@SuppressLint("SimpleDateFormat")
public class CurrentDate {
	public String currentDate,kotdate,crt_ts;
	public String fullDateTime,slashDate,onlytime,billdate,mealorderdate;
	Date date = null;    
	
	public CurrentDate(){
		SimpleDateFormat sdf1 = new SimpleDateFormat("ddMMMyy");
		currentDate = sdf1.format(new Date());  
		
		SimpleDateFormat sdf2 = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss");
		fullDateTime = sdf2.format(new Date());
		
		SimpleDateFormat sdf3 = new SimpleDateFormat("dd/MM/yyyy");
		slashDate = sdf3.format(new Date());
		
		SimpleDateFormat sdf4 = new SimpleDateFormat("h:mm a");
		onlytime = sdf4.format(new Date());
		
		SimpleDateFormat sdf5=new SimpleDateFormat("dd-MM-yyyy");
		kotdate=sdf5.format(new Date());
		
		SimpleDateFormat sdf6=new SimpleDateFormat("yyMMdd");
		billdate=sdf6.format(new Date());

		SimpleDateFormat sdf7=new SimpleDateFormat("yyyy-MM-dd");
		mealorderdate= sdf7.format(new Date());
		
		
		DateFormat df = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss aa");
		String inputcrt_ts=df.format(new Date());
		
		DateFormat outputformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		
		try {
			date=df.parse(inputcrt_ts);
			
			crt_ts=outputformat.format(date);
			
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	/**
	 * We are getting the Indain timezone Time and Date in 24 hour format
	 * @return String
	 */
	public String getOrdertime() {

		DateFormat df = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss aa");
		DateFormat outputformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String sbCurrentTimestamp = null;
		Calendar cSchedStartCal = Calendar.getInstance(TimeZone.getTimeZone("GMT"));
		long gmtTime = cSchedStartCal.getTime().getTime();

		long timezoneAlteredTime = gmtTime + TimeZone.getTimeZone("Asia/Calcutta").getRawOffset();
		Calendar cSchedStartCal1 = Calendar.getInstance(TimeZone.getTimeZone("Asia/Calcutta"));
		cSchedStartCal1.setTimeInMillis(timezoneAlteredTime);


		Date date = cSchedStartCal1.getTime();

		String input_crt_ts = df.format(date);

		Date outputDate = null;
		try {
			outputDate = df.parse(input_crt_ts);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		sbCurrentTimestamp = outputformat.format(outputDate);

		return sbCurrentTimestamp;
	}


	public String getOrdertime(Date passed_date) {

		DateFormat df = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss aa");
		DateFormat outputformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String sbCurrentTimestamp = null;
		/*Calendar cSchedStartCal = Calendar.getInstance(TimeZone.getTimeZone("GMT"));
		long gmtTime = cSchedStartCal.getTime().getTime();

		long timezoneAlteredTime = gmtTime + TimeZone.getTimeZone("Asia/Calcutta").getRawOffset();
		Calendar cSchedStartCal1 = Calendar.getInstance(TimeZone.getTimeZone("Asia/Calcutta"));
		cSchedStartCal1.setTimeInMillis(timezoneAlteredTime);


		Date date = cSchedStartCal1.getTime();*/

		String input_crt_ts = df.format(passed_date);

		Date outputDate = null;
		try {
			outputDate = df.parse(input_crt_ts);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		sbCurrentTimestamp = outputformat.format(outputDate);

		return sbCurrentTimestamp;
	}

	public String getselecteddate(Date date)
	{
		SimpleDateFormat sdf5=new SimpleDateFormat("dd-MM-yyyy");
		return sdf5.format(date);
	}

}
