package com.yits.rugshop.utilities;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import java.util.HashMap;


public class PreferenceUtils 
{
	private SharedPreferences preferences;
	private SharedPreferences.Editor edit;
	public static String KEY 			= 	"key";

	public PreferenceUtils(Context context) 
	{
		preferences		=	PreferenceManager.getDefaultSharedPreferences(context);
		edit			=	preferences.edit();
	}
	
	public void saveString(String strKey,String strValue)
	{
		edit.putString(strKey, strValue);
		edit.commit();
	}
	
	public void saveInt(String strKey,int value)
	{
		edit.putInt(strKey, value);
		edit.commit();
	}

	public String getStringFromPreference(String strKey,String defaultValue )
	{
		return preferences.getString(strKey, defaultValue);
	}
	


	public int getIntFromPreference(String strKey,int defaultValue)
	{
		return preferences.getInt(strKey, defaultValue);
	}
	


	public void createLoginSession(String loginid,String userfstname,String email,String socialtype,String contact_num)
	{
		edit.putString(AppConstants.KEY_USER_ID,loginid);

		edit.putString(AppConstants.KEY_USER_FST_NAME,userfstname);

		edit.putString(AppConstants.KEY_USER_EMAIL, email);

		edit.putBoolean(AppConstants.KEY_IS_LOGIN, true);

		edit.putString(AppConstants.KEY_SOCIAL_TYPE,socialtype);

		edit.putString(AppConstants.KEY_USER_CONTACT,contact_num);

		edit.commit();
	}

	public void createAccessToken(String accessToken)
	{
		edit.putString(AppConstants.KEY_ACCESS_TOKEN, accessToken);
	}

	public void logoutUser()
	{
		/*edit.clear();
		edit.commit();*/
		edit.putBoolean(AppConstants.KEY_IS_LOGIN, false);
		edit.putString(AppConstants.LOGINCOOKIE, "null");
		edit.commit();

	}

	public void saveLoginCookie(String cookie)
	{

		edit.putString(AppConstants.LOGINCOOKIE, cookie);
		edit.commit();

	}

	public String getLoginCookie()
	{
		return preferences.getString(AppConstants.LOGINCOOKIE,"null");
	}

	/**
	 * Get stored session data
	 * */
	public HashMap<String, String> getUserDetails(){
		HashMap<String, String> user = new HashMap<String, String>();
		// user id
		user.put(AppConstants.KEY_USER_ID, preferences.getString(AppConstants.KEY_USER_ID, "0"));

		// user name
		user.put(AppConstants.KEY_USER_FST_NAME, preferences.getString(AppConstants.KEY_USER_FST_NAME, "NA"));

		user.put(AppConstants.KEY_USER_LST_NAME, preferences.getString(AppConstants.KEY_USER_LST_NAME, "NA"));

		// user email id
		user.put(AppConstants.KEY_USER_EMAIL, preferences.getString(AppConstants.KEY_USER_EMAIL, "NA"));

		// user contact number
		user.put(AppConstants.KEY_USER_CONTACT, preferences.getString(AppConstants.KEY_USER_CONTACT, "0000000000"));

		// return user
		return user;
	}

	public HashMap<String, String> getAccessToken()
	{
		HashMap<String, String> user = new HashMap<String, String>();
		// access Token
		user.put(AppConstants.KEY_USER_ID, preferences.getString(AppConstants.KEY_ACCESS_TOKEN, "0"));

		return user;
	}

	public String getthe_socialtype()
	{
		return preferences.getString(AppConstants.KEY_SOCIAL_TYPE, "NA");
	}

	/**
	 * Quick check for login
	 * **/
	// Get Login State
	public boolean isLoggedIn(){
		return preferences.getBoolean(AppConstants.KEY_IS_LOGIN, false);
	}

	/**
	 * Logout User method
	 */



	public void save_cust_contct_number(String contactnumber)
	{
		edit.putString(AppConstants.KEY_USER_CONTACT,contactnumber);

		edit.commit();

	}





}
