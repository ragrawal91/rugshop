package com.yits.rugshop.utilities;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.yits.rugshop.objects.ItemCartsDO;
import com.yits.rugshop.objects.ItemsDO;
import com.yits.rugshop.objects.ReviewSizeListDO;
import com.yits.rugshop.objects.SizeListDO;

import java.util.ArrayList;

public class CartDatabase {
	public static final boolean DEBUG = true;

	/******************** Logcat TAG ************/
	public static final String LOG_TAG = "CARTDATABASE";

	/********************Cartlist Table Fields ************/
	public static final String Column_itemid = "itemid";
	public static final String Column_productid = "productid";
	public static final String Column_itemname = "itemname";
	public static final String Column_itemqty = "itemqty";
	public static final String Column_itmbaseprice = "itmbaseprice";
	public static final String Column_itmdiscprice = "itmdiscprice";
	public static final String Column_itmfinalprice = "itmfinalprice";
	public static final String Column_itemsize = "itemsize";
	public static final String Column_itemcolor = "itemcolor";
	public static final String Column_itempattern = "itempattern";
	public static final String Column_itemmaterial = "itemmaterial";
	public static final String Column_itemsmallimgurl = "smallimgurl";
	public static final String Column_itemmediumimgurl= "mediumimgurl";
	public static final String Column_itembigimgurl = "bigimgurl";
	public static final String Column_itemstatus = "itemstatus";
	public static final String Column_itemCatogiryId="itemcatogiryid";

	public static final String Column_itemrating = "itemrating";
	public static final String Column_itempersaved = "itempersaved";

	/********************Reviewlist Table Fields ************/
	public static final String Column_reviewitemid = "itemid";
	public static final String Column_reviewproductid = "productid";
	public static final String Column_reviewitemname = "itemname";
	public static final String Column_reviewitemqty = "itemqty";
	public static final String Column_reviewitmbaseprice = "itmbaseprice";
	public static final String Column_reviewitmdiscprice = "itmdiscprice";
	public static final String Column_reviewitmfinalprice = "itmfinalprice";
	public static final String Column_reviewitemsize = "itemsize";
	public static final String Column_reviewitemcolor = "itemcolor";
	public static final String Column_reviewitempattern = "itempattern";
	public static final String Column_reviewitemmaterial = "itemmaterial";
	public static final String Column_reviewitemsmallimgurl = "smallimgurl";
	public static final String Column_reviewitemmediumimgurl= "mediumimgurl";
	public static final String Column_reviewitembigimgurl = "bigimgurl";
	public static final String Column_reviewitemstatus = "itemstatus";
	public static final String Column_reviewitemrating = "itemrating";
	public static final String Column_reviewitempersaved = "itempersaved";

	/********************Favouritelist Table Fields ************/
	public static final String Column_favitemid = "itemid";
	public static final String Column_favproductid = "productid";
	public static final String Column_favitemname = "itemname";
	public static final String Column_favitemqty = "itemqty";
	public static final String Column_favitmbaseprice = "itmbaseprice";
	public static final String Column_favitmdiscprice = "itmdiscprice";
	public static final String Column_favitmfinalprice = "itmfinalprice";
	public static final String Column_favitemsize = "itemsize";
	public static final String Column_favitemcolor = "itemcolor";
	public static final String Column_favitempattern = "itempattern";
	public static final String Column_favitemmaterial = "itemmaterial";
	public static final String Column_favitemsmallimgurl = "smallimgurl";
	public static final String Column_favitemmediumimgurl= "mediumimgurl";
	public static final String Column_favitembigimgurl = "bigimgurl";
	public static final String Column_favitemstatus = "itemstatus";
	public static final String Column_favitemrating = "itemrating";
	public static final String Column_favitempersaved = "itempersaved";


	/********************Colorlist Table Fields ************/
	public static final String Column_Coloritemid = "itemid";
	public static final String Column_ColorName = "colorname";

	/********************ReviewColorlist Table Fields ************/
	public static final String Column_ReviewColoritemid = "itemid";
	public static final String Column_ReviewColorName = "colorname";

	/********************FavouriteColorlist Table Fields ************/
	public static final String Column_favColoritemid = "itemid";
	public static final String Column_favColorName = "colorname";

	/********************Sizelistlist Table Fields ************/
	public static final String Column_sizeitemid = "itemid";
	public static final String Column_sizeName = "sizename";
	public static final String Column_sizebaseprice = "baseprice";
	public static final String Column_sizediscprice = "discprice";
	public static final String Column_sizefinalprice = "finalprice";

	/********************ReviewSizelist Table Fields ************/
	public static final String Column_Reviewsizeitemid = "itemid";
	public static final String Column_ReviewsizeName = "sizename";
	public static final String Column_Reviewsizebaseprice = "baseprice";
	public static final String Column_Reviewsizediscprice = "discprice";
	public static final String Column_Reviewsizefinalprice = "finalprice";

	/********************FavouriteSizelist Table Fields ************/
	public static final String Column_favsizeitemid = "itemid";
	public static final String Column_favsizeName = "sizename";
	public static final String Column_favsizebaseprice = "baseprice";
	public static final String Column_favsizediscprice = "discprice";
	public static final String Column_favsizefinalprice = "finalprice";

	/********************Last Selected Position Table Fields ************/
	public static final String Column_position = "itemposition";
	public static final String Column_position_id = "id";



	/******************** Database Name ************/
	public static final String DATABASE_NAME = "cart3";

	/******************** Database Version (Increase one if want to also upgrade your database) ************/
	public static final int DATABASE_VERSION = 1;// started at 1

	/** Table names */
	public static final String Table_Cartlist = "cartlist";
	public static final String Table_Colorlist = "colorlist";
	public static final String Table_Sizelist = "sizelist";
	public static final String Table_Position = "position";
	public static final String Table_ReviewList = "reviewlist";
	public static final String Table_ReviewColorlist = "reviewcolorlist";
	public static final String Table_ReviewSizelist = "reviewsizelist";

	public static final String Table_FavouriteList = "favouritelist";
	public static final String Table_FavouriteColorlist = "favouritecolorlist";
	public static final String Table_FavouriteSizelist = "favouritesizelist";

	/******************** Set all table with comma seperated like USER_TABLE,ABC_TABLE ************/
	private static final String[] ALL_TABLES = { Table_Cartlist,Table_Colorlist,Table_Sizelist,Table_Position,
			Table_ReviewList,Table_ReviewColorlist,Table_ReviewSizelist,Table_FavouriteList,
			Table_FavouriteColorlist,Table_FavouriteSizelist };

	/** Create table syntax */
	private static final String USER_CREATECARTLIST = "create table cartlist(itemid TEXT,productid TEXT,itemname TEXT,itemqty TEXT,itmbaseprice TEXT,itmdiscprice TEXT,itmfinalprice TEXT,itemsize TEXT,itemcolor TEXT,itempattern TEXT,itemmaterial TEXT,smallimgurl TEXT,mediumimgurl TEXT,bigimgurl TEXT,itemstatus INTEGER,itemrating TEXT,itempersaved TEXT,itemcatogiryid TEXT);";

	private static final String USER_CREATEREVIEWLIST = "create table reviewlist(itemid TEXT,productid TEXT,itemname TEXT,itemqty TEXT,itmbaseprice TEXT,itmdiscprice TEXT,itmfinalprice TEXT,itemsize TEXT,itemcolor TEXT,itempattern TEXT,itemmaterial TEXT,smallimgurl TEXT,mediumimgurl TEXT,bigimgurl TEXT,itemstatus INTEGER,itemrating TEXT,itempersaved TEXT);";

	private static final String USER_CREATEFAVOURITELIST = "create table favouritelist(itemid TEXT,productid TEXT,itemname TEXT,itemqty TEXT,itmbaseprice TEXT,itmdiscprice TEXT,itmfinalprice TEXT,itemsize TEXT,itemcolor TEXT,itempattern TEXT,itemmaterial TEXT,smallimgurl TEXT,mediumimgurl TEXT,bigimgurl TEXT,itemstatus INTEGER,itemrating TEXT,itempersaved TEXT);";

	private static final String USER_CREATECOLORLIST = "create table colorlist(itemid TEXT,colorname TEXT);";

	private static final String USER_CREATEREVIEWCOLORLIST = "create table reviewcolorlist(itemid TEXT,colorname TEXT);";

	private static final String USER_CREATEFAVOURITECOLORLIST = "create table favouritecolorlist(itemid TEXT,colorname TEXT);";

	private static final String USER_CREATESIZELIST = "create table sizelist(itemid TEXT,sizename TEXT,baseprice TEXT,discprice TEXT,finalprice TEXT);";

	private static final String USER_CREATEREVIEWSIZELIST = "create table reviewsizelist(itemid TEXT,sizename TEXT,baseprice TEXT,discprice TEXT,finalprice TEXT);";

	private static final String USER_CREATEFAVOURITESIZELIST = "create table favouritesizelist(itemid TEXT,sizename TEXT,baseprice TEXT,discprice TEXT,finalprice TEXT);";

	private static final String USER_CREATEPOSITION = "create table position(id INTEGER,itemposition INTEGER);";

	/******************** Used to open database in syncronized way ************/
	private static DataBaseHelper DBHelper = null;

	protected CartDatabase() {
	}
	/******************* Initialize database *************/
	public static void init(Context context) {
		if (DBHelper == null) {
			if (DEBUG)
				Log.i("DBAdapter", context.toString());
			DBHelper = new DataBaseHelper(context);
		}
	}

	public static void deleteFavouritelist(ItemsDO model) {
		final SQLiteDatabase db = open();
		db.delete(Table_FavouriteList, Column_itemid + " = ?", new String[]{model.ItemId});
		db.close();
	}

	public static void deleteFavouritecolorlist(ItemsDO model) {
		final SQLiteDatabase db = open();
		db.delete(Table_FavouriteColorlist, Column_Coloritemid + " = ?",new String[] { model.ItemId});
		db.close();
	}

	public static void deleteFavouritesizelist(ItemsDO model) {
		final SQLiteDatabase db = open();
		db.delete(Table_FavouriteSizelist, Column_sizeitemid + " = ?",new String[] { model.ItemId});
		db.close();
	}


	/********************** Main Database creation INNER class ********************/
	private static class DataBaseHelper extends SQLiteOpenHelper {
		public DataBaseHelper(Context context) {
			super(context, DATABASE_NAME, null, DATABASE_VERSION);
		}

		@Override
		public void onCreate(SQLiteDatabase db) {
			if (DEBUG)
				Log.i(LOG_TAG, "new create");
			try {
				db.execSQL(USER_CREATECARTLIST);
				db.execSQL(USER_CREATECOLORLIST);
				db.execSQL(USER_CREATESIZELIST);
				db.execSQL(USER_CREATEPOSITION);
				db.execSQL(USER_CREATEREVIEWLIST);
				db.execSQL(USER_CREATEREVIEWCOLORLIST);
				db.execSQL(USER_CREATEREVIEWSIZELIST);
				db.execSQL(USER_CREATEFAVOURITELIST);
				db.execSQL(USER_CREATEFAVOURITECOLORLIST);
				db.execSQL(USER_CREATEFAVOURITESIZELIST);

				addPositionDefault(0);


			} catch (Exception exception) {
				if (DEBUG)
					Log.i(LOG_TAG, "Exception onCreate() exception");
			}
		}

		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
			if (DEBUG)
				Log.w(LOG_TAG, "Upgrading database from version" + oldVersion
						+ "to" + newVersion + "...");

			for (String table : ALL_TABLES) {
				db.execSQL("DROP TABLE IF EXISTS " + table);
			}
			onCreate(db);
		}

	} // Inner class closed


	/********************** Open database for insert,update,delete in syncronized manner ********************/
	private static synchronized SQLiteDatabase open() throws SQLException {
		return DBHelper.getWritableDatabase();
	}


	/************************ General functions**************************/


	/*********************** Escape string for single quotes (Insert,Update)************/
	private static String sqlEscapeString(String aString) {
		String aReturn = "";

		if (null != aString) {
			//aReturn = aString.replace("'", "''");
			aReturn = DatabaseUtils.sqlEscapeString(aString);
			// Remove the enclosing single quotes ...
			aReturn = aReturn.substring(1, aReturn.length() - 1);
		}

		return aReturn;
	}
	/*********************** UnEscape string for single quotes (show data)************/
	private static String sqlUnEscapeString(String aString) {

		String aReturn = "";

		if (null != aString) {
			aReturn = aString.replace("''", "'");
		}

		return aReturn;
	}


	/********************************************************************/


	/**
	 * All Operations (Create, Read, Update, Delete)
	 */
	// Adding new contact
	public static void addColorData(String ItemId,ArrayList<String> Colorlist)
	{
		final SQLiteDatabase db = open();
		for(int i=0;i<Colorlist.size();i++)
		{
			ContentValues value = new ContentValues();
			value.put(Column_Coloritemid, ItemId);
			value.put(Column_ColorName, Colorlist.get(i));
			db.insert(Table_Colorlist, null, value);
			// Closing database connection
		}
		db.close();
	}

	public static void addReviewColorData(String ItemId,ArrayList<String> Colorlist)
	{
		final SQLiteDatabase db = open();
		for(int i=0;i<Colorlist.size();i++)
		{
			ContentValues value = new ContentValues();
			value.put(Column_ReviewColoritemid, ItemId);
			value.put(Column_ReviewColorName, Colorlist.get(i));
			db.insert(Table_ReviewColorlist, null, value);
			// Closing database connection
		}
		db.close();
	}

	public static void addFavouriteColorData(String ItemId,ArrayList<String> Colorlist)
	{
		final SQLiteDatabase db = open();
		for(int i=0;i<Colorlist.size();i++)
		{
			ContentValues value = new ContentValues();
			value.put(Column_favColoritemid, ItemId);
			value.put(Column_favColorName, Colorlist.get(i));
			db.insert(Table_FavouriteColorlist, null, value);
			// Closing database connection
		}
		db.close();
	}

	public static void addSizeData(SizeListDO sizeListDO)
	{
		final SQLiteDatabase db = open();
		String itemid = sqlEscapeString(sizeListDO.ItemId);
		String itemSize = sqlEscapeString(sizeListDO.SizeName);
		String basePrice = sqlEscapeString(sizeListDO.BasePrice);
		String DiscPrice = sqlEscapeString(sizeListDO.DiscountPrice);
		String finalPrice = sqlEscapeString(sizeListDO.FinalPrice);

		ContentValues value = new ContentValues();
		value.put(Column_sizeitemid, itemid);
		value.put(Column_sizeName, itemSize);
		value.put(Column_sizebaseprice, basePrice);
		value.put(Column_sizediscprice, DiscPrice);
		value.put(Column_sizefinalprice, finalPrice);
		db.insert(Table_Sizelist, null, value);
		db.close(); // Closing database connection

	}

	public static void addReviewSizeData(ReviewSizeListDO reviewsizeListDO)
	{
		final SQLiteDatabase db = open();
		String itemid = sqlEscapeString(reviewsizeListDO.ItemId);
		String itemSize = sqlEscapeString(reviewsizeListDO.SizeName);
		String basePrice = sqlEscapeString(reviewsizeListDO.BasePrice);
		String DiscPrice = sqlEscapeString(reviewsizeListDO.DiscountPrice);
		String finalPrice = sqlEscapeString(reviewsizeListDO.FinalPrice);

		ContentValues value = new ContentValues();
		value.put(Column_Reviewsizeitemid, itemid);
		value.put(Column_ReviewsizeName, itemSize);
		value.put(Column_Reviewsizebaseprice, basePrice);
		value.put(Column_Reviewsizediscprice, DiscPrice);
		value.put(Column_Reviewsizefinalprice, finalPrice);
		db.insert(Table_ReviewSizelist, null, value);
		db.close(); // Closing database connection
	}

	public static void addFavouriteSizeData(SizeListDO  reviewsizeListDO)
	{
		final SQLiteDatabase db = open();
		String itemid = sqlEscapeString(reviewsizeListDO.ItemId);
		String itemSize = sqlEscapeString(reviewsizeListDO.SizeName);
		String basePrice = sqlEscapeString(reviewsizeListDO.BasePrice);
		String DiscPrice = sqlEscapeString(reviewsizeListDO.DiscountPrice);
		String finalPrice = sqlEscapeString(reviewsizeListDO.FinalPrice);

		ContentValues value = new ContentValues();
		value.put(Column_favsizeitemid, itemid);
		value.put(Column_favsizeName, itemSize);
		value.put(Column_favsizebaseprice, basePrice);
		value.put(Column_favsizediscprice, DiscPrice);
		value.put(Column_favsizefinalprice, finalPrice);
		db.insert(Table_FavouriteSizelist, null, value);
		db.close(); // Closing database connection
	}

	public static int getCartQuantity(String itemId){
		SQLiteDatabase sqLiteDatabase = open();
		int itemQuantity = 0;
		String selectQuery = "SELECT "+Column_itemqty+" FROM "+Table_Cartlist+" where "+Column_itemid+" = '"+itemId+"'";

		try{

		final SQLiteDatabase db = open();
		Cursor cursor = db.rawQuery(selectQuery, null);


		if (cursor.moveToFirst()) {
			do {
				itemQuantity = cursor.getInt(cursor.getColumnIndex(Column_itemqty));
			}
			while (cursor.moveToNext());
		}

		}catch (Exception e){
			LogUtils.error("getCartQuantity : ", e.getMessage());
		}
		return itemQuantity;


	}
	public static String getItemCartAmount(String itemId){
		SQLiteDatabase sqLiteDatabase = open();
		String itemBasePrice = "";
		String selectQuery = "SELECT "+Column_itmbaseprice+" FROM "+Table_Cartlist+" where "+Column_itemid+" = '"+itemId+"'";

		try{

			final SQLiteDatabase db = open();
			Cursor cursor = db.rawQuery(selectQuery, null);


			if (cursor.moveToFirst()) {
				do {
					itemBasePrice = cursor.getString(cursor.getColumnIndex(Column_itemqty));
				}
				while (cursor.moveToNext());
			}

		}catch (Exception e){
			LogUtils.error("getCartQuantity : ", e.getMessage());
		}
		return itemBasePrice;


	}
	public static void addCartData(ItemCartsDO itemCartsDO) {
		final SQLiteDatabase db = open();

		String itemid = sqlEscapeString(itemCartsDO.ItemId);
		String productid = sqlEscapeString(itemCartsDO.ItemProductId);
		String itemname = sqlEscapeString(itemCartsDO.ItemName);
		int itemqty = itemCartsDO.ItemQuantity;
		String itembaseprice = sqlEscapeString(itemCartsDO.ItemBasePrice);
		String itemdiscprice = sqlEscapeString(itemCartsDO.ItemDsicountedPrice);
		String itemfinalprice = sqlEscapeString(itemCartsDO.ItemFinalPrice);
		String itemsize = sqlEscapeString(itemCartsDO.ItemSize);
		String itemcolor = sqlEscapeString(itemCartsDO.ItemColor);
		String itempattern = sqlEscapeString(itemCartsDO.ItemPattern);
		String itemmaterial = sqlEscapeString(itemCartsDO.ItemMaterial);
		String itemSmallImgUrl = sqlEscapeString(itemCartsDO.ItemSmallImgUrl);
		String itemMediumImgUrl = sqlEscapeString(itemCartsDO.ItemMediumImgUrl);
		String itemBigImgUrl = sqlEscapeString(itemCartsDO.ItemBigImgUrl);
		int itemstatus = itemCartsDO.ItemStatus;
		String itemRating=sqlEscapeString(itemCartsDO.ItemRating);
		String itemPerSaved=sqlEscapeString(itemCartsDO.ItemPerSaved);
		String itemCatogiryId=sqlEscapeString(itemCartsDO.categoryId);



		ContentValues value = new ContentValues();
		value.put(Column_itemid, itemid);
		value.put(Column_productid, productid);
		value.put(Column_itemname, itemname);
		value.put(Column_itemqty, itemqty);
		value.put(Column_itmbaseprice, itembaseprice);
		value.put(Column_itmdiscprice, itemdiscprice);
		value.put(Column_itmfinalprice, itemfinalprice);
		value.put(Column_itemsize, itemsize);
		value.put(Column_itemcolor, itemcolor);
		value.put(Column_itempattern, itempattern);
		value.put(Column_itemmaterial, itemmaterial);
		value.put(Column_itemsmallimgurl, itemSmallImgUrl);
		value.put(Column_itemmediumimgurl, itemMediumImgUrl);
		value.put(Column_itembigimgurl, itemBigImgUrl);
		value.put(Column_itemstatus, itemstatus);
		value.put(Column_itemrating, itemRating);
		value.put(Column_itempersaved, itemPerSaved);
		value.put(Column_itemCatogiryId,itemCatogiryId);

		db.insert(Table_Cartlist, null, value);
		db.close(); // Closing database connection
	}


	public static void addReviewListData(ItemsDO itemReviewDO) {
		final SQLiteDatabase db = open();

		String itemid = sqlEscapeString(itemReviewDO.ItemId);
		String productid = sqlEscapeString(itemReviewDO.ProductID);
		String itemname = sqlEscapeString(itemReviewDO.ItemName);
		int itemqty = itemReviewDO.ItemQuantity;
		String itembaseprice = sqlEscapeString(itemReviewDO.ItemBasePrice);
		String itemdiscprice = sqlEscapeString(itemReviewDO.ItemDsicountedPrice);
		String itemfinalprice = sqlEscapeString(itemReviewDO.ItemFinalPrice);
		String itemsize = sqlEscapeString(itemReviewDO.ItemSize);
		String itemcolor = sqlEscapeString(itemReviewDO.ItemColor);
		String itempattern = sqlEscapeString(itemReviewDO.ItemPattern);
		String itemmaterial = sqlEscapeString(itemReviewDO.ItemMaterial);
		String itemSmallImgUrl = sqlEscapeString(itemReviewDO.ItemSmallImgUrl);
		String itemMediumImgUrl = sqlEscapeString(itemReviewDO.ItemMediumImgUrl);
		String itemBigImgUrl = sqlEscapeString(itemReviewDO.ItemBigImgUrl);
		int itemstatus = itemReviewDO.ItemStatus;
		String itemRating=sqlEscapeString(itemReviewDO.ItemRating);
		String itemPerSaved=sqlEscapeString(itemReviewDO.ItemPerSaved);


		ContentValues value = new ContentValues();
		value.put(Column_reviewitemid, itemid);
		value.put(Column_reviewproductid, productid);
		value.put(Column_reviewitemname, itemname);
		value.put(Column_reviewitemqty, itemqty);
		value.put(Column_reviewitmbaseprice, itembaseprice);
		value.put(Column_reviewitmdiscprice, itemdiscprice);
		value.put(Column_reviewitmfinalprice, itemfinalprice);
		value.put(Column_reviewitemsize, itemsize);
		value.put(Column_reviewitemcolor, itemcolor);
		value.put(Column_reviewitempattern, itempattern);
		value.put(Column_reviewitemmaterial, itemmaterial);
		value.put(Column_reviewitemsmallimgurl, itemSmallImgUrl);
		value.put(Column_reviewitemmediumimgurl, itemMediumImgUrl);
		value.put(Column_reviewitembigimgurl, itemBigImgUrl);
		value.put(Column_reviewitemstatus, itemstatus);
		value.put(Column_reviewitemrating, itemRating);
		value.put(Column_reviewitempersaved, itemPerSaved);
		db.insert(Table_ReviewList, null, value);
		db.close(); // Closing database connection
	}

	public static void addFavouriteListData(ItemCartsDO  itemReviewDO) {
		final SQLiteDatabase db = open();

		String itemid = sqlEscapeString(itemReviewDO.ItemId);
		String productid = sqlEscapeString(itemReviewDO.ItemProductId);
		String itemname = sqlEscapeString(itemReviewDO.ItemName);
		int itemqty = itemReviewDO.ItemQuantity;
		String itembaseprice = sqlEscapeString(itemReviewDO.ItemBasePrice);
		String itemdiscprice = sqlEscapeString(itemReviewDO.ItemDsicountedPrice);
		String itemfinalprice = sqlEscapeString(itemReviewDO.ItemFinalPrice);
		String itemsize = sqlEscapeString(itemReviewDO.ItemSize);
		String itemcolor = sqlEscapeString(itemReviewDO.ItemColor);
		String itempattern = sqlEscapeString(itemReviewDO.ItemPattern);
		String itemmaterial = sqlEscapeString(itemReviewDO.ItemMaterial);
		String itemSmallImgUrl = sqlEscapeString(itemReviewDO.ItemSmallImgUrl);
		String itemMediumImgUrl = sqlEscapeString(itemReviewDO.ItemMediumImgUrl);
		String itemBigImgUrl = sqlEscapeString(itemReviewDO.ItemBigImgUrl);
		int itemstatus = itemReviewDO.ItemStatus;
		String itemRating=sqlEscapeString(itemReviewDO.ItemRating);
		String itemPerSaved=sqlEscapeString(itemReviewDO.ItemPerSaved);


		ContentValues value = new ContentValues();
		value.put(Column_favitemid, itemid);
		value.put(Column_favproductid, productid);
		value.put(Column_favitemname, itemname);
		value.put(Column_favitemqty, itemqty);
		value.put(Column_favitmbaseprice, itembaseprice);
		value.put(Column_favitmdiscprice, itemdiscprice);
		value.put(Column_favitmfinalprice, itemfinalprice);
		value.put(Column_favitemsize, itemsize);
		value.put(Column_favitemcolor, itemcolor);
		value.put(Column_favitempattern, itempattern);
		value.put(Column_favitemmaterial, itemmaterial);
		value.put(Column_favitemsmallimgurl, itemSmallImgUrl);
		value.put(Column_favitemmediumimgurl, itemMediumImgUrl);
		value.put(Column_favitembigimgurl, itemBigImgUrl);
		value.put(Column_favitemstatus, itemstatus);
		value.put(Column_favitemrating, itemRating);
		value.put(Column_favitempersaved, itemPerSaved);
		db.insert(Table_FavouriteList, null, value);
		db.close(); // Closing database connection
	}

	// Getting single item in cartlist
	public static ItemCartsDO getSingleItemInCartlist(String id) {
		ItemCartsDO data = null;
		try{
			final SQLiteDatabase db = open();

			Cursor cursor = db.query(Table_Cartlist, new String[]{Column_itemid, Column_productid, Column_itemname, Column_itemqty, Column_itmbaseprice, Column_itmdiscprice, Column_itmfinalprice, Column_itemsize, Column_itemcolor, Column_itempattern,
							Column_itemmaterial, Column_itemsmallimgurl, Column_itemmediumimgurl, Column_itembigimgurl, Column_itemstatus}, Column_itemid + "=?",
					new String[]{String.valueOf(id)}, null, null, null, null);
			if (cursor != null)
				cursor.moveToFirst();

			data = new ItemCartsDO(cursor.getString(0),cursor.getString(1), cursor.getString(2),cursor.getInt(3),cursor.getString(4),cursor.getString(5),cursor.getString(6),cursor.getString(7),cursor.getString(8),cursor.getString(9),
					cursor.getString(10),cursor.getString(11),cursor.getString(12),cursor.getString(13),cursor.getInt(14),cursor.getString(15),cursor.getString(16),cursor.getString(17));
			// return contact
		}
		catch (Exception e){
			LogUtils.error("getSingleItemInCartlist : ", e.getMessage());
		}
		return data;
	}

	// Getting All Cartlist
	public static ArrayList<ItemCartsDO> getAllCartList() {
		ArrayList<ItemCartsDO> cartList = new ArrayList<ItemCartsDO>();
		// Select All Query
		String selectQuery = "SELECT  * FROM " + Table_Cartlist;

		final SQLiteDatabase db = open();
		Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				ItemCartsDO data = new ItemCartsDO();
				data.ItemId=cursor.getString(0);
				data.ItemProductId=cursor.getString(1);
				data.ItemName=cursor.getString(2);
				data.ItemQuantity=cursor.getInt(3);

				data.ItemBasePrice=cursor.getString(4);
				data.ItemDsicountedPrice=cursor.getString(5);
				data.ItemFinalPrice=cursor.getString(6);
				data.ItemSize=cursor.getString(7);
				data.ItemColor=cursor.getString(8);
				data.ItemPattern=cursor.getString(9);
				data.ItemMaterial=cursor.getString(10);
				data.ItemSmallImgUrl=cursor.getString(11);
				data.ItemMediumImgUrl=cursor.getString(12);
				data.ItemBigImgUrl=cursor.getString(13);
				data.ItemStatus=cursor.getInt(14);
				data.ItemRating=cursor.getString(15);
				data.ItemPerSaved=cursor.getString(16);
				data.categoryId =cursor.getString(17);


				// Adding contact to list
				cartList.add(data);
			} while (cursor.moveToNext());
		}

		// return cartlist list
		return cartList;
	}


	// Getting All Cartlist
	public static ArrayList<ItemsDO> getAllReviewList() {
		ArrayList<ItemsDO> reviewList = new ArrayList<ItemsDO>();
		// Select All Query
		String selectQuery = "SELECT  * FROM " + Table_ReviewList;

		final SQLiteDatabase db = open();
		Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				ItemsDO data = new ItemsDO();
				data.ItemId=cursor.getString(0);
				data.ProductID=cursor.getString(1);
				data.ItemName=cursor.getString(2);
				data.ItemQuantity=cursor.getInt(3);
				data.ItemBasePrice=cursor.getString(4);
				data.ItemDsicountedPrice=cursor.getString(5);
				data.ItemFinalPrice=cursor.getString(6);
				data.ItemSize=cursor.getString(7);
				data.ItemColor=cursor.getString(8);
				data.ItemPattern=cursor.getString(9);
				data.ItemMaterial=cursor.getString(10);
				data.ItemSmallImgUrl=cursor.getString(11);
				data.ItemMediumImgUrl=cursor.getString(12);
				data.ItemBigImgUrl=cursor.getString(13);
				data.ItemStatus=cursor.getInt(14);
				data.ItemRating=cursor.getString(15);
				data.ItemPerSaved=cursor.getString(16);

				// Adding contact to list
				reviewList.add(data);
			} while (cursor.moveToNext());
		}

		// return cartlist list
		return reviewList;
	}


	// Getting All Cartlist
	public static ArrayList<ItemsDO> getAllFavouriteList() {
		ArrayList<ItemsDO> favouriteList = new ArrayList<ItemsDO>();
		// Select All Query
		String selectQuery = "SELECT  * FROM " + Table_FavouriteList;

		final SQLiteDatabase db = open();
		Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				ItemsDO data = new ItemsDO();
				data.ItemId=cursor.getString(0);
				data.ProductID=cursor.getString(1);
				data.ItemName=cursor.getString(2);
				data.ItemQuantity=cursor.getInt(3);
				data.ItemBasePrice=cursor.getString(4);
				data.ItemDsicountedPrice=cursor.getString(5);
				data.ItemFinalPrice=cursor.getString(6);
				data.ItemSize=cursor.getString(7);
				data.ItemColor=cursor.getString(8);
				data.ItemPattern=cursor.getString(9);
				data.ItemMaterial=cursor.getString(10);
				data.ItemSmallImgUrl=cursor.getString(11);
				data.ItemMediumImgUrl=cursor.getString(12);
				data.ItemBigImgUrl=cursor.getString(13);
				data.ItemStatus=cursor.getInt(14);
				data.ItemRating=cursor.getString(15);
				data.ItemPerSaved=cursor.getString(16);

				// Adding contact to list
				favouriteList.add(data);
			} while (cursor.moveToNext());
		}

		// return favourite list
		return favouriteList;
	}


	public static ArrayList<String> getAllColorlist(String ItemId) {
		ArrayList<String> colorList = new ArrayList<String>();
		final SQLiteDatabase db = open();
		String query="select colorname from colorlist where itemid="+"'"+ItemId+"'";
		Cursor cursor = db.rawQuery(query, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				String data = cursor.getString(0);
				colorList.add(data);
			} while (cursor.moveToNext());
		}

		// return cartlist list
		return colorList;
	}

	public static ArrayList<SizeListDO> getAllSizeList(String ItemId) {
		ArrayList<SizeListDO> sizeList = new ArrayList<SizeListDO>();
		final SQLiteDatabase db = open();
		String query="select * from sizelist where itemid="+"'"+ItemId+"'";
		Cursor cursor = db.rawQuery(query, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				SizeListDO data = new SizeListDO();
				data.ItemId=cursor.getString(0);
				data.SizeName=cursor.getString(1);
				data.BasePrice=cursor.getString(2);
				data.DiscountPrice=cursor.getString(3);
				data.FinalPrice=cursor.getString(4);

				sizeList.add(data);
			} while (cursor.moveToNext());
		}

		// return cartlist list
		return sizeList;
	}


	public static ArrayList<ReviewSizeListDO> getAllReviewSizeList(String ItemId) {
		ArrayList<ReviewSizeListDO> reviewsizeList = new ArrayList<ReviewSizeListDO>();
		final SQLiteDatabase db = open();
		String query="select * from reviewsizelist where itemid="+"'"+ItemId+"'";
		Cursor cursor = db.rawQuery(query, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				ReviewSizeListDO data = new ReviewSizeListDO();
				data.ItemId=cursor.getString(0);
				data.SizeName=cursor.getString(1);
				data.BasePrice=cursor.getString(2);
				data.DiscountPrice=cursor.getString(3);
				data.FinalPrice=cursor.getString(4);

				reviewsizeList.add(data);
			} while (cursor.moveToNext());
		}

		// return cartlist list
		return reviewsizeList;
	}

	public static ArrayList<SizeListDO> getAllFavouriteSizeList(String ItemId) {
		ArrayList<SizeListDO> favouritesizeList = new ArrayList<SizeListDO>();
		final SQLiteDatabase db = open();
		String query="select * from favouritesizelist where itemid="+"'"+ItemId+"'";
		Cursor cursor = db.rawQuery(query, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				SizeListDO data = new SizeListDO();
				data.ItemId=cursor.getString(0);
				data.SizeName=cursor.getString(1);
				data.BasePrice=cursor.getString(2);
				data.DiscountPrice=cursor.getString(3);
				data.FinalPrice=cursor.getString(4);

				favouritesizeList.add(data);
			} while (cursor.moveToNext());
		}

		// return favourite size list
		return favouritesizeList;
	}


	// Updating single item in cartlist
	public static int updateCartlist(ItemCartsDO data) {
		final SQLiteDatabase db = open();

		ContentValues values = new ContentValues();
		values.put(Column_itemqty, data.ItemQuantity);
		values.put(Column_itemsize, data.ItemSize);
		values.put(Column_itmbaseprice, data.ItemBasePrice);
		values.put(Column_itmdiscprice, data.ItemDsicountedPrice);
		values.put(Column_itmfinalprice, data.ItemFinalPrice);
		values.put(Column_itemcolor, data.ItemColor);
		values.put(Column_itempattern, data.ItemPattern);
		values.put(Column_itemmaterial, data.ItemMaterial);
		values.put(Column_itemsmallimgurl, data.ItemSmallImgUrl);
		values.put(Column_itemmediumimgurl, data.ItemMediumImgUrl);
		values.put(Column_itembigimgurl,data.ItemBigImgUrl);
		values.put(Column_itemstatus,data.ItemStatus);
		values.put(Column_itemrating,data.ItemRating);
		values.put(Column_itempersaved,data.ItemPerSaved);

		// updating row
		return db.update(Table_Cartlist, values, Column_itemid + " = ?",new String[] { data.ItemId });
	}

	public static void addPositionDefault(int position)
	{
		final SQLiteDatabase db = open();
		ContentValues value = new ContentValues();
		value.put(Column_position_id, 1);
		value.put(Column_position, position);
		db.insert(Table_Position, null, value);
		db.close(); // Closing database connection

	}

	public static int updatePositionData(int position)
	{
		String id="1";
		final SQLiteDatabase db = open();
		Log.e(CartDatabase.class.getSimpleName(), "Database Position is =" + position);
		ContentValues value = new ContentValues();
		value.put(Column_position, position);
		int dbstatus=db.update(Table_Position, value, Column_position_id + " = ?",new String[] { id });
		Log.e(CartDatabase.class.getSimpleName(), "dbstatus =" + dbstatus);
		return dbstatus;

	}

	public static int getPosition() {
		SQLiteDatabase db = open();
		int position=0,iiid=0;
		String query="select * from position ";
		Cursor cursor = db.rawQuery(query, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				iiid = cursor.getInt(0);
				position = cursor.getInt(1);

			} while (cursor.moveToNext());
		}
		return position;
	}

	public static int updateSizePriceInCartlist(SizeListDO data) {
		final SQLiteDatabase db = open();

		ContentValues values = new ContentValues();
		values.put(Column_itemsize, data.SizeName);
		values.put(Column_itmbaseprice, data.BasePrice);
		values.put(Column_itmdiscprice, data.DiscountPrice);
		values.put(Column_itmfinalprice, data.FinalPrice);

		// updating row
		return db.update(Table_Cartlist, values, Column_itemid + " = ?",new String[] { data.ItemId });
	}

	public static int updateSizePriceInReviewlist(ReviewSizeListDO data) {
		final SQLiteDatabase db = open();

		ContentValues values = new ContentValues();
		values.put(Column_reviewitemsize, data.SizeName);
		values.put(Column_reviewitmbaseprice, data.BasePrice);
		values.put(Column_reviewitmdiscprice, data.DiscountPrice);
		values.put(Column_reviewitmfinalprice, data.FinalPrice);

		// updating row
		return db.update(Table_ReviewList, values, Column_reviewitemid + " = ?",new String[] { data.ItemId });
	}

	public static int updateSizePriceInFavouritelist(SizeListDO data) {
		final SQLiteDatabase db = open();

		ContentValues values = new ContentValues();
		values.put(Column_favitemsize, data.SizeName);
		values.put(Column_favitmbaseprice, data.BasePrice);
		values.put(Column_favitmdiscprice, data.DiscountPrice);
		values.put(Column_favitmfinalprice, data.FinalPrice);

		// updating row
		return db.update(Table_FavouriteList, values, Column_favitemid + " = ?",new String[] { data.ItemId });
	}

	public static int updateColorInCartlist(ItemCartsDO itemCartsDO) {
		final SQLiteDatabase db = open();

		ContentValues values = new ContentValues();
		values.put(Column_itemcolor, itemCartsDO.ItemColor);

		// updating row
		return db.update(Table_Cartlist, values, Column_itemid + " = ?",new String[] { itemCartsDO.ItemId });
	}

	public static int updateQuantityInCartlist(ItemCartsDO data,int quantity) {
		final SQLiteDatabase db = open();

		ContentValues values = new ContentValues();
		values.put(Column_itemqty, quantity);

		// updating row
		return db.update(Table_Cartlist, values, Column_itemid + " = ?",new String[] { data.ItemId });
	}

	public static int updateQuantityInReviewlist(ItemsDO data,int quantity) {
		final SQLiteDatabase db = open();

		ContentValues values = new ContentValues();
		values.put(Column_reviewitemqty, quantity);

		// updating row
		return db.update(Table_ReviewList, values, Column_reviewitemid + " = ?",new String[] { data.ItemId });
	}

	// Deleting single item in cartlist
	public static void deletecartlist(ItemCartsDO data)
	{
		final SQLiteDatabase db = open();
		db.delete(Table_Cartlist, Column_itemid + " = ?", new String[]{data.ItemId});
		db.close();
	}

	public static void deletecartlistById(String itemId)
	{
		final SQLiteDatabase db = open();
		db.delete(Table_Cartlist, Column_itemid + " = ?", new String[]{itemId});
		db.delete(Table_Colorlist, Column_Coloritemid + " = ?",new String[] { itemId});
		db.delete(Table_Sizelist, Column_sizeitemid + " = ?",new String[] {itemId});
		db.close();
	}

	public static void deletereviewlistById(String itemId)
	{
		final SQLiteDatabase db = open();
		db.delete(Table_ReviewList, Column_reviewitemid + " = ?", new String[]{itemId});
		db.delete(Table_ReviewColorlist, Column_ReviewColoritemid + " = ?",new String[] { itemId});
		db.delete(Table_ReviewSizelist, Column_Reviewsizeitemid + " = ?",new String[] {itemId});
		db.close();
	}

	public static void deletecolorlist(ItemCartsDO data)
	{
		final SQLiteDatabase db = open();
		db.delete(Table_Colorlist, Column_Coloritemid + " = ?",new String[] { data.ItemId});
		db.close();
	}

	public static void deletesizelist(ItemCartsDO data)
	{
		final SQLiteDatabase db = open();
		db.delete(Table_Sizelist, Column_sizeitemid + " = ?",new String[] { data.ItemId});
		db.close();
	}

	// Getting cartlist Count
	public static int getCartlistCount()
	{
		SQLiteDatabase db = open();
		Cursor cursor;
		cursor=db.rawQuery("SELECT count(*) FROM "+Table_Cartlist,null );
		cursor.moveToFirst();
		int icount = cursor.getInt(0);
		cursor.close();
		return icount;
	}

	public static int getReviewlistCount()
	{
		SQLiteDatabase db = open();
		Cursor cursor;
		cursor=db.rawQuery("SELECT count(*) FROM "+Table_ReviewList,null );
		cursor.moveToFirst();
		int icount = cursor.getInt(0);
		cursor.close();
		return icount;
	}

	public static int getPositionlistCount()
	{
		SQLiteDatabase db = open();
		Cursor cursor;
		cursor=db.rawQuery("SELECT count(*) FROM "+Table_Position,null );
		cursor.moveToFirst();
		int icount = cursor.getInt(0);
		cursor.close();
		return icount;
	}

	public static int getColorListCount()
	{
		SQLiteDatabase db = open();
		Cursor cursor;
		cursor=db.rawQuery("SELECT count(*) FROM "+Table_Colorlist,null );
		cursor.moveToFirst();
		int icount = cursor.getInt(0);
		cursor.close();
		return icount;
	}

	public static int getSizeListCount()
	{
		SQLiteDatabase db = open();
		Cursor cursor;
		cursor=db.rawQuery("SELECT count(*) FROM "+Table_Sizelist,null );
		cursor.moveToFirst();
		int icount = cursor.getInt(0);
		cursor.close();
		return icount;
	}

	public static void clearDatabase()
	{
		SQLiteDatabase db = open();
		db.execSQL("delete from " + Table_Cartlist);
		db.execSQL("delete from " + Table_Colorlist);
		db.execSQL("delete from " + Table_Sizelist);
		db.execSQL("delete from " + Table_Position);
		db.execSQL("delete from " + Table_ReviewList);
		db.execSQL("delete from " + Table_ReviewColorlist);
		db.execSQL("delete from " + Table_ReviewSizelist);
	}
}