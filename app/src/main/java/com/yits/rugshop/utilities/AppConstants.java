package com.yits.rugshop.utilities;

import android.graphics.Typeface;
import android.os.Environment;

import com.yits.rugshop.objects.ColorsDO;

import java.io.File;
import java.net.HttpURLConnection;
import java.util.ArrayList;

public class AppConstants 
{
	public static final String SORTPRICELH = "Price(Low to High)";
	public static final String SORTPRICEHL = "Price(High to Low)";
	public static final String SORTSAVEHL ="Savings(High to Low)" ;
	public static final String SORTSAVELH = "Savings(Low to High)";
	public static final String TRAJANPRO ="TRAJANPRO" ;
	public static final String GOTHIC = "GOTHIC";
	//Device height & width
	public static int DEVICE_DISPLAY_WIDTH;
	public static int DEVICE_DISPLAY_HEIGHT;
	
	public static final int GET  = 1;
	public static final int POST = 2;

//	-----------------------------------------------------------------------------------------

	public static final String FONT_LATO_BLACK                   = "lato_black.ttf";
	public static final String FONT_LATO_BLACK_ITALIC            = "lato_black_italic.ttf";
	public static final String FONT_LATO_BOLD                    = "lato_bold.ttf";
	public static final String FONT_LATO_BOLD_ITALIC             = "lato_bold_italic.ttf";
	public static final String FONT_LATO_ITALIC                  = "lato_italic.ttf";
	public static final String FONT_LATO_LIGHT                   = "lato_light.ttf";
	public static final String FONT_LATO_LIGHT_ITALIC            = "lato_light_italic.ttf";
	public static final String FONT_LATO_REGULAR                 = "lato_regular.ttf";


	public static ArrayList<ColorsDO> colorsDOs 				 = new ArrayList<>();
//	===========================================================================================



	public static Typeface typeFace;

	public static String IMAGE_URL="http://fitros-ws.elasticbeanstalk.com/";
	
	public static String SDCARD_ROOT = Environment.getExternalStorageDirectory().toString() + File.separator;
	
	public static String appCacheDir = "";
	
	public static String ContentTypeJson = "application/json";

	public static String ContentTypeXML = "application/xml";
	
	public static String ContentTypeForm = "application/x-www-form-urlencoded";
	
	public static String CHECK_NETWORK_CONN="Please check internet connection!!";
	
	public static String NO_RESPONSE="No Response from Server";
	public static String VENDORADDRESS			="vendoraddress";
	public static final String RESET_FILTER		="reset filter";
	public static final String Nopreference		="No preference";
	public static final String MostViewed		="Most Viewed";
	public static final String MaxSavings		="Max Savings";
	public static final String Popularity		="Popularity";
	public static final String Newest			="Newest";

	public int  UNAUTHORISED= HttpURLConnection.HTTP_UNAUTHORIZED;

	public static String KEY_USER_ID="";
	public static String KEY_USER_FST_NAME="fstname";
	public static String KEY_USER_LST_NAME="lstname";
	public static String KEY_USER_EMAIL="email";
	public static String KEY_IS_LOGIN = "IsLoggedIn";
	public static String KEY_SOCIAL_TYPE="socialtype";
	public static String KEY_USER_CONTACT="contact";
	public static String KEY_ACCESS_TOKEN="accessToken";

	public static String LOGINCOOKIE="loginCookie";



	
	
	
}
