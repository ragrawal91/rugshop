package com.yits.rugshop.businesslayer;

import android.content.Context;

import com.yits.rugshop.objects.FilternameAndIDDo;
import com.yits.rugshop.objects.ItemCartsDO;
import com.yits.rugshop.utilities.LogUtils;
import com.yits.rugshop.webaccess.BaseWA;
import com.yits.rugshop.webaccess.BuildXMLRequest;
import com.yits.rugshop.webaccess.ServiceMethods;

import java.util.ArrayList;


public class CommonBL extends BaseBL {

	public CommonBL(Context mContext, DataListener listener) {
		super(mContext, listener);
	}



	public boolean checkLogin(String UserId, String UserLoginPwd)
	{
		return new BaseWA(mContext, this).startDataDownload(ServiceMethods.WS_CHECK_LOGIN, BuildXMLRequest.checkLogin(UserId, UserLoginPwd));
	}

	public boolean doRegistration(String UserFstName,String UserEmailId,String UserMobile,String UserPassword,String source,String role,String UserAddress)
	{


		LogUtils.info(CommonBL.class.getSimpleName(),BuildXMLRequest.doRegistration(UserFstName, UserEmailId, UserMobile, UserPassword, source, role, UserAddress));

		return new BaseWA(mContext, this).startDataDownload(ServiceMethods.WS_DO_REGISTRATION,BuildXMLRequest.doRegistration(UserFstName, UserEmailId, UserMobile, UserPassword, source, role, UserAddress));


	}

	public boolean getUserDetails(String queryParams)
	{
		return new BaseWA(mContext, this).startDataDownload(ServiceMethods.WS_GET_USER_DETAILS,BuildXMLRequest.getUserDetails(),queryParams);
	}

	public boolean getCategories(String queryParams)
	{
		return new BaseWA(mContext, this).startDataDownload(ServiceMethods.WS_GET_CATEGORIES,BuildXMLRequest.getCategories(),queryParams);
	}

	public boolean getColors(String queryParams)
	{
		return new BaseWA(mContext, this).startDataDownload(ServiceMethods.WS_GET_COLORS,BuildXMLRequest.getColors(),queryParams);
	}

	public boolean getItems(String queryParams)
	{
		return new BaseWA(mContext, this).startDataDownload(ServiceMethods.WS_GET_ITEMS,BuildXMLRequest.getItems(),queryParams);
	}

	public boolean getFilterData(String queryParamsFilter)
	{
		return new BaseWA(mContext, this).startDataDownload(ServiceMethods.WS_GET_FILTER_DATA,BuildXMLRequest.getFilterData(),queryParamsFilter);
	}

	public boolean placeOrder(String userName, String userEmail, String userMobile, String userAddress, String vendorID, String vendorShopName, Double subtotal, Double GrandTotal, Double TotalTaxValue, ArrayList<ItemCartsDO> orderCartList, String paymentType, String paymentstatus, String delChrgs, String addressType, String vendoraddress, Double subtotalvendor, Double GrandTotalvendor, Double TotalTaxValuevendor,String vendoremail) {
		return new BaseWA(mContext, this).startDataDownload(ServiceMethods.WS_POST_ORDER_DATA,BuildXMLRequest.placeOrder(userName,userEmail,userMobile,userAddress,vendorID,vendorShopName,subtotal,GrandTotal,TotalTaxValue,orderCartList,paymentType,paymentstatus,delChrgs,addressType,vendoraddress, subtotalvendor, GrandTotalvendor, TotalTaxValuevendor,vendoremail));


	}

	public boolean getFilteredProducts(ArrayList<FilternameAndIDDo> selectedSizeFilterList,
									   ArrayList<FilternameAndIDDo> selectedPatternFilterList,
									   ArrayList<FilternameAndIDDo> selectedMaterialFilterList,
									   ArrayList<FilternameAndIDDo> selectedColorFilterList,
									   ArrayList<FilternameAndIDDo> selectedSizeGroupFilterList,
									   ArrayList<FilternameAndIDDo> selectedPriceFilterList,
									   ArrayList<FilternameAndIDDo> selectedFeelerFilterList,
									   ArrayList<FilternameAndIDDo> selectedCategoryFilterList,
									   ArrayList<FilternameAndIDDo> selectedRunnerFilterList, String email)
	{
		return new BaseWA(mContext, this).startDataDownload(ServiceMethods.WS_POST_FILTER_PRODUCTS,BuildXMLRequest.getFilteredProducts(selectedSizeFilterList,selectedPatternFilterList,
				selectedMaterialFilterList,selectedColorFilterList,selectedSizeGroupFilterList, selectedFeelerFilterList, selectedPriceFilterList,
				selectedCategoryFilterList,selectedRunnerFilterList,email));


	}

	public boolean getColorFilteredProducts(ArrayList<String> selectedColors) {
		return new BaseWA(mContext, this).startDataDownload(ServiceMethods.WS_POST_FILTER_PRODUCTS,BuildXMLRequest.getColorFilteredProducts(selectedColors));


	}
}
