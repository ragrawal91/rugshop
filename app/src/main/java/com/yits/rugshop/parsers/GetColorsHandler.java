package com.yits.rugshop.parsers;


import com.yits.rugshop.objects.ColorsDO;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class GetColorsHandler extends BaseHandler {
	ArrayList<ColorsDO>  arrColorsDO;
	ColorsDO	colorsDO;
	int opstatus;
	String httpStatus="";
	String errorMessage="",Status="",Error="";


	public GetColorsHandler(String inputStream)
	{
		arrColorsDO =new ArrayList<ColorsDO>();
		getInputStream(inputStream);
	}

	private void getInputStream(String inputStream) {
		try {
			JSONObject jObject = new JSONObject(inputStream);
			Status = jObject.getString("status");
			if (Status.equalsIgnoreCase("success"))
			{
				if (jObject.has("data"))
				{
					JSONArray jsonArray=jObject.getJSONArray("data");
					for(int i=0; i<jsonArray.length();i++)
					{
						colorsDO = new ColorsDO();
						JSONObject jsonObject=jsonArray.getJSONObject(i);
						colorsDO.ColorName=jsonObject.getString("name");
						colorsDO.ColorId=jsonObject.getString("id");
						colorsDO.ColorImageUrl=jsonObject.getString("imageUrl");
						//colorsDO.ColorImageUrl="https://s3-us-west-2.amazonaws.com/usrugshop/beige.jpg";

						colorsDO.Status=Status;
						errorMessage="null";
						arrColorsDO.add(colorsDO);
					}
				}
			}

			else
			{
				errorMessage="error";
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}

	@Override
	public Object getData() {
		if(arrColorsDO!=null&&arrColorsDO.size()>0)
			return arrColorsDO;
		else 
			return 0;
	}

	@Override
	public String getErrorData() {
		return errorMessage;
	}

}
