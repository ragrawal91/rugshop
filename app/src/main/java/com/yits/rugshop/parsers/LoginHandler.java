package com.yits.rugshop.parsers;


import com.yits.rugshop.objects.LoginDO;

import org.json.JSONObject;

import java.util.ArrayList;

public class LoginHandler extends BaseHandler {
	ArrayList<LoginDO>  arrLoginDO;
	LoginDO	loginDO;
	int opstatus;
	String httpStatus="";
	String errorMessage="",Status="",Error="";



	public LoginHandler(String inputStream)
	{
		arrLoginDO =new ArrayList<LoginDO>();
		getInputStream(inputStream);
	}

	private void getInputStream(String inputStream) {
		try {
			int i;
			JSONObject jObject = new JSONObject(inputStream);
			Status = jObject.getString("status");
			if (Status.equalsIgnoreCase("success"))
			{
				if (jObject.has("data"))
				{
					JSONObject jsonObject=jObject.getJSONObject("data");
					loginDO = new LoginDO();
					loginDO.VendorId=jsonObject.getString("userId");
					loginDO.AccessToken=jsonObject.getString("id");
					errorMessage="null";
					loginDO.Status=Status;
					arrLoginDO.add(loginDO);
				}
			}
			else if (Status.equalsIgnoreCase("Login Failed"))
			{
				loginDO = new LoginDO();
				loginDO.Status=Status;
				errorMessage="null";
				arrLoginDO.add(loginDO);
			}
			else
			{
				errorMessage="error";
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@Override
	public Object getData() {
		if(arrLoginDO!=null&&arrLoginDO.size()>0)
			return arrLoginDO;
		else 
			return 0;
	}

	@Override
	public String getErrorData() {
		return errorMessage;
	}

}
