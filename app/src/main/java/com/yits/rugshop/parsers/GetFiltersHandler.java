package com.yits.rugshop.parsers;


import com.yits.rugshop.objects.FilterMetadataDO;
import com.yits.rugshop.objects.FilternameAndIDDo;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class GetFiltersHandler extends BaseHandler {
	ArrayList<FilterMetadataDO>  arrFilterDO;
	FilterMetadataDO	filterMetadataDO;

	int opstatus;
	String httpStatus="";
	String errorMessage="",Status="",Error="";


	public GetFiltersHandler(String inputStream)
	{
		arrFilterDO =new ArrayList<FilterMetadataDO>();
		getInputStream(inputStream);
	}

	private void getInputStream(String inputStream) {
		try {
			JSONObject jObject = new JSONObject(inputStream);
			Status = jObject.getString("status");
			if (Status.equalsIgnoreCase("success"))
			{
				if (jObject.has("data"))
				{
					JSONObject jsonObject1=jObject.getJSONObject("data");
					/*for(int i=0; i<jsonArray.length();i++)
					{*/
						filterMetadataDO = new FilterMetadataDO();
					//	JSONObject jsonObject=jsonArray.getJSONObject(i);
						filterMetadataDO.Status=Status;
						errorMessage="null";

					JSONObject jsonObject=jsonObject1.getJSONObject("data");
					JSONArray CategoryNameArray=jsonObject.getJSONArray("categeoryList");
					for(int j=0;j<CategoryNameArray.length();j++)
					{
						JSONObject CategoryObject=CategoryNameArray.getJSONObject(j);
						FilternameAndIDDo filternameAndIDDo=new FilternameAndIDDo();

						filternameAndIDDo.id=CategoryObject.getString("id");
						filternameAndIDDo.name=CategoryObject.getString("name");
						filternameAndIDDo.type="CATEGORIES";
						filterMetadataDO.CategoryList.add(filternameAndIDDo);
					}

					JSONArray RunnerNameArray=jsonObject.getJSONArray("runnerList");
					for(int j=0;j<RunnerNameArray.length();j++)
					{
						JSONObject RunnerObject=RunnerNameArray.getJSONObject(j);
						FilternameAndIDDo filternameAndIDDo=new FilternameAndIDDo();

						filternameAndIDDo.id=RunnerObject.getString("id");
						filternameAndIDDo.name=RunnerObject.getString("name");
						filterMetadataDO.RunnerList.add(filternameAndIDDo);
					}

					JSONArray ColorNameArray=jsonObject.getJSONArray("colorList");
						for(int j=0;j<ColorNameArray.length();j++)
						{
							JSONObject ColorObject=ColorNameArray.getJSONObject(j);
							FilternameAndIDDo filternameAndIDDo=new FilternameAndIDDo();

							filternameAndIDDo.id=ColorObject.getString("id");
							filternameAndIDDo.type="COLOUR";
							filternameAndIDDo.name=ColorObject.getString("name");
							filterMetadataDO.ColorNamesList.add(filternameAndIDDo);
						}

						JSONArray SizeNameArray=jsonObject.getJSONArray("sizeTypeList");
						for(int k=0;k<SizeNameArray.length();k++)
						{
							JSONObject SizeNameObject=SizeNameArray.getJSONObject(k);
							FilternameAndIDDo filternameAndIDDo=new FilternameAndIDDo();

							filternameAndIDDo.id=SizeNameObject.getString("id");
							filternameAndIDDo.name=SizeNameObject.getString("name");
							filternameAndIDDo.type="SIZEGROUP";
							filterMetadataDO.SizeGroupNamesList.add(filternameAndIDDo);
						}

						JSONArray MaterialsNameArray=jsonObject.getJSONArray("materialList");
						for(int l=0;l<MaterialsNameArray.length();l++)
						{
							JSONObject MaterialNameObject=MaterialsNameArray.getJSONObject(l);
							FilternameAndIDDo filternameAndIDDo=new FilternameAndIDDo();

							filternameAndIDDo.id=MaterialNameObject.getString("id");
							filternameAndIDDo.name=MaterialNameObject.getString("name");
							filternameAndIDDo.type="MATERIAL";
							filterMetadataDO.MaterialNamesList.add(filternameAndIDDo);
						}

						JSONArray PatternsNameArray=jsonObject.getJSONArray("patternList");
						for(int m=0;m<PatternsNameArray.length();m++)
						{
							JSONObject PatternNameObject=PatternsNameArray.getJSONObject(m);
							FilternameAndIDDo filternameAndIDDo=new FilternameAndIDDo();

							filternameAndIDDo.id=PatternNameObject.getString("id");
							filternameAndIDDo.name=PatternNameObject.getString("name");
							filternameAndIDDo.type="PATTERN";
							filterMetadataDO.PatternNamesList.add(filternameAndIDDo);
						}

						JSONArray SizegroupNameArray=jsonObject.getJSONArray("sizeList");
						for(int n=0;n<SizegroupNameArray.length();n++)
						{
							JSONObject SizeGroupNameObject=SizegroupNameArray.getJSONObject(n);
							FilternameAndIDDo filternameAndIDDo=new FilternameAndIDDo();

							filternameAndIDDo.id=SizeGroupNameObject.getString("id");
							filternameAndIDDo.name=SizeGroupNameObject.getString("name");
							filternameAndIDDo.type="SIZE";
							filterMetadataDO.SizeNamesList.add(filternameAndIDDo);
						}

						JSONArray PriceNameArray=jsonObject.getJSONArray("priceList");
						for(int n=0;n<PriceNameArray.length();n++)
						{
							JSONObject PriceObject=PriceNameArray.getJSONObject(n);
							FilternameAndIDDo filternameAndIDDo=new FilternameAndIDDo();

							filternameAndIDDo.id=PriceObject.getString("id");
							filternameAndIDDo.name=PriceObject.getString("range");
							filternameAndIDDo.type="PRICE";
							filterMetadataDO.PricesNamesList.add(filternameAndIDDo);
						}


						arrFilterDO.add(filterMetadataDO);
					}
				}


			else
			{
				errorMessage="error";
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}

	@Override
	public Object getData() {
		if(arrFilterDO!=null&&arrFilterDO.size()>0)
			return arrFilterDO;
		else 
			return 0;
	}

	@Override
	public String getErrorData() {
		return errorMessage;
	}

}
