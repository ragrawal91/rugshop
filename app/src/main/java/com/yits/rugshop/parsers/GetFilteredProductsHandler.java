package com.yits.rugshop.parsers;


import com.yits.rugshop.objects.FilternameAndIDDo;
import com.yits.rugshop.objects.ItemsDO;
import com.yits.rugshop.objects.ItemsSizeDO;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class GetFilteredProductsHandler extends BaseHandler {
    ArrayList<ItemsDO> arrItemsDO;
    ItemsDO itemDO;
    ItemsSizeDO itemsSizeDO;
    int opstatus;
    String httpStatus = "";
    String errorMessage = "", Status = "", Error = "";


    public GetFilteredProductsHandler(String inputStream) {
        arrItemsDO = new ArrayList<ItemsDO>();
        getInputStream(inputStream);
    }

    private void getInputStream(String inputStream) {
        try {
            JSONObject jObject = new JSONObject(inputStream);
            Status = jObject.getString("status");
            if (Status.equalsIgnoreCase("success")) {
                errorMessage = "null";

                if (jObject.has("data")) {
                    JSONObject dataObject = jObject.getJSONObject("data");
                    JSONArray jsonArray = dataObject.getJSONArray("products");
                    for (int i = 0; i < jsonArray.length(); i++) {
                        itemDO = new ItemsDO();
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        itemDO.ItemName = jsonObject.getString("name");
                        itemDO.viewcount = jsonObject.getInt("viewCount");
                        itemDO.ProductID = jsonObject.getString("productId");
                        itemDO.ProductID = jsonObject.getString("skuId");
                        itemDO.ItemId = jsonObject.getString("id");
                        itemDO.catogiryname = jsonObject.getJSONObject("category").getString("name");
                        itemDO.CategoryId = jsonObject.getJSONObject("category").getString("id");

                        //itemDO.ItemSizeGroup=jsonObject.getString("sizeGroup");
//						itemDO.ItemSizeGroup=jsonObject.getString("sizetype");

                        //itemDO.ItemPerSaved=jsonObject.getString("percentageSaved");
                        //itemDO.ItemMaterial=jsonObject.getString("material");
                        //itemDO.ItemPattern=jsonObject.getString("pattern");
                        itemDO.ItemMaterial = jsonObject.getJSONObject("material").getString("name");
                        itemDO.ItemPattern = jsonObject.getJSONObject("pattern").getString("name");
                        itemDO.ItemRating = jsonObject.getString("rating");
                        itemDO.ItemDesc = jsonObject.getString("description");
                        itemDO.ItemProDesc = jsonObject.getString("productDescription");
                        itemDO.ItemSmallImgUrl = jsonObject.getString("smallImgUrl");
                        itemDO.ItemMediumImgUrl = jsonObject.getString("thumbImgUrl");
                        itemDO.ItemBigImgUrl = jsonObject.getString("bigImgUrl");
                        itemDO.urls.add(jsonObject.getString("bigImgUrl"));
                        itemDO.urls.add("http://www.therugshopuk.co.uk/media/catalog/product/m/a/matrix_fr40_beige_red_rug_by_think_rugs.jpg");
                        itemDO.urls.add("http://www.therugshopuk.co.uk/media/catalog/product/m/a/matrix_cuzzo_max22_cuzzo_purple_wool_rug_by_asiatic_1.jpg");
                        itemDO.urls.add("http://www.therugshopuk.co.uk/media/catalog/product/m/a/matrix_sofia_max33_sofia_red_teal_wool_rug_by_asiatic_1.jpg");
                        itemDO.urls.add("http://www.therugshopuk.co.uk/media/catalog/product/m/a/matrix_ripley_max40_ripley_twilight_wool_rug_by_asiatic_1.jpg");

                        itemDO.Status = Status;
                        JSONArray PropertiesArray = jsonObject.getJSONArray("itemgroups");
                        for (int j = 0; j < PropertiesArray.length(); j++) {
                            itemsSizeDO = new ItemsSizeDO();
                            JSONObject propertiesObject = PropertiesArray.getJSONObject(j);
                            //	itemsSizeDO.ItemSize=propertiesObject.getString("sizesAvailable");
                            itemsSizeDO.BasePrice = propertiesObject.getString("vendorPrice");
//                            itemsSizeDO.DiscountedPrice = propertiesObject.getJSONArray("priceDetails").getJSONObject(0).getString("basePrice");
//                            itemsSizeDO.FinalPrice = propertiesObject.getString("vendorFinalPrice");
                            if ((100 - Integer.parseInt(propertiesObject.getString("vendorSellingPricePercentage")) > 0)) {
                                itemsSizeDO.savingstate = true;
                                itemsSizeDO.savings = (100 - Integer.parseInt(propertiesObject.getString("vendorSellingPricePercentage")));
                            }
                            itemsSizeDO.ItemSize = propertiesObject.getString("size");
                            itemDO.itemSizeList.add(itemsSizeDO);
                        }

                        JSONObject ColorArray = jsonObject.getJSONObject("color");

                        String ColorName = ColorArray.getString("name");
                        itemDO.itemColorList.add(ColorName);

                        JSONArray sizearray = jsonObject.getJSONArray("size");
                        for (int j = 0; j < sizearray.length(); j++) {
                            FilternameAndIDDo filternameAndIDDo = new FilternameAndIDDo();

                            filternameAndIDDo.name = sizearray.getJSONObject(j).getString("name");
                            filternameAndIDDo.id = sizearray.getJSONObject(j).getString("id");

                            for (int k = 0; k < itemDO.itemSizeList.size(); k++) {
                                if (itemDO.itemSizeList.get(k).ItemSize.equalsIgnoreCase(sizearray.getJSONObject(j).getString("id"))) {
                                    itemDO.itemSizeList.get(k).ItemSize = sizearray.getJSONObject(j).getString("name");
                                }
                            }

                        }

                        arrItemsDO.add(itemDO);
                    }
                }
            } else {
                errorMessage = "error";
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public Object getData() {
        if (errorMessage.equalsIgnoreCase("null"))
            return arrItemsDO;
        else
            return 0;
    }

    @Override
    public String getErrorData() {
        return errorMessage;
    }

}
