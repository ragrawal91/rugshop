package com.yits.rugshop.parsers;


import com.yits.rugshop.objects.OrderDO;

import org.json.JSONObject;

import java.util.ArrayList;

public class GetOrderSuccessHandler extends BaseHandler {
	ArrayList<OrderDO>  arrOrderDO;
	OrderDO	orderDO;
	int opstatus;
	String httpStatus="";
	String errorMessage="",Status="",Error="";


	public GetOrderSuccessHandler(String inputStream)
	{
		arrOrderDO =new ArrayList<OrderDO>();
		getInputStream(inputStream);
	}

	private void getInputStream(String inputStream) {
		try {
			JSONObject jObject = new JSONObject(inputStream);
			Status = jObject.getString("status");
			if (Status.equalsIgnoreCase("success"))
			{
				if (jObject.has("data"))
				{
					JSONObject jsonObject=jObject.getJSONObject("data");
					orderDO = new OrderDO();
					orderDO.OrderId=jsonObject.getString("id");
					orderDO.Status=Status;
					errorMessage="null";
					arrOrderDO.add(orderDO);
				}
			}

			else
			{
				errorMessage="error";
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}

	@Override
	public Object getData() {
		if(arrOrderDO!=null&&arrOrderDO.size()>0)
			return arrOrderDO;
		else 
			return 0;
	}

	@Override
	public String getErrorData() {
		return errorMessage;
	}

}
