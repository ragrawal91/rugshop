package com.yits.rugshop.parsers;

import com.yits.rugshop.objects.OrderDO;
import com.yits.rugshop.objects.VendorDetailsDO;

import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by sai on 4/25/2016.
 */
public class GetVendorDetails extends BaseHandler
{
    ArrayList<VendorDetailsDO> arrOrderDO;
    int opstatus;
    String httpStatus="";
    String errorMessage="",Status="",Error="";


    public GetVendorDetails(String inputStream)
    {
        arrOrderDO =new ArrayList<VendorDetailsDO>();
        getInputStream(inputStream);
    }

    private void getInputStream(String inputStream) {
        try {
            JSONObject jObject = new JSONObject(inputStream);
            Status = jObject.getString("status");
            if (Status.equalsIgnoreCase("success"))
            {
                if (jObject.has("data"))
                {
                    JSONObject jsonObject=jObject.getJSONObject("data");
                    VendorDetailsDO vendorDetailsDO=new VendorDetailsDO();

                    vendorDetailsDO.shopname=jsonObject.getString("shopName");
                    vendorDetailsDO.address=jsonObject.getString("address");
                    vendorDetailsDO.contact=jsonObject.getString("contact");
                    vendorDetailsDO.email=jsonObject.getString("email");
                    vendorDetailsDO.id=jsonObject.getString("id");

                    arrOrderDO.add(vendorDetailsDO);
                }
            }

            else
            {
                errorMessage="error";
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public Object getData() {
        if(arrOrderDO!=null&&arrOrderDO.size()>0)
            return arrOrderDO;
        else
            return 0;
    }

    @Override
    public String getErrorData() {
        return errorMessage;
    }
}
