package com.yits.rugshop.webaccess;

public interface WebAccessListener
{
	void dataDownloaded(Response data);
}
