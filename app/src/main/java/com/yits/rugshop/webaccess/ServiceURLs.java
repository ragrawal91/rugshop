package com.yits.rugshop.webaccess;


import com.yits.rugshop.utilities.AppConstants;

public class ServiceURLs {

    public static String HOST_URL_1 = "http://139.162.49.246:4646/api/";

    //Method Names
    public static String DO_REGISTRATION = "Vendors";// not usig
    public static String GET_LOGIN = "Vendors/login";
    public static String GET_USER_DETAILS = "Vendors/";  // user details
    public static String GET_CATEGORIES = "Categories?";//not using
    public static String GET_COLORS = "Colors?"; //
    public static String GET_ITEMS = "VendorProducts?filter="; //colors, price, sizes, etc(all) filters
    public static String GET_FILTER_DATA = "FilterSchemas/getData";//
    public static String PLACE_ORDERS_DATA = "Orders";    // posting orders
    public static String GET_FILTERED_PRODUCTS = "VendorProducts/getFilterData"; // multiple colors, sizes, prices,
    public static String GETVENDORDETAILS="Vendors?filter=";


    public static int getRequestType(ServiceMethods wsMethod) {
        switch (wsMethod) {
            case WS_DO_REGISTRATION:
                return AppConstants.POST;//(or) AppConstants.GET based on request method

            case WS_CHECK_LOGIN:
                return AppConstants.POST;

            case WS_GET_USER_DETAILS:
                return AppConstants.GET;

            case WS_GET_CATEGORIES:
                return AppConstants.GET;

            case WS_GET_COLORS:
                return AppConstants.GET;

            case WS_GET_ITEMS:
                return AppConstants.GET;

            case WS_GET_FILTER_DATA:
                return AppConstants.GET;

            case WS_POST_ORDER_DATA:
                return AppConstants.POST;

            case WS_GET_USERDETAILS:
                return AppConstants.GET;

            case WS_POST_FILTER_PRODUCTS:
                return AppConstants.POST;

        }
        return 0;
    }

    public static String getContentType(ServiceMethods wsMethod) {
        switch (wsMethod) {
            case WS_DO_REGISTRATION:
                return AppConstants.ContentTypeJson;

            case WS_CHECK_LOGIN:
                return AppConstants.ContentTypeJson;

            case WS_GET_USER_DETAILS:
                return AppConstants.ContentTypeJson;

            case WS_GET_CATEGORIES:
                return AppConstants.ContentTypeJson;

            case WS_GET_COLORS:
                return AppConstants.ContentTypeJson;

            case WS_GET_ITEMS:
                return AppConstants.ContentTypeJson;

            case WS_GET_FILTER_DATA:
                return AppConstants.ContentTypeJson;

            case WS_POST_ORDER_DATA:
                return AppConstants.ContentTypeJson;

            case  WS_GET_USERDETAILS:
                return AppConstants.ContentTypeJson;

            case  WS_POST_FILTER_PRODUCTS:
                return AppConstants.ContentTypeJson;
        }
        return "NA";
    }

    public static String getRequestedURL(ServiceMethods wsMethod) {
        switch (wsMethod) {
            case WS_DO_REGISTRATION:
                return HOST_URL_1 + DO_REGISTRATION;

            case WS_CHECK_LOGIN:
                return HOST_URL_1 + GET_LOGIN;

            case WS_GET_USER_DETAILS:
                return HOST_URL_1 + GET_USER_DETAILS;

            case WS_GET_CATEGORIES:
                return HOST_URL_1 + GET_CATEGORIES;

            case WS_GET_COLORS:
                return HOST_URL_1 + GET_COLORS;

            case WS_GET_ITEMS:
                return HOST_URL_1 + GET_ITEMS;

            case WS_GET_FILTER_DATA:
                return HOST_URL_1 + GET_FILTER_DATA;

            case WS_POST_ORDER_DATA:
                return HOST_URL_1 + PLACE_ORDERS_DATA;

            case WS_GET_USERDETAILS:
                return HOST_URL_1+GET_USER_DETAILS;

            case WS_POST_FILTER_PRODUCTS:
                return HOST_URL_1+GET_FILTERED_PRODUCTS;

        }
        return getRequestedURL(wsMethod);
    }

    public static String getRequestedURL(ServiceMethods wsMethod, String parameters) {
        switch (wsMethod) {
            case WS_DO_REGISTRATION:
                return String.format(HOST_URL_1 + DO_REGISTRATION, parameters);

            case WS_CHECK_LOGIN:
                return String.format(HOST_URL_1 + GET_LOGIN, parameters);

            case WS_GET_USER_DETAILS:
                return String.format(HOST_URL_1 + GET_USER_DETAILS, parameters);

            case WS_GET_CATEGORIES:
                return String.format(HOST_URL_1 + GET_CATEGORIES, parameters);

            case WS_GET_COLORS:
                return String.format(HOST_URL_1 + GET_COLORS, parameters);

            case WS_GET_ITEMS:
                return String.format(HOST_URL_1 + GET_ITEMS, parameters);

            case WS_GET_FILTER_DATA:
                return String.format(HOST_URL_1 + GET_FILTER_DATA, parameters);

            case WS_POST_ORDER_DATA:
                return String.format(HOST_URL_1 + PLACE_ORDERS_DATA, parameters);

            case WS_POST_FILTER_PRODUCTS:
                return String.format(HOST_URL_1 + GET_FILTERED_PRODUCTS, parameters);

            default:
                return getRequestedURL(wsMethod);
        }
    }

}
