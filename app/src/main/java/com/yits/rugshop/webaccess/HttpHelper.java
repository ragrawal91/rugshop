package com.yits.rugshop.webaccess;

import android.content.Context;
import android.text.format.DateUtils;

import com.yits.rugshop.utilities.AppConstants;
import com.yits.rugshop.utilities.LogUtils;
import com.yits.rugshop.utilities.PreferenceUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;


public class HttpHelper
{
    private long TIMEOUT_CONNECT_MILLIS = (60 * DateUtils.SECOND_IN_MILLIS);
    private long TIMEOUT_READ_MILLIS = TIMEOUT_CONNECT_MILLIS - 5000;
    InputStream responseStream;
    PreferenceUtils preferenceUtils;
    String cookieValue="null";



    public HttpHelper(Context mContext) {

        preferenceUtils=new PreferenceUtils(mContext);

    }

    public String sendPOSTRequest(String strPostURL,String queryparams,String strParamToPost,String contentType)
    {
        LogUtils.info(HttpHelper.class.getSimpleName(), "on HttpHelper sendPOSTRequest");

        DataOutputStream outputstream;

        int statuscode=0;

        String response = "NA";

        try {
        	 strPostURL = strPostURL.replaceAll(" ", "%20");
        	 
        	 if(queryparams!=null)
     		{
     		strPostURL+=queryparams;
                strPostURL = strPostURL.replaceAll(" ", "%20");
     		}
            URL Url=new URL(strPostURL);
            LogUtils.error(HttpHelper.class.getSimpleName(), "strPostURL="+strPostURL);

            try {
                HttpURLConnection urlConnection= (HttpURLConnection) Url.openConnection();

                urlConnection.setRequestMethod("POST");

                urlConnection.setConnectTimeout((int) TIMEOUT_CONNECT_MILLIS);

                urlConnection.setDoInput(true);

                urlConnection.setDoOutput(true);

                urlConnection.setUseCaches(false);
                
                urlConnection.setRequestProperty("Accept", "application/json");
                urlConnection.setRequestProperty("Content-Type", contentType);
                LogUtils.debug(HttpHelper.class.getSimpleName(), "Login cookie=" + preferenceUtils.getLoginCookie());
                urlConnection.setRequestProperty("Cookie",preferenceUtils.getLoginCookie());

                outputstream=new DataOutputStream(urlConnection.getOutputStream());

                outputstream.writeBytes(strParamToPost);

                outputstream.flush();

                outputstream.close();

                urlConnection.connect();

                statuscode=urlConnection.getResponseCode();

                LogUtils.error(HttpHelper.class.getSimpleName(), "HTTP STATUS CODE is="+statuscode);
                if(statuscode== HttpURLConnection.HTTP_OK)
                {
                    responseStream = new BufferedInputStream(urlConnection.getInputStream());

                    BufferedReader responseStreamReader = new BufferedReader(new InputStreamReader(responseStream));

                    String line = "";
                    StringBuilder stringBuilder = new StringBuilder();
                    while ((line = responseStreamReader.readLine()) != null) {
                        stringBuilder.append(line);
                    }
                    responseStreamReader.close();

                    response=stringBuilder.toString();

                    LogUtils.info(HttpHelper.class.getSimpleName(),"response"+response);
                }

                else if(statuscode== HttpURLConnection.HTTP_UNAUTHORIZED)
                {

                    JSONObject jsonobject=new JSONObject();

                    try {
                        jsonobject.put("status","Login Failed");
                        jsonobject.put("message","login failed");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    response=jsonobject.toString();
                }
                else if(statuscode== 422)
                {

                    JSONObject jsonobject=new JSONObject();

                    try {
                        jsonobject.put("status","Already registered");
                        jsonobject.put("message","Already registered");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    response=jsonobject.toString();
                }
                else
                {
                    response = AppConstants.NO_RESPONSE;
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        return response;
    }

    public String sendGETRequest(String strGetURL,String queryparams,String contentType) {
        LogUtils.error(HttpHelper.class.getSimpleName(), "on HttpHelper sendGETRequest");
        DataOutputStream outputstream;
        int statuscode = 0;
        String response = "NA";
        try {
            strGetURL = strGetURL.replace(" ", "%20");
    		if(queryparams!=null)
    		{
    		    strGetURL+=queryparams;
                strGetURL = strGetURL.replaceAll(" ", "%20");
    		}

            LogUtils.info(HttpHelper.class.getSimpleName(),"url is:"+strGetURL);
            URL Url = new URL(strGetURL);
            LogUtils.error(HttpHelper.class.getSimpleName(), "strGetURL="+strGetURL);
            try {
                HttpURLConnection connection = (HttpURLConnection) Url.openConnection();
                connection.setRequestMethod("GET");
                connection.setConnectTimeout((int) TIMEOUT_CONNECT_MILLIS);
                connection.setDoOutput(false);
                
                connection.setRequestProperty("Accept", "application/json");

               // connection.setRequestProperty("Content-Type", contentType);
                
                connection.connect();

                //Get Response

                 statuscode=connection.getResponseCode();

                LogUtils.error(HttpHelper.class.getSimpleName(), "HTTP STATUS CODE is="+statuscode);
                if(statuscode==HttpURLConnection.HTTP_OK)
                {
                    responseStream = connection.getInputStream();

                    BufferedReader responseStreamReader = new BufferedReader(new InputStreamReader(responseStream));

                    String line = "";
                    StringBuilder stringBuilder = new StringBuilder();
                    while ((line = responseStreamReader.readLine()) != null) {
                        stringBuilder.append(line);
                    }
                    responseStreamReader.close();

                    response = stringBuilder.toString();
                }
                else if(statuscode==HttpURLConnection.HTTP_UNAUTHORIZED )
                {
                    JSONObject jsonobject=new JSONObject();

                    try {
                        jsonobject.put("status","Invalid Credentials");
                        jsonobject.put("message","Invalid Credentials");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    response=jsonobject.toString();
                }
                else
                {
                    response = AppConstants.NO_RESPONSE;
                }

            } catch (Exception e) {
                e.printStackTrace();
            }


        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        return response;
    }
}
