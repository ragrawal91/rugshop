package com.yits.rugshop.webaccess;

public interface HttpListener 
{
	void onResponseReceived(Response response);
}
