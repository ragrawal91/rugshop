package com.yits.rugshop.webaccess;

import com.yits.rugshop.objects.FilternameAndIDDo;
import com.yits.rugshop.objects.ItemCartsDO;
import com.yits.rugshop.utilities.LogUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class BuildXMLRequest {

	public static String checkLogin(String UserId, String UserLoginPwd) {
		JSONObject jsonObject=new JSONObject();
		try {
			jsonObject.put("email", UserId);
			jsonObject.put("password", UserLoginPwd);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return jsonObject.toString();
	}

	public static String doRegistration(String UserFstName,String UserEmailId,String UserMobile,String UserPassword,String source,String role,String UserAddress) {
		JSONObject jsonObject=new JSONObject();
		try {
			jsonObject.put("shopName", UserFstName);
			jsonObject.put("address", UserAddress);
			jsonObject.put("email", UserEmailId);
			jsonObject.put("contact", UserMobile);
			jsonObject.put("password", UserPassword);
			jsonObject.put("name", "name");
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return jsonObject.toString();
	}

	public static String getUserDetails()
	{
		StringBuilder stringBuilder = new StringBuilder();
		return stringBuilder.toString();
	}

	public static String getCategories()
	{
		StringBuilder stringBuilder = new StringBuilder();
		return stringBuilder.toString();
	}

	public static String getColors()
	{
		StringBuilder stringBuilder = new StringBuilder();
		return stringBuilder.toString();
	}

	public static String getItems()
	{
		StringBuilder stringBuilder = new StringBuilder();
		return stringBuilder.toString();
	}

	public static String getFilterData()
	{
		StringBuilder stringBuilder = new StringBuilder();
		return stringBuilder.toString();
	}

	public static String placeOrder(String userName, String userEmail, String userMobile, String userAddress, String vendorID, String vendorShopName, Double subtotal, Double GrandTotal, Double TotalTaxValue, ArrayList<ItemCartsDO> orderCartList, String paymentType, String paymentstatus, String delchrgs, String addressType, String vendoraddress, Double subtotalvendor, Double GrandTotalvendor, Double TotalTaxValuevendor,String vendorEmail) {
		JSONObject jsonObject=new JSONObject();
		JSONObject couponObject=new JSONObject();
		JSONArray cartArray=new JSONArray();
		try {
			jsonObject.put("vendorEmail",vendorEmail);
			jsonObject.put("Addresstype",addressType);
			jsonObject.put("deliveryCharge",Double.valueOf(delchrgs));
			jsonObject.put("paymodeStaus",paymentstatus);
			jsonObject.put("orderStatus","new");
			jsonObject.put("createdBy",userMobile);
			jsonObject.put("vendoraddress",vendoraddress);
			jsonObject.put("personname", userName);
			jsonObject.put("email", userEmail);
			jsonObject.put("phone", userMobile);
			jsonObject.put("orderOrginId","Android");
			jsonObject.put("Useraddress", userAddress);
			jsonObject.put("vendorId", vendorID);
			jsonObject.put("paymentType",paymentType);
			jsonObject.put("vendorShopName", vendorShopName);
			jsonObject.put("subtotalVendorFinal", subtotal);
			jsonObject.put("GrandTotalVendorFinal", GrandTotal);
			jsonObject.put("TotalTaxValueVendorFinal", TotalTaxValue);
			jsonObject.put("subtotalVendor", subtotalvendor);
			jsonObject.put("GrandTotalVendor", GrandTotalvendor);
			jsonObject.put("TotalTaxValueVendor", TotalTaxValuevendor);
			/*couponObject.put("couponFlag",false);
			couponObject.put("couponCode","NA");
			couponObject.put("couponValue","NA");*/

			//jsonObject.put("couponinfo",couponObject);

			for(int i=0;i<orderCartList.size();i++)
			{
				JSONObject cartObject=new JSONObject();
				ItemCartsDO model=orderCartList.get(i);
				cartObject.put("productId",model.ItemProductId);
				cartObject.put("itemId",model.ItemId);
				cartObject.put("itemName",model.ItemName);
				cartObject.put("ItemSize",model.ItemSize);
				cartObject.put("ItemVendorPrice",Double.valueOf(model.ItemBasePrice));
				cartObject.put("ItemVendorPriceTotal",Math.round(Double.parseDouble(model.ItemBasePrice))*model.ItemQuantity);

				cartObject.put("ItemFinalPrice",Double.valueOf(model.ItemFinalPrice));
				cartObject.put("ItemTotalPrice",Math.round(Double.parseDouble(model.ItemFinalPrice))*model.ItemQuantity);
				cartObject.put("ItemColor",model.ItemColor);
				cartObject.put("ItemMaterial",model.ItemMaterial);
				cartObject.put("ItemPattern",model.ItemPattern);
				cartObject.put("ItemCatogiry",model.categoryId);
				cartObject.put("ItemQuantity",model.ItemQuantity);
				cartArray.put(cartObject);
				jsonObject.put("items",cartArray);
			}

		} catch (JSONException e) {
			e.printStackTrace();
		}

		LogUtils.error(BuildXMLRequest.class.getSimpleName(),"Order Details = "+jsonObject.toString());
		return jsonObject.toString();
	}

	public static String getFilteredProducts(ArrayList<FilternameAndIDDo> selectedSizeFilterList,
											 ArrayList<FilternameAndIDDo> selectedPatternFilterList,
											 ArrayList<FilternameAndIDDo> selectedMaterialFilterList,
											 ArrayList<FilternameAndIDDo> selectedColorFilterList,
											 ArrayList<FilternameAndIDDo> selectedSizeGroupFilterList,
											 ArrayList<FilternameAndIDDo> selectedPriceFilterList,
											 ArrayList<FilternameAndIDDo> selectedFeelerFilterList,
											 ArrayList<FilternameAndIDDo> selectedCategoryFilterList,
											 ArrayList<FilternameAndIDDo> selectedRunnerFilterList, String email)
	{
		LogUtils.error(BuildXMLRequest.class.getSimpleName(),"selectedSizeFilterList size = "+selectedSizeFilterList.size());
		LogUtils.error(BuildXMLRequest.class.getSimpleName(),"selectedPatternFilterList size = "+selectedPatternFilterList.size());
		LogUtils.error(BuildXMLRequest.class.getSimpleName(),"selectedMaterialFilterList size = "+selectedMaterialFilterList.size());
		LogUtils.error(BuildXMLRequest.class.getSimpleName(),"selectedColorFilterList size = "+selectedColorFilterList.size());
		LogUtils.error(BuildXMLRequest.class.getSimpleName(),"selectedSizeGroupFilterList size = "+selectedSizeGroupFilterList.size());
		LogUtils.error(BuildXMLRequest.class.getSimpleName(),"selectedPriceFilterList size = "+selectedPriceFilterList.size());
		LogUtils.error(BuildXMLRequest.class.getSimpleName(),"selectedFeelerFilterList size = "+selectedFeelerFilterList.size());
		LogUtils.error(BuildXMLRequest.class.getSimpleName(),"selectedCategoryFilterList size = "+selectedCategoryFilterList.size());
		LogUtils.error(BuildXMLRequest.class.getSimpleName(),"selectedRunnerFilterList size = "+selectedRunnerFilterList.size());

		JSONObject jsonObject=new JSONObject();

		JSONArray sizeArray				 = new JSONArray();
		JSONArray patternArray			 = new JSONArray();
		JSONArray materialArray			 = new JSONArray();
		JSONArray colorArray			 = new JSONArray();
		JSONArray feeleArray			 = new JSONArray();
		JSONArray sizegroupArray		 = new JSONArray();
		JSONArray priceArray			 = new JSONArray();
		JSONArray categoryArray			 = new JSONArray();
		JSONArray runnerArray			 = new JSONArray();

		try {
			for(int i=0;i<selectedSizeFilterList.size();i++)
			{
				FilternameAndIDDo model=selectedSizeFilterList.get(i);
				sizeArray.put(model.id);
			}

			for(int i=0;i<selectedPatternFilterList.size();i++)
			{
				FilternameAndIDDo model=selectedPatternFilterList.get(i);
				patternArray.put(model.id);
			}

			for(int i=0;i<selectedMaterialFilterList.size();i++)
			{
				FilternameAndIDDo model=selectedMaterialFilterList.get(i);
				materialArray.put(model.id);
			}

			for(int i=0;i<selectedColorFilterList.size();i++)
			{
				FilternameAndIDDo model=selectedColorFilterList.get(i);
				colorArray.put(model.id);
			}

			for(int i=0;i<selectedSizeGroupFilterList.size();i++)
			{
				FilternameAndIDDo model=selectedSizeGroupFilterList.get(i);
				sizegroupArray.put(model.id);
			}

			for(int i=0;i<selectedPriceFilterList.size();i++)
			{
				FilternameAndIDDo model=selectedPriceFilterList.get(i);
				priceArray.put(model.name);
			}

			for(int i=0;i<selectedFeelerFilterList.size();i++)
			{
				FilternameAndIDDo model=selectedFeelerFilterList.get(i);
				feeleArray.put(model.id);
			}
			for(int i=0;i<selectedCategoryFilterList.size();i++)
			{
				FilternameAndIDDo model=selectedCategoryFilterList.get(i);
				categoryArray.put(model.id);
			}

			for(int i=0;i<selectedRunnerFilterList.size();i++)
			{
				FilternameAndIDDo model=selectedRunnerFilterList.get(i);
				runnerArray.put(model.id);
			}

			jsonObject.put("priceId",priceArray);
			jsonObject.put("categoryId",categoryArray);
			jsonObject.put("sizeId",sizeArray);
			jsonObject.put("sizeTypeId",sizegroupArray);
			jsonObject.put("colorId",colorArray);
			jsonObject.put("feelerId", feeleArray);
			jsonObject.put("materialId",materialArray);
			jsonObject.put("patternId",patternArray);
			jsonObject.put("runnerId",runnerArray);
			jsonObject.put("email",email);

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		LogUtils.error(BuildXMLRequest.class.getSimpleName(),"Filter Inputs = "+jsonObject.toString());
		return jsonObject.toString();
	}

	public static String getColorFilteredProducts(ArrayList<String> selectedColors) {

		JSONObject jsonObject  = new JSONObject();
		JSONArray filterArray  = new JSONArray();
		try {
			JSONObject colorJson  = new JSONObject();

		}
		catch (Exception e) {
			e.printStackTrace();
		}

		LogUtils.error(BuildXMLRequest.class.getSimpleName(),"Order Details = "+jsonObject.toString());
		return jsonObject.toString();
	}
}