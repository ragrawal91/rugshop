package com.yits.rugshop.adapter;


import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.Paint;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.jaredrummler.materialspinner.MaterialSpinner;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.yits.rugshop.R;
import com.yits.rugshop.listener.CartListListener;
import com.yits.rugshop.objects.ItemCartsDO;
import com.yits.rugshop.objects.SizeListDO;
import com.yits.rugshop.utilities.BgViewAware;
import com.yits.rugshop.utilities.CartDatabase;
import com.yits.rugshop.utilities.FontType;

import java.text.DecimalFormat;
import java.util.ArrayList;

import tyrantgit.explosionfield.ExplosionField;

public class CartAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<ItemCartsDO> arrCartDO;
    ItemCartsDO cartsDO;
    ImageLoaderConfiguration imgLoaderConf;
    DisplayImageOptions dispImage;
    ImageLoader imgLoader = ImageLoader.getInstance();
    FontType fonttype;
    CartListListener cartListListener;
    ArrayList<String> cartColorList = new ArrayList<>();
    ArrayList<SizeListDO> cartSizeList = new ArrayList<>();
    CartColorAdapter cartColorAdapter;
    CartSizeAdapter cartSizeAdapter;
    SizeListDO sizeListDO;
    ListView lst_cartsize;
    LinearLayout ll_IMG;
    DecimalFormat decimalFormat = new DecimalFormat("00.00######");
    ImageView img_crossSize, img_crossColor;
    TextView txt_dialogitemName, txt_no, txt_yes;
    int qty;
    ExplosionField mExplosionField;
    private Dialog dialog;

    public CartAdapter(Context context, ArrayList<ItemCartsDO> arrCartDO) {
        this.context = context;
        this.arrCartDO = arrCartDO;

        CartDatabase.init(context);
        mExplosionField = ExplosionField.attach2Window((Activity) context);
        imgLoaderConf = new ImageLoaderConfiguration.Builder(context).build();
        imgLoader.init(imgLoaderConf);
        dispImage = new DisplayImageOptions.Builder()
                .showImageOnFail(R.mipmap.wool_rug)
                .showImageOnLoading(R.mipmap.wool_rug)
                .showImageForEmptyUri(R.mipmap.wool_rug)
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .delayBeforeLoading(100)
                .imageScaleType(ImageScaleType.EXACTLY)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .build();
    }

    public void refresh(ArrayList<ItemCartsDO> arrCartDO){
        this.arrCartDO = arrCartDO;
        notifyDataSetChanged();
    }
    @Override
    public int getCount() {

        return arrCartDO.size();

    }

    @Override
    public Object getItem(int position) {
        return arrCartDO.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder = null;
        if (convertView == null) {
            convertView                     = LayoutInflater.from(context).inflate(R.layout.row_cart, parent, false);
            viewHolder                      = new ViewHolder();
            viewHolder.sprSizes             = (MaterialSpinner) convertView.findViewById(R.id.sprSizes);
            viewHolder.txt_itemName         = (TextView) convertView.findViewById(R.id.txt_itemName);
            viewHolder.txt_productId        = (TextView) convertView.findViewById(R.id.txt_productId);
            viewHolder.txt_qty              = (TextView) convertView.findViewById(R.id.txt_qty);
            viewHolder.txt_totalprice       = (TextView) convertView.findViewById(R.id.txt_totalprice);
            viewHolder.txt_unitPrice        = (TextView) convertView.findViewById(R.id.txt_unitPrice);
            viewHolder.txt_BasePrice        = (TextView) convertView.findViewById(R.id.txt_BasePrice);
            viewHolder.txt_material         = (TextView) convertView.findViewById(R.id.txt_material);
            viewHolder.txt_size             = (TextView) convertView.findViewById(R.id.txt_size);
            viewHolder.txt_persaved         = (TextView) convertView.findViewById(R.id.txt_persaved);
            viewHolder.item_rating          = (RatingBar) convertView.findViewById(R.id.item_rating);
            viewHolder.ll_itemIMG           = (FrameLayout) convertView.findViewById(R.id.ll_itemIMG);
            viewHolder.ll_color             = (LinearLayout) convertView.findViewById(R.id.ll_color);
            viewHolder.ll_size              = (LinearLayout) convertView.findViewById(R.id.ll_size);
            viewHolder.img_plus             = (Button) convertView.findViewById(R.id.img_plus);
            viewHolder.img_minus            = (Button) convertView.findViewById(R.id.img_minus);
            viewHolder.img_remove           = (ImageView) convertView.findViewById(R.id.img_remove);
            convertView.setTag(viewHolder);
        }
        else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        ViewGroup root = (ViewGroup) convertView.findViewById(R.id.row_cart);
        //fonttype = new FontType(context, root);

        cartsDO = (ItemCartsDO) getItem(position);
        final ArrayList<SizeListDO> sizelist = CartDatabase.getAllSizeList(cartsDO.ItemId);

        final ArrayList<String> sizesStrings = new ArrayList<>();

        for (int i = 0; i < sizelist.size(); i++) {
            sizesStrings.add(sizelist.get(i).SizeName);
        }
        if(sizesStrings!=null && sizesStrings.size()>0)
            viewHolder.sprSizes.setItems(sizesStrings);
        //cartSizeList=CartDatabase.getAllSizeList(cartsDO.ItemId);
        qty = cartsDO.ItemQuantity;

        viewHolder.txt_itemName.setText(cartsDO.ItemName);
        viewHolder.txt_productId.setText(cartsDO.ItemProductId);
        viewHolder.txt_qty.setText("" + qty);
        String multipliedPrice = String.format("%.2f", Double.parseDouble(cartsDO.ItemFinalPrice) * qty);
        viewHolder.txt_totalprice.setText("£" + multipliedPrice);
        viewHolder.txt_unitPrice.setText("£"+cartsDO.ItemFinalPrice);
        viewHolder.txt_BasePrice.setText("£"+cartsDO.ItemBasePrice);
        viewHolder.txt_BasePrice.setPaintFlags(viewHolder.txt_BasePrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);

        viewHolder.txt_material.setText(cartsDO.ItemMaterial);
        viewHolder.txt_size.setText(cartsDO.ItemSize);
        viewHolder.txt_persaved.setText("Save " + cartsDO.ItemPerSaved);
        viewHolder.item_rating.setRating(Float.valueOf(cartsDO.ItemRating));


        viewHolder.ll_size.setTag(cartsDO);
//        viewHolder.ll_size.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                final ItemCartsDO itemCartsDO = (ItemCartsDO) v.getTag();
//
//                final ArrayList<SizeListDO> sizelist = CartDatabase.getAllSizeList(itemCartsDO.ItemId);
//                /*final DialogPlus dialog = DialogPlus.newDialog(context)
//						.setContentHolder(new com.orhanobut.dialogplus.ViewHolder(R.layout.cart_dialogsize))
//						.setGravity(Gravity.NO_GRAVITY)
//						.setGravity(Gravity.AXIS_X_SHIFT)
//						.setExpanded(true)  // This will enable the expand feature, (similar to android L share dialog)
//						.setCancelable(false)
//						.setContentWidth(800)
//						.setContentHeight(550)
//						.create();*/
//                //		showDialog(context, (int) ((v.getLeft()*1.43)-(v.getWidth())), (int) (v.getTop()+(v.getHeight()*3.4)));
//                showDialogSize(context, 700, 450);
//
//
//                lst_cartsize = (ListView) dialog.findViewById(R.id.lst_cartsize);
//                img_crossSize = (ImageView) dialog.findViewById(R.id.img_crossSize);
//                txt_dialogitemName = (TextView) dialog.findViewById(R.id.txt_dialogitemName);
//
//                txt_dialogitemName.setTag(itemCartsDO);
//                lst_cartsize.setTag(itemCartsDO);
//
//                cartSizeAdapter = new CartSizeAdapter(context, sizelist);
//                lst_cartsize.setAdapter(cartSizeAdapter);
//
//                txt_dialogitemName.setText(itemCartsDO.ItemName);
//
//                img_crossSize.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//
//                        dialog.dismiss();
//                    }
//                });
//
//                lst_cartsize.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//                    @Override
//                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                        SizeListDO sizeListDO = (SizeListDO) parent.getItemAtPosition(position);
//                        itemCartsDO.ItemSize = sizeListDO.SizeName;
//                        itemCartsDO.ItemBasePrice = sizeListDO.BasePrice;
//                        CartDatabase.updateSizePriceInCartlist(sizeListDO);
//                        cartListListener.getLocalDatabaseCartList();
//                        notifyDataSetChanged();
//                        dialog.dismiss();
//                    }
//                });
//                dialog.show();
//            }
//        });
//

        viewHolder.img_remove.setTag(cartsDO);
        viewHolder.img_remove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ItemCartsDO model = (ItemCartsDO) view.getTag();
                //mExplosionField.explode(view);
                appCompatAlert(context, model);
//                showDialogRemove(context, 700, 450, model);


            }
        });

        viewHolder.img_plus.setTag(cartsDO);
        viewHolder.img_plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                ItemCartsDO model = (ItemCartsDO) view.getTag();
                int crtQuantity = model.ItemQuantity;
                model.ItemQuantity = crtQuantity + 1;
                CartDatabase.updateQuantityInCartlist(model, model.ItemQuantity);
                cartListListener.getTheIncreasedSubtotal(Double.parseDouble(model.ItemFinalPrice), Double.parseDouble(model.ItemBasePrice));
                notifyDataSetChanged();


            }
        });

        viewHolder.img_minus.setTag(cartsDO);
        viewHolder.img_minus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                ItemCartsDO model = (ItemCartsDO) view.getTag();
                int crtQuantity = model.ItemQuantity;
                if (crtQuantity <= 1) {
                    model.ItemQuantity = 1;

                } else {
                    model.ItemQuantity = crtQuantity - 1;
                    cartListListener.getTheDecreasedSubtotal(Double.parseDouble(model.ItemFinalPrice), Double.parseDouble(model.ItemBasePrice));
                }

                CartDatabase.updateQuantityInCartlist(model, model.ItemQuantity);

                notifyDataSetChanged();
            }
        });


        viewHolder.ll_itemIMG.setTag(cartsDO);
        viewHolder.ll_itemIMG.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final ItemCartsDO itemCartsDO = (ItemCartsDO) v.getTag();

                showDialogImage(context, 700, 450);

                ll_IMG = (LinearLayout) dialog.findViewById(R.id.ll_IMG);
                img_crossColor = (ImageView) dialog.findViewById(R.id.img_crossColor);

                ll_IMG.setTag(itemCartsDO);
                img_crossColor.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                    }
                });
                imgLoader.getInstance().displayImage(itemCartsDO.ItemBigImgUrl, new BgViewAware(ll_IMG), dispImage);

                dialog.show();
            }
        });

        viewHolder.ll_itemIMG.setTag(cartsDO);
        imgLoader.getInstance().displayImage(cartsDO.ItemBigImgUrl, new BgViewAware(viewHolder.ll_itemIMG), dispImage);

        return convertView;
    }


    private class ViewHolder {
        MaterialSpinner sprSizes;
        TextView txt_itemName, txt_productId, txt_qty, txt_totalprice, txt_unitPrice, txt_material, txt_size;
        TextView txt_BasePrice, txt_persaved;
        RatingBar item_rating;
        ImageView img_remove;
        Button img_plus, img_minus;
        LinearLayout ll_color, ll_size;
        FrameLayout ll_itemIMG;


    }

    public void registerLocaldatabaseListener(CartListListener cartListListener) {
        this.cartListListener = cartListListener;
    }


    public void showDialogSize(Context context, int x, int y) {
        dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.cart_dialogsize);
        dialog.setCanceledOnTouchOutside(false);


		/*WindowManager.LayoutParams wmlp = dialog.getWindow().getAttributes();
		wmlp.gravity = Gravity.TOP | Gravity.LEFT;
		wmlp.x = x;
		wmlp.y = y;*/
    }

    public void showDialogImage(Context context, int x, int y) {
        dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.cart_dialogimage);
        dialog.setCanceledOnTouchOutside(false);


		/*WindowManager.LayoutParams wmlp = dialog.getWindow().getAttributes();
		wmlp.gravity = Gravity.TOP | Gravity.LEFT;
		wmlp.x = x;
		wmlp.y = y;*/
    }

    private AlertDialog.Builder alertDialog;
    public void appCompatAlert(Context context, final ItemCartsDO model) {
        if (alertDialog == null)
            alertDialog = new AlertDialog.Builder(context, R.style.AppCompatAlertDialogStyle);
        alertDialog.setTitle("Alert!");
        alertDialog.setMessage("Do you really want to delete this item?");
        alertDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                onButtonYesClick(model);
            }
        });//second parameter used for onclicklistener
            alertDialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    onButtonNoClick(model);
                }
            });
        alertDialog.setCancelable(false);
        alertDialog.show();
    }

    public void onButtonNoClick(ItemCartsDO model) {
    }

    public void onButtonYesClick(ItemCartsDO model) {
        CartDatabase.deletecartlist(model);
        CartDatabase.deletecolorlist(model);
        CartDatabase.deletesizelist(model);
        cartListListener.getLocalDatabaseCartList();
    }


    public void showDialogRemove(Context context, int x, int y, final ItemCartsDO model) {
        if(dialog == null)
        dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.cart_dialogremove);
        dialog.setCanceledOnTouchOutside(false);

        txt_yes = (TextView) dialog.findViewById(R.id.txt_yes);
        txt_no = (TextView) dialog.findViewById(R.id.txt_no);

        txt_yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                CartDatabase.deletecartlist(model);
                CartDatabase.deletecolorlist(model);
                CartDatabase.deletesizelist(model);
                cartListListener.getLocalDatabaseCartList();
                dialog.dismiss();

            }
        });

        txt_no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();

            }
        });

        dialog.show();
    }

}
