package com.yits.rugshop.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.yits.rugshop.R;
import com.yits.rugshop.listener.CartListListener;
import com.yits.rugshop.objects.ItemCartsDO;
import com.yits.rugshop.objects.SizeListDO;
import com.yits.rugshop.utilities.CartDatabase;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by sai on 8/12/2016.
 */
public class CartHomeAdapter extends BaseAdapter {

    ArrayList<ItemCartsDO> itemCartsDOs;
    Context context;
    LayoutInflater layoutInflater;
    DisplayImageOptions dispImage;
    CartListListener cartListListener;

    ImageLoaderConfiguration imgLoaderConf;

    ImageLoader imgLoader = ImageLoader.getInstance();


    public CartHomeAdapter(ArrayList<ItemCartsDO> itemCartsDOs, Context context, CartListListener cartListListener) {

        CartDatabase.init(context);

        this.itemCartsDOs = itemCartsDOs;
        this.cartListListener = cartListListener;


        this.context = context;
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        imgLoaderConf = new ImageLoaderConfiguration.Builder(context).build();
        imgLoader.init(imgLoaderConf);
        dispImage = new DisplayImageOptions.Builder()
                .showImageOnFail(R.mipmap.wool_rug)
                .showImageOnLoading(R.mipmap.wool_rug)
                .showImageForEmptyUri(R.mipmap.wool_rug)
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .delayBeforeLoading(100)
                .imageScaleType(ImageScaleType.EXACTLY)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .build();
    }
    public void refresh(ArrayList<ItemCartsDO> itemCartsDOs){
        this.itemCartsDOs = itemCartsDOs;
        notifyDataSetChanged();
    }
    @Override
    public int getCount() {
        return itemCartsDOs.size();
    }

    @Override
    public ItemCartsDO getItem(int position) {
        return itemCartsDOs.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ItemCartsDO itemCartsDO = itemCartsDOs.get(position);
        ViewHolder viewHolder = null;
        if (convertView == null) {
            viewHolder   = new ViewHolder();
            convertView = layoutInflater.inflate(R.layout.row_cart_home, parent, false);
            viewHolder.img_cart_home        = (ImageView) convertView.findViewById(R.id.img_cart_home);
            viewHolder.tv_item_price        = (TextView) convertView.findViewById(R.id.tv_item_price);
            viewHolder.tv_item_name         = (TextView) convertView.findViewById(R.id.tv_item_name);
            viewHolder.img_cancel           = (ImageView) convertView.findViewById(R.id.img_cancel);
            viewHolder.spSizes              = (Spinner) convertView.findViewById(R.id.spSizes);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        viewHolder.tv_item_name.setText(itemCartsDO.ItemName);
        viewHolder.tv_item_price.setText("" + itemCartsDO.ItemFinalPrice);

        final ArrayList<SizeListDO> sizelist = CartDatabase.getAllSizeList(itemCartsDO.ItemId);

        final List<String> sizesStrings = new ArrayList<>();

        for (int i = 0; i < sizelist.size(); i++) {
            sizesStrings.add(sizelist.get(i).SizeName);
        }
        if(sizesStrings!=null && sizesStrings.size()>0){
            ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(context, R.layout.spinner_text_view, sizesStrings);
            dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            viewHolder.spSizes.setAdapter(dataAdapter);
        }
        final ViewHolder dummyHolder = viewHolder;
        viewHolder.spSizes.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                dummyHolder.tv_item_price.setText(""+sizelist.get(position).FinalPrice+"");
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        viewHolder.img_cancel.setTag(itemCartsDO);
        imgLoader.getInstance().displayImage(itemCartsDO.ItemBigImgUrl, viewHolder.img_cart_home, dispImage);
        final ViewHolder finalViewHolder = viewHolder;
        viewHolder.img_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ItemCartsDO model = (ItemCartsDO) finalViewHolder.img_cancel.getTag();
                CartDatabase.deletecartlist(model);
                CartDatabase.deletecolorlist(model);
                CartDatabase.deletesizelist(model);
                cartListListener.getLocalDatabaseCartList();
            }
        });

        return convertView;
    }

    private class ViewHolder {
        Spinner spSizes;
        TextView tv_item_name, tv_item_price;
        ImageView img_cart_home, img_cancel;
    }

}
