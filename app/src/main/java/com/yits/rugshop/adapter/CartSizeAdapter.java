package com.yits.rugshop.adapter;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.yits.rugshop.R;
import com.yits.rugshop.objects.SizeListDO;
import com.yits.rugshop.utilities.FontType;

import java.util.ArrayList;

public class CartSizeAdapter extends BaseAdapter
{

	private Context context;
	private ArrayList<SizeListDO> arrSizeDO;
	ViewHolder viewHolder;
	LayoutInflater inflater;
	FontType fonttype;
	SizeListDO sizeListDO;

	public CartSizeAdapter(Context context, ArrayList<SizeListDO> arrSizeDO)
	{
		this.context       = context;
		this.arrSizeDO    = arrSizeDO;
		inflater= (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

	}

	@Override
	public int getCount()
	{

		return arrSizeDO.size();

	}

	@Override
	public Object getItem(int position)
	{
		return arrSizeDO.get(position);
	}

	@Override
	public long getItemId(int position)
	{
		return position;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent)
	{
		if(convertView==null)
		{
			convertView = inflater.inflate(R.layout.row_cartsize, parent,false);
			viewHolder = new ViewHolder();
			viewHolder.txt_cartsize 	    = (TextView)convertView.findViewById(R.id.txt_cartsize);
			viewHolder.txt_sizePrice 	    = (TextView)convertView.findViewById(R.id.txt_sizePrice);
			viewHolder.txt_count 	    = (TextView)convertView.findViewById(R.id.txt_count);

			convertView.setTag(viewHolder);
		}
		else
		{
			viewHolder= (ViewHolder) convertView.getTag();
		}

		sizeListDO = (SizeListDO) getItem(position);

		ViewGroup root = (ViewGroup) convertView.findViewById(R.id.row_cartsize);
		fonttype = new FontType(context, root);

		viewHolder.txt_cartsize.setText(sizeListDO.SizeName);
		viewHolder.txt_sizePrice.setText("£ "+sizeListDO.FinalPrice);
		int Count =position+1;
		viewHolder.txt_count.setText(String.valueOf(Count));


		//imgLoader.displayImage(AppConstants.IMAGE_URL + categoryDO.C, viewHolder.img_profile, dispImage);

		return convertView;
	}


	private class ViewHolder
	{
		TextView  txt_cartsize,txt_sizePrice,txt_count;

	}



}
