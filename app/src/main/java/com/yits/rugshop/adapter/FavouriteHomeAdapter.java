package com.yits.rugshop.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.yits.rugshop.R;
import com.yits.rugshop.activity.BaseActivity;
import com.yits.rugshop.listener.CartListListener;
import com.yits.rugshop.objects.ItemsDO;
import com.yits.rugshop.objects.SizeListDO;
import com.yits.rugshop.utilities.CartDatabase;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by sai on 8/12/2016.
 */
public class FavouriteHomeAdapter extends BaseAdapter {

    private ArrayList<ItemsDO> itemFavDOs;
    private Context context;
    private DisplayImageOptions dispImage;
    private CartListListener cartListListener;
    private ImageLoaderConfiguration imgLoaderConf;
    private ImageLoader imgLoader = ImageLoader.getInstance();


    public FavouriteHomeAdapter(ArrayList<ItemsDO> itemFavDOs, Context context, CartListListener cartListListener) {
        CartDatabase.init(context);
        this.itemFavDOs = itemFavDOs;
        this.cartListListener = cartListListener;
        this.context = context;
        imgLoaderConf = new ImageLoaderConfiguration.Builder(context).build();
        imgLoader.init(imgLoaderConf);
        dispImage = new DisplayImageOptions.Builder()
                .showImageOnFail(R.mipmap.wool_rug)
                .showImageOnLoading(R.mipmap.wool_rug)
                .showImageForEmptyUri(R.mipmap.wool_rug)
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .delayBeforeLoading(100)
                .imageScaleType(ImageScaleType.EXACTLY)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .build();
    }
    public void refresh(ArrayList<ItemsDO> itemFavDOs){
        this.itemFavDOs = itemFavDOs;
        notifyDataSetChanged();
    }
    @Override
    public int getCount() {
        return itemFavDOs!=null?itemFavDOs.size():0;
    }

    @Override
    public ItemsDO getItem(int position) {
        return itemFavDOs.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ItemsDO itemsDO = itemFavDOs.get(position);
        ViewHolder viewHolder = null;
        if (convertView == null) {
            viewHolder   = new ViewHolder();
            convertView = LayoutInflater.from(context).inflate(R.layout.row_cart_home, parent, false);
            viewHolder.img_cart_home        = (ImageView) convertView.findViewById(R.id.img_cart_home);
            viewHolder.tv_item_price        = (TextView) convertView.findViewById(R.id.tv_item_price);
            viewHolder.tv_item_name         = (TextView) convertView.findViewById(R.id.tv_item_name);
            viewHolder.img_cancel           = (ImageView) convertView.findViewById(R.id.img_cancel);
            viewHolder.spSizes              = (Spinner) convertView.findViewById(R.id.spSizes);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        viewHolder.tv_item_name.setText(itemsDO.ItemName);
        viewHolder.tv_item_price.setText("" + itemsDO.ItemFinalPrice);
        imgLoader.getInstance().displayImage(itemsDO.ItemBigImgUrl, viewHolder.img_cart_home, dispImage);

        final ArrayList<SizeListDO> sizeList = CartDatabase.getAllFavouriteSizeList(itemsDO.ItemId);

        final List<String> sizesStrings = new ArrayList<>();

        for (int i = 0; i < sizeList.size(); i++) {
            sizesStrings.add(sizeList.get(i).SizeName);
        }
        if(sizesStrings!=null && sizesStrings.size()>0){
            ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(context, R.layout.spinner_text_view, sizesStrings);
            dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            viewHolder.spSizes.setAdapter(dataAdapter);
        }
        final ViewHolder dummyHolder = viewHolder;
        viewHolder.spSizes.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                dummyHolder.tv_item_price.setText(""+sizeList.get(pos).FinalPrice);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });



        viewHolder.img_cancel.setTag(itemsDO);
        final ViewHolder finalViewHolder = viewHolder;
        viewHolder.img_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ItemsDO itemDo = (ItemsDO) finalViewHolder.img_cancel.getTag();
                CartDatabase.deleteFavouritelist(itemDo);
                CartDatabase.deleteFavouritecolorlist(itemDo);
                CartDatabase.deleteFavouritesizelist(itemDo);
                ((BaseActivity)context).getLocalDatabaseFavList();
            }
        });

        return convertView;
    }

    private class ViewHolder {
        Spinner spSizes;
        TextView tv_item_name, tv_item_price;
        ImageView img_cart_home, img_cancel;
    }

}
