package com.yits.rugshop.adapter;

import android.content.Context;
import android.database.DataSetObserver;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.SpinnerAdapter;
import android.widget.TextView;

import com.jaredrummler.materialspinner.MaterialSpinnerAdapter;
import com.yits.rugshop.R;
import com.yits.rugshop.objects.ItemsSizeDO;

import java.util.ArrayList;

/**
 * Created by sai on 8/16/2016.
 */
public class SizeSpinnerAdapter extends MaterialSpinnerAdapter {

    ArrayList<ItemsSizeDO> itemsSizeDOs;
    Context context;
    LayoutInflater layoutInflater;

    public SizeSpinnerAdapter(ArrayList<ItemsSizeDO> itemsSizeDOs, Context context) {
        super(context,itemsSizeDOs);
        this.itemsSizeDOs = itemsSizeDOs;
        this.context = context;

        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }




    @Override
    public int getCount() {
        return itemsSizeDOs.size();
    }

    @Override
    public ItemsSizeDO getItem(int position) {
        return itemsSizeDOs.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }



    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ItemsSizeDO itemsSizeDO = getItem(position);
        ViewHolder viewHolder = new ViewHolder();
        if (convertView == null) {

            convertView = layoutInflater.inflate(R.layout.row_sizes_spinner, null);

            viewHolder.tv_size = (TextView) convertView.findViewById(R.id.tv_size);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        viewHolder.tv_size.setText(itemsSizeDO.ItemSize);
        return convertView;
    }


    class ViewHolder {
        TextView tv_size;
    }
}
