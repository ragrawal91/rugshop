package com.yits.rugshop.adapter;


import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.yits.rugshop.R;
import com.yits.rugshop.activity.ItemsActivity;
import com.yits.rugshop.objects.CategoryDO;
import com.yits.rugshop.utilities.BgViewAware;
import com.yits.rugshop.utilities.FontType;

import java.util.ArrayList;

public class CategoryAdapter extends BaseAdapter
{

	private Context context;
	private ArrayList<CategoryDO> arrCategoryDO;
	CategoryDO categoryDO;
	ViewHolder viewHolder;
	LayoutInflater inflater;
	ImageLoaderConfiguration imgLoaderConf;
	DisplayImageOptions dispImage;
	ImageLoader imgLoader= ImageLoader.getInstance();
	FontType fonttype;

	public CategoryAdapter(Context context, ArrayList<CategoryDO> arrCategoryDO)
	{
		this.context       = context;
		this.arrCategoryDO    = arrCategoryDO;
		inflater= (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		imgLoaderConf = new ImageLoaderConfiguration.Builder(context).build();
		imgLoader.init(imgLoaderConf);
		dispImage=new DisplayImageOptions.Builder()
				.showImageOnFail(R.mipmap.wool_rug)
				.showImageOnLoading(R.mipmap.wool_rug)
				.showImageForEmptyUri(R.mipmap.wool_rug)
				.cacheInMemory(true)
				.cacheOnDisk(true)
				.considerExifParams(true)
				.delayBeforeLoading(100)
				.imageScaleType(ImageScaleType.EXACTLY)
				.bitmapConfig(Bitmap.Config.RGB_565)
				.build();

	}

	@Override
	public int getCount()
	{

		return arrCategoryDO.size();

	}

	@Override
	public Object getItem(int position)
	{
		return arrCategoryDO.get(position);
	}

	@Override
	public long getItemId(int position)
	{
		return position;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent)
	{
		if(convertView==null)
		{
			convertView = inflater.inflate(R.layout.row_catg, parent,false);
			viewHolder = new ViewHolder();
			viewHolder.txt_catg 	    = (TextView)convertView.findViewById(R.id.txt_catg);
			viewHolder.ivCatImage      = (ImageView) convertView.findViewById(R.id.ivCatImage);

			convertView.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) convertView.getTag();
		}

		categoryDO = (CategoryDO) getItem(position);


		viewHolder.txt_catg.setText(categoryDO.CatgName);

		viewHolder.ivCatImage.setTag(categoryDO);
		viewHolder.ivCatImage.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				categoryDO = (CategoryDO) view.getTag();
				String CategoryId = categoryDO.CatgId;
				String CatgName = categoryDO.CatgName;
				Intent intent = new Intent(context, ItemsActivity.class);
				intent.putExtra("CatgId", CategoryId);
				intent.putExtra("CatgName", CatgName);
				context.startActivity(intent);

				//Toast.makeText(context, "Row  Clicked", Toast.LENGTH_SHORT).show();
			}
		});

		viewHolder.ivCatImage.setTag(categoryDO);
		new FontType(context).applyfonttoView(viewHolder.txt_catg,FontType.ROBOTOBOLD);
//		Picasso.with(context).load(categoryDO.CatgImageUrl).placeholder(R.mipmap.loader_1).into(viewHolder.ivCatImage);
		imgLoader.getInstance().displayImage(categoryDO.CatgImageUrl, new BgViewAware(viewHolder.ivCatImage), dispImage);

		return convertView;
	}


	private class ViewHolder
	{
		TextView  txt_catg;
		ImageView ivCatImage;
	}



}
