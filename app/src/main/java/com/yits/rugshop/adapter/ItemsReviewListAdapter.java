package com.yits.rugshop.adapter;


import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Paint;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.yits.rugshop.R;
import com.yits.rugshop.listener.CartListListener;
import com.yits.rugshop.listener.FavouriteListListener;
import com.yits.rugshop.listener.SelectedItemListener;
import com.yits.rugshop.objects.ItemCartsDO;
import com.yits.rugshop.objects.ItemfavouriteDO;
import com.yits.rugshop.objects.ItemsDO;
import com.yits.rugshop.objects.SizeListDO;
import com.yits.rugshop.utilities.BgViewAware;
import com.yits.rugshop.utilities.CartDatabase;
import com.yits.rugshop.utilities.FontType;

import java.util.ArrayList;

public class ItemsReviewListAdapter extends BaseAdapter
{

	private Context context;
	private ArrayList<ItemsDO> arrItemsDO;
	ItemsDO itemsDO;
	ViewHolder viewHolder;
	LayoutInflater inflater;
	ImageLoaderConfiguration imgLoaderConf;
	DisplayImageOptions dispImage;
	ImageLoader imgLoader= ImageLoader.getInstance();
	FontType fonttype;
	CartListListener cartListListener;
	FavouriteListListener favouriteListListener;
	SelectedItemListener selectedItemListener;
	ArrayList<ItemCartsDO> itemCartList;
	ArrayList<ItemfavouriteDO> itemFavouriteList;
	CartDatabase cartDatabase;
	ItemfavouriteDO itemfavouriteDO;
	ItemCartsDO itemCartsDO;
	ItemsDO model;

	public ItemsReviewListAdapter(Context context, ArrayList<ItemsDO> arrItemsDO, ArrayList<ItemCartsDO> itemCartList, ArrayList<ItemfavouriteDO> itemFavouriteList)
	{
		this.context       = context;
		this.arrItemsDO    = arrItemsDO;
		this.itemCartList=itemCartList;
		this.itemFavouriteList=itemFavouriteList;
		inflater= (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		imgLoaderConf = new ImageLoaderConfiguration.Builder(context).build();
		imgLoader.init(imgLoaderConf);
		dispImage=new DisplayImageOptions.Builder()
				.showImageOnFail(R.mipmap.wool_rug)
				.showImageOnLoading(R.mipmap.wool_rug)
				.showImageForEmptyUri(R.mipmap.wool_rug)
				.cacheInMemory(true)
				.cacheOnDisk(true)
				.considerExifParams(true)
				.delayBeforeLoading(100)
				.imageScaleType(ImageScaleType.EXACTLY)
				.bitmapConfig(Bitmap.Config.RGB_565)
				.build();
	}

	@Override
	public int getCount()
	{

		return arrItemsDO.size();

	}

	@Override
	public Object getItem(int position)
	{
		return arrItemsDO.get(position);
	}

	@Override
	public long getItemId(int position)
	{
		return position;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent)
	{
		if(convertView==null)
		{
			convertView = inflater.inflate(R.layout.row_listitems, parent,false);
			viewHolder = new ViewHolder();
			viewHolder.txt_percentage 	    = (TextView)convertView.findViewById(R.id.txt_percentage);
			viewHolder.txt_productId 	    = (TextView)convertView.findViewById(R.id.txt_productId);
			viewHolder.txt_basePrice 	    = (TextView)convertView.findViewById(R.id.txt_basePrice);
			viewHolder.txt_discountedPrice 	    = (TextView)convertView.findViewById(R.id.txt_discountedPrice);
			viewHolder.chk_fav 	    = (CheckBox)convertView.findViewById(R.id.chk_fav);
			viewHolder.row_listitem 	    = (LinearLayout)convertView.findViewById(R.id.row_listitem);
			viewHolder.ll_itmimg 	    = (FrameLayout)convertView.findViewById(R.id.ll_itmimg);
			viewHolder.ll_border 	    = (LinearLayout)convertView.findViewById(R.id.ll_border);


			convertView.setTag(viewHolder);
		}
		else
		{
			viewHolder= (ViewHolder) convertView.getTag();
		}

		itemsDO = (ItemsDO) getItem(position);
		ViewGroup root = (ViewGroup) convertView.findViewById(R.id.row_listitem);
		fonttype = new FontType(context, root);


		viewHolder.txt_basePrice.setText("£"+itemsDO.ItemBasePrice);
		viewHolder.txt_basePrice.setPaintFlags(viewHolder.txt_basePrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
		viewHolder.txt_discountedPrice.setText("£" + itemsDO.ItemDsicountedPrice);
		viewHolder.txt_productId.setText(itemsDO.ProductID);
		viewHolder.txt_percentage.setText("Save " + itemsDO.ItemPerSaved);

		if(itemsDO.favIconChecked)
		{
			viewHolder.chk_fav.setChecked(itemsDO.favIconChecked);
		}
		else
		{
			viewHolder.chk_fav.setChecked(itemsDO.favIconChecked);
		}


		if (itemsDO.isPositionSelected())
		{
			viewHolder.ll_border.setBackground(ContextCompat.getDrawable(context, R.drawable.edt_selection));
		}
		else
		{
			viewHolder.ll_border.setBackground(ContextCompat.getDrawable(context, R.drawable.edt_unselection));
		}

		viewHolder.ll_itmimg.setTag(itemsDO);
		viewHolder.ll_itmimg.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				model = (ItemsDO) view.getTag();
				for(int i=0;i<arrItemsDO.size();i++)
				{
					arrItemsDO.get(i).setPositionSelected(false);

				}
				model.setPositionSelected(true);
				Log.e(ItemsReviewListAdapter.class.getSimpleName(),"Position is ="+position);
				CartDatabase.updatePositionData(position);
				notifyDataSetChanged();
				selectedItemListener.getSelectedListItemListener(model);

			}
		});



		viewHolder.chk_fav.setTag(itemsDO);
		viewHolder.chk_fav.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				model = (ItemsDO) view.getTag();
				itemfavouriteDO = new ItemfavouriteDO();
				if (model.favIconChecked) {
					removeItemsFromFavourites();
					notifyDataSetChanged();
				} else {
					addItemsToFavouriteFunction();
					notifyDataSetChanged();
				}
			}
		});

		viewHolder.ll_itmimg.setTag(itemsDO);
		imgLoader.getInstance().displayImage(itemsDO.ItemBigImgUrl, new BgViewAware(viewHolder.ll_itmimg), dispImage);

		return convertView;
	}

	private class ViewHolder
	{
		TextView  txt_percentage,txt_productId,txt_basePrice,txt_discountedPrice;
		CheckBox chk_fav;
		LinearLayout row_listitem,ll_border;
		FrameLayout ll_itmimg;
	}


	private void removeItemsFromCartList() {

		model.cartIconChecked=false;
		if(itemCartList.size()>0)
		{
			for(int i=0;i<itemCartList.size();i++)
			{
				itemCartsDO=itemCartList.get(i);

				if(itemCartsDO.ItemId.equalsIgnoreCase(model.ItemId))
				{
					itemCartList.remove(i);
					CartDatabase.deletecartlist(itemCartsDO);
					CartDatabase.deletecolorlist(itemCartsDO);
					CartDatabase.deletesizelist(itemCartsDO);
				}
			}
			notifyDataSetChanged();
			cartListListener.getCartList(model);
		}
	}

	private void addItemsToCartList()
	{
		model.cartIconChecked=true;
		itemCartsDO.ItemId=model.ItemId;
		itemCartsDO.ItemProductId=model.ProductID;
		itemCartsDO.categoryId =model.CategoryId;
		itemCartsDO.ItemName=model.ItemName;
		itemCartsDO.ItemQuantity=1;
		itemCartsDO.ItemBasePrice=model.itemSizeList.get(0).BasePrice;
		itemCartsDO.ItemDsicountedPrice = model.itemSizeList.get(0).DiscountedPrice;

		itemCartsDO.ItemSize = model.itemSizeList.get(0).ItemSize;
		itemCartsDO.itemColorList = model.itemColorList;
		itemCartsDO.itemSizeList=model.itemSizeList;
		itemCartList.add(itemCartsDO);
		notifyDataSetChanged();
		cartDatabase.addCartData(new ItemCartsDO(model.ItemId, model.ProductID, model.ItemName, 1, model.itemSizeList.get(0).BasePrice, model.itemSizeList.get(0).DiscountedPrice, model.itemSizeList.get(0).FinalPrice, model.itemSizeList.get(0).ItemSize,model.itemColorList.get(0),model.ItemPattern,model.ItemMaterial,model.ItemSmallImgUrl,model.ItemMediumImgUrl,model.ItemBigImgUrl,1,model.ItemRating,model.ItemPerSaved,model.catogiryname));
		if(model.itemColorList.size()>0)
		{
			cartDatabase.addColorData(model.ItemId,model.itemColorList);
		}
		for(int i=0;i<model.itemSizeList.size();i++)
		{
			cartDatabase.addSizeData(new SizeListDO(model.ItemId,model.itemSizeList.get(i).ItemSize,model.itemSizeList.get(i).BasePrice,model.itemSizeList.get(i).DiscountedPrice,model.itemSizeList.get(i).FinalPrice));
		}

		cartListListener.getCartList(model);

	}

	private void removeItemsFromFavourites() {

		model.favIconChecked=false;
		if(itemFavouriteList.size()>0)
		{
			for(int i=0;i<itemFavouriteList.size();i++)
			{
				itemfavouriteDO=itemFavouriteList.get(i);

				if(itemfavouriteDO.ItemId.equalsIgnoreCase(model.ItemId))
				{
					itemFavouriteList.remove(i);
				}
			}
			notifyDataSetChanged();
			favouriteListListener.getFavouriteList(itemFavouriteList);
		}
	}

	private void addItemsToFavouriteFunction()
	{
		model.favIconChecked=true;
		itemfavouriteDO.ItemId=model.ItemId;
		itemfavouriteDO.ItemProductId=model.ProductID;
		itemfavouriteDO.ItemName=model.ItemName;
		itemfavouriteDO.ItemQuantity=1;
		itemfavouriteDO.ItemBasePrice=model.itemSizeList.get(0).BasePrice;
		itemfavouriteDO.ItemDsicountedPrice=model.itemSizeList.get(0).DiscountedPrice;
		itemfavouriteDO.ItemSize=model.itemSizeList.get(0).ItemSize;
		itemFavouriteList.add(itemfavouriteDO);
		notifyDataSetChanged();
		favouriteListListener.getFavouriteList(itemFavouriteList);
	}



	public void registerCartListener(CartListListener cartListListener)
	{
		this.cartListListener= cartListListener;
	}

	public void registerFavouriteListener(FavouriteListListener favouriteListListener)
	{
		this.favouriteListListener= favouriteListListener;
	}

	public void registerSelectedListItemListener(SelectedItemListener selectedItemListener)
	{
		this.selectedItemListener= selectedItemListener;
	}
}
