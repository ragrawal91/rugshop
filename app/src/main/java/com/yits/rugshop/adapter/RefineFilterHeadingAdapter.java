package com.yits.rugshop.adapter;


import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.yits.rugshop.R;
import com.yits.rugshop.listener.FilterClear;
import com.yits.rugshop.objects.FilterHeadingDO;
import com.yits.rugshop.utilities.FontType;

import java.util.ArrayList;

public class RefineFilterHeadingAdapter extends BaseAdapter {

    private Context context;
    ArrayList<FilterHeadingDO> filterheadings;
    FontType fonttype;
    FilterClear filterClear;

    public RefineFilterHeadingAdapter(Context mContext, ArrayList<FilterHeadingDO> filterheadings, FilterClear filterClear) {
        this.context = mContext;
        this.filterheadings = filterheadings;
        this.filterClear=filterClear;
    }

    @Override
    public int getCount() {

        return filterheadings.size();

    }

    @Override
    public Object getItem(int position) {
        return filterheadings.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @SuppressLint("SetTextI18n")
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder = null;
        if (convertView == null) {
            convertView             = LayoutInflater.from(context).inflate(R.layout.refine_filter_parent_cell, null);
            viewHolder              = new ViewHolder();
            viewHolder.tvParentName = (TextView) convertView.findViewById(R.id.tvParentName);
            convertView.setTag(viewHolder);
        } else {
            viewHolder              = (ViewHolder) convertView.getTag();
        }

        FilterHeadingDO filterHeadingDO = (FilterHeadingDO) getItem(position);

        ViewGroup root = (ViewGroup) convertView.findViewById(R.id.ll_filter);
        fonttype = new FontType(context, root);


        viewHolder.tvParentName.setText(filterHeadingDO.filterName);
        viewHolder.tvParentName.setTag(filterHeadingDO);
//        convertView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v)
//            {
//                filterClear.clearFilter((FilterHeadingDO) viewHolder.tvParentName.getTag())                                                                          );
//            }
//        });
        return convertView;
    }


    private class ViewHolder {
        TextView tvParentName;
    }
}
