package com.yits.rugshop.adapter;


import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.yits.rugshop.R;
import com.yits.rugshop.listener.FilterClear;
import com.yits.rugshop.objects.FilterHeadingDO;
import com.yits.rugshop.utilities.FontType;

import java.util.ArrayList;

public class FilterHeadingAdapter extends BaseAdapter {

    private Context context;
    ArrayList<FilterHeadingDO> filterheadings;
    FilterHeadingDO filterHeadingDO;
    ViewHolder viewHolder;
    LayoutInflater inflater;
    FontType fonttype;
    FilterClear filterClear;

    public FilterHeadingAdapter(Context mContext, ArrayList<FilterHeadingDO> filterheadings,FilterClear filterClear) {
        this.context = mContext;
        this.filterheadings = filterheadings;
        this.filterClear=filterClear;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    @Override
    public int getCount() {

        return filterheadings.size();

    }

    @Override
    public Object getItem(int position) {
        return filterheadings.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @SuppressLint("SetTextI18n")
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if (convertView == null) {

            convertView = inflater.inflate(R.layout.row_filterheading, parent, false);
            viewHolder = new ViewHolder();
            viewHolder.ll_bg_filterhead= (LinearLayout) convertView.findViewById(R.id.ll_bg_filterhead);
            viewHolder.tv_clear = (TextView) convertView.findViewById(R.id.tv_clear);
           // viewHolder.img_selected= (ImageView) convertView.findViewById(R.id.img_selected);
            viewHolder.txt_filters = (TextView) convertView.findViewById(R.id.txt_filters);
            viewHolder.txt_count = (TextView) convertView.findViewById(R.id.txt_count);

            viewHolder.ll_filter = (LinearLayout) convertView.findViewById(R.id.ll_filter);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        filterHeadingDO = (FilterHeadingDO) getItem(position);

        ViewGroup root = (ViewGroup) convertView.findViewById(R.id.ll_filter);
        fonttype = new FontType(context, root);

       // viewHolder.img_selected.setVisibility(View.GONE);

        viewHolder.txt_filters.setText(filterHeadingDO.filterName);
        viewHolder.txt_count.setText("" + filterHeadingDO.Counter);
        viewHolder.tv_clear.setTag(filterHeadingDO);
        viewHolder.tv_clear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                filterClear.clearFilter((FilterHeadingDO) v.getTag()                                                                           );
            }
        });
        if (filterHeadingDO.selected)
        {
           // viewHolder.img_selected.setVisibility(View.VISIBLE);
            viewHolder.ll_bg_filterhead.setBackgroundResource(R.color.light_grey_divider);

            new FontType(context).applyfonttoView(viewHolder.txt_filters,FontType.ROBOTOREGULAR);
        }
        else
        {
           // viewHolder.img_selected.setVisibility(View.GONE);
            viewHolder.ll_bg_filterhead.setBackgroundResource(R.color.white);
            new FontType(context).applyfonttoView(viewHolder.txt_filters,FontType.ROBOTOLIGHT);


        }

        return convertView;
    }


    private class ViewHolder {
        TextView txt_filters, txt_count, tv_clear;
        LinearLayout ll_filter,ll_bg_filterhead;
        ImageView img_selected;
    }
}
