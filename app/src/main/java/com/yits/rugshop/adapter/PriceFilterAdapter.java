package com.yits.rugshop.adapter;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;

import com.yits.rugshop.R;
import com.yits.rugshop.listener.SelectedFilterListener;
import com.yits.rugshop.objects.FilterMetadataDO;
import com.yits.rugshop.objects.FilternameAndIDDo;
import com.yits.rugshop.utilities.FontType;

import java.util.ArrayList;

public class PriceFilterAdapter extends BaseAdapter
{

	private Context context;
	private ArrayList<FilternameAndIDDo> arrFilterMetdataDO;
	FilterMetadataDO filterMetadataDO;
	ViewHolder viewHolder;
	LayoutInflater inflater;
	ViewGroup viewGroup;
	FontType fonttype;
	ArrayList<FilternameAndIDDo> SelectedPriceList;
	SelectedFilterListener selectedFilterListener;

	public PriceFilterAdapter(Context context, ArrayList<FilternameAndIDDo> arrFilterMetdataDO)
	{
		this.context       = context;
		SelectedPriceList=new ArrayList<FilternameAndIDDo>();
		this.arrFilterMetdataDO    = arrFilterMetdataDO;
		inflater= (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	@Override
	public int getCount()
	{

		return arrFilterMetdataDO.size();

	}

	@Override
	public Object getItem(int position)
	{
		return arrFilterMetdataDO.get(position);
	}

	@Override
	public long getItemId(int position)
	{
		return position;
	}

	public ViewGroup getviewgroup()
	{
		return this.viewGroup;
	}


	@Override
	public View getView(final int position, View convertView, ViewGroup parent)
	{
		if(convertView==null)
		{
			convertView = inflater.inflate(R.layout.row_sizes, parent,false);
			viewHolder = new ViewHolder();
			viewHolder.chk_sizeinfo= (CheckBox)convertView.findViewById(R.id.chk_sizeinfo);
			viewHolder.row_sizes= (LinearLayout) convertView.findViewById(R.id.row_sizes);
			convertView.setTag(viewHolder);
		}
		else
		{
			viewHolder= (ViewHolder) convertView.getTag();
		}

		this.viewGroup=viewHolder.row_sizes;


		String Name= (String) getItem(position);
		ViewGroup root = (ViewGroup) convertView.findViewById(R.id.row_sizes);
		fonttype = new FontType(context, root);

		viewHolder.chk_sizeinfo.setText(Name);

		viewHolder.chk_sizeinfo.setTag(Name);
		viewHolder.chk_sizeinfo.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
		{
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
			{
				FilternameAndIDDo model= (FilternameAndIDDo) buttonView.getTag();
				if(isChecked)
				{
					SelectedPriceList.add(model);
				}
				else
				{
					SelectedPriceList.remove(model);
				}

					selectedFilterListener.getSelectedPriceFilterListener(model);

				//Toast.makeText(context,"SelectedSizeList  Size is ="+SelectedSizeList.size(), Toast.LENGTH_SHORT).show();
			}
		});
		return convertView;
	}


	private class ViewHolder
	{
		CheckBox chk_sizeinfo;

		LinearLayout row_sizes;
	}

	public void registerSelectedPriceFilterListener(SelectedFilterListener selectedFilterListener)
	{
		this.selectedFilterListener = selectedFilterListener;
	}

}
