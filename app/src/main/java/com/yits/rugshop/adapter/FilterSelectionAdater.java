package com.yits.rugshop.adapter;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.yits.rugshop.R;
import com.yits.rugshop.listener.FilterCloseListener;
import com.yits.rugshop.objects.FilternameAndIDDo;

import java.util.ArrayList;

/**
 * Created by sai on 8/11/2016.
 */
public class FilterSelectionAdater extends BaseAdapter {
    ArrayList<FilternameAndIDDo> filternameAndIDDos;
    Context context;
    private FilterCloseListener filterCloseListener;

    public FilterSelectionAdater(ArrayList<FilternameAndIDDo> filternameAndIDDos, Context context) {
        this.context = context;
        this.filternameAndIDDos = filternameAndIDDos;

    }

    @Override
    public int getCount() {
        return filternameAndIDDos!=null?filternameAndIDDos.size():0;
    }

    @Override
    public FilternameAndIDDo getItem(int position) {
        return filternameAndIDDos.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        final FilternameAndIDDo filternameAndIDDo = getItem(position);
        ViewHolder viewHolder = null;
        if (convertView == null) {
<<<<<<< HEAD
            viewHolder                  = new ViewHolder();
            convertView                 = LayoutInflater.from(context).inflate(R.layout.row_filetrcells, null);
            viewHolder.tv_filter_name   = (TextView) convertView.findViewById(R.id.tv_filter_name);
            viewHolder.img_cancel       = (ImageView) convertView.findViewById(R.id.img_cancel);
=======
            convertView = layoutInflater.inflate(R.layout.row_filetrcells, null);
            AbsListView.LayoutParams layoutParams = new AbsListView.LayoutParams(200, ViewGroup.LayoutParams.WRAP_CONTENT);

            convertView.setLayoutParams(layoutParams);
            viewHolder.tv_filter_name = (TextView) convertView.findViewById(R.id.tv_filter_name);
            viewHolder.img_cancel = (ImageView) convertView.findViewById(R.id.img_cancel);

>>>>>>> f76f5d1fbfcca738becd105dc6d4a8a2bbc91a40
            convertView.setTag(viewHolder);
        }
        else {
            viewHolder = (ViewHolder) convertView.getTag();

        }

        viewHolder.img_cancel.setTag(filternameAndIDDo);
        viewHolder.img_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FilternameAndIDDo filternameAndID = (FilternameAndIDDo) v.getTag();
                filterCloseListener.filterselctionclose(filternameAndIDDo);

            }
        });
        viewHolder.tv_filter_name.setText(filternameAndIDDo.name);
        return convertView;
    }

    public void registerListener(FilterCloseListener filterCloseListener) {
        this.filterCloseListener = filterCloseListener;
    }

    private class ViewHolder {
        TextView tv_filter_name;
        ImageView img_cancel;
    }
}
