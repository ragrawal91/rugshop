package com.yits.rugshop.adapter;


import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.yits.rugshop.R;
import com.yits.rugshop.activity.ItemsActivity;
import com.yits.rugshop.objects.ColorsDO;
import com.yits.rugshop.utilities.BgViewAware;
import com.yits.rugshop.utilities.FontType;

import java.util.ArrayList;

public class ColorsAdapter extends BaseAdapter
{

	private Context context;
	private ArrayList<ColorsDO> arrColors;
	ColorsDO colorsDO;
	ViewHolder viewHolder;
	LayoutInflater inflater;
	ImageLoaderConfiguration imgLoaderConf;
	DisplayImageOptions dispImage;
	ImageLoader imgLoader= ImageLoader.getInstance();
	FontType fonttype;

	public ColorsAdapter(Context context, ArrayList<ColorsDO> arrColors)
	{
		this.context       = context;
		this.arrColors    = arrColors;
		inflater= (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		imgLoaderConf = new ImageLoaderConfiguration.Builder(context).build();
		imgLoader.init(imgLoaderConf);
		dispImage=new DisplayImageOptions.Builder()
				.showImageOnFail(R.mipmap.wool_rug)
				.showImageOnLoading(R.mipmap.wool_rug)
				.showImageForEmptyUri(R.mipmap.wool_rug)
				.cacheInMemory(true)
				.cacheOnDisk(true)
				.considerExifParams(true)
				.delayBeforeLoading(100)
				.imageScaleType(ImageScaleType.EXACTLY)
				.bitmapConfig(Bitmap.Config.RGB_565)
				.build();

	}

	@Override
	public int getCount()
	{

		return arrColors.size();

	}

	@Override
	public Object getItem(int position)
	{
		return arrColors.get(position);
	}

	@Override
	public long getItemId(int position)
	{
		return position;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent)
	{
		if(convertView==null)
		{
			convertView = inflater.inflate(R.layout.row_catg, parent,false);
			viewHolder = new ViewHolder();
			viewHolder = new ViewHolder();
			viewHolder.txt_catg 	    = (TextView)convertView.findViewById(R.id.txt_catg);
			viewHolder.ivCatImage      = (ImageView) convertView.findViewById(R.id.ivCatImage);
			convertView.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) convertView.getTag();
		}

		colorsDO = (ColorsDO) getItem(position);

//		ViewGroup root = (ViewGroup) convertView.findViewById(R.id.row_catg);
//		fonttype = new FontType(context, root);

		viewHolder.txt_catg.setText(colorsDO.ColorName);

		viewHolder.ivCatImage.setTag(colorsDO);
		viewHolder.ivCatImage.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				colorsDO = (ColorsDO) view.getTag();
				String ColorId = colorsDO.ColorId;
				String ColorName = colorsDO.ColorName;
				Intent intent = new Intent(context, ItemsActivity.class);
				intent.putExtra("CatgId", ColorId);
				intent.putExtra("CatgName", ColorName);
				context.startActivity(intent);

				//Toast.makeText(context, "Row  Clicked", Toast.LENGTH_SHORT).show();
			}
		});

		viewHolder.ivCatImage.setTag(colorsDO);

		new FontType(context).applyfonttoView(viewHolder.txt_catg,FontType.ROBOTOMEDIUM);
//		Picasso.with(context).load(colorsDO.ColorImageUrl).resize(420, 420).placeholder(R.mipmap.loginbg).into(viewHolder.ivCatImage);
		imgLoader.getInstance().displayImage(colorsDO.ColorImageUrl, new BgViewAware(viewHolder.ivCatImage), dispImage);

		return convertView;
	}


	private class ViewHolder
	{
		TextView  txt_catg;
		ImageView ivCatImage;
	}



}
