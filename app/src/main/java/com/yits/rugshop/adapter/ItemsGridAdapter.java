package com.yits.rugshop.adapter;


import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.jaredrummler.materialspinner.MaterialSpinner;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.yits.rugshop.R;
import com.yits.rugshop.listener.CartListListener;
import com.yits.rugshop.listener.FavouriteListListener;
import com.yits.rugshop.listener.SelectedItemListener;
import com.yits.rugshop.objects.ItemCartsDO;
import com.yits.rugshop.objects.ItemfavouriteDO;
import com.yits.rugshop.objects.ItemsDO;
import com.yits.rugshop.objects.SizeListDO;
import com.yits.rugshop.utilities.BgViewAware;
import com.yits.rugshop.utilities.CartDatabase;
import com.yits.rugshop.utilities.FontType;
import com.yits.rugshop.utilities.LogUtils;

import java.text.DecimalFormat;
import java.util.ArrayList;

public class ItemsGridAdapter extends BaseAdapter {
    private Context context;
    private ArrayList<ItemsDO> arrItemsDO;
    ImageLoaderConfiguration imgLoaderConf;
    DisplayImageOptions dispImage;
    ImageLoader imgLoader = ImageLoader.getInstance();
    FontType fonttype;
    CartListListener cartListListener;
    FavouriteListListener favouriteListListener;
    SelectedItemListener selectedItemListener;
    ArrayList<ItemCartsDO> itemCartList;
    ArrayList<ItemfavouriteDO> itemFavouriteList;
    CartDatabase cartDatabase;
    ItemfavouriteDO itemfavouriteDO;
    ItemCartsDO itemCartsDO;
    DecimalFormat df = new DecimalFormat("#.00");

    public ItemsGridAdapter(Context context, ArrayList<ItemsDO> arrItemsDO, ArrayList<ItemCartsDO> itemCartList, ArrayList<ItemfavouriteDO> itemFavouriteList) {
        this.context = context;
        this.arrItemsDO = arrItemsDO;
        this.itemCartList = itemCartList;
        this.itemFavouriteList = itemFavouriteList;
        imgLoaderConf = new ImageLoaderConfiguration.Builder(context).build();
        imgLoader.init(imgLoaderConf);
        dispImage = new DisplayImageOptions.Builder()
                .showImageOnFail(R.mipmap.wool_rug)
                .showImageOnLoading(R.mipmap.wool_rug)
                .showImageForEmptyUri(R.mipmap.wool_rug)
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .delayBeforeLoading(100)
                .imageScaleType(ImageScaleType.EXACTLY)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .build();
    }

    public void refresh(ArrayList<ItemsDO> arrItemsDO){
        this.arrItemsDO = arrItemsDO;
        notifyDataSetChanged();
    }
    @Override
    public int getCount() {
        return arrItemsDO.size();
    }

    @Override
    public Object getItem(int position) {
        return arrItemsDO.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder = null;
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.row_griditems, parent, false);
            viewHolder = new ViewHolder();
            viewHolder.tv_material_label = (TextView) convertView.findViewById(R.id.tv_material_label);
            viewHolder.tv_productId_label = (TextView) convertView.findViewById(R.id.tv_productId_label);
            viewHolder.txt_material = (TextView) convertView.findViewById(R.id.txt_material);
            viewHolder.tv_price_label = (TextView) convertView.findViewById(R.id.tv_price_label);
            viewHolder.txt_productname = (TextView) convertView.findViewById(R.id.txt_productname);


            viewHolder.tv_size_label = (TextView) convertView.findViewById(R.id.tv_size_label);
            viewHolder.txt_productname = (TextView) convertView.findViewById(R.id.txt_productname);
            viewHolder.txt_percentage = (TextView) convertView.findViewById(R.id.txt_percentage);
            viewHolder.txt_productId = (TextView) convertView.findViewById(R.id.txt_productId);
            viewHolder.txt_basePrice = (TextView) convertView.findViewById(R.id.txt_basePrice);
            viewHolder.txt_discountedPrice = (TextView) convertView.findViewById(R.id.txt_discountedPrice);
            viewHolder.chk_fav = (CheckBox) convertView.findViewById(R.id.chk_fav);
            viewHolder.spn_size = (MaterialSpinner) convertView.findViewById(R.id.spn_size);
            viewHolder.btn_buy = (Button) convertView.findViewById(R.id.btn_buy);
            viewHolder.btn_view_item = (TextView) convertView.findViewById(R.id.btn_view_item);
            viewHolder.row_griditem = (LinearLayout) convertView.findViewById(R.id.row_griditem);
            viewHolder.ll_itmimg = (FrameLayout) convertView.findViewById(R.id.ll_itmimg);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        final ItemsDO itemsDO = (ItemsDO) getItem(position);
        ViewGroup root = (ViewGroup) convertView.findViewById(R.id.row_griditem);
        fonttype = new FontType(context, root);
        //viewHolder.txt_item.setShadowLayer(2, 2, 2, Color.WHITE);
        viewHolder.txt_discountedPrice.setPaintFlags(viewHolder.txt_discountedPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        viewHolder.txt_productId.setText(itemsDO.ProductID);
        viewHolder.txt_percentage.setText("Save " + itemsDO.ItemPerSaved);
        viewHolder.txt_productname.setText(itemsDO.ItemName);
        viewHolder.txt_material.setText(itemsDO.ItemMaterial);
        if (itemsDO.itemSizeList.get(0).savingstate) {

            viewHolder.txt_basePrice.setText("£" + df.format(Double.parseDouble(itemsDO.itemSizeList.get(itemsDO.selectedSizePosition).BasePrice)));
            viewHolder.txt_discountedPrice.setText("£" + df.format(Double.parseDouble(itemsDO.itemSizeList.get(itemsDO.selectedSizePosition).BasePrice)));

            viewHolder.txt_percentage.setVisibility(View.VISIBLE);
            viewHolder.txt_basePrice.setVisibility(View.VISIBLE);
            viewHolder.txt_percentage.setText("save " + itemsDO.itemSizeList.get(itemsDO.selectedSizePosition).savings + " %");

        } else {
            viewHolder.txt_basePrice.setText("£" + df.format(Double.parseDouble(itemsDO.itemSizeList.get(itemsDO.selectedSizePosition).BasePrice)));
            viewHolder.txt_discountedPrice.setText("£" + df.format(Double.parseDouble(itemsDO.itemSizeList.get(itemsDO.selectedSizePosition).FinalPrice)));

            viewHolder.txt_percentage.setVisibility(View.VISIBLE);
            viewHolder.txt_basePrice.setVisibility(View.VISIBLE);
        }

        if (itemsDO.favIconChecked) {
            viewHolder.chk_fav.setChecked(itemsDO.favIconChecked);
        } else {
            viewHolder.chk_fav.setChecked(itemsDO.favIconChecked);
        }

        viewHolder.btn_view_item.setTag(itemsDO);
//        SizeSpinnerAdapter sizeSpinnerAdapter = new SizeSpinnerAdapter(itemsDO.itemSizeList, context);
        final ArrayList<String> sizesStrings = new ArrayList<>();

        for (int i = 0; i < itemsDO.itemSizeList.size(); i++) {
            sizesStrings.add(itemsDO.itemSizeList.get(i).ItemSize);
        }
        // viewHolder.spn_size.setAdapter(sizeSpinnerAdapter);
        //viewHolder.spn_size.setTag(itemsDO);
        viewHolder.spn_size.setItems(sizesStrings);
        viewHolder.spn_size.setTag(itemsDO);
        final ViewHolder dummyHolder = viewHolder;
        viewHolder.spn_size.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
            @Override
            public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {

                dummyHolder.txt_basePrice.setVisibility(View.VISIBLE);
                dummyHolder.txt_discountedPrice.setVisibility(View.VISIBLE);
                dummyHolder.txt_basePrice.setText(""+itemsDO.itemSizeList.get(position).BasePrice+"");
                dummyHolder.txt_discountedPrice.setText(""+itemsDO.itemSizeList.get(position).FinalPrice+"");

                LogUtils.error("Prices : ", ""+itemsDO.itemSizeList.get(position).BasePrice+", "+itemsDO.itemSizeList.get(position).FinalPrice+", "+itemsDO.itemSizeList.get(position).DiscountedPrice);
                dummyHolder.txt_discountedPrice.setPaintFlags(dummyHolder.txt_discountedPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            }
        });

//       viewHolder.spn_size.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {
//                for (int i = 0; i < sizesStrings.size(); i++) {
//                    if (sizesStrings.get(i).equalsIgnoreCase(String.valueOf(item))) {
//                        ItemsDO itemsDO = (ItemsDO) viewHolder.spn_size.getTag();
//                        itemsDO.selectedSizePosition = i;
//                        notifyDataSetChanged();
//                    }
//                }
//            }
//       });


      /*  viewHolder.spn_size.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                *//*ItemsSizeDO itemsSizeDO= (ItemsSizeDO) parent.getItemAtPosition(position);
                for (int i = 0; i < sizesStrings.size(); i++) {
                    if (sizesStrings.get(i).equalsIgnoreCase(String.valueOf(itemsSizeDO.ItemSize))) {
                        ItemsDO itemsDO = (ItemsDO) viewHolder.spn_size.getTag();
                        itemsDO.selectedSizePosition = i;
                        notifyDataSetChanged();
                    }
                }*//*

                ItemsSizeDO itemsSizeDO = (ItemsSizeDO) parent.getItemAtPosition(position);
                for (int i = 0; i < sizesStrings.size(); i++) {
                    if (sizesStrings.get(i).equalsIgnoreCase(String.valueOf(itemsSizeDO.ItemSize))) {
                        ItemsDO itemsDO = (ItemsDO) viewHolder.spn_size.getTag();
                        itemsDO.selectedSizePosition = i;
                        // notifyDataSetChanged();


                    }
                }

                Toast.makeText(context, itemsSizeDO.ItemSize, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });*/
        viewHolder.btn_view_item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ItemsDO model = (ItemsDO) view.getTag();
                String ItemId = model.ItemId;

                for (int i = 0; i < arrItemsDO.size(); i++) {
                    arrItemsDO.get(i).setPositionSelected(false);

                }
                model.setPositionSelected(true);
                notifyDataSetChanged();
                selectedItemListener.getSelectedGridItemListener(model);
            }
        });
        viewHolder.ll_itmimg.setTag(itemsDO);
        viewHolder.ll_itmimg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dummyHolder.btn_view_item.performClick();
            }
        });

        viewHolder.chk_fav.setTag(itemsDO);
        viewHolder.chk_fav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ItemsDO model = (ItemsDO) view.getTag();
                itemfavouriteDO = new ItemfavouriteDO();
                if (model.favIconChecked)
                {
                    removeItemsFromFavourites(model);
                    notifyDataSetChanged();
                }
                else
                {
                    addItemsToFavouriteFunction(model);
                    notifyDataSetChanged();
                }
            }
        });

        viewHolder.btn_buy.setTag(itemsDO);
        viewHolder.btn_buy.setVisibility(View.GONE);
        viewHolder.btn_buy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ItemsDO itemsDO = (ItemsDO) dummyHolder.btn_buy.getTag();
                selectedItemListener.getClickBuynow(itemsDO);
            }
        });
        viewHolder.ll_itmimg.setTag(itemsDO);
        imgLoader.getInstance().displayImage(itemsDO.ItemBigImgUrl, new BgViewAware(viewHolder.ll_itmimg), dispImage);

        new FontType(context).applyfonttoView(viewHolder.tv_size_label, FontType.ROBOTOREGULAR);
        new FontType(context).applyfonttoView(viewHolder.txt_productname, FontType.ROBOTOREGULAR);
        new FontType(context).applyfonttoView(viewHolder.tv_material_label, FontType.ROBOTOREGULAR);
        new FontType(context).applyfonttoView(viewHolder.tv_productId_label, FontType.ROBOTOREGULAR);

        new FontType(context).applyfonttoView(viewHolder.spn_size, FontType.ROBOTOREGULAR);

        new FontType(context).applyfonttoView(viewHolder.txt_productId, FontType.ROBOTOLIGHT);
        new FontType(context).applyfonttoView(viewHolder.txt_material, FontType.ROBOTOLIGHT);

        new FontType(context).applyfonttoView(viewHolder.tv_price_label, FontType.ROBOTOREGULAR);
        new FontType(context).applyfonttoView(viewHolder.txt_basePrice, FontType.ROBOTOREGULAR);
        new FontType(context).applyfonttoView(viewHolder.txt_discountedPrice, FontType.ROBOTOREGULAR);
        new FontType(context).applyfonttoView(viewHolder.txt_percentage, FontType.ROBOTOREGULAR);

        new FontType(context).applyfonttoView(viewHolder.btn_view_item, FontType.ROBOTOREGULAR);


        return convertView;
    }

    private void removeItemsFromCartList(ItemsDO model) {
        model.cartIconChecked = false;
        if (itemCartList.size() > 0) {
            for (int i = 0; i < itemCartList.size(); i++) {
                itemCartsDO = itemCartList.get(i);

                if (itemCartsDO.ItemId.equalsIgnoreCase(model.ItemId)) {
                    itemCartList.remove(i);
                    CartDatabase.deletecartlist(itemCartsDO);
                    CartDatabase.deletecolorlist(itemCartsDO);
                    CartDatabase.deletesizelist(itemCartsDO);
                }
            }
            notifyDataSetChanged();
            cartListListener.getCartList(model);
        }
    }

    private void addItemsToCartList(ItemsDO model) {
        model.cartIconChecked = true;
        itemCartsDO.ItemId = model.ItemId;
        itemCartsDO.ItemProductId = model.ProductID;
        itemCartsDO.categoryId = model.CategoryId;
        itemCartsDO.ItemName = model.ItemName;
        itemCartsDO.ItemQuantity = 1;
        itemCartsDO.ItemBasePrice = model.itemSizeList.get(0).BasePrice;
        itemCartsDO.ItemDsicountedPrice = model.itemSizeList.get(0).DiscountedPrice;
        itemCartsDO.ItemFinalPrice = model.itemSizeList.get(0).FinalPrice;
        itemCartsDO.ItemSize = model.itemSizeList.get(0).ItemSize;
        itemCartsDO.itemColorList = model.itemColorList;
        itemCartsDO.itemSizeList = model.itemSizeList;
        itemCartList.add(itemCartsDO);
        notifyDataSetChanged();
        cartDatabase.addCartData(new ItemCartsDO(model.ItemId, model.ProductID, model.ItemName, 1, model.itemSizeList.get(0).BasePrice, model.itemSizeList.get(0).DiscountedPrice, model.itemSizeList.get(0).FinalPrice, model.itemSizeList.get(0).ItemSize, model.itemColorList.get(0), model.ItemPattern, model.ItemMaterial, model.ItemSmallImgUrl, model.ItemMediumImgUrl, model.ItemBigImgUrl, 1, model.ItemRating, model.ItemPerSaved, model.catogiryname));
        if (model.itemColorList.size() > 0) {
            cartDatabase.addColorData(model.ItemId, model.itemColorList);
        }
        for (int i = 0; i < model.itemSizeList.size(); i++) {
            cartDatabase.addSizeData(new SizeListDO(model.ItemId, model.itemSizeList.get(i).ItemSize, model.itemSizeList.get(i).BasePrice, model.itemSizeList.get(i).DiscountedPrice, model.itemSizeList.get(i).FinalPrice));
        }

        cartListListener.getCartList(model);
    }

    private void removeItemsFromFavourites(ItemsDO model) {
        model.favIconChecked = false;
        if (itemFavouriteList.size() > 0) {
            for (int i = 0; i < itemFavouriteList.size(); i++)
            {
                itemfavouriteDO = itemFavouriteList.get(i);
                if (itemfavouriteDO.ItemId.equalsIgnoreCase(model.ItemId))
                {
                    itemFavouriteList.remove(i);
                }
            }

            cartDatabase.deleteFavouritelist(model);
            if (model.itemColorList.size() > 0) {
                cartDatabase.addFavouriteColorData(model.ItemId, model.itemColorList);
            }
            for (int i = 0; i < model.itemSizeList.size(); i++) {
                cartDatabase.addFavouriteSizeData(new SizeListDO(model.ItemId, model.itemSizeList.get(i).ItemSize, model.itemSizeList.get(i).BasePrice, model.itemSizeList.get(i).DiscountedPrice, model.itemSizeList.get(i).FinalPrice));
            }

            favouriteListListener.getFavouriteList(itemFavouriteList);
            notifyDataSetChanged();
        }
    }

    private void addItemsToFavouriteFunction(ItemsDO model) {
        model.favIconChecked = true;
        itemfavouriteDO.ItemId = model.ItemId;
        itemfavouriteDO.ItemProductId = model.ProductID;
        itemfavouriteDO.ItemName = model.ItemName;
        itemfavouriteDO.ItemQuantity = 1;
        itemfavouriteDO.ItemBasePrice = model.itemSizeList.get(0).BasePrice;
        itemfavouriteDO.ItemDsicountedPrice = model.itemSizeList.get(0).DiscountedPrice;
        itemfavouriteDO.ItemSize = model.itemSizeList.get(0).ItemSize;
        itemFavouriteList.add(itemfavouriteDO);
        notifyDataSetChanged();
        cartDatabase.addFavouriteListData(new ItemCartsDO(model.ItemId, model.ProductID, model.ItemName, 1, model.itemSizeList.get(0).BasePrice, model.itemSizeList.get(0).DiscountedPrice, model.itemSizeList.get(0).FinalPrice, model.itemSizeList.get(0).ItemSize, model.itemColorList.get(0), model.ItemPattern, model.ItemMaterial, model.ItemSmallImgUrl, model.ItemMediumImgUrl, model.ItemBigImgUrl, 1, model.ItemRating, model.ItemPerSaved, model.CategoryId));
        if (model.itemColorList.size() > 0) {
            cartDatabase.addFavouriteColorData(model.ItemId, model.itemColorList);
        }
        for (int i = 0; i < model.itemSizeList.size(); i++) {
            cartDatabase.addFavouriteSizeData(new SizeListDO(model.ItemId, model.itemSizeList.get(i).ItemSize, model.itemSizeList.get(i).BasePrice, model.itemSizeList.get(i).DiscountedPrice, model.itemSizeList.get(i).FinalPrice));
        }
        favouriteListListener.getFavouriteList(itemFavouriteList);
    }

    private class ViewHolder {
        TextView txt_percentage, txt_productId, tv_size_label, btn_view_item;
        CheckBox chk_fav;
        MaterialSpinner spn_size;
        LinearLayout row_griditem;
        Button btn_buy;
        FrameLayout ll_itmimg;
        public TextView txt_productname, tv_productId_label, tv_material_label, txt_material, tv_price_label, txt_basePrice, txt_discountedPrice;
    }

    public void registerCartListener(CartListListener cartListListener) {
        this.cartListListener = cartListListener;
    }

    public void registerFavouriteListener(FavouriteListListener favouriteListListener) {
        this.favouriteListListener = favouriteListListener;
    }

    public void registerSelectedGridItemListener(SelectedItemListener selectedItemListener) {
        this.selectedItemListener = selectedItemListener;
    }
}
