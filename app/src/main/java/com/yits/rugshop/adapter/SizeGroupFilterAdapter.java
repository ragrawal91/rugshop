package com.yits.rugshop.adapter;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.yits.rugshop.R;
import com.yits.rugshop.listener.SelectedFilterListener;
import com.yits.rugshop.objects.FilterMetadataDO;
import com.yits.rugshop.objects.FilternameAndIDDo;
import com.yits.rugshop.utilities.FontType;

import java.util.ArrayList;

public class SizeGroupFilterAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<FilternameAndIDDo> arrFilterMetdataDO;
    FilterMetadataDO filterMetadataDO;
    ViewHolder viewHolder;
    LayoutInflater inflater;
    FontType fonttype;
    ArrayList<FilternameAndIDDo> SelectedSizeGroupList;
    SelectedFilterListener selectedFilterListener;
    String SelectedTab;

    public SizeGroupFilterAdapter(Context context, ArrayList<FilternameAndIDDo> arrFilterMetdataDO, ArrayList<FilternameAndIDDo> dummylist, String SelectedTab) {
        this.context = context;
        this.SelectedTab = SelectedTab;
        SelectedSizeGroupList = new ArrayList<FilternameAndIDDo>();
        this.arrFilterMetdataDO = dummylist;
        this.arrFilterMetdataDO.addAll(arrFilterMetdataDO);
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {

        return arrFilterMetdataDO.size();

    }

    @Override
    public Object getItem(int position) {
        return arrFilterMetdataDO.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.row_sizes, parent, false);
            viewHolder = new ViewHolder();
            viewHolder.tvSubFilterName          = (TextView) convertView.findViewById(R.id.tvSubFilterName);
            viewHolder.viewVertDivider1         = (View) convertView.findViewById(R.id.viewVertDivider1);
            viewHolder.viewVertDivider2         = (View) convertView.findViewById(R.id.viewVertDivider2);
            viewHolder.viewVertDivider3         = (View) convertView.findViewById(R.id.viewVertDivider3);
            viewHolder.viewVerticalDivider      = (View) convertView.findViewById(R.id.viewVerticalDivider);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }


        FilternameAndIDDo Name = (FilternameAndIDDo) getItem(position);

        if(Name.name.equalsIgnoreCase("")){
            convertView.setBackgroundColor(R.color.transparent_color);
            viewHolder.viewVertDivider1.setVisibility(View.GONE);
            viewHolder.viewVertDivider2.setVisibility(View.GONE);
            viewHolder.viewVertDivider3.setVisibility(View.GONE);
            viewHolder.viewVerticalDivider.setVisibility(View.GONE);
        }
        else {
            convertView.setBackgroundColor(R.color.white_color);
            viewHolder.viewVertDivider1.setVisibility(View.VISIBLE);
            viewHolder.viewVertDivider2.setVisibility(View.VISIBLE);
            viewHolder.viewVertDivider3.setVisibility(View.VISIBLE);
            viewHolder.viewVerticalDivider.setVisibility(View.VISIBLE);
        }


        if (!Name.type.equalsIgnoreCase("DUMMY")) {
            ViewGroup root = (ViewGroup) convertView.findViewById(R.id.row_sizes);
            fonttype = new FontType(context, root);

            viewHolder.tvSubFilterName.setText(Name.name);
            if (SelectedTab.equalsIgnoreCase("PRICE")) {
                StringBuilder op = new StringBuilder();
                op.append("£ ");
                op.append(Name.name.replaceAll("-", "- £ "));

                viewHolder.tvSubFilterName.setText(op);

            }

//            viewHolder.tvSubFilterName.setChecked(Name.checkState);

            viewHolder.tvSubFilterName.setTag(Name);
            viewHolder.tvSubFilterName.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    FilternameAndIDDo model = (FilternameAndIDDo) v.getTag();
                    if (model.checkState) {
                        model.checkState = false;

                        if (SelectedTab.equalsIgnoreCase("PRICE")) {
                            selectedFilterListener.removeSelectedPriceFilterListener(model);
                        } else if (SelectedTab.equalsIgnoreCase("COLOUR")) {
                            selectedFilterListener.removeSelectedColorFilterListener(model);
                        } else if (SelectedTab.equalsIgnoreCase("MATERIAL")) {
                            selectedFilterListener.removeSelectedMaterialFilterListener(model);
                        } else if (SelectedTab.equalsIgnoreCase("PATTERN")) {
                            selectedFilterListener.removeSelectedPatternFilterListener(model);
                        } else if (SelectedTab.equalsIgnoreCase("SIZEGROUP")) {
                            selectedFilterListener.removeSelectedSizegroupFilterListener(model);

                        } else if (SelectedTab.equalsIgnoreCase("SIZE")) {
                            selectedFilterListener.removeSelectedSizeFilterListener(model);
                        } else if (SelectedTab.equalsIgnoreCase("RUNNERS")) {
                            selectedFilterListener.removeSelectedRunnerFilterListener(model);
                        } else if (SelectedTab.equalsIgnoreCase("CATEGORIES")) {
                            selectedFilterListener.removeSelectedCategoryFilterListener(model);
                        }

                    } else {
                        model.checkState = true;

                        if (SelectedTab.equalsIgnoreCase("PRICE")) {
                            selectedFilterListener.getSelectedPriceFilterListener(model);
                        } else if (SelectedTab.equalsIgnoreCase("COLOUR")) {
                            selectedFilterListener.getSelectedColorFilterListener(model);
                        } else if (SelectedTab.equalsIgnoreCase("MATERIAL")) {
                            selectedFilterListener.getSelectedMaterialFilterListener(model);
                        } else if (SelectedTab.equalsIgnoreCase("PATTERN")) {
                            selectedFilterListener.getSelectedPatternFilterListener(model);
                        } else if (SelectedTab.equalsIgnoreCase("SIZEGROUP")) {
                            selectedFilterListener.getSelectedSizegroupFilterListener(model);
                        } else if (SelectedTab.equalsIgnoreCase("SIZE")) {
                            selectedFilterListener.getSelectedSizeFilterListener(model);
                        } else if (SelectedTab.equalsIgnoreCase("RUNNERS")) {
                            selectedFilterListener.getSelectedRunnerFilterListener(model);
                        } else if (SelectedTab.equalsIgnoreCase("CATEGORIES")) {
                            selectedFilterListener.getSelectedCategoryFilterListener(model);
                        }
                    }
                }
            });
            convertView.setVisibility(View.VISIBLE);

        } else {
            convertView.setVisibility(View.INVISIBLE);
        }

        return convertView;
    }


    private class ViewHolder {
        TextView tvSubFilterName;
        View viewVertDivider1, viewVertDivider2, viewVertDivider3, viewVerticalDivider;
    }

    public void registerSelectedGroupFilterListener(SelectedFilterListener selectedFilterListener) {
        this.selectedFilterListener = selectedFilterListener;
    }

}
