package com.yits.rugshop.adapter;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RadioButton;
import android.widget.TextView;

import com.yits.rugshop.R;
import com.yits.rugshop.listener.GetSelectedSizeListener;
import com.yits.rugshop.objects.ItemsSizeDO;
import com.yits.rugshop.utilities.FontType;

import java.util.ArrayList;

public class PositionSizeAdapter extends BaseAdapter
{

	private Context context;
	private ArrayList<ItemsSizeDO> arrSizeDO;
	ViewHolder viewHolder;
	LayoutInflater inflater;
	FontType fonttype;
	ItemsSizeDO sizeListDO;
	GetSelectedSizeListener getSelectedSizeListener;

	public PositionSizeAdapter(Context context, ArrayList<ItemsSizeDO> arrSizeDO)
	{
		this.context       = context;
		this.arrSizeDO    = arrSizeDO;
		inflater= (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

	}



	@Override
	public int getCount()
	{

		return arrSizeDO.size();

	}

	@Override
	public Object getItem(int position)
	{
		return arrSizeDO.get(position);
	}

	@Override
	public long getItemId(int position)
	{
		return position;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent)
	{
		if(convertView==null)
		{
			convertView = inflater.inflate(R.layout.row_positionsize, parent,false);
			viewHolder = new ViewHolder();
			viewHolder.txt_cartsize 	    = (TextView)convertView.findViewById(R.id.txt_cartsize);
			viewHolder.txt_sizePrice 	    = (TextView)convertView.findViewById(R.id.txt_sizePrice);
			viewHolder.radioButton 	    = (RadioButton)convertView.findViewById(R.id.radioButton);

			convertView.setTag(viewHolder);
		}
		else
		{
			viewHolder= (ViewHolder) convertView.getTag();
		}

		sizeListDO = (ItemsSizeDO) getItem(position);

		ViewGroup root = (ViewGroup) convertView.findViewById(R.id.row_cartsize);
		fonttype = new FontType(context, root);

		viewHolder.txt_cartsize.setText(sizeListDO.ItemSize);
		viewHolder.txt_sizePrice.setText("£ " + sizeListDO.FinalPrice);

		viewHolder.radioButton.setTag(sizeListDO);

		if(sizeListDO.itemSelected)
		{
			viewHolder.radioButton.setChecked(sizeListDO.itemSelected);
		}
		else
		{
			viewHolder.radioButton.setChecked(sizeListDO.itemSelected);
		}

		viewHolder.radioButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v)
			{
				sizeListDO= (ItemsSizeDO) v.getTag();
				for(int i=0;i<arrSizeDO.size();i++)
				{

					arrSizeDO.get(i).itemSelected=false;
				}
				sizeListDO.itemSelected=true;

				notifyDataSetChanged();
				getSelectedSizeListener.getSelectedSize(sizeListDO);

			}
		});
		/*int Count =position+1;
		viewHolder.txt_count.setText(String.valueOf(Count));*/


		//imgLoader.displayImage(AppConstants.IMAGE_URL + categoryDO.C, viewHolder.img_profile, dispImage);

		return convertView;
	}


	private class ViewHolder
	{
		TextView  txt_cartsize,txt_sizePrice;
		RadioButton radioButton;

	}


	public void registerSizePriceListener(GetSelectedSizeListener getSelectedSizeListener)
	{
		this.getSelectedSizeListener=getSelectedSizeListener;
	}


}
