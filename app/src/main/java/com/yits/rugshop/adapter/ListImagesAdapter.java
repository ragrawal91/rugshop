package com.yits.rugshop.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.yits.rugshop.R;
import com.yits.rugshop.listener.GetStringOnClickListener;
import com.yits.rugshop.utilities.BgViewAware;

import java.util.ArrayList;


/**
 * Created by sai on 8/25/2016.
 */
public class ListImagesAdapter extends RecyclerView.Adapter<ListImagesAdapter.MyViewHolder> {
    ArrayList<String> imageUrls;
    ImageLoaderConfiguration imgLoaderConf;
    DisplayImageOptions dispImage;
    Context context;
    ImageLoader imgLoader = ImageLoader.getInstance();
    GetStringOnClickListener getStringOnClickListener;

    public ListImagesAdapter(ArrayList<String> imageUrls, Context context, GetStringOnClickListener getStringOnClickListener) {
        this.imageUrls = imageUrls;
        this.getStringOnClickListener = getStringOnClickListener;
        this.context = context;
        imgLoaderConf = new ImageLoaderConfiguration.Builder(context).build();
        imgLoader.init(imgLoaderConf);
        dispImage = new DisplayImageOptions.Builder()
                .showImageOnFail(R.mipmap.wool_rug)
                .showImageOnLoading(R.mipmap.wool_rug)
                .showImageForEmptyUri(R.mipmap.wool_rug)
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .delayBeforeLoading(100)
                .imageScaleType(ImageScaleType.EXACTLY)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .build();
    }

    public void refresh(ArrayList<String> imageUrls) {
        this.imageUrls = imageUrls;
        notifyDataSetChanged();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView ivItemImage;

        public MyViewHolder(View itemView) {
            super(itemView);
            ivItemImage = (ImageView) itemView.findViewById(R.id.ivItemImage);
        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(context).inflate(R.layout.row_img_image, parent, false);

        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {

        imgLoader.getInstance().displayImage(imageUrls.get(position), new BgViewAware(holder.ivItemImage), dispImage);

        holder.ivItemImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getStringOnClickListener.getStringOnClick(imageUrls.get(position));
            }
        });
    }

    @Override
    public int getItemCount() {
        return imageUrls.size();
    }
}
