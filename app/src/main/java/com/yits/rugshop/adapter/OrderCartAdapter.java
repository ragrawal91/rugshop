package com.yits.rugshop.adapter;


import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.yits.rugshop.R;
import com.yits.rugshop.listener.CartListListener;
import com.yits.rugshop.objects.ItemCartsDO;
import com.yits.rugshop.objects.ItemsDO;
import com.yits.rugshop.objects.SizeListDO;
import com.yits.rugshop.utilities.BgViewAware;
import com.yits.rugshop.utilities.CartDatabase;
import com.yits.rugshop.utilities.FontType;

import java.util.ArrayList;

import tyrantgit.explosionfield.ExplosionField;

public class OrderCartAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<ItemCartsDO> arrCartDO;
    ItemCartsDO cartsDO;
    ViewHolder viewHolder;
    LayoutInflater inflater;
    ImageLoaderConfiguration imgLoaderConf;
    DisplayImageOptions dispImage;
    ImageLoader imgLoader = ImageLoader.getInstance();
    FontType fonttype;
    ItemsDO itemsDO;
    CartListListener cartListListener;
    ArrayList<String> cartColorList = new ArrayList<>();
    ArrayList<SizeListDO> cartSizeList = new ArrayList<>();
    CartColorAdapter cartColorAdapter;
    CartSizeAdapter cartSizeAdapter;
    SizeListDO sizeListDO;
    ListView lst_cartsize, lst_cartColor;
    TextView txt_dialogitemName;
    Spinner spn_color;
    int qty;
    ExplosionField mExplosionField;
    Dialog dialog;

    public OrderCartAdapter(Context context, ArrayList<ItemCartsDO> arrCartDO) {
        this.context = context;
        this.arrCartDO = arrCartDO;

        CartDatabase.init(context);
        mExplosionField = ExplosionField.attach2Window((Activity) context);
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        imgLoaderConf = new ImageLoaderConfiguration.Builder(context).build();
        imgLoader.init(imgLoaderConf);
        dispImage = new DisplayImageOptions.Builder()
                .showImageOnFail(R.mipmap.wool_rug)
                .showImageOnLoading(R.mipmap.wool_rug)
                .showImageForEmptyUri(R.mipmap.wool_rug)
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .delayBeforeLoading(100)
                .imageScaleType(ImageScaleType.EXACTLY)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .build();
    }

    @Override
    public int getCount() {

        return arrCartDO.size();

    }

    @Override
    public Object getItem(int position) {
        return arrCartDO.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.row_ordercart, parent, false);
            viewHolder = new ViewHolder();
            viewHolder.txt_baseprice = (TextView) convertView.findViewById(R.id.txt_baseprice);
            viewHolder.btn_minus = (Button) convertView.findViewById(R.id.btn_minus);
            viewHolder.btn_plus = (Button) convertView.findViewById(R.id.btn_plus);
            viewHolder.tv_quantity_label = (TextView) convertView.findViewById(R.id.tv_quantity_label);
            viewHolder.tv_material_label = (TextView) convertView.findViewById(R.id.tv_material_label);
            viewHolder.tv_price_label = (TextView) convertView.findViewById(R.id.tv_price_label);
            viewHolder.tv_size_label = (TextView) convertView.findViewById(R.id.tv_size_label);
            viewHolder.tv_code_label = (TextView) convertView.findViewById(R.id.tv_code_label);
            viewHolder.txt_itemName = (TextView) convertView.findViewById(R.id.txt_itemName);
            viewHolder.txt_productId = (TextView) convertView.findViewById(R.id.txt_productId);
            viewHolder.txt_qty = (TextView) convertView.findViewById(R.id.txt_qty);
            viewHolder.txt_price = (TextView) convertView.findViewById(R.id.txt_price);
            viewHolder.txt_color = (TextView) convertView.findViewById(R.id.txt_color);
            viewHolder.txt_pattern = (TextView) convertView.findViewById(R.id.txt_pattern);
            viewHolder.txt_material = (TextView) convertView.findViewById(R.id.txt_material);
            viewHolder.txt_size = (TextView) convertView.findViewById(R.id.txt_size);

            viewHolder.ll_itemIMG = (LinearLayout) convertView.findViewById(R.id.ll_itemIMG);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        ViewGroup root = (ViewGroup) convertView.findViewById(R.id.row_cart);
        fonttype = new FontType(context, root);

        cartsDO = (ItemCartsDO) getItem(position);


        //cartSizeList=CartDatabase.getAllSizeList(cartsDO.ItemId);
        qty = cartsDO.ItemQuantity;

        viewHolder.txt_itemName.setText(cartsDO.ItemName);
        viewHolder.txt_productId.setText(cartsDO.ItemProductId);
        viewHolder.txt_qty.setText(String.valueOf(qty));
        viewHolder.txt_price.setText("£" + cartsDO.ItemFinalPrice);
        viewHolder.txt_baseprice.setText("£" + cartsDO.ItemBasePrice);

        viewHolder.txt_baseprice.setPaintFlags(viewHolder.txt_baseprice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);

        viewHolder.txt_color.setText(cartsDO.ItemColor);
        viewHolder.txt_pattern.setText(cartsDO.ItemPattern);
        viewHolder.txt_material.setText(cartsDO.ItemMaterial);
        viewHolder.txt_size.setText(cartsDO.ItemSize);
        viewHolder.ll_itemIMG.setTag(cartsDO);
        imgLoader.getInstance().displayImage(cartsDO.ItemBigImgUrl, new BgViewAware(viewHolder.ll_itemIMG), dispImage);

        new FontType(context).applyfonttoView(viewHolder.txt_itemName, FontType.ROBOTOREGULAR);
        new FontType(context).applyfonttoView(viewHolder.tv_code_label, FontType.ROBOTOREGULAR);
        new FontType(context).applyfonttoView(viewHolder.tv_material_label, FontType.ROBOTOREGULAR);
        new FontType(context).applyfonttoView(viewHolder.tv_price_label, FontType.ROBOTOREGULAR);
        new FontType(context).applyfonttoView(viewHolder.tv_size_label, FontType.ROBOTOREGULAR);

        new FontType(context).applyfonttoView(viewHolder.txt_productId, FontType.ROBOTOLIGHT);
        new FontType(context).applyfonttoView(viewHolder.txt_material, FontType.ROBOTOLIGHT);
        new FontType(context).applyfonttoView(viewHolder.txt_price, FontType.ROBOTOLIGHT);
        new FontType(context).applyfonttoView(viewHolder.txt_size, FontType.ROBOTOLIGHT);
        new FontType(context).applyfonttoView(viewHolder.tv_quantity_label, FontType.ROBOTOREGULAR);
        new FontType(context).applyfonttoView(viewHolder.btn_plus, FontType.ROBOTOREGULAR);
        new FontType(context).applyfonttoView(viewHolder.btn_plus, FontType.ROBOTOREGULAR);
        new FontType(context).applyfonttoView(viewHolder.txt_qty, FontType.ROBOTOREGULAR);

        return convertView;
    }


    private class ViewHolder {
        TextView txt_itemName, txt_productId, txt_qty, txt_price, txt_pattern, txt_color, txt_material, txt_size;
        LinearLayout ll_itemIMG;
        Button btn_minus, btn_plus;
        TextView txt_baseprice;
        TextView tv_code_label, tv_material_label, tv_price_label, tv_size_label, tv_quantity_label;
    }
}
