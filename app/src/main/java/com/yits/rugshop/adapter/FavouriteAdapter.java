package com.yits.rugshop.adapter;


import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.jaredrummler.materialspinner.MaterialSpinner;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.yits.rugshop.R;
import com.yits.rugshop.activity.FavouriteActivity;
import com.yits.rugshop.listener.UpdateListView;
import com.yits.rugshop.objects.ItemsDO;
import com.yits.rugshop.objects.SizeListDO;
import com.yits.rugshop.utilities.BgViewAware;
import com.yits.rugshop.utilities.CartDatabase;
import com.yits.rugshop.utilities.FontType;

import java.util.ArrayList;

import tyrantgit.explosionfield.ExplosionField;

public class FavouriteAdapter extends BaseAdapter
{

	private Context context;
	private ArrayList<ItemsDO> arrCartDO;
	ImageLoaderConfiguration imgLoaderConf;
	DisplayImageOptions dispImage;
	ImageLoader imgLoader= ImageLoader.getInstance();
	FontType fonttype;
	UpdateListView cartListListener;
	ArrayList<String> cartColorList=new ArrayList<>();
	CartColorAdapter cartColorAdapter;
	CartSizeAdapter cartSizeAdapter;
	SizeListDO sizeListDO;
	ListView lst_cartsize;
	LinearLayout ll_IMG;
	ImageView img_crossSize,img_crossColor;
	TextView txt_dialogitemName,txt_no,txt_yes;
	int qty;
	ExplosionField mExplosionField;
	Dialog dialog  ;
	private AlertDialog.Builder alertDialog;

	public FavouriteAdapter(Context context, ArrayList<ItemsDO> arrCartDO)
	{
		this.context       = context;
		this.arrCartDO    = arrCartDO;

		CartDatabase.init(context);
		mExplosionField = ExplosionField.attach2Window((Activity) context);
		imgLoaderConf = new ImageLoaderConfiguration.Builder(context).build();
		imgLoader.init(imgLoaderConf);
		dispImage=new DisplayImageOptions.Builder()
				.showImageOnFail(R.mipmap.wool_rug)
				.showImageOnLoading(R.mipmap.wool_rug)
				.showImageForEmptyUri(R.mipmap.wool_rug)
				.cacheInMemory(true)
				.cacheOnDisk(true)
				.considerExifParams(true)
				.delayBeforeLoading(100)
				.imageScaleType(ImageScaleType.EXACTLY)
				.bitmapConfig(Bitmap.Config.RGB_565)
				.build();
	}

	@Override
	public int getCount()
	{

		return arrCartDO.size();

	}

	@Override
	public Object getItem(int position)
	{
		return arrCartDO.get(position);
	}

	@Override
	public long getItemId(int position)
	{
		return position;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent)
	{
		ViewHolder viewHolder = null;
		if(convertView==null)
		{
			convertView = LayoutInflater.from(context).inflate(R.layout.row_favourite, parent,false);
			viewHolder = new ViewHolder();
			viewHolder.txt_itemName				= (TextView) convertView.findViewById(R.id.txt_itemName);
			viewHolder.txt_productId 	    	= (TextView)convertView.findViewById(R.id.txt_productId);
			viewHolder.txt_qty 	            	= (TextView)convertView.findViewById(R.id.txt_qty);
			viewHolder.txt_totalprice 	        = (TextView)convertView.findViewById(R.id.txt_totalprice);
			viewHolder.txt_BasePrice 	        = (TextView)convertView.findViewById(R.id.txt_BasePrice);
			viewHolder.txt_size 	        	= (TextView)convertView.findViewById(R.id.txt_size);

			viewHolder.ll_itemIMG 	    		= (FrameLayout) convertView.findViewById(R.id.ll_itemIMG);
			viewHolder.ll_size 	    			= (LinearLayout)convertView.findViewById(R.id.ll_size);
			viewHolder.img_addtocart			= (ImageView) convertView.findViewById(R.id.img_addtocart);
			viewHolder.img_remove 	   			= (ImageView)convertView.findViewById(R.id.img_remove);

			viewHolder.sprSizes					= (MaterialSpinner) convertView.findViewById(R.id.sprSizes);
			convertView.setTag(viewHolder);
		}
		else
		{
			viewHolder= (ViewHolder) convertView.getTag();
		}
		ViewGroup root = (ViewGroup) convertView.findViewById(R.id.row_cart);
//		fonttype = new FontType(context, root);

		ItemsDO cartsDO = (ItemsDO) getItem(position);
		viewHolder.txt_productId.setText(cartsDO.ProductID);
		viewHolder.txt_itemName.setText(cartsDO.ItemName);
		viewHolder.txt_totalprice.setText(""+cartsDO.ItemFinalPrice);

		final ArrayList<SizeListDO> favSizeList = CartDatabase.getAllFavouriteSizeList(cartsDO.ItemId);

		final ArrayList<String> sizesStrings = new ArrayList<>();

		for (int i = 0; i < (favSizeList!=null?favSizeList.size():0); i++) {
			sizesStrings.add(favSizeList.get(i).SizeName);
		}
		if(sizesStrings!=null && sizesStrings.size()>0)
			viewHolder.sprSizes.setItems(sizesStrings);

//		String multipliedPrice=String.format("%.2f",Double.parseDouble(cartsDO.ItemFinalPrice)*qty);
//		viewHolder.txt_unitPrice.setText(cartsDO.ItemBasePrice);
//		viewHolder.txt_BasePrice.setText(cartsDO.ItemFinalPrice);
//		viewHolder.txt_BasePrice.setPaintFlags(viewHolder.txt_BasePrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
//		viewHolder.txt_size.setText(cartsDO.ItemSize);


		final ViewHolder dummyHolder = viewHolder;
		viewHolder.img_addtocart.setTag(cartsDO);
		viewHolder.img_addtocart.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v)
			{
				ItemsDO itemsDO= (ItemsDO) dummyHolder.img_addtocart.getTag();
				cartListListener.addTocart(itemsDO);
				CartDatabase.deleteFavouritelist(itemsDO);
				CartDatabase.deleteFavouritecolorlist(itemsDO);
				CartDatabase.deleteFavouritesizelist(itemsDO);
				cartListListener.update();
				((FavouriteActivity)context).getCartListNormal();
				((FavouriteActivity)context).getFavouriteList(null);


			}
		});
		viewHolder.ll_size.setTag(cartsDO);


		viewHolder.img_remove.setTag(cartsDO);
		viewHolder.img_remove.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				Toast.makeText(context, "Remove icon clicked is", Toast.LENGTH_SHORT).show();
				ItemsDO model = (ItemsDO) view.getTag();
				appCompatAlert(context, model);
			}
		});

		viewHolder.ll_itemIMG.setTag(cartsDO);
		viewHolder.ll_itemIMG.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				final ItemsDO itemCartsDO = (ItemsDO) v.getTag();

				showDialogImage(context, 700, 450);

				ll_IMG = (LinearLayout) dialog.findViewById(R.id.ll_IMG);
				img_crossColor= (ImageView) dialog.findViewById(R.id.img_crossColor);

				ll_IMG.setTag(itemCartsDO);
				img_crossColor.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View view) {
						dialog.dismiss();
					}
				});
				imgLoader.getInstance().displayImage(itemCartsDO.ItemBigImgUrl, new BgViewAware(ll_IMG), dispImage);

				dialog.show();
			}
		});

		viewHolder.ll_itemIMG.setTag(cartsDO);
		imgLoader.getInstance().displayImage(cartsDO.ItemBigImgUrl, new BgViewAware(viewHolder.ll_itemIMG), dispImage);

		return convertView;
	}


	private class ViewHolder
	{
		TextView  txt_productId,txt_qty, txt_totalprice, txt_size,txt_itemName, txt_BasePrice;
		ImageView img_remove,img_addtocart;
		LinearLayout ll_size;
		private FrameLayout ll_itemIMG;
		private MaterialSpinner sprSizes;
	}
	public void appCompatAlert(final Context context, final ItemsDO model) {
		if (alertDialog == null)
			alertDialog = new AlertDialog.Builder(context, R.style.AppCompatAlertDialogStyle);
		alertDialog.setTitle("Alert!");
		alertDialog.setMessage("Do you really want to delete this item?");
		alertDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				CartDatabase.deleteFavouritelist(model);
				CartDatabase.deleteFavouritecolorlist(model);
				CartDatabase.deleteFavouritesizelist(model);
				cartListListener.update();
				((FavouriteActivity)context).getFavouriteList(null);
				notifyDataSetChanged();

			}
		});//second parameter used for onclicklistener
		alertDialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				onButtonNoClick(model);
			}
		});
		alertDialog.setCancelable(false);
		alertDialog.show();
	}

	public void onButtonNoClick(ItemsDO model) {
	}

	public void onButtonYesClick(ItemsDO model, ArrayList<SizeListDO> favSizeList) {




		cartListListener.addTocart(model);
		cartListListener.update();
//		CartDatabase.addCartData(favSizeList);

		((FavouriteActivity)context).getCartListNormal();
		CartDatabase.deleteFavouritelist(model);
		CartDatabase.deleteFavouritecolorlist(model);
		CartDatabase.deleteFavouritesizelist(model);
		((FavouriteActivity)context).getFavouriteList(null);
	}

	public void registerLocaldatabaseListener(UpdateListView cartListListener)
	{
		this.cartListListener=cartListListener;
	}

	public void showDialogSize(Context context, int x, int y)
	{
		dialog  = new Dialog(context);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
		dialog.setContentView(R.layout.cart_dialogsize);
		dialog.setCanceledOnTouchOutside(false);
	}

	public void showDialogImage(Context context, int x, int y)
	{
		dialog  = new Dialog(context);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
		dialog.setContentView(R.layout.cart_dialogimage);
		dialog.setCanceledOnTouchOutside(false);
	}

	public void showDialogRemove(Context context, int x, int y, final ItemsDO model)
	{
		dialog  = new Dialog(context);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
		dialog.setContentView(R.layout.cart_dialogremove);
		dialog.setCanceledOnTouchOutside(false);

		txt_yes = (TextView) dialog.findViewById(R.id.txt_yes);
		txt_no= (TextView) dialog.findViewById(R.id.txt_no);

		txt_yes.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

				CartDatabase.deleteFavouritelist(model);
				CartDatabase.deleteFavouritecolorlist(model);
				CartDatabase.deleteFavouritesizelist(model);
				cartListListener.update();
				dialog.dismiss();
				//notifyDataSetChanged();

			}
		});

		txt_no.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.dismiss();
			}
		});

		dialog.show();
	}

}
