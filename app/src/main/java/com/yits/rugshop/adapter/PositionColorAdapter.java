package com.yits.rugshop.adapter;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.yits.rugshop.R;
import com.yits.rugshop.utilities.FontType;

import java.util.ArrayList;

public class PositionColorAdapter extends BaseAdapter
{

	private Context context;
	private ArrayList<String> arrColorDO;
	ViewHolder viewHolder;
	LayoutInflater inflater;
	FontType fonttype;

	public PositionColorAdapter(Context context, ArrayList<String> arrColorDO)
	{
		this.context       = context;
		this.arrColorDO    = arrColorDO;
		inflater= (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

	}



	@Override
	public int getCount()
	{

		return arrColorDO.size();

	}

	@Override
	public Object getItem(int position)
	{
		return arrColorDO.get(position);
	}

	@Override
	public long getItemId(int position)
	{
		return position;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent)
	{
		if(convertView==null)
		{
			convertView = inflater.inflate(R.layout.row_positioncolor, parent,false);
			viewHolder = new ViewHolder();
			viewHolder.txt_cartcolor 	    = (TextView)convertView.findViewById(R.id.txt_cartcolor);
			viewHolder.txt_count 	    = (TextView)convertView.findViewById(R.id.txt_count);


			convertView.setTag(viewHolder);
		}
		else
		{
			viewHolder= (ViewHolder) convertView.getTag();
		}

		String Colorname = (String) getItem(position);

		ViewGroup root = (ViewGroup) convertView.findViewById(R.id.row_cartcolor);
		fonttype = new FontType(context, root);

		viewHolder.txt_cartcolor.setText(Colorname);
		int Count =position+1;
		viewHolder.txt_count.setText(String.valueOf(Count));


		//imgLoader.displayImage(AppConstants.IMAGE_URL + categoryDO.C, viewHolder.img_profile, dispImage);

		return convertView;
	}


	private class ViewHolder
	{
		TextView  txt_cartcolor,txt_count;

	}



}
