package com.yits.rugshop.activity;

import android.app.ActionBar;
import android.content.Context;
import android.view.View;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.yits.rugshop.R;
import com.yits.rugshop.adapter.CategoryAdapter;
import com.yits.rugshop.adapter.ColorsAdapter;
import com.yits.rugshop.businesslayer.CommonBL;
import com.yits.rugshop.businesslayer.DataListener;
import com.yits.rugshop.objects.CategoryDO;
import com.yits.rugshop.objects.ColorsDO;
import com.yits.rugshop.objects.ItemsDO;
import com.yits.rugshop.utilities.AppConstants;
import com.yits.rugshop.utilities.CartDatabase;
import com.yits.rugshop.utilities.FontType;
import com.yits.rugshop.utilities.PreferenceUtils;
import com.yits.rugshop.webaccess.Response;
import com.yits.rugshop.webaccess.ServiceMethods;

import java.util.ArrayList;
import java.util.HashMap;

public class Dashboard extends BaseActivity implements DataListener {

    LinearLayout ll_Dashboard;
    Context mContext;
    TextView tvDashBoardNoData;
    String Message;
    PreferenceUtils preferenceUtils;
    public HashMap<String, String> Userdata;
    public String UserShopName, UserPkID, queryParams;
    //  private SliderLayout mDemoSlider;
    GridView grd_catg;
    private ArrayList<CategoryDO> arrCatgories;
    private ArrayList<ColorsDO> arrColors;
    CategoryAdapter categoryAdapter;
    ColorsAdapter colorsAdapter;
    CategoryDO categoryDO;
    TextView tv_rug_shop_tittle;
    TextView tv_rug_shop_message;


    @Override
    public void initialize() {

        ll_Dashboard = (LinearLayout) inflater.inflate(R.layout.activity_dashboard, null);
        llContent.addView(ll_Dashboard, new LinearLayout.LayoutParams(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.MATCH_PARENT));
        mContext = Dashboard.this;
        preferenceUtils = new PreferenceUtils(mContext);

        initializeControls();

        CartDatabase.init(mContext);
        Userdata = preferenceUtils.getUserDetails();
        UserPkID = Userdata.get(AppConstants.KEY_USER_ID);
        UserShopName = Userdata.get(AppConstants.KEY_USER_FST_NAME);
        queryParams = Userdata.get(AppConstants.KEY_ACCESS_TOKEN);

        txt_actionbar.setText(UserShopName);
        //getCategories(queryParams);
        getColors(queryParams);

        new FontType(mContext).applyfonttoView(tv_rug_shop_message, FontType.ROBOTOREGULAR);
        new FontType(mContext).applyfonttoView(tv_rug_shop_tittle, FontType.ROBOTOBOLD);

       /* img_cart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(cartCount>0)
                {
                    Intent intent = new Intent(mContext, CartActivity.class);
                    startActivity(intent);
                }
                else
                {
                    showAlertDialog("Please add some items to cart","OK");
                }
            }
        });*/

        /*img_home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Toast.makeText(mContext,"Home  Clicked",Toast.LENGTH_SHORT).show();
            }
        });*/

        /*img_favourite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(mContext, FavouriteActivity.class);
                startActivity(intent);
            }
        });*/

/*
        HashMap<String,String> url_maps = new HashMap<String, String>();
        url_maps.put("Hannibal", "https://s3-us-west-2.amazonaws.com/rugshop/ad.jpg");
        url_maps.put("Big Bang Theory", "https://s3-us-west-2.amazonaws.com/rugshop/ad_2.jpg");
        url_maps.put("House of Cards", "https://s3-us-west-2.amazonaws.com/rugshop/ad_3.jpg");


        for(String name : url_maps.keySet()){
            TextSliderView textSliderView = new TextSliderView(this);
            textSliderView.image(url_maps.get(name)) .setScaleType(BaseSliderView.ScaleType.Fit);
            mDemoSlider.addSlider(textSliderView);
        }

        mDemoSlider.setPresetTransformer(SliderLayout.Transformer.ZoomOut);
        mDemoSlider.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
        mDemoSlider.setDuration(6000);*/



       /* grd_catg.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id)
            {
                categoryDO= (CategoryDO) parent.getItemAtPosition(position);
                String CategoryId=categoryDO.CatgId;
                Intent intent=new Intent(mContext,ItemsActivity.class);
                intent.putExtra("CatgId",CategoryId);
                startActivity(intent);
            }
        });
*/
    }

    @Override
    protected void onResume() {
        cartCount = CartDatabase.getCartlistCount();
        if(cartCount > 0)
            tvCartCount.setText(""+cartCount);
        if(cartCount>0){
            tvCartCount.setVisibility(View.VISIBLE);
        }
        else {
            tvCartCount.setVisibility(View.GONE);
        }
        ArrayList<ItemsDO> itemsDOs = CartDatabase.getAllFavouriteList();
        if(itemsDOs!=null && itemsDOs.size()>0) {
            favCount = itemsDOs.size();
        }
        if(favCount>0){
            tvFavouriteCount.setText(""+favCount);
            tvFavouriteCount.setVisibility(View.VISIBLE);
            ivFavourite.setImageResource(R.mipmap.fav_disabled);
        }
        else {
            tvFavouriteCount.setVisibility(View.GONE);
            ivFavourite.setImageResource(R.mipmap.fav_disabled);
        }
        super.onResume();
    }

    private void getCategories(String queryParams) {

        showLoaderNew();
        //2?access_token=yfcQsCgL789o5Vfe1ReA83iPstX7RClJwFjg3rAf6S0bmwyqVOYUE0rpeIYmd5dH

        if (new CommonBL(mContext, Dashboard.this).getCategories(queryParams)) {
//			showLoader("Loading all brands...");

        } else {
            hideloader();
            showAlertDialog("NO INTERNET", "OK");
        }

    }

    private void getColors(String queryParams) {

        showLoaderNew();
        //2?access_token=yfcQsCgL789o5Vfe1ReA83iPstX7RClJwFjg3rAf6S0bmwyqVOYUE0rpeIYmd5dH

        if (new CommonBL(mContext, Dashboard.this).getColors(queryParams)) {
//			showLoader("Loading all brands...");

        } else {
            hideloader();
            showAlertDialog("NO INTERNET", "OK");
        }

    }

    private void initializeControls() {
        //  mDemoSlider=(SliderLayout)findViewById(R.id.slider);
        grd_catg                = (GridView) findViewById(R.id.grd_catg);
        tvDashBoardNoData       = (TextView) findViewById(R.id.tvDashBoardNoData);
        tv_rug_shop_tittle      = (TextView) findViewById(R.id.tv_rug_shop_tittle);
        tv_rug_shop_message     = (TextView) findViewById(R.id.tv_rug_shop_message);

    }

    @Override
    public void dataRetreived(Response data) {
        hideloader();
        if (data != null && data.method == ServiceMethods.WS_GET_CATEGORIES) {
            if (!data.isError) {
                arrCatgories = (ArrayList<CategoryDO>) data.data;
                if (arrCatgories != null && arrCatgories.size() > 0) {
                    tvDashBoardNoData.setVisibility(View.GONE);
                    grd_catg.setVisibility(View.VISIBLE);
                    categoryAdapter = new CategoryAdapter(mContext, arrCatgories);
                    grd_catg.setAdapter(categoryAdapter);
                } else {
                    tvDashBoardNoData.setVisibility(View.VISIBLE);
                    grd_catg.setVisibility(View.GONE);
                }
            }
        } else if (data != null && data.method == ServiceMethods.WS_GET_COLORS) {
            if (!data.isError) {
                arrColors = (ArrayList<ColorsDO>) data.data;
                AppConstants.colorsDOs = arrColors;
                if (arrColors != null && arrColors.size() > 0) {
                    tvDashBoardNoData.setVisibility(View.GONE);
                    grd_catg.setVisibility(View.VISIBLE);
                    colorsAdapter = new ColorsAdapter(mContext, arrColors);
                    grd_catg.setAdapter(colorsAdapter);
                } else {
                    tvDashBoardNoData.setVisibility(View.VISIBLE);
                    grd_catg.setVisibility(View.GONE);
                }
            }
        }

    }
}
