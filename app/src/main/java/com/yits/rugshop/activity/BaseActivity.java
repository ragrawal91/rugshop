package com.yits.rugshop.activity;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.yits.rugshop.R;
import com.yits.rugshop.adapter.CartHomeAdapter;
import com.yits.rugshop.adapter.FavouriteHomeAdapter;
import com.yits.rugshop.adapter.RefineFilterHeadingAdapter;
import com.yits.rugshop.listener.CartListListener;
import com.yits.rugshop.listener.FilterClear;
import com.yits.rugshop.objects.ColorsDO;
import com.yits.rugshop.objects.FilterHeadingDO;
import com.yits.rugshop.objects.FilternameAndIDDo;
import com.yits.rugshop.objects.ItemCartsDO;
import com.yits.rugshop.objects.ItemsDO;
import com.yits.rugshop.utilities.AppConstants;
import com.yits.rugshop.utilities.CartDatabase;
import com.yits.rugshop.utilities.FontType;
import com.yits.rugshop.utilities.PreferenceUtils;
import com.yits.rugshop.utilities.StringUtils;

import java.util.ArrayList;
import java.util.regex.Pattern;

@SuppressWarnings("ResourceType")
public abstract class BaseActivity extends AppCompatActivity implements CartListListener {
    public Dialog dialog;
    private AnimationDrawable animationDrawable;
    public LinearLayout llContent;
    Context mContext;
    public LinearLayout view, llFavourite;
    public AlertDialog alertDialog;
    public AlertDialog.Builder alertBuilder;
    public Bundle savedInstanceState;
    public TextView tv_grand_total, txt_actionbar, tvCartCount, tvFavouriteCount;
    public ImageView img_home, ivCart, ivFavourite;
    public LinearLayout toolbar, mtoolbar, base_filter, ll_search_parent;
    public FrameLayout frm_color_selection;
    public LayoutInflater inflater;
    private boolean cartClicked, favClicked;
    public String currentClass = "";
    public ListView lst_cart_items, lvFavourite;
    public CartHomeAdapter cartHomeAdapter;
    public FavouriteHomeAdapter favouriteHomeAdapter;
    public ArrayList<ItemCartsDO> itemCartsDOs;
    public ArrayList<ItemsDO>  itemFavDOs;
    public TextView btn_go_cart, btn_go_check_out, tvFavouriteList;
    private double subtotal;
    private double subtotalVendor;
    private double vatTax;
    private double vatTaxVendor;
    private double GrandTotal;
    private double TotalTaxValue;
    private double GrandTotalVendor;
    private double TotalTaxValueVendor;
    private double deliveryCharges = 4.99;
    private double deliveryChargesVendor = 4.99;
    public int favCount, cartCount;
    public Animation animRotate;

    protected PreferenceUtils preferenceUtils;
    private PopupWindow popupWindow, popupRefineFilter;
    private ArrayList<FilterHeadingDO> filterheadings                   = new ArrayList<FilterHeadingDO>();
    private ArrayList<FilternameAndIDDo> SelectedSizeFilterList         = new ArrayList<>();
    private ArrayList<FilternameAndIDDo> SelectedPatternFilterList      = new ArrayList<>();
    private ArrayList<FilternameAndIDDo> SelectedMaterialFilterList     = new ArrayList<>();
    private ArrayList<FilternameAndIDDo> SelectedColorFilterList        = new ArrayList<>();
    private ArrayList<FilternameAndIDDo> SelectedSizeGroupFilterList    = new ArrayList<>();
    private ArrayList<FilternameAndIDDo> SelectedPriceFilterList        = new ArrayList<>();
    private ArrayList<FilternameAndIDDo> SelectedCategoryFilterList     = new ArrayList<>();
    private ArrayList<FilternameAndIDDo> SelectedRunnerFilterList       = new ArrayList<>();
    private ArrayList<FilternameAndIDDo> SelectedFeelerFilterList       = new ArrayList<>();


    @Override
    public void getCartList(ItemsDO model) {

    }

    @Override
    public void getLocalDatabaseCartList() {
        itemCartsDOs = CartDatabase.getAllCartList();
        cartCount = CartDatabase.getCartlistCount();
        tvCartCount.setText("" + cartCount);
        if(cartCount > 0){
            tvCartCount.setVisibility(View.VISIBLE);
            tvCartCount.startAnimation(animRotate);
            cartHomeAdapter.refresh(itemCartsDOs);
        }
        else {
            cartClicked = false;
            view.animate().translationY(-view.getHeight()).setDuration(300);
            tvCartCount.setVisibility(View.GONE);
        }

    }

    @Override
    public void getTheIncreasedSubtotal(Double itemFinalPrice, Double itemVendorPrice) {

    }

    @Override
    public void getTheDecreasedSubtotal(Double itemFinalPrice, Double itemVendorPrice) {

    }

    public void getLocalDatabaseFavList() {
        itemFavDOs = CartDatabase.getAllFavouriteList();
        if(itemFavDOs!=null)
            favCount = itemFavDOs.size();

        tvFavouriteCount.setText("" + favCount);
        if(favCount > 0)
        {
            tvFavouriteCount.setVisibility(View.VISIBLE);
            favouriteHomeAdapter.refresh(itemFavDOs);
        }
        else {
            cartClicked = false;
            llFavourite.animate().translationY(-llFavourite.getHeight()).setDuration(300);
            tvFavouriteCount.setVisibility(View.GONE);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.savedInstanceState = savedInstanceState;
        setContentView(R.layout.base_activity);
        CartDatabase.init(this);
        mContext = BaseActivity.this;
        preferenceUtils = new PreferenceUtils(mContext);
        animRotate = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.rotate);
        inflater = this.getLayoutInflater();
        InitializeControls();
        getLocalDatabaseFavList();
        view.setTranslationY(-1 * 1000);


        btn_go_cart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cartClicked = false;
                view.animate().translationY(-view.getHeight()).setDuration(300);
                if(!(BaseActivity.this instanceof CartActivity))
                    gotoCartActivity();
            }
        });

        btn_go_check_out.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cartClicked = false;
                view.animate().translationY(-view.getHeight()).setDuration(300);
                if(!(BaseActivity.this instanceof OrderActivity))
                gotoCheckOut();
            }
        });
        tvFavouriteList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!(BaseActivity.this instanceof FavouriteActivity))
                {
                    favClicked = false;
                    llFavourite.animate().translationY(-llFavourite.getHeight()).setDuration(300);
                    Intent intent= new Intent(BaseActivity.this, FavouriteActivity.class);
                    startActivity(intent);
                    overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
                }
            }
        });
       /* if(mtoolbar!=null)
        {
            setSupportActionBar(mtoolbar);

        }*/

        /*try
        {
            getSupportActionBar().setHomeButtonEnabled(true);
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }*/


//        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        //   getSupportActionBar().setDisplayShowTitleEnabled(false);

        initialize();
        if (!currentClass.equalsIgnoreCase(CartActivity.class.getSimpleName())
                && !currentClass.equalsIgnoreCase(OrderActivity.class.getSimpleName())) {
            base_filter.setVisibility(View.VISIBLE);
            ll_search_parent.setVisibility(View.VISIBLE);
            frm_color_selection.setVisibility(View.VISIBLE);
        } else {
            base_filter.setVisibility(View.INVISIBLE);
            ll_search_parent.setVisibility(View.INVISIBLE);
            frm_color_selection.setVisibility(View.INVISIBLE);
        }

        filterheadings.add(new FilterHeadingDO("Colour", SelectedColorFilterList.size(), true));
        filterheadings.add(new FilterHeadingDO("Price", SelectedPriceFilterList.size(), false));
        filterheadings.add(new FilterHeadingDO("Material", SelectedMaterialFilterList.size(), false));
        filterheadings.add(new FilterHeadingDO("Pattern", SelectedPatternFilterList.size(), false));
        filterheadings.add(new FilterHeadingDO("SizeGroup", SelectedSizeGroupFilterList.size(), false));
        filterheadings.add(new FilterHeadingDO("Size", SelectedSizeFilterList.size(), false));
        filterheadings.add(new FilterHeadingDO("Categories", SelectedCategoryFilterList.size(), false));
        filterheadings.add(new FilterHeadingDO("Runners", SelectedRunnerFilterList.size(), false));

//        base_filter.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                showRefineFilterPopup(v);
//            }
//        });
        cartCount = CartDatabase.getCartlistCount();
        tvCartCount.setText(""+cartCount);
        if(cartCount>0){
            tvCartCount.setVisibility(View.VISIBLE);
        }
        else {
            tvCartCount.setVisibility(View.GONE);
        }
        img_home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!(BaseActivity.this instanceof Dashboard))
                {
                    Intent dashBoardIntent = new Intent(BaseActivity.this, Dashboard.class);
                    dashBoardIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(dashBoardIntent);
                    overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
                }
            }
        });

        ivFavourite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cartClicked = false;
                view.animate().translationY(-view.getHeight()).setDuration(300);

                getLocalDatabaseFavList();
                favouriteHomeAdapter.refresh(itemFavDOs);
                if (!favClicked)
                {
                    if(favCount > 0){
                        llFavourite.setVisibility(View.VISIBLE);
                        favClicked = true;
                        llFavourite.animate().translationY(toolbar.getHeight()).setDuration(300);
                    }
                    else {
                        showToast("No items in Favourite List");
                    }
                }
                else {
                    favClicked = false;
                    llFavourite.animate().translationY(-llFavourite.getHeight()).setDuration(300);
                }

            }
        });
        ivCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // gotoCartActivity();
                //int interval= Float.compare(view.getTranslationX(),80);
                favClicked = false;
                llFavourite.animate().translationY(-llFavourite.getHeight()).setDuration(300);

                    if ((!cartClicked))
                    {
                        if(cartCount > 0){
                            view.setVisibility(View.VISIBLE);
                            cartClicked = true;
                            view.animate().translationY(toolbar.getHeight()).setDuration(300);
                        }
                        else {
                            showToast("No items in Cart");
                        }
                    }
                    else {
                        cartClicked = false;
                        view.animate().translationY(-view.getHeight()).setDuration(300);
                    }
            }
        });

        ArrayList<ItemsDO> itemsDOs = CartDatabase.getAllFavouriteList();
        if(itemsDOs!=null && itemsDOs.size()>0) {
            favCount = itemsDOs.size();
        }
        if(favCount>0){
            tvFavouriteCount.setText(""+favCount);
            tvFavouriteCount.setVisibility(View.VISIBLE);
            ivFavourite.setImageResource(R.mipmap.fav_disabled);
        }
        else {
            tvFavouriteCount.setVisibility(View.GONE);
            ivFavourite.setImageResource(R.mipmap.fav_disabled);
        }

        frm_color_selection.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                showColorFilters(v);
            }
        });
    }

    private RefineFilterHeadingAdapter refineFilterHeadingAdapter;
    private void showRefineFilterPopup(View view){
        View refineFilterView = inflater.inflate(R.layout.popup_refine_filter_layout, null);
        if(popupRefineFilter == null)
            popupRefineFilter                   = new PopupWindow(refineFilterView, 600, ViewGroup.LayoutParams.WRAP_CONTENT);
        final ListView lvFilterParents                = (ListView) refineFilterView.findViewById(R.id.lvFilterParents);
        popupRefineFilter.setOutsideTouchable(true);
        lvFilterParents.setAdapter(refineFilterHeadingAdapter = new RefineFilterHeadingAdapter(mContext, filterheadings, new FilterClear(){
            @Override
            public void clearFilter(FilterHeadingDO filterHeadingDO) {

            }
        }));

//        lvFilterParents.setAdapter(refineFilterAdapter = new RefineFilterAdapter(mContext, popupRefineFilter));

        lvFilterParents.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                FilterHeadingDO model = (FilterHeadingDO) parent.getItemAtPosition(position);

                for (int i = 0; i < filterheadings.size(); i++) {
                    filterheadings.get(i).selected = false;
                }
                model.selected = true;
//                getTheFilterMetadata(model.filterName, position);
                showRefineFilterChildPopup(popupRefineFilter.getContentView());

                refineFilterHeadingAdapter.notifyDataSetChanged();
            }
        });

        if(!popupRefineFilter.isShowing())
            popupRefineFilter.showAsDropDown(view);//, 50, -30);
        else
            popupRefineFilter.dismiss();
    }

    private PopupWindow filterChildPopup;
    private void showRefineFilterChildPopup(View view){
        View refineFilterView = inflater.inflate(R.layout.popup_refine_filter_layout, null);
        if(filterChildPopup == null)
            filterChildPopup                    = new PopupWindow(refineFilterView, 600, ViewGroup.LayoutParams.WRAP_CONTENT);
        ListView lvFilterChilds                 = (ListView) refineFilterView.findViewById(R.id.lvFilterParents);
        filterChildPopup.setOutsideTouchable(true);
        lvFilterChilds.setAdapter(refineFilterHeadingAdapter = new RefineFilterHeadingAdapter(mContext, filterheadings, new FilterClear(){
            @Override
            public void clearFilter(FilterHeadingDO filterHeadingDO) {

            }
        }));

//        lvFilterParents.setAdapter(refineFilterAdapter = new RefineFilterAdapter(mContext, popupRefineFilter));

        lvFilterChilds.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                FilterHeadingDO model = (FilterHeadingDO) parent.getItemAtPosition(position);

                for (int i = 0; i < filterheadings.size(); i++) {
                    filterheadings.get(i).selected = false;
                }
                model.selected = true;


                refineFilterHeadingAdapter.notifyDataSetChanged();
            }
        });

        if(!filterChildPopup.isShowing()){
            view.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
            filterChildPopup.showAsDropDown(view, view.getWidth() - view.getMeasuredWidth(), 0);

        }
        else
            filterChildPopup.dismiss();

    }

    private class RefineFilterAdapter extends BaseAdapter{

        private Context context;
        private PopupWindow popupRefineFilter;
        public RefineFilterAdapter(Context context, PopupWindow popupRefineFilter){
            this.context = context;
            this.popupRefineFilter = popupRefineFilter;
        }
        @Override
        public int getCount() {
            return 0;
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            return null;
        }
    }
    private ColorFilterAdapter colorFilterAdapter;
    private void showColorFilters(View view){
        View popupView = inflater.inflate(R.layout.drop_down_list_view, null);
        if(popupWindow == null)
            popupWindow                         = new PopupWindow(popupView, 480, 280);
        RecyclerView rvFilterItems              = (RecyclerView) popupView.findViewById(R.id.rvFilterItems);
        TextView tvApply                        = (TextView)popupView.findViewById(R.id.tvApply);
        tvApply.setVisibility(View.GONE);
        popupWindow.setOutsideTouchable(true);
        rvFilterItems.setLayoutManager(new GridLayoutManager(mContext, 2, GridLayoutManager.VERTICAL, false));
        rvFilterItems.setAdapter(colorFilterAdapter = new ColorFilterAdapter(mContext, popupWindow));

        if(!popupWindow.isShowing())
            popupWindow.showAsDropDown(view);//, 50, -30);
        else
            popupWindow.dismiss();

    }

    private class ColorFilterAdapter extends RecyclerView.Adapter<ViewHolder>{

        private PopupWindow popupWindow;
        private Context context;
        public ColorFilterAdapter(Context context, PopupWindow popupWindow){
            this.context     = context;
            this.popupWindow = popupWindow;
        }
        private void refresh(ArrayList<ColorsDO> colorsDOs){
            notifyDataSetChanged();
        }
        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View convertView = LayoutInflater.from(context).inflate(R.layout.spinner_cell_layout, parent, false);
            return new ViewHolder(convertView);
        }

        @Override
        public void onBindViewHolder(final ViewHolder holder, final int position) {

            holder.tvItemCell.setText(AppConstants.colorsDOs.get(position).ColorName);
            holder.ivSelected.setVisibility(View.INVISIBLE);

            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(!(BaseActivity.this instanceof ItemsActivity)){
                        Intent intent = new Intent(BaseActivity.this, ItemsActivity.class);
                        intent.putExtra("CatgId", AppConstants.colorsDOs.get(position).ColorId);
                        intent.putExtra("CatgName", AppConstants.colorsDOs.get(position).ColorName);
                        startActivity(intent);
                    }
                    else if((BaseActivity.this instanceof ItemsActivity)){
                        Intent intent = new Intent(BaseActivity.this, ItemsActivity.class);
                        intent.putExtra("CatgId", AppConstants.colorsDOs.get(position).ColorId);
                        intent.putExtra("CatgName", AppConstants.colorsDOs.get(position).ColorName);
                        startActivity(intent);
                        finish();
                    }
                    popupWindow.dismiss();
                }
            });
        }

        @Override
        public int getItemCount() {
            return AppConstants.colorsDOs!=null?AppConstants.colorsDOs.size():0;
        }
    }
    private static class ViewHolder extends RecyclerView.ViewHolder{
        private TextView tvItemCell;
        private ImageView ivSelected;
        public ViewHolder(View itemView) {
            super(itemView);
            tvItemCell          = (TextView) itemView.findViewById(R.id.tvItemCell);
            ivSelected          = (ImageView) itemView.findViewById(R.id.ivSelected);
        }
    }


    private void gotoCheckOut() {
        Context context = BaseActivity.this;
        Intent dashBoardIntent = new Intent(context, OrderActivity.class);
        startActivity(dashBoardIntent);
    }

    public void gotoCartActivity() {
        Context context = BaseActivity.this;
        Intent dashBoardIntent = new Intent(context, CartActivity.class);
        startActivity(dashBoardIntent);
    }

    public abstract void initialize();

    private FavouriteItemsAdapter favouriteItemsAdapter;
    private void showFavouriteItems(final Context context, ArrayList<ItemsDO> itemsDOs){
        View popupView = inflater.inflate(R.layout.favourite_item_list_dialog, null);
        if(popupWindow == null)
            popupWindow                         = new PopupWindow(popupView, ViewGroup.LayoutParams.WRAP_CONTENT, 500);
        RecyclerView rvFilterItems              = (RecyclerView) popupView.findViewById(R.id.rvFavouriteItems);
        Button btnViewAll                       = (Button)popupView.findViewById(R.id.btnViewAll);
        popupWindow.setOutsideTouchable(true);
        rvFilterItems.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false));
        rvFilterItems.setAdapter(favouriteItemsAdapter = new FavouriteItemsAdapter(mContext, itemsDOs));

        btnViewAll.setOnClickListener(new Button.OnClickListener(){

            @Override
            public void onClick(View v) {
                Intent dashBoardIntent = new Intent(context, FavouriteActivity.class);
                startActivity(dashBoardIntent);
                overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
                popupWindow.dismiss();
            }});

        if(!popupWindow.isShowing())
            popupWindow.showAsDropDown(view);//, 50, -30);
//        else
//            popupWindow.dismiss();

    }

    private class FavouriteItemsAdapter extends RecyclerView.Adapter<ColorViewHolder>{
        private ImageLoader imgLoader = ImageLoader.getInstance();
        private Context context;
        private ArrayList<ItemsDO> itemsDOs;
        public FavouriteItemsAdapter(Context context, ArrayList<ItemsDO> itemsDOs){
            this.context     = context;
            this.itemsDOs    = itemsDOs;
        }
        private void refresh(ArrayList<ColorsDO> colorsDOs){
            notifyDataSetChanged();
        }
        @Override
        public ColorViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View convertView = LayoutInflater.from(context).inflate(R.layout.row_cart_home, parent, false);
            return new ColorViewHolder(convertView);
        }

        @Override
        public void onBindViewHolder(final ColorViewHolder holder, final int position) {
            holder.itemView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
            holder.tvItemName.setText(itemsDOs.get(position).ItemName);
            holder.tvItemPrice.setText("£" + itemsDOs.get(position).ItemFinalPrice);
            holder.ivCancel.setVisibility(View.GONE);
            imgLoader.getInstance().displayImage(itemsDOs.get(position).ItemBigImgUrl, holder.ivItemImage);
        }

        @Override
        public int getItemCount() {
            return itemsDOs!=null?itemsDOs.size():0;
        }
    }
    private static class ColorViewHolder extends RecyclerView.ViewHolder{
        TextView tvItemName, tvItemPrice;
        ImageView ivItemImage, ivCancel;

        public ColorViewHolder(View itemView) {
            super(itemView);
            tvItemName           = (TextView) itemView.findViewById(R.id.tv_item_name);
            tvItemPrice          = (TextView) itemView.findViewById(R.id.tv_item_price);
            ivCancel             = (ImageView) itemView.findViewById(R.id.img_cancel);
            ivItemImage          = (ImageView) itemView.findViewById(R.id.img_cart_home);
        }
    }

    private void InitializeControls() {

        ll_search_parent                = (LinearLayout) findViewById(R.id.ll_search_parent);
        frm_color_selection             = (FrameLayout) findViewById(R.id.frm_color_selection);
        base_filter                     = (LinearLayout) findViewById(R.id.base_filter);
        img_home                        = (ImageView) findViewById(R.id.img_home);
        ivCart                          = (ImageView) findViewById(R.id.ivCart);
        ivFavourite                     = (ImageView) findViewById(R.id.ivFavourite);
        tvFavouriteCount                = (TextView) findViewById(R.id.tvFavouriteCount);
        tvCartCount                     = (TextView) findViewById(R.id.tvCartCount);
        mtoolbar                        = (LinearLayout) findViewById(R.id.toolbar);
        tv_grand_total                  = (TextView) findViewById(R.id.tv_grand_total);
        toolbar                         = (LinearLayout) findViewById(R.id.toolbar);
        btn_go_cart                     = (TextView) findViewById(R.id.btn_go_cart);
        btn_go_check_out                = (TextView) findViewById(R.id.btn_go_check_out);
        tvFavouriteList                 = (TextView) findViewById(R.id.tvFavouriteList);
        lst_cart_items                  = (ListView) findViewById(R.id.lst_cart_items);
        lvFavourite                     = (ListView) findViewById(R.id.lvFavourite);
        view                            = (LinearLayout) findViewById(R.id.view);
        llFavourite                     = (LinearLayout) findViewById(R.id.llFavourite);
        llContent                       = (LinearLayout) findViewById(R.id.llContent);
        txt_actionbar                   = (TextView) findViewById(R.id.txt_actionbar);

        itemCartsDOs                    = CartDatabase.getAllCartList();

        cartHomeAdapter                 = new CartHomeAdapter(itemCartsDOs, BaseActivity.this, this);
        lst_cart_items.setAdapter(cartHomeAdapter);

        favouriteHomeAdapter            = new FavouriteHomeAdapter(itemFavDOs, BaseActivity.this, this);
        lvFavourite.setAdapter(favouriteHomeAdapter);

        calculateSubtotalPrice(itemCartsDOs);
       /* Shader textShader = new LinearGradient(0, 0, 50, 0,
                new int[]{Color.GRAY, Color.WHITE},
                new float[]{0, 0.5f}, Shader.TileMode.MIRROR);
        txt_actionbar.getPaint().setShader(textShader);*/

        new FontType(mContext).applyfonttoView(txt_actionbar, AppConstants.TRAJANPRO);

    }

    private void calculateTax(double subtotal, Double subtotalVendor) {
        vatTax = subtotal * 0.2;
        // txt_vattax.setText("£"+String.format("%.2f", vatTax));
        vatTaxVendor = subtotalVendor * 0.2;

    }

    private void calculateGrandTotal(double subtotal, double vatTax, double deliveryCharges, Double subtotalVendor, double vatTaxVendor, Double deliveryChargesVendor) {
        GrandTotal = subtotal + vatTax + deliveryCharges;
        TotalTaxValue = vatTax + deliveryCharges;

        GrandTotalVendor = subtotalVendor + vatTaxVendor + deliveryChargesVendor;
        TotalTaxValueVendor = vatTaxVendor + deliveryChargesVendor;

//        tv_grand_total.setText("£" + String.format("%.2f", GrandTotal));
        calcCartTotalAmount();

    }

    public void calcCartTotalAmount(){
        double total = 0;
        ArrayList<ItemCartsDO> itemCartsDOs = CartDatabase.getAllCartList();
        if(itemCartsDOs!=null && itemCartsDOs.size()>0){
            for (ItemCartsDO itemCartsDO : itemCartsDOs)
            {
                total = total+ StringUtils.getDouble(itemCartsDO.ItemFinalPrice);
            }
            tv_grand_total.setText("£" + String.format("%.2f", total));
        }
        else {
            tv_grand_total.setText("£"+total);
        }
    }

    private void calculateSubtotalPrice(ArrayList<ItemCartsDO> newCartList) {
        double tempsubtotal = 0;
        double tempVendorSubTotal = 0;
        for (int i = 0; i < newCartList.size(); i++) {
            ItemCartsDO model = newCartList.get(i);
            tempsubtotal = model.ItemQuantity * Double.parseDouble(model.ItemFinalPrice) + tempsubtotal;
            tempVendorSubTotal = model.ItemQuantity * Double.parseDouble(model.ItemBasePrice) + tempsubtotal;

        }
        subtotal = tempsubtotal;
        subtotalVendor = tempVendorSubTotal;

        //txt_subtotal.setText("£"+String.format("%.2f", subtotal));

        calculateTax(subtotal, subtotalVendor);

        calculateGrandTotal(subtotal, vatTax, deliveryCharges, subtotalVendor, vatTaxVendor, deliveryChargesVendor);


        if (subtotal < 50) {

            // txt_deliveryCharge.setVisibility(View.VISIBLE);
            //txt_deliveryCharge.setText("£"+String.format("%.2f",deliveryCharges));
        } else {
            // txt_deliveryCharge.setVisibility(View.GONE);
            // calculateGrandTotal(subtotal,vatTax,0, subtotalVendor, vatTaxVendor,0d);
        }
    }

    public void showAlertDialog(String strMessage, String firstBtnName) {
        runOnUiThread(new RunshowCustomDialogs(strMessage, firstBtnName));
    }


    class RunshowCustomDialogs implements Runnable {
        private int imageid = R.mipmap.caution;
        private String strMessage;// Message to be shown in dialog
        private String firstBtnName;
        private int titleGravity;
        private boolean isShowNestedDialog;
        private String dialogFrom;
        Drawable drawable;

        public RunshowCustomDialogs(String strMessage, String firstBtnName) {
            this.strMessage = strMessage;
            this.firstBtnName = firstBtnName;
        }

        public RunshowCustomDialogs(String strMessage, String firstBtnName, int imageid) {
            this.strMessage = strMessage;
            this.firstBtnName = firstBtnName;
            this.imageid = imageid;
        }


        @Override
        public void run() {
            closeAlertDialog();
            alertBuilder = new AlertDialog.Builder(mContext);
            alertBuilder.setCancelable(true);

            final LinearLayout linearLayout = (LinearLayout) getLayoutInflater().inflate(R.layout.notification_dailog, null);

            TextView dialogtvTitle = (TextView) linearLayout.findViewById(R.id.tvTitle);
            TextView btnYes = (TextView) linearLayout.findViewById(R.id.btnYes);
            ImageView img_alerttype = (ImageView) linearLayout.findViewById(R.id.img_alerttype);

            Drawable drawable = ContextCompat.getDrawable(mContext, imageid);
            img_alerttype.setImageDrawable(drawable);

            if (titleGravity != 0) {
                // Only in the case of Crash Report Dialog, i am customizing it with custom padding.
                dialogtvTitle.setGravity(titleGravity);
                dialogtvTitle.setPadding(35, 35, 0, 35);

            }
            dialogtvTitle.setText(strMessage);
            btnYes.setText(firstBtnName);
            btnYes.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    alertDialog.cancel();

                }
            });

            try {
                alertDialog = alertBuilder.create();
                alertDialog.setView(linearLayout, 0, 0, 0, 0);
                alertDialog.setInverseBackgroundForced(true);
                alertDialog.show();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


    public void closeAlertDialog() {
        if (alertDialog != null && alertDialog.isShowing())
            alertDialog.dismiss();
    }

    public void hideKeyBoard(View v) {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
    }

    public void showLoaderNew() {
        runOnUiThread(new Runloader(getResources().getString(R.string.loading)));
    }

    class Runloader implements Runnable {
        private String strrMsg;

        public Runloader(String strMsg) {
            this.strrMsg = strMsg;
        }

        @Override
        public void run() {
            try {
                if (dialog == null) {
                    dialog = new Dialog(mContext, R.style.Theme_Dialog_Translucent);
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    dialog.getWindow().setBackgroundDrawable(
                            new ColorDrawable(android.graphics.Color.TRANSPARENT));
                }
                dialog.setContentView(R.layout.loading);
                dialog.setCancelable(false);

                if (dialog != null && dialog.isShowing()) {
                    dialog.dismiss();
                    dialog = null;
                }
                dialog.show();

                ImageView imgeView = (ImageView) dialog.findViewById(R.id.imgeView);
                TextView tvLoading = (TextView) dialog.findViewById(R.id.tvLoading);
                if (!strrMsg.equalsIgnoreCase(""))
                    tvLoading.setText(strrMsg);

                imgeView.setBackgroundResource(R.anim.frame);

                animationDrawable = (AnimationDrawable) imgeView
                        .getBackground();
                imgeView.post(new Runnable() {
                    @Override
                    public void run() {
                        if (animationDrawable != null)
                            animationDrawable.start();
                    }
                });
            } catch (Exception e) {

            }
        }
    }

    public void hideloader() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                try {
                    if (dialog != null && dialog.isShowing())
                        dialog.dismiss();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

//    ----------------------------------------------------written kishore----------------

    protected void showSnackBar(View view, String message){
        Snackbar.make(view, message,Snackbar.LENGTH_LONG).show();
    }
    protected void showToast(String message){
        Toast.makeText(mContext, message, Toast.LENGTH_LONG).show();
    }
    public boolean isNetworkConnectionAvailable(Context context)
    {
        boolean isNetworkConnectionAvailable = false;
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();

        if (activeNetworkInfo != null) {
            isNetworkConnectionAvailable = activeNetworkInfo.getState() == NetworkInfo.State.CONNECTED;
        }

        return isNetworkConnectionAvailable;
    }
    public static final Pattern EMAIL_ADDRESS_PATTERN = Pattern.compile(
            "[a-zA-Z0-9\\+\\.\\_\\%\\-\\+]{1,256}" +
                    "\\@" +
                    "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}" +
                    "(" +
                    "\\." +
                    "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25}" +
                    ")+"
    );
//    protected boolean isValidEmail(String email) {
//        return EMAIL_ADDRESS_PATTERN.matcher(email).matches();
//    }


    protected void setTypeFace(ViewGroup viewGroup, String font)
    {
        Typeface tf = Typeface.createFromAsset(getAssets(),font);
        for(int i=0;i<viewGroup.getChildCount();i++)
        {
            View v = viewGroup.getChildAt(i);
            if(v instanceof  TextView || v instanceof EditText || v instanceof Button)
            {
                setTypeFace(v,AppConstants.FONT_LATO_BLACK);
            }
            else if(v instanceof ViewGroup)
            {
                ViewGroup vg= (ViewGroup) v;
                setTypeFace(vg,AppConstants.FONT_LATO_BLACK);
            }
        }
    }

    protected void setTypeFace(View v, String font)
    {
        Typeface tf = Typeface.createFromAsset(getAssets(),font);
        if(v instanceof  TextView)
        {
            TextView tv = (TextView) v;
            tv.setTypeface(tf);
        }
        else if(v instanceof  EditText)
        {
            EditText et = (EditText) v;
            et.setTypeface(tf);
        }
        else if(v instanceof  Button)
        {
            Button btn = (Button) v;
            btn.setTypeface(tf);
        }
    }


    // common color filter


//    private ColorFilterAdapter colorFilterAdapter;
//    private void showColorFilters(View view){
//        View popupView = inflater.inflate(R.layout.drop_down_list_view, null);
//        if(popupWindow == null)
//            popupWindow                         = new PopupWindow(popupView, ViewGroup.LayoutParams.WRAP_CONTENT, 500);
//        RecyclerView rvFilterItems              = (RecyclerView) popupView.findViewById(R.id.rvFilterItems);
//        TextView tvApply                        = (TextView)popupView.findViewById(R.id.tvApply);
//        popupWindow.setOutsideTouchable(true);
//        rvFilterItems.setLayoutManager(new GridLayoutManager(mContext, 2, GridLayoutManager.VERTICAL, false));
//        rvFilterItems.setAdapter(colorFilterAdapter = new ColorFilterAdapter(mContext));
//        tvApply.setOnClickListener(new Button.OnClickListener(){
//
//            @Override
//            public void onClick(View v) {
//                ArrayList<String> selectedColors = new ArrayList<String>();
////                SelectedColorFilterList.clear();
//                for(int i=0; i<AppConstants.colorsDOs.size(); i++){
//                    if(AppConstants.colorsDOs.get(i).isSelected){
//                        selectedColors.add(AppConstants.colorsDOs.get(i).ColorId);
//                        FilternameAndIDDo filternameAndIDDo = new FilternameAndIDDo();
//                        filternameAndIDDo.id                = AppConstants.colorsDOs.get(i).ColorId;
//                        filternameAndIDDo.name              = AppConstants.colorsDOs.get(i).ColorName;
////                        SelectedColorFilterList.add(filternameAndIDDo);
//                    }
//                }
//
//                if(isNetworkConnectionAvailable(BaseActivity.this)){
//                    showLoaderNew();
//                    new CommonBL(BaseActivity.this, new DataListener() {
//                        @Override
//                        public void dataRetreived(Response data) {
//                            hideloader();
//
//
//                        }
//                    }).getFilteredProducts(selectedSizeFilterList, selectedPatternFilterList,
//                            selectedMaterialFilterList, selectedColorFilterList, selectedSizeGroupFilterList, selectedPriceFilterList,SelectedFeelerFilterList,
//                            selectedCategoryFilterList, selectedRunnerFilterList, preferenceUtils.getUserDetails().get(AppConstants.KEY_USER_EMAIL)));
//                }
//                popupWindow.dismiss();
//            }});
//
//        if(!popupWindow.isShowing())
//            popupWindow.showAsDropDown(view);//, 50, -30);
//        else
//            popupWindow.dismiss();
//
//    }

//    private class ColorFilterAdapter extends RecyclerView.Adapter<ViewHolders>{
//
//        private Context context;
//        public ColorFilterAdapter(Context context){
//            this.context     = context;
//        }
//        private void refresh(ArrayList<ColorsDO> colorsDOs){
//            notifyDataSetChanged();
//        }
//        @Override
//        public ViewHolders onCreateViewHolder(ViewGroup parent, int viewType) {
//            View convertView = LayoutInflater.from(context).inflate(R.layout.spinner_cell_layout, parent, false);
//            return new ViewHolders(convertView);
//        }
//
//        @Override
//        public void onBindViewHolder(final ViewHolders holder, final int position) {
//
//            holder.tvItemCell.setText(AppConstants.colorsDOs.get(position).ColorName);
//            if(AppConstants.colorsDOs.get(position).isSelected){
//                holder.ivSelected.setVisibility(View.VISIBLE);
//            }
//            else {
//                holder.ivSelected.setVisibility(View.INVISIBLE);
//            }
//            holder.tvItemCell.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    if(AppConstants.colorsDOs.get(position).isSelected){
//                        AppConstants.colorsDOs.get(position).isSelected = false;
//                        holder.ivSelected.setVisibility(View.INVISIBLE);
//                    }
//                    else {
//                        AppConstants.colorsDOs.get(position).isSelected = true;
//                        holder.ivSelected.setVisibility(View.VISIBLE);
//                    }
////                    notifyDataSetChanged();
//                }
//            });
//        }
//
//        @Override
//        public int getItemCount() {
//            return AppConstants.colorsDOs!=null?AppConstants.colorsDOs.size():0;
//        }
//    }
//    private static class ViewHolders extends RecyclerView.ViewHolder{
//        private TextView tvItemCell;
//        private ImageView ivSelected;
//        public ViewHolders(View itemView) {
//            super(itemView);
//            tvItemCell          = (TextView) itemView.findViewById(R.id.tvItemCell);
//            ivSelected          = (ImageView) itemView.findViewById(R.id.ivSelected);
//        }
//    }
//

}



