package com.yits.rugshop.activity;

import android.app.ActionBar;
import android.content.Context;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.yits.rugshop.R;
import com.yits.rugshop.adapter.CartHomeAdapter;
import com.yits.rugshop.adapter.FavouriteAdapter;
import com.yits.rugshop.listener.FavouriteListListener;
import com.yits.rugshop.listener.UpdateListView;
import com.yits.rugshop.objects.ItemCartsDO;
import com.yits.rugshop.objects.ItemfavouriteDO;
import com.yits.rugshop.objects.ItemsDO;
import com.yits.rugshop.objects.SizeListDO;
import com.yits.rugshop.utilities.AppConstants;
import com.yits.rugshop.utilities.CartDatabase;
import com.yits.rugshop.utilities.PreferenceUtils;

import java.util.ArrayList;
import java.util.HashMap;

public class FavouriteActivity extends BaseActivity implements UpdateListView, FavouriteListListener {

    LinearLayout ll_Cart;
    Context mContext;
    TextView tvCartNoData;
    ListView lst_fav;
    PreferenceUtils preferenceUtils;
    public HashMap<String, String> Userdata;
    public String UserShopName, UserPkID, queryParams;
    private FavouriteAdapter favouriteAdapter;
    private TextView txt_unitPrice, txt_multipliedPrice;

    @Override
    public void initialize() {

        ll_Cart = (LinearLayout) inflater.inflate(R.layout.favourite_screen, null);
        llContent.addView(ll_Cart, new LinearLayout.LayoutParams(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.MATCH_PARENT));
        mContext = FavouriteActivity.this;
        preferenceUtils = new PreferenceUtils(mContext);
        CartDatabase.init(this);
        initializeControls();
        itemFavDOs = new ArrayList<>();
        Userdata = preferenceUtils.getUserDetails();
        UserPkID = Userdata.get(AppConstants.KEY_USER_ID);
        UserShopName = Userdata.get(AppConstants.KEY_USER_FST_NAME);
        queryParams = Userdata.get(AppConstants.KEY_ACCESS_TOKEN);
        txt_actionbar.setText(UserShopName);

        getLocalDatabaseFavList();
        tvCartCount.setText("" + CartDatabase.getCartlistCount());
        favouriteAdapter = new FavouriteAdapter(mContext, itemFavDOs);
        favouriteAdapter.registerLocaldatabaseListener(this);

        lst_fav.setAdapter(favouriteAdapter);

       /* img_cart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Toast.makeText(mContext, "Cart Clicked", Toast.LENGTH_SHORT).show();
            }
        });*/

        /*img_home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Toast.makeText(mContext, "Home  Clicked", Toast.LENGTH_SHORT).show();
            }
        });*/
    }

    private void initializeControls() {
        txt_multipliedPrice     = (TextView) findViewById(R.id.txt_multipliedPrice);
        txt_unitPrice           = (TextView) findViewById(R.id.txt_unitPrice);
        lst_fav                 = (ListView) findViewById(R.id.lst_fav);
        tvCartNoData = (TextView) findViewById(R.id.tvCartNoData);
        txt_unitPrice.setVisibility(View.GONE);
        txt_multipliedPrice.setText("Price");
    }


    @Override
    public void update() {
        getLocalDatabaseFavList();
        favouriteAdapter = new FavouriteAdapter(mContext, itemFavDOs);
        favouriteAdapter.registerLocaldatabaseListener(this);
        lst_fav.setAdapter(favouriteAdapter);
        favCount = itemFavDOs!=null?itemFavDOs.size():0;
        if(favCount>0){
            tvFavouriteCount.setText(""+favCount);
            tvFavouriteCount.setVisibility(View.VISIBLE);
            ivFavourite.setImageResource(R.mipmap.fav_disabled);
        }
        else {
            tvFavouriteCount.setVisibility(View.GONE);
            ivFavourite.setImageResource(R.mipmap.fav_disabled);
        }
    }

    @Override
    public void addTocart(ItemsDO itemsDO) {
        CartDatabase.addCartData(new ItemCartsDO(itemsDO.ItemId, itemsDO.ProductID, itemsDO.ItemName, itemsDO.ItemQuantity, itemsDO.ItemBasePrice, itemsDO.ItemDsicountedPrice, itemsDO.ItemFinalPrice, itemsDO.ItemSize, itemsDO.ItemColor, itemsDO.ItemPattern, itemsDO.ItemMaterial,
                itemsDO.ItemSmallImgUrl, itemsDO.ItemMediumImgUrl, itemsDO.ItemBigImgUrl, itemsDO.ItemStatus, itemsDO.ItemRating, itemsDO.ItemPerSaved, itemsDO.catogiryname));

        CartDatabase.addColorData(itemsDO.ItemId, itemsDO.itemColorList);
        for (int i = 0; i < itemsDO.itemSizeList.size(); i++) {
            CartDatabase.addSizeData(new SizeListDO(itemsDO.ItemId, itemsDO.itemSizeList.get(i).ItemSize, itemsDO.itemSizeList.get(i).BasePrice, itemsDO.itemSizeList.get(i).DiscountedPrice, itemsDO.itemSizeList.get(i).FinalPrice));
        }

        getCartListNormal();
    }

    public void getCartListNormal() {
        cartCount = CartDatabase.getCartlistCount();
        itemCartsDOs = CartDatabase.getAllCartList();
        if(cartCount > 0){
            tvCartCount.setVisibility(View.VISIBLE);
            tvCartCount.setText("" + cartCount);
            tvCartCount.startAnimation(animRotate);
            cartHomeAdapter = new CartHomeAdapter(itemCartsDOs, FavouriteActivity.this, this);
            lst_cart_items.setAdapter(cartHomeAdapter);
        }
        else {
            tvCartCount.setVisibility(View.GONE);
        }
    }

    @Override
    public void getFavouriteList(ArrayList<ItemfavouriteDO> itemFavouriteList) {
        ArrayList<ItemsDO> itemsDOs = CartDatabase.getAllFavouriteList();
        if(itemsDOs!=null && itemsDOs.size()>0)
            favCount = itemsDOs.size();
        tvFavouriteCount.setText(""+favCount);
        if(favCount > 0){
            tvFavouriteCount.setVisibility(View.VISIBLE);
            ivFavourite.setImageResource(R.mipmap.fav_disabled);
        }
        else {
            tvFavouriteCount.setVisibility(View.GONE);
        }
    }
}
