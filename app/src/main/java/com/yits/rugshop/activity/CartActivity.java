package com.yits.rugshop.activity;

import android.app.ActionBar;
import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.yits.rugshop.R;
import com.yits.rugshop.adapter.CartAdapter;
import com.yits.rugshop.listener.CartListListener;
import com.yits.rugshop.objects.ItemCartsDO;
import com.yits.rugshop.objects.ItemsDO;
import com.yits.rugshop.utilities.AppConstants;
import com.yits.rugshop.utilities.CartDatabase;
import com.yits.rugshop.utilities.PreferenceUtils;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;

public class CartActivity extends BaseActivity implements CartListListener {

    private LinearLayout ll_Cart, llCartLayout;
    Context mContext;
    TextView tvCartNoData, txt_subtotal, txt_grandtotal, txt_vattax, txt_deliveryCharge, tvProceed;
    private ListView lst_cart;
    PreferenceUtils preferenceUtils;
    public HashMap<String, String> Userdata;
    public String UserShopName, UserPkID, queryParams;
    ArrayList<ItemCartsDO> NewCartList;
    CartAdapter cartAdapter;
    DecimalFormat df = new DecimalFormat("00.00######");
    double subtotal, GrandTotal;
    double vatTax;
    double vatTaxValue = 20, deliveryCharges = 4.99, TotalTaxValue;
    private Double subtotalVendor;
    private Double TotalTaxValueVendor;
    private Double GrandTotalVendor;
    private Double vatTaxVendor;
    private Double deliveryChargesVendor = 4.99;
//    private View footerview;
    private boolean cartClicked;

    @Override
    public void initialize() {

        ll_Cart = (LinearLayout) inflater.inflate(R.layout.cart_screen, null);
        llContent.addView(ll_Cart, new LinearLayout.LayoutParams(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.MATCH_PARENT));
        mContext = CartActivity.this;
        preferenceUtils = new PreferenceUtils(mContext);

        currentClass = CartActivity.class.getSimpleName();
        CartDatabase.init(this);
        initializeControls();


        NewCartList = new ArrayList<>();
        Userdata = preferenceUtils.getUserDetails();
        UserPkID = Userdata.get(AppConstants.KEY_USER_ID);
        UserShopName = Userdata.get(AppConstants.KEY_USER_FST_NAME);
        queryParams = Userdata.get(AppConstants.KEY_ACCESS_TOKEN);
        txt_actionbar.setText(UserShopName);


        callLocaldatabase();

        tvCartCount.setText("" + CartDatabase.getCartlistCount());
        cartAdapter = new CartAdapter(mContext, NewCartList);
        cartAdapter.registerLocaldatabaseListener(this);
        lst_cart.setAdapter(cartAdapter);
//        lst_cart.addFooterView(footerview);


        /*img_cart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Toast.makeText(mContext, "Cart Clicked", Toast.LENGTH_SHORT).show();
            }
        });*/

        /*img_home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Toast.makeText(mContext,"Home  Clicked",Toast.LENGTH_SHORT).show();
            }
        });*/


        tvProceed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, OrderActivity.class);
                intent.putExtra("OrderSubtotalValue", Double.parseDouble(String.format("%.2f", subtotal)));
                intent.putExtra("OrderTotalTaxValue", Double.parseDouble(String.format("%.2f", TotalTaxValue)));
                intent.putExtra("OrderGrandTotalValue", Double.parseDouble(String.format("%.2f", GrandTotal)));
                intent.putExtra("OrderSubtotalValuefinal", Double.parseDouble(String.format("%.2f", subtotalVendor)));
                intent.putExtra("OrderTotalTaxValuefinal", Double.parseDouble(String.format("%.2f", TotalTaxValueVendor)));
                intent.putExtra("OrderGrandTotalValuefinal", Double.parseDouble(String.format("%.2f", GrandTotalVendor)));

                startActivity(intent);
            }
        });

        ivCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // gotoCartActivity();
                //int interval= Float.compare(view.getTranslationX(),80);
                view.setVisibility(View.VISIBLE);
                if ((!cartClicked))
                {
                    if(cartCount > 0){
                        cartClicked = true;
                        view.animate().translationY(toolbar.getHeight()).setDuration(300);
                    }
                    else {
                        showToast("No items in Cart");
                    }
                }
                else {
                    cartClicked = false;
                    view.animate().translationY(-view.getHeight()).setDuration(300);
                }
            }
        });

    }

    private void callLocaldatabase() {
        NewCartList = CartDatabase.getAllCartList();
        calculateSubtotalPrice(NewCartList);
    }

    private void calculateSubtotalPrice(ArrayList<ItemCartsDO> newCartList) {
        double tempsubtotal = 0;
        double tempVendorSubTotal = 0;
        for (int i = 0; i < newCartList.size(); i++) {
            ItemCartsDO model = newCartList.get(i);
            tempsubtotal = model.ItemQuantity * Double.parseDouble(model.ItemFinalPrice) + tempsubtotal;
            tempVendorSubTotal = model.ItemQuantity * Double.parseDouble(model.ItemBasePrice) + tempsubtotal;

        }
        subtotal = tempsubtotal;
        subtotalVendor = tempVendorSubTotal;

        txt_subtotal.setText("£"+String.format("%.2f", subtotal));
//        txt_subtotal.setText("£" + String.format("%.2f", subtotal));

        calculateTax(subtotal, subtotalVendor);


        if (subtotal < 50) {
            calculateGrandTotal(subtotal, vatTax, deliveryCharges, subtotalVendor, vatTaxVendor, deliveryChargesVendor);
            txt_deliveryCharge.setVisibility(View.VISIBLE);
            txt_deliveryCharge.setText("£" + String.format("%.2f", deliveryCharges));
        } else {
            txt_deliveryCharge.setVisibility(View.GONE);
            calculateGrandTotal(subtotal, vatTax, 0, subtotalVendor, vatTaxVendor, 0d);
        }
    }

    private void calculateGrandTotal(double subtotal, double vatTax, double deliveryCharges, Double subtotalVendor, double vatTaxVendor, Double deliveryChargesVendor) {
        GrandTotal = subtotal + vatTax + deliveryCharges;
        TotalTaxValue = vatTax + deliveryCharges;

        GrandTotalVendor = subtotalVendor + vatTaxVendor + deliveryChargesVendor;
        TotalTaxValueVendor = vatTaxVendor + deliveryChargesVendor;

        txt_grandtotal.setText("£" + String.format("%.2f", GrandTotal));
//        txt_grandtotal.setText("£" + String.format("%.2f", GrandTotal));
    }

    private void calculateTax(double subtotal, Double subtotalVendor) {
        vatTax = subtotal * 0.2;
        txt_vattax.setText("£" + String.format("%.2f", vatTax));
//        txt_vattax.setText("£" + String.format("%.2f", vatTax));
        vatTaxVendor = subtotalVendor * 0.2;

    }


    private void initializeControls() {
        llCartLayout            = (LinearLayout) ll_Cart.findViewById(R.id.llCartLayout);
        lst_cart                = (ListView) findViewById(R.id.lst_cart);
        tvCartNoData            = (TextView) findViewById(R.id.tvCartNoData);
        txt_deliveryCharge      = (TextView) findViewById(R.id.txt_deliveryCharge);
        tvProceed               = (TextView) ll_Cart.findViewById(R.id.tvProceed);
        txt_subtotal            = (TextView) ll_Cart.findViewById(R.id.txt_subtotal);
        txt_grandtotal          = (TextView) ll_Cart.findViewById(R.id.txt_grandtotal);
        txt_vattax              = (TextView) ll_Cart.findViewById(R.id.txt_vattax);
        tvCartNoData            = (TextView) ll_Cart.findViewById(R.id.tvCartNoData);
    }

    @Override
    public void getCartList(ItemsDO model) {

    }

    @Override
    public void getLocalDatabaseCartList() {
        callLocaldatabase();
        //  Toast.makeText(mContext,"DB  color listlist is ="+CartDatabase.getColorListCount(),Toast.LENGTH_SHORT).show();

        itemCartsDOs = CartDatabase.getAllCartList();
        cartCount = CartDatabase.getCartlistCount();
        tvCartCount.setText("" + cartCount);
        if(cartCount > 0){
            llCartLayout.setVisibility(View.VISIBLE);
            lst_cart.setVisibility(View.VISIBLE);
            tvCartNoData.setVisibility(View.GONE);
            tvCartCount.setVisibility(View.VISIBLE);
            cartAdapter.refresh(itemCartsDOs);
            cartAdapter.registerLocaldatabaseListener(this);
//            lst_cart.addFooterView(footerview);
            calculateSubtotalPrice(itemCartsDOs);
            cartHomeAdapter.refresh(itemCartsDOs);
            tvCartCount.startAnimation(animRotate);
        }
        else {
            llCartLayout.setVisibility(View.GONE);
            lst_cart.setVisibility(View.GONE);
            tvCartNoData.setVisibility(View.VISIBLE);
            tvCartCount.setVisibility(View.GONE);
            cartClicked = false;
            view.animate().translationY(-view.getHeight()).setDuration(300);

        }
    }

    @Override
    public void getTheIncreasedSubtotal(Double itemFinalPrice, Double itemVendorPrice) {
        double tempPrice = itemFinalPrice + subtotal;
        subtotal = tempPrice;
        txt_subtotal.setText("£"+String.format("%.2f", tempPrice));
//        txt_subtotal.setText("£" + String.format("%.2f", tempPrice));
        calculateTax(subtotal, subtotalVendor);
        if (subtotal < 50) {
            calculateGrandTotal(subtotal, vatTax, deliveryCharges, subtotalVendor, vatTaxVendor, deliveryChargesVendor);
            txt_deliveryCharge.setVisibility(View.VISIBLE);
            txt_deliveryCharge.setText("£" + String.format("%.2f", deliveryCharges));
        } else {
            txt_deliveryCharge.setVisibility(View.GONE);
            calculateGrandTotal(subtotal, vatTax, 0, subtotalVendor, vatTaxVendor, 0d);
        }
    }

    @Override
    public void getTheDecreasedSubtotal(Double itemFinalPrice, Double itemVendorPrice) {
        double tempPrice = subtotal - itemFinalPrice;
        subtotal = tempPrice;
        txt_subtotal.setText("£"+String.format("%.2f", tempPrice));
//        txt_subtotal.setText("£" + String.format("%.2f", tempPrice));
        calculateTax(subtotal, subtotalVendor);
        if (subtotal < 50) {
            calculateGrandTotal(subtotal, vatTax, deliveryCharges, subtotalVendor, vatTaxVendor, deliveryChargesVendor);
            txt_deliveryCharge.setVisibility(View.VISIBLE);
            txt_deliveryCharge.setText("£" + String.format("%.2f", deliveryCharges));
        } else {
            txt_deliveryCharge.setVisibility(View.GONE);
            calculateGrandTotal(subtotal, vatTax, 0, subtotalVendor, vatTaxVendor, 0d);
        }
    }
}
