package com.yits.rugshop.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.LinearGradient;
import android.graphics.Shader;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.yits.rugshop.R;
import com.yits.rugshop.businesslayer.CommonBL;
import com.yits.rugshop.businesslayer.DataListener;
import com.yits.rugshop.objects.LoginDO;
import com.yits.rugshop.objects.RegistrationDO;
import com.yits.rugshop.utilities.AppConstants;
import com.yits.rugshop.utilities.FontType;
import com.yits.rugshop.utilities.LogUtils;
import com.yits.rugshop.utilities.PreferenceUtils;
import com.yits.rugshop.webaccess.Response;
import com.yits.rugshop.webaccess.ServiceMethods;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class LoginActivity extends BaseActivity implements DataListener{
    RelativeLayout login_layout,registration_layout,rl_login,rl_signup;
    EditText edt_userid,edt_pwd,edt_fstname,edt_email,edt_mobile,edt_userpwd,edt_cnfrmpwd,edt_address;
    Button btn_forgot,btn_login,btn_signup;
    TextView txt_name;
    Context mContext;
    char mDigit;
    ImageView img_arrow2,img_arrow1;
    String AccessToken,UserId,UserLoginPwd,UserFstName,UserEmailId,UserMobile,UserPassword,UserCnfmPassword,UserAddress;
    String  source="CUSTOM",role="USER",Message;
    private ArrayList<RegistrationDO> arrRegistration;
    private ArrayList<LoginDO> arrLogin;
    PreferenceUtils preferenceUtils;
    String key,Error="";
    Typeface typeFace_AmTypeWriter;
    FontType fonttype;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        mContext=LoginActivity.this;

        initializeControls();
        preferenceUtils=new PreferenceUtils(mContext);

        registration_layout.setVisibility(View.GONE);
        img_arrow1 .setVisibility(View.INVISIBLE);
        rl_login.setBackground(ContextCompat.getDrawable(mContext, R.drawable.edt_orange));
        rl_signup.setBackground(ContextCompat.getDrawable(mContext, R.drawable.edt_black));

       // fonttype = new FontType(mContext, root);

        typeFace_AmTypeWriter = Typeface.createFromAsset(getAssets(), "TRAJANPRO-REGULAR.OTF");
        txt_name.setTypeface(typeFace_AmTypeWriter);
        Shader textShader=new LinearGradient(0, 0, 50,0,
                new int[]{Color.GRAY,Color.WHITE},
                new float[]{0, 1}, Shader.TileMode.MIRROR);
        txt_name.getPaint().setShader(textShader);


        rl_signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                login_layout.setVisibility(View.GONE);
                img_arrow2.setVisibility(View.INVISIBLE);
                registration_layout.setVisibility(View.VISIBLE);
                img_arrow1.setVisibility(View.VISIBLE);
                rl_signup.setBackground(ContextCompat.getDrawable(mContext, R.drawable.edt_orange));
                rl_login.setBackground(ContextCompat.getDrawable(mContext,R.drawable.edt_black));

            }
        });

        rl_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                login_layout.setVisibility(View.VISIBLE);
                img_arrow2.setVisibility(View.VISIBLE);
                registration_layout.setVisibility(View.GONE);
                img_arrow1.setVisibility(View.INVISIBLE);
                rl_login.setBackground(ContextCompat.getDrawable(mContext, R.drawable.edt_orange));
                rl_signup.setBackground(ContextCompat.getDrawable(mContext, R.drawable.edt_black));
            }
        });



        edt_userid.setText("a@a.com");
        edt_pwd.setText("asdfgh");
        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

               /* Intent intent = new Intent(mContext, Dashboard.class);
                startActivity(intent);
                finish();*/
                getTextLogin();
                if (UserId.length() > 0)
                {
                    if(isValidEmail(UserId))
                    {
                        if (UserLoginPwd.length() > 0)
                        {
                           getLogin();

                        }
                        else
                        {
                            showAlertDialog("Please enter Password .", "Ok");
                        }
                    }
                    else
                    {
                        showAlertDialog("Please enter valid Email", "Ok");
                    }
                }
                else
                {
                    showAlertDialog("Please enter Email", "Ok");
                }
            }
        });


        btn_signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getTextSignup();

                if (UserMobile.length() > 0 && UserFstName.length() > 0 && UserAddress.length() > 0 && UserEmailId.length() > 0 && UserPassword.length() > 0 && UserCnfmPassword.length() > 0) {
                    if (isValidEmail(UserEmailId)) {
                        mDigit = UserMobile.charAt(0);
                        if (UserMobile.length() == 10) {
                            if (mDigit == '7' || mDigit == '8' || mDigit == '9') {
                                if (isAlphaNumeric(UserPassword)) {
                                    if (UserPassword.equals(UserCnfmPassword))
                                    {
                                       getSignup();
                                    } else {
                                        showAlertDialog("Passwords do not match", "Ok");
                                    }
                                } else {
                                    showAlertDialog("Enter minimum 6 digit password", "Ok");
                                }
                            } else {
                                showAlertDialog("Enter a valid mobile number", "Ok");
                            }
                        } else {
                            showAlertDialog("Enter a valid mobile number", "Ok");
                        }
                    } else {
                        showAlertDialog("Enter a valid email", "Ok");
                    }

                } else {
                    showAlertDialog("Please fill mandatory fields", "Ok");
                }

            }
        });

        btn_forgot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


            }
        });


    }

    @Override
    public void initialize() {

    }


    private void getSignup()
    {
        showLoaderNew();
         if(new CommonBL(mContext, LoginActivity.this).doRegistration(UserFstName, UserEmailId, UserMobile,
                 UserPassword, source, role, UserAddress))
        {


        }
        else
        {
            hideloader();
            showAlertDialog("NO INTERNET","OK");
        }
    }

    private void getLogin()
    {
        showLoaderNew();
        if (new CommonBL(mContext, LoginActivity.this).checkLogin(UserId, UserLoginPwd))
        {


        }
        else
        {
            hideloader();
            showAlertDialog("NO INTERNET","OK");
        }
    }



    public void getAccessTokenLogin(String userId, String accessToken)
    {
        showLoaderNew();
        String queryParams=userId + "?access_token="+accessToken;
         if(new CommonBL(mContext, LoginActivity.this).getUserDetails(queryParams))
        {

        }
        else
        {
            hideloader();
            showAlertDialog("NO INTERNET","OK");
        }
    }

    private void getTextLogin()
    {
        /*UserId=edt_userid.getText().toString();
        UserLoginPwd=edt_pwd.getText().toString();*/
        UserId=edt_userid.getText().toString();
        UserLoginPwd=edt_pwd.getText().toString();
    }

    private void getTextSignup()
    {
        UserFstName=edt_fstname.getText().toString();
        UserAddress=edt_address.getText().toString();
        UserEmailId=edt_email.getText().toString();
        UserMobile=edt_mobile.getText().toString();
        UserPassword=edt_userpwd.getText().toString();
        UserCnfmPassword=edt_cnfrmpwd.getText().toString();

        preferenceUtils.saveString(AppConstants.VENDORADDRESS,UserAddress);
    }

    public static boolean isValidEmail(String string)
    {
        final Pattern EMAIL_ADDRESS_PATTERN = Pattern.compile(
                "[a-zA-Z0-9\\+\\.\\_\\%\\-\\+]{1,256}" +
                        "\\@" +
                        "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}" +
                        "(" +
                        "\\." +
                        "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25}" +
                        ")+"
        );
        Matcher matcher = EMAIL_ADDRESS_PATTERN.matcher(string);
        boolean value = matcher.matches();
        return value;
    }

    public boolean isAlphaNumeric(String pwd) {
        //String pattern = "^[a-zA-Z0-9]*$";
        //String pattern ="^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\S+$).{8,}$";
		/*if (pwd.matches(pattern))
		{
			return true;
		}
		return false;*/

        if (pwd.length()>=6)
        {
            return true;
        }
        return false;
    }


    private void initializeControls() {

        ViewGroup root = (ViewGroup) findViewById(R.id.ll_login);

        edt_userid =(EditText) findViewById(R.id.edt_userid);
        edt_pwd =(EditText) findViewById(R.id.edt_pwd);
        edt_fstname=(EditText) findViewById(R.id.edt_fstname);
        edt_address=(EditText) findViewById(R.id.edt_address);
        edt_email=(EditText) findViewById(R.id.edt_email);
        edt_mobile=(EditText) findViewById(R.id.edt_mobile);
        edt_userpwd=(EditText) findViewById(R.id.edt_userpwd);
        edt_cnfrmpwd=(EditText) findViewById(R.id.edt_cnfrmpwd);

        btn_signup=(Button) findViewById(R.id.btn_signup);
        btn_forgot=(Button) findViewById(R.id.btn_forgot);
        btn_login=(Button) findViewById(R.id.btn_login);
        rl_login=(RelativeLayout) findViewById(R.id.rl_login);
        rl_signup=(RelativeLayout) findViewById(R.id.rl_signup);
        img_arrow1=(ImageView) findViewById(R.id.img_arrow1);
        img_arrow2=(ImageView) findViewById(R.id.img_arrow2);
        login_layout=(RelativeLayout) findViewById(R.id.login_layout);
        registration_layout=(RelativeLayout) findViewById(R.id.registration_layout);
        txt_name=(TextView)findViewById(R.id.txt_name);

        btn_login.setShadowLayer((float) 0.001, -1, 1,   getResources().getColor(R.color.black_color));

        new FontType(mContext).applyfonttoViewGroup(root,AppConstants.TRAJANPRO);
        new FontType(mContext).applyfonttoView(edt_userid,AppConstants.GOTHIC);
        new FontType(mContext).applyfonttoView(edt_pwd,AppConstants.GOTHIC);


    }

    @Override
    public void dataRetreived(Response data) {

        hideloader();
        if (data != null && data.method == ServiceMethods.WS_DO_REGISTRATION)
        {
            if(!data.isError)
            {
                arrRegistration = (ArrayList<RegistrationDO>) data.data;
                if(arrRegistration!=null&&arrRegistration.size()>0)
                {
                    for(int i=0;i<arrRegistration.size();i++)
                    {
                        Message=arrRegistration.get(i).Status;
                    }

                    if (Message.equalsIgnoreCase("success"))
                    {
                        for(int i=0;i<arrRegistration.size();i++)
                        {
                            Message=arrRegistration.get(i).Status;
                            UserId=arrRegistration.get(i).VendorId;
                            Error=arrRegistration.get(i).ErrorMessage;
                            UserFstName=arrRegistration.get(i).ShopName;
                            UserEmailId=arrRegistration.get(i).Email;
                            UserMobile=arrRegistration.get(i).ContactNumber;
                        }
                        preferenceUtils.createLoginSession(UserId, UserFstName, UserEmailId, "CUSTOM", UserMobile);
                        Toast.makeText(mContext, "Registered Successfully", Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(mContext, Dashboard.class);
                        startActivity(intent);
                        finish();
                    }
                    else if (Message.equalsIgnoreCase("Already registered"))
                    {
                        showAlertDialog("Already registered", "Ok");
                    }
                }
            }
        }

        else if (data != null && data.method == ServiceMethods.WS_CHECK_LOGIN)
        {
            if(!data.isError)
            {
                arrLogin = (ArrayList<LoginDO>) data.data;
                if(arrLogin!=null&&arrLogin.size()>0)
                {
                    for(int i=0;i<arrLogin.size();i++)
                    {
                        Message=arrLogin.get(i).Status;
                    }

                    if (Message.equalsIgnoreCase("success"))
                    {
                        for(int i=0;i<arrLogin.size();i++)
                        {
                            Message=arrLogin.get(i).Status;
                            UserId=arrLogin.get(i).VendorId;
                            AccessToken=arrLogin.get(i).AccessToken;

                            //getVendorDetailas();
                        }
                        preferenceUtils.createAccessToken(AccessToken);
                        getAccessTokenLogin(UserId,AccessToken);
                    }
                    else if (Message.equalsIgnoreCase("Login Failed"))
                    {
                        showAlertDialog("Invalid Credentials", "Ok");
                    }
                }
            }
        }

        if (data != null && data.method == ServiceMethods.WS_GET_USER_DETAILS)
        {
            if(!data.isError)
            {
                arrRegistration = (ArrayList<RegistrationDO>) data.data;
                if(arrRegistration!=null&&arrRegistration.size()>0)
                {
                    for(int i=0;i<arrRegistration.size();i++)
                    {
                        Message=arrRegistration.get(i).Status;
                        UserId=arrRegistration.get(i).VendorId;
                        Error=arrRegistration.get(i).ErrorMessage;
                        UserFstName=arrRegistration.get(i).ShopName;
                        UserEmailId=arrRegistration.get(i).Email;
                        UserMobile=arrRegistration.get(i).ContactNumber;
                        UserAddress=arrRegistration.get(i).Address;

                        preferenceUtils.saveString(AppConstants.VENDORADDRESS,UserAddress);

                        LogUtils.info(LoginActivity.class.getSimpleName(),"user address"+UserAddress);
                    }

                    if (Message.equalsIgnoreCase("success"))
                    {
                        preferenceUtils.createLoginSession(UserId, UserFstName, UserEmailId, "CUSTOM", UserMobile);
                        Toast.makeText(mContext, "Logged In Successfully", Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(mContext, Dashboard.class);
                        startActivity(intent);
                        finish();

                    }
                }
            }
        }

    }

    private void getVendorDetailas()
    {

    }
}
