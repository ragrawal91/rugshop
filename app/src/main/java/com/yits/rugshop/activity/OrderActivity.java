package com.yits.rugshop.activity;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatDialog;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.yits.rugshop.R;
import com.yits.rugshop.adapter.OrderCartAdapter;
import com.yits.rugshop.businesslayer.CommonBL;
import com.yits.rugshop.businesslayer.DataListener;
import com.yits.rugshop.objects.ItemCartsDO;
import com.yits.rugshop.objects.OrderDO;
import com.yits.rugshop.utilities.AppConstants;
import com.yits.rugshop.utilities.CartDatabase;
import com.yits.rugshop.utilities.FontType;
import com.yits.rugshop.utilities.PreferenceUtils;
import com.yits.rugshop.webaccess.Response;
import com.yits.rugshop.webaccess.ServiceMethods;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class OrderActivity extends BaseActivity implements DataListener {

    LinearLayout ll_OrderScreen;
    String addressType = "User Address";
<<<<<<< HEAD
    TextView tv_yourdetails,tv_payment_methods,tv_delivery_details;

=======
    TextView tv_yourdetails, tv_payment_methods, tv_delivery_details;
    boolean vendorselected = false;
>>>>>>> f76f5d1fbfcca738becd105dc6d4a8a2bbc91a40
    String paymodeStaus = "", deliveryCharge = "";
    Button btn_submit;
    Context mContext;
    Button btn_cashondelivery;
    RelativeLayout paymentdetail_layout, basicdetail_layout;
    TextView tvCartNoData, txt_cartCount1;
    TextView tv_subtotal, tv_taxtotal, tv_groundtotal, tv_basicdetail, tv_crumb, tv_payment;
    ListView lst_ordercart;
    PreferenceUtils preferenceUtils;
    public HashMap<String, String> Userdata;
    public String VendorShopName, VendorID, queryParams;
    ArrayList<ItemCartsDO> orderCartList;
    OrderCartAdapter orderCartAdapter;
    private Animation rightIn;
    char mDigit;
    //public double subtotal, GrandTotal, TotalTaxValue;
    String UserName, UserEmail, UserMobile, UserAddress, Message;
    private String paymentType;
    private Button btn_onlinepayment;
    TextView txt_yes;
    TextView txt_text;
    FontType fonttype;
    private double TotalTaxValuevendor;
    private double GrandTotalvendor;
    private double subtotalvendor;
    private double vatTax;
    private double subtotalVendor;
    private double deliveryCharges = 4.99;
    private double vatTaxVendor;
    private double deliveryChargesVendor = 4.99;
    private double TotalTaxValueVendor;
    private double GrandTotal;
    private double TotalTaxValue;
    private double subtotal;
    TextView tv_your_order_label;
    LinearLayout ll_place_order;

    private boolean isCollectFromStore = true, isPayCashInStore = true;
    private CheckBox cbSameAsAbove;
    private RadioButton rbCollectFromStore, rbHomeDelivey, rbPayCashInStore, rbPayByCard;
    private EditText etFirstName, etLastName, etEmail, etMobile, etAddress1, etAddress2, etCountry, etZipcode, etAdditionalNote;
    private TextView etDelFirstName, etDelLastName, etDelEmail, etDelMobile, etDelAddress1, etDelAddress2, etDelCountry, etDelZipcode, etDelAdditionalNote;
    private LinearLayout llPostOrder;
    private AlertDialog.Builder alertDialogBuilder;

    @SuppressLint({"SetTextI18n", "DefaultLocale", "InflateParams"})
    @Override
    public void initialize() {
        currentClass = OrderActivity.class.getSimpleName();
        ll_OrderScreen = (LinearLayout) inflater.inflate(R.layout.order_screen, null);
        llContent.addView(ll_OrderScreen, new LinearLayout.LayoutParams(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.MATCH_PARENT));
        mContext = OrderActivity.this;
        preferenceUtils = new PreferenceUtils(mContext);
        CartDatabase.init(this);
        initializeControls();

        ll_place_order.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getTextUserDetails();
                paymentType = "Online";
                paymodeStaus = "un paid";
                placeOrder();
            }
        });
        orderCartList = new ArrayList<>();
        Userdata = preferenceUtils.getUserDetails();
        VendorID = Userdata.get(AppConstants.KEY_USER_ID);
        VendorShopName = Userdata.get(AppConstants.KEY_USER_FST_NAME);
        queryParams = Userdata.get(AppConstants.KEY_ACCESS_TOKEN);
        txt_actionbar.setText(VendorShopName);
        callLocaldatabase();

        tvCartCount.setText("" + CartDatabase.getCartlistCount());
        rightIn = AnimationUtils.loadAnimation(this, R.anim.right_in);


       /* subtotal = getIntent().getExtras().getDouble("OrderSubtotalValue");
        GrandTotal = getIntent().getExtras().getDouble("OrderGrandTotalValue");
        TotalTaxValue = getIntent().getExtras().getDouble("OrderTotalTaxValue");
        subtotalvendor = getIntent().getExtras().getDouble("OrderSubtotalValuefinal");
        GrandTotalvendor = getIntent().getExtras().getDouble("OrderGrandTotalValuefinal");
        TotalTaxValuevendor = getIntent().getExtras().getDouble("OrderTotalTaxValuefinal");
*/

        deliveryCharge = String.valueOf(TotalTaxValue);
        orderCartAdapter = new OrderCartAdapter(mContext, orderCartList);
        lst_ordercart.setAdapter(orderCartAdapter);
        calculateSubtotalPrice(orderCartList);
        //tv_subtotal.setText("Subtotal        : £ " + String.format("%.2f", subtotal));
        // tv_taxtotal.setText("Total Taxes   : £ " + String.format("%.2f", TotalTaxValue));
        tv_groundtotal.setText("£ " + String.format("%.2f", GrandTotal));


       /* img_cart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Toast.makeText(mContext, "Cart Clicked", Toast.LENGTH_SHORT).show();
            }
        });*/

        /*img_home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Toast.makeText(mContext, "Home  Clicked", Toast.LENGTH_SHORT).show();
            }
        });*/

        btn_onlinepayment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                paymentType = "Online";
                paymodeStaus = "un paid";
                placeOrder();
            }
        });
        btn_cashondelivery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                paymodeStaus = "unpaid";
                paymentType = "COD";
                placeOrder();
            }
        });


        /*btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                getTextUserDetails();

                if (UserMobile.length() > 0 && UserName.length() > 0 && UserAddress.length() > 0 && UserEmail.length() > 0) {
                    if (isValidEmail(UserEmail)) {
                        mDigit = UserMobile.charAt(0);
                        char firstdigit = UserMobile.charAt(0);
                        if (UserMobile.length() == 10) {
                            if (mDigit == '7' || mDigit == '8' || mDigit == '9') {

                                paymentdetail_layout.startAnimation(rightIn);
                                paymentdetail_layout.bringToFront();
                                txt_cartCount1.setVisibility(View.VISIBLE);

                            } else {
                                showAlertDialog("Enter a valid mobile number", "Ok");
                            }
                        } else {
                            showAlertDialog("Enter a valid mobile number", "Ok");
                        }
                    } else {
                        showAlertDialog("Enter a valid email", "Ok");
                    }

                } else {
                    showAlertDialog("Please fill mandatory fields", "Ok");
                }

            }
        });*/

        rbPayCashInStore.setChecked(true);
        rbPayCashInStore.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                isPayCashInStore = true;
                rbPayByCard.setChecked(!isPayCashInStore);
                rbPayCashInStore.setChecked(isPayCashInStore);
            }
        });

        rbPayByCard.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                isPayCashInStore = false;
                rbPayCashInStore.setChecked(isPayCashInStore);
                rbPayByCard.setChecked(!isPayCashInStore);
            }
        });

        rbCollectFromStore.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                isCollectFromStore = true;
                addressType = "Vendor Address";
                UserAddress = etAddress1.getText().toString();

            }
        });
        rbHomeDelivey.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                isCollectFromStore = false;
                addressType = "User Address";
                UserAddress = etAddress1.getText().toString();
            }
        });

        cbSameAsAbove.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    if(etFirstName.getText().toString().equalsIgnoreCase("")
                            || etLastName.getText().toString().equalsIgnoreCase("")
                            || etEmail.getText().toString().equalsIgnoreCase("")
                            || etMobile.getText().toString().equalsIgnoreCase("")
                            || etAddress1.getText().toString().equalsIgnoreCase("")
                            || etAddress2.getText().toString().equalsIgnoreCase("")
                            || etCountry.getText().toString().equalsIgnoreCase("")
                            || etZipcode.getText().toString().equalsIgnoreCase("")
                            || etAdditionalNote.getText().toString().equalsIgnoreCase(""))
                    {
                        showToast("Please enter all the above fields");
                        cbSameAsAbove.setChecked(false);
                    }
                    else {
                        etDelFirstName.setText(etFirstName.getText().toString().trim());
                        etDelLastName.setText(etLastName.getText().toString().trim());
                        etDelEmail.setText(etEmail.getText().toString().trim());
                        etDelMobile.setText(etMobile.getText().toString().trim());
                        etDelAddress1.setText(etAddress1.getText().toString().trim());
                        etDelAddress2.setText(etAddress2.getText().toString().trim());
                        etDelCountry.setText(etCountry.getText().toString().trim());
                        etDelZipcode.setText(etZipcode.getText().toString().trim());
                        etDelAdditionalNote.setText(etAdditionalNote.getText().toString().trim());
                        cbSameAsAbove.setChecked(true);
                    }
                }
                else {
                    etDelFirstName.setText("");
                    etDelLastName.setText("");
                    etDelEmail.setText("");
                    etDelMobile.setText("");
                    etDelAddress1.setText("");
                    etDelAddress2.setText("");
                    etDelCountry.setText("");
                    etDelZipcode.setText("");
                    etDelAdditionalNote.setText("");
                    cbSameAsAbove.setChecked(false);
                }
            }
        });

        llPostOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(appCompatDialog==null)
                appCompatDialog = new AppCompatDialog(OrderActivity.this);
                appCompatDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                appCompatDialog.setCancelable(false);
                appCompatDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
                View view = inflater.inflate(R.layout.make_payment_dialog, null);
                ProgressBar progressBar = (ProgressBar) view.findViewById(R.id.progressBar1);
                progressBar.getIndeterminateDrawable().setColorFilter(getResources().getColor(R.color.colorPrimary), android.graphics.PorterDuff.Mode.MULTIPLY);
                appCompatDialog.setContentView(view);
                progressBar.setIndeterminate(true);
                if(!appCompatDialog.isShowing()) {
                    appCompatDialog.show();

//                    new CommonBL(OrderActivity.this, new DataListener() {
//                        @Override
//                        public void dataRetreived(Response data) {
//
//                        }
//                    }).placeOrder();
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                            if (alertDialogBuilder == null)
                                alertDialogBuilder = new AlertDialog.Builder(OrderActivity.this, R.style.AppCompatAlertDialogStyle);
                        alertDialogBuilder.setTitle("Alert!");
                        alertDialogBuilder.setMessage("You order has been successfully placed");
                        alertDialogBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    Intent intent = new Intent(OrderActivity.this, Dashboard.class);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    startActivity(intent);
                                    overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
                                }
                            });//second parameter used for onclicklistener
                        alertDialogBuilder.setNegativeButton("", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                }
                            });
                        alertDialogBuilder.setCancelable(false);
                        alertDialogBuilder.show();

                    }
                }, 3000);

                }
            }
        });

    }

    private AppCompatDialog  appCompatDialog;
    private void validateFields(){

        if(etFirstName.getText().toString().equalsIgnoreCase("")){
            showToast("Please enter first name");
        }
        else if(etLastName.getText().toString().equalsIgnoreCase("")){
            showToast("Please enter last name");
        }
        else if(etEmail.getText().toString().equalsIgnoreCase("")){
            showToast("Please enter email address");
        }
        else if(!isValidEmail(etEmail.getText().toString().trim())){
            showToast("Please enter valid email address");
        }
        else if(etMobile.getText().toString().equalsIgnoreCase("")){
            showToast("Please enter mobile number");
        }
        else if(etAddress1.getText().toString().equalsIgnoreCase("")){
            showToast("Please enter address1");
        }
        else if(etAddress2.getText().toString().equalsIgnoreCase("")){
            showToast("Please enter address2");
        }
        else if(etCountry.getText().toString().equalsIgnoreCase("")){
            showToast("Please enter country name");
        }
        else if(etZipcode.getText().toString().equalsIgnoreCase("")){
            showToast("Please enter zipcode");
        }
        else if(etAdditionalNote.getText().toString().equalsIgnoreCase("")){
            showToast("Please enter additional notes");

        }

        // for delivery address fields

        if(etDelFirstName.getText().toString().equalsIgnoreCase("")){
            showToast("Please enter first name in delivery address");
        }
        else if(etDelLastName.getText().toString().equalsIgnoreCase("")){
            showToast("Please enter last name in delivery address");
        }
        else if(etDelEmail.getText().toString().equalsIgnoreCase("")){
            showToast("Please enter email address in delivery address");
        }
        else if(!isValidEmail(etDelEmail.getText().toString().trim())){
            showToast("Please enter valid email address in delivery address");
        }
        else if(etDelMobile.getText().toString().equalsIgnoreCase("")){
            showToast("Please enter mobile number in delivery address");
        }
        else if(etDelAddress1.getText().toString().equalsIgnoreCase("")){
            showToast("Please enter address1 in delivery address");
        }
        else if(etDelAddress2.getText().toString().equalsIgnoreCase("")){
            showToast("Please enter address2 in delivery address");
        }
        else if(etDelCountry.getText().toString().equalsIgnoreCase("")){
            showToast("Please enter country name in delivery address");
        }
        else if(etDelZipcode.getText().toString().equalsIgnoreCase("")){
            showToast("Please enter zipcode in delivery address");
        }
        else if(etDelAdditionalNote.getText().toString().equalsIgnoreCase("")){
            showToast("Please enter additional notes in delivery address");

        }


    }
    private void placeOrder() {
        showLoaderNew();
        if (new CommonBL(mContext, OrderActivity.this).placeOrder(UserName, UserEmail, UserMobile,
                UserAddress, VendorID, VendorShopName, subtotal, GrandTotal, TotalTaxValue, orderCartList, paymentType, paymodeStaus, deliveryCharge, addressType, preferenceUtils.getStringFromPreference(AppConstants.VENDORADDRESS, "NA"), subtotalvendor, GrandTotalvendor, TotalTaxValuevendor, preferenceUtils.getUserDetails().get(AppConstants.KEY_USER_EMAIL))) {


        } else {
            hideloader();
            showAlertDialog("NO INTERNET", "OK");
        }

    }

    private void getTextUserDetails() {
        UserName            = etFirstName.getText().toString().trim()+" "+etLastName.getText().toString().trim();
        UserEmail           = etEmail.getText().toString().trim();
        UserMobile          = etMobile.getText().toString();
        UserAddress = etAddress1.getText().toString();
    }

    public static boolean isValidEmail(String string) {
        final Pattern EMAIL_ADDRESS_PATTERN = Pattern.compile(
                "[a-zA-Z0-9\\+\\.\\_\\%\\-\\+]{1,256}" +
                        "\\@" +
                        "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}" +
                        "(" +
                        "\\." +
                        "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25}" +
                        ")+"
        );
        Matcher matcher = EMAIL_ADDRESS_PATTERN.matcher(string);
        return matcher.matches();
    }


    private void callLocaldatabase() {
        orderCartList = CartDatabase.getAllCartList();
    }

    private void initializeControls() {
<<<<<<< HEAD

        etFirstName                     =   (EditText) ll_OrderScreen.findViewById(R.id.etFirstName);
        etLastName                      =   (EditText) ll_OrderScreen.findViewById(R.id.etLastName);
        etEmail                         =   (EditText) ll_OrderScreen.findViewById(R.id.etEmail);
        etMobile                        =   (EditText) ll_OrderScreen.findViewById(R.id.etMobile);
        etAddress1                      =   (EditText) ll_OrderScreen.findViewById(R.id.etAddress1);
        etAddress2                      =   (EditText) ll_OrderScreen.findViewById(R.id.etAddress2);
        etCountry                       =   (EditText) ll_OrderScreen.findViewById(R.id.etCountry);
        etZipcode                       =   (EditText) ll_OrderScreen.findViewById(R.id.etZipcode);
        etAdditionalNote                =   (EditText) ll_OrderScreen.findViewById(R.id.etAdditionalNote);

        etDelFirstName                  =   (EditText) ll_OrderScreen.findViewById(R.id.etDelFirstName);
        etDelLastName                   =   (EditText) ll_OrderScreen.findViewById(R.id.etDelLastName);
        etDelEmail                      =   (EditText) ll_OrderScreen.findViewById(R.id.etDelEmail);
        etDelMobile                     =   (EditText) ll_OrderScreen.findViewById(R.id.etDelMobile);
        etDelAddress1                   =   (EditText) ll_OrderScreen.findViewById(R.id.etDelAddress1);
        etDelAddress2                   =   (EditText) ll_OrderScreen.findViewById(R.id.etDelAddress2);
        etDelCountry                    =   (EditText) ll_OrderScreen.findViewById(R.id.etDelCountry);
        etDelZipcode                    =   (EditText) ll_OrderScreen.findViewById(R.id.etDelZipcode);
        etDelAdditionalNote             =   (EditText) ll_OrderScreen.findViewById(R.id.etDelAdditionalNote);

        cbSameAsAbove                   =   (CheckBox) ll_OrderScreen.findViewById(R.id.cbSameAsAbove);

        rbPayCashInStore                =   (RadioButton) ll_OrderScreen.findViewById(R.id.rbPayCashInStore);
        rbPayByCard                     =   (RadioButton) ll_OrderScreen.findViewById(R.id.rbPayByCard);
        rbCollectFromStore              =   (RadioButton) ll_OrderScreen.findViewById(R.id.rbCollectFromStore);
        rbHomeDelivey                   =   (RadioButton) ll_OrderScreen.findViewById(R.id.rbHomeDelivey);

        llPostOrder                     =   (LinearLayout) ll_OrderScreen.findViewById(R.id.llPostOrder);

        tv_yourdetails= (TextView) findViewById(R.id.tv_yourdetails);
        tv_payment_methods= (TextView) findViewById(R.id.tv_payment_methods);
        tv_delivery_details= (TextView) findViewById(R.id.tv_delivery_details);
=======
        ll_place_order = (LinearLayout) findViewById(R.id.ll_place_order);
        tv_yourdetails = (TextView) findViewById(R.id.tv_yourdetails);
        tv_payment_methods = (TextView) findViewById(R.id.tv_payment_methods);
        tv_delivery_details = (TextView) findViewById(R.id.tv_delivery_details);
>>>>>>> f76f5d1fbfcca738becd105dc6d4a8a2bbc91a40

        tv_your_order_label = (TextView) findViewById(R.id.tv_your_order_label);
        btn_cashondelivery = (Button) findViewById(R.id.btn_cashondelivery);
        btn_onlinepayment = (Button) findViewById(R.id.btn_onlinepayment);
        lst_ordercart = (ListView) findViewById(R.id.lst_ordercart);
        tvCartNoData = (TextView) findViewById(R.id.tvCartNoData);
        txt_cartCount1 = (TextView) findViewById(R.id.txt_cartCount1);

        tv_subtotal = (TextView) findViewById(R.id.tv_subtotal);
        tv_taxtotal = (TextView) findViewById(R.id.tv_taxtotal);
        tv_groundtotal = (TextView) findViewById(R.id.tv_groundtotal);
        //tv_basicdetail = (TextView) findViewById(R.id.tv_basicdetail);
        //tv_crumb = (TextView) findViewById(R.id.tv_crumb);
        //tv_payment = (TextView) findViewById(R.id.tv_payment);

        btn_submit = (Button) findViewById(R.id.btn_submit);
        paymentdetail_layout = (RelativeLayout) findViewById(R.id.paymentdetail_layout);
        basicdetail_layout = (RelativeLayout) findViewById(R.id.basicdetail_layout);
    }

    private void calculateTax(double subtotal, Double subtotalVendor) {
        vatTax = subtotal * 0.2;
        // txt_vattax.setText("£"+String.format("%.2f", vatTax));
        vatTaxVendor = subtotalVendor * 0.2;

    }

    private void calculateGrandTotal(double subtotal, double vatTax, double deliveryCharges, Double subtotalVendor, double vatTaxVendor, Double deliveryChargesVendor) {
        GrandTotal = subtotal + vatTax + deliveryCharges;
        TotalTaxValue = vatTax + deliveryCharges;


        TotalTaxValueVendor = vatTaxVendor + deliveryChargesVendor;

        tv_groundtotal.setText("£" + String.format("%.2f", GrandTotal));
    }

    private void calculateSubtotalPrice(ArrayList<ItemCartsDO> newCartList) {
        double tempsubtotal = 0;
        double tempVendorSubTotal = 0;
        for (int i = 0; i < newCartList.size(); i++) {
            ItemCartsDO model = newCartList.get(i);
            tempsubtotal = model.ItemQuantity * Double.parseDouble(model.ItemFinalPrice) + tempsubtotal;
            tempVendorSubTotal = model.ItemQuantity * Double.parseDouble(model.ItemBasePrice) + tempsubtotal;

        }
        subtotal = tempsubtotal;
        subtotalVendor = tempVendorSubTotal;

        //txt_subtotal.setText("£"+String.format("%.2f", subtotal));

        calculateTax(subtotal, subtotalVendor);

        calculateGrandTotal(subtotal, vatTax, deliveryCharges, subtotalVendor, vatTaxVendor, deliveryChargesVendor);


        if (subtotal < 50) {

            // txt_deliveryCharge.setVisibility(View.VISIBLE);
            //txt_deliveryCharge.setText("£"+String.format("%.2f",deliveryCharges));
        } else {
            // txt_deliveryCharge.setVisibility(View.GONE);
            // calculateGrandTotal(subtotal,vatTax,0, subtotalVendor, vatTaxVendor,0d);
        }
    }

    @Override
    public void dataRetreived(Response data) {

        hideloader();
        if (data != null && data.method == ServiceMethods.WS_POST_ORDER_DATA) {
            if (!data.isError) {

                ArrayList<OrderDO> arrOrderDo = (ArrayList<OrderDO>) data.data;
                if (arrOrderDo != null && arrOrderDo.size() > 0) {
                    for (int i = 0; i < arrOrderDo.size(); i++) {
                        Message = arrOrderDo.get(i).Status;
                    }

                    if (Message.equalsIgnoreCase("success")) {

                        ArrayList<OrderDO> orderDOs = (ArrayList<OrderDO>) data.data;

                        showDialogRemove(mContext);
                        //  runOnUiThread(new RunshowCustomDialogs("Your Order Saved \n your Order ID:" + orderDOs.get(0).OrderId, "ok", R.mipmap.success_icon));

                       /* paymentdetail_layout.startAnimation(rightIn);
                        paymentdetail_layout.bringToFront();
                        txt_cartCount1.setVisibility(View.VISIBLE);*/
                    }
                }
            }
        }
    }

    @SuppressLint("SetTextI18n")
    private void showDialogRemove(final Context mContext) {
        dialog = new Dialog(mContext);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.cart_dialogplaceorder);
        dialog.setCanceledOnTouchOutside(false);

        txt_yes = (TextView) dialog.findViewById(R.id.txt_yes);
        txt_text = (TextView) dialog.findViewById(R.id.txt_text);


        ViewGroup root = (ViewGroup) dialog.findViewById(R.id.row_dialog);
        fonttype = new FontType(mContext, root);

        new FontType(mContext).applyfonttoView(tv_your_order_label, FontType.ROBOTOREGULAR);
        new FontType(mContext).applyfonttoView(tv_groundtotal, FontType.ROBOTOREGULAR);

        new FontType(mContext).applyfonttoView(tv_delivery_details, FontType.ROBOTOBOLD);
        new FontType(mContext).applyfonttoView(tv_yourdetails, FontType.ROBOTOBOLD);
        new FontType(mContext).applyfonttoView(tv_payment_methods, FontType.ROBOTOBOLD);


        txt_text.setText("Your order has been placed");

        txt_yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(mContext, Dashboard.class);
                startActivity(intent);
                CartDatabase.clearDatabase();
                dialog.dismiss();

            }
        });

        dialog.show();
    }
}
