package com.yits.rugshop.activity;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Paint;
import android.graphics.drawable.ColorDrawable;
import android.os.Parcelable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.jaredrummler.materialspinner.MaterialSpinner;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.yits.rugshop.R;
import com.yits.rugshop.adapter.CartHomeAdapter;
import com.yits.rugshop.adapter.FilterHeadingAdapter;
import com.yits.rugshop.adapter.FilterSelectionAdater;
import com.yits.rugshop.adapter.ItemsGridAdapter;
import com.yits.rugshop.adapter.ItemsListAdapter;
import com.yits.rugshop.adapter.ItemsReviewListAdapter;
import com.yits.rugshop.adapter.ListImagesAdapter;
import com.yits.rugshop.adapter.PositionColorAdapter;
import com.yits.rugshop.adapter.PositionSizeAdapter;
import com.yits.rugshop.adapter.SizeGroupFilterAdapter;
import com.yits.rugshop.businesslayer.CommonBL;
import com.yits.rugshop.businesslayer.DataListener;
import com.yits.rugshop.listener.CartListListener;
import com.yits.rugshop.listener.FavouriteListListener;
import com.yits.rugshop.listener.FilterClear;
import com.yits.rugshop.listener.FilterCloseListener;
import com.yits.rugshop.listener.GetSelectedSizeListener;
import com.yits.rugshop.listener.GetStringOnClickListener;
import com.yits.rugshop.listener.SelectedFilterListener;
import com.yits.rugshop.listener.SelectedItemListener;
import com.yits.rugshop.objects.ColorsDO;
import com.yits.rugshop.objects.FilterHeadingDO;
import com.yits.rugshop.objects.FilterMetadataDO;
import com.yits.rugshop.objects.FilternameAndIDDo;
import com.yits.rugshop.objects.ItemCartsDO;
import com.yits.rugshop.objects.ItemfavouriteDO;
import com.yits.rugshop.objects.ItemsDO;
import com.yits.rugshop.objects.ItemsSizeDO;
import com.yits.rugshop.objects.ReviewSizeListDO;
import com.yits.rugshop.objects.SizeListDO;
import com.yits.rugshop.utilities.AppConstants;
import com.yits.rugshop.utilities.BgViewAware;
import com.yits.rugshop.utilities.CartDatabase;
import com.yits.rugshop.utilities.FontType;
import com.yits.rugshop.utilities.LogUtils;
import com.yits.rugshop.utilities.PreferenceUtils;
import com.yits.rugshop.utilities.StringUtils;
import com.yits.rugshop.webaccess.Response;
import com.yits.rugshop.webaccess.ServiceMethods;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

public class ItemsActivity extends BaseActivity implements DataListener, CartListListener, FavouriteListListener, SelectedFilterListener, SelectedItemListener, GetSelectedSizeListener, FilterClear
        , FilterCloseListener {

    ListImagesAdapter listImagesAdapter;

    LinearLayout ll_Items;
    TextView tv_product_id;

    ArrayList<FilternameAndIDDo> allSelectedFilternameAndIDDo = new ArrayList<>();

    private boolean sortPriceHL, sortPriceLH, sortsavingsHL, sortsavingsLH, sortmostviewed, sortPopularity, sortNewest, sortnopreff = true;
    private ImageButton btn_price_lh, btn_price_hl, btn_savings_hl, btn_savings_lh, ibPopularity, ibNewest;

    private TextView tv_num_items_fou, tv_most_view, tv_price, tv_savings, tvPopularity, tvNewest;
    MaterialSpinner spn_sort;
    ListView lst_items, lst_review, lst_filterheadings;
    RelativeLayout ll_filters, grd_layout, ll_review;
    LinearLayout lst_layout;
    RelativeLayout rl_filterOpen, rl_filterClose;
    Context mContext;
    TextView tv_code_label, tv_material_label, tv_size_label, tv_qauntity_label, tv_price_label, tv_desc_label;
    TextView tvItemsNoData, txt_catgName, txt_catg, tv_itemName;
    String Message, CatgId;
    ArrayList<ItemsDO> favsList;
    PreferenceUtils preferenceUtils;
    public HashMap<String, String> Userdata;
    public String UserShopName, UserPkID, queryParams, queryParamsFilter, CatgName;
    GridView grd_items, grd_filterdata;
    LinearLayout ll_img_item, ll_IMG;
    ImageView img_crossColor;
    TextView tv_offer_price, tv_original_price;
    CheckBox img_review;
    private ImageView img_grd, img_lst;
    private ArrayList<ItemsDO> arrItems;
    private ArrayList<ItemsDO> defaultlist;

    private ArrayList<ItemsDO> reviewList;
    Animation animRotate;
    ArrayList<ItemCartsDO> LocalCartList;

    private LinearLayout llItems;
    private ArrayList<FilterMetadataDO> arrfilterMetadataDO;
    ItemsGridAdapter itemsGridAdapter;
    ItemsListAdapter itemsListAdapter;
    ItemsReviewListAdapter itemsReviewListAdapter;
    SizeGroupFilterAdapter sizeGroupFilterAdapter;
    public ArrayList<ItemCartsDO> itemCartList;
    public ArrayList<ItemCartsDO> NewitemCartList;
    public ArrayList<ItemfavouriteDO> itemFavouriteList;

    ArrayList<FilternameAndIDDo> SelectedSizeFilterList = new ArrayList<>();
    ArrayList<FilternameAndIDDo> SelectedPatternFilterList = new ArrayList<>();
    ArrayList<FilternameAndIDDo> SelectedMaterialFilterList = new ArrayList<>();
    ArrayList<FilternameAndIDDo> SelectedColorFilterList = new ArrayList<>();
    ArrayList<FilternameAndIDDo> SelectedSizeGroupFilterList = new ArrayList<>();
    ArrayList<FilternameAndIDDo> SelectedPriceFilterList = new ArrayList<>();
    ArrayList<FilternameAndIDDo> SelectedCategoryFilterList = new ArrayList<>();
    ArrayList<FilternameAndIDDo> SelectedRunnerFilterList = new ArrayList<>();
    ArrayList<FilternameAndIDDo> SelectedFeelerFilterList = new ArrayList<>();

    private Animation animUp, animDown, animLeft, animRight;
    FilterMetadataDO filterMetadataDO;
    ItemsDO itemsDo;
    CartDatabase cartDatabase;
    ItemCartsDO itemCartsDO;
    ImageLoaderConfiguration imgLoaderConf;
    DisplayImageOptions dispImage;
    ImageLoader imgLoader = ImageLoader.getInstance();

    //Views Of included list layouts
    TextView tv_material;

    ImageButton btn_applysearch;
    EditText edt_search;
    private TextView chk_addtocart, btn_buy;

    JSONArray catJSON;
    ListView lst_SizePrice;
    ArrayList<ItemsSizeDO> positionSizeList;
    ArrayList<String> positionColorList;
    PositionColorAdapter positionColorAdapter;
    PositionSizeAdapter positionSizeAdapter;
    ItemsDO selecteditemsDOImg;
    ItemsSizeDO itemSizeDO;
    int selectedPosition = 0;


    Parcelable state;

    Button btn_resetfilter, btn_applyfilter;

        ArrayList<FilterHeadingDO> filterheadings = new ArrayList<FilterHeadingDO>();
    FilterHeadingAdapter filterHeadingAdapter;
    public ArrayList<FilternameAndIDDo> ColorNamesList = new ArrayList<>();
    public ArrayList<FilternameAndIDDo> SizeNamesList = new ArrayList<>();
    public ArrayList<FilternameAndIDDo> SizeGroupNamesList = new ArrayList<>();
    public ArrayList<FilternameAndIDDo> PatternNamesList = new ArrayList<>();
    public ArrayList<FilternameAndIDDo> MaterialNamesList = new ArrayList<>();
    public ArrayList<FilternameAndIDDo> PricesNamesList = new ArrayList<>();
    public ArrayList<FilternameAndIDDo> CategoryList = new ArrayList<>();
    public ArrayList<FilternameAndIDDo> RunnerList = new ArrayList<>();
    private String action = AppConstants.NO_RESPONSE;
    private MaterialSpinner spn_sizelist;
    private Spinner spSizes;
    GridView grd_filter_selections;

    FilterSelectionAdater filterSelectionAdater;
    private LinearLayout refine_filter;
    ListView lst_filter_head;
    private ListView lst_filter_data;
    TextView tv_disc;
    private RecyclerView lst_images;
    private TextView tv_selection;
    Button btn_clear_all;
    DecimalFormat df = new DecimalFormat("#.00");

    private PopupWindow popupWindow;
    private TextView tvQuantity;
    private TextView tvDecrese, tvIncrese;
    private FrameLayout flFilterList;
    private LinearLayout llPriceSort, llMaxSavings, llPopularity, llNewest;
    @SuppressLint({"SetTextI18n", "InflateParams"})
    @Override
    public void initialize() {

        ll_Items = (LinearLayout) inflater.inflate(R.layout.items, null);
        llContent.addView(ll_Items, new LinearLayout.LayoutParams(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.MATCH_PARENT));
        mContext = ItemsActivity.this;
        preferenceUtils = new PreferenceUtils(mContext);
        initializeControls();
        catJSON             = new JSONArray();
        itemCartList        = new ArrayList<ItemCartsDO>();
        itemFavouriteList   = new ArrayList<ItemfavouriteDO>();
        NewitemCartList     = new ArrayList<ItemCartsDO>();
        LocalCartList       = new ArrayList<>();
        positionColorList   = new ArrayList<>();
        positionSizeList    = new ArrayList<>();
        itemSizeDO          = new ItemsSizeDO();
        reviewList          = new ArrayList<ItemsDO>();
        animRotate          = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.rotate);
        imgLoaderConf       = new ImageLoaderConfiguration.Builder(mContext).build();

        CartDatabase.init(this);
        imgLoader.init(imgLoaderConf);
        dispImage = new DisplayImageOptions.Builder()
                .showImageOnFail(R.mipmap.wool_rug)
                .showImageOnLoading(R.mipmap.wool_rug)
                .showImageForEmptyUri(R.mipmap.wool_rug)
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .delayBeforeLoading(100)
                .imageScaleType(ImageScaleType.EXACTLY)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .build();


        lst_layout.setVisibility(View.GONE);
        animUp = AnimationUtils.loadAnimation(this, R.anim.anim_up);
        animDown = AnimationUtils.loadAnimation(this, R.anim.anim_down);
        animLeft = AnimationUtils.loadAnimation(this, R.anim.anim_left);
        animRight = AnimationUtils.loadAnimation(this, R.anim.anim_right);

        Userdata = preferenceUtils.getUserDetails();
        UserPkID = Userdata.get(AppConstants.KEY_USER_ID);
        UserShopName = Userdata.get(AppConstants.KEY_USER_FST_NAME);
        queryParamsFilter = Userdata.get(AppConstants.KEY_ACCESS_TOKEN);

        if(getIntent().getExtras()!=null){
            if(getIntent().hasExtra("CatgId"))
                CatgId = getIntent().getExtras().getString("CatgId");
            if(getIntent().hasExtra("CatgName")){
                CatgName = getIntent().getExtras().getString("CatgName");
                tv_selection.setText(CatgName);
            }
        }

        filterheadings.add(new FilterHeadingDO("Colour", SelectedColorFilterList.size(), true));
        filterheadings.add(new FilterHeadingDO("Price", SelectedPriceFilterList.size(), false));
        filterheadings.add(new FilterHeadingDO("Material", SelectedMaterialFilterList.size(), false));
        filterheadings.add(new FilterHeadingDO("Pattern", SelectedPatternFilterList.size(), false));
        filterheadings.add(new FilterHeadingDO("SizeGroup", SelectedSizeGroupFilterList.size(), false));
        filterheadings.add(new FilterHeadingDO("Size", SelectedSizeFilterList.size(), false));
        filterheadings.add(new FilterHeadingDO("Categories", SelectedCategoryFilterList.size(), false));
        filterheadings.add(new FilterHeadingDO("Runners", SelectedRunnerFilterList.size(), false));


        filterHeadingAdapter = new FilterHeadingAdapter(mContext, filterheadings, this);
        lst_filterheadings.setAdapter(filterHeadingAdapter);
        lst_filter_head.setAdapter(filterHeadingAdapter);

//        View headerView = getLayoutInflater().inflate(R.layout.filter_tittle, null);
//        lst_filter_head.addHeaderView(headerView);
//        new FontType(mContext).applyfonttoView(headerView.findViewById(R.id.tv_filter_tittle), FontType.ROBOTOMEDIUM);
//        new FontType(mContext).applyfonttoView(findViewById(R.id.tv_code_label), FontType.ROBOTOBOLD);
//        new FontType(mContext).applyfonttoView(findViewById(R.id.tv_material_label), FontType.ROBOTOBOLD);
//        new FontType(mContext).applyfonttoView(findViewById(R.id.tv_desc_label), FontType.ROBOTOBOLD);
//        new FontType(mContext).applyfonttoView(findViewById(R.id.tv_price_label), FontType.ROBOTOBOLD);
//        new FontType(mContext).applyfonttoView(findViewById(R.id.tv_qauntity_label), FontType.ROBOTOBOLD);
//        new FontType(mContext).applyfonttoView(findViewById(R.id.tv_size_label), FontType.ROBOTOBOLD);
        new FontType(mContext).applyfonttoView(tv_selection, FontType.ROBOTOMEDIUM);

//        frm_color_selection.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                showColorFilters(v);
//            }
//        });

        base_filter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openFilterLayout();
            }
        });
        llPriceSort.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!sortPriceHL)
                {
                    sortactivation(AppConstants.SORTPRICEHL);
                    setListToAdapter();

                }
                else
                {
                    sortactivation(AppConstants.Nopreference);
                    setListToAdapter();

                }
            }
        });
        llPriceSort.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!sortPriceLH) {
                    sortactivation(AppConstants.SORTPRICELH);
                    setListToAdapter();
                } else {
                    sortactivation(AppConstants.Nopreference);
                    setListToAdapter();
                }
            }
        });
//        llMaxSavings.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (!sortsavingsHL) {
//                    sortactivation(AppConstants.SORTSAVEHL);
//                    setListToAdapter();
//                } else {
//                    sortactivation(AppConstants.Nopreference);
//                    setListToAdapter();
//                }
//            }
//        });
        llNewest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!sortNewest) {
                    sortactivation(AppConstants.Newest);
                    setListToAdapter();
                } else {
                    sortactivation(AppConstants.Nopreference);
                    setListToAdapter();
                }
            }
        });
        llPopularity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!sortPopularity) {
                    sortactivation(AppConstants.Popularity);
                    setListToAdapter();
                } else {
                    sortactivation(AppConstants.Nopreference);
                    setListToAdapter();
                }
            }
        });
        llMaxSavings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!sortsavingsLH) {
                    sortactivation(AppConstants.SORTSAVELH);
                    setListToAdapter();
                } else {
                    sortactivation(AppConstants.Nopreference);
                    setListToAdapter();
                }
            }
        });
        tv_most_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!sortmostviewed) {
                    sortactivation(AppConstants.MostViewed);
                    setListToAdapter();
                } else {
                    sortactivation(AppConstants.Nopreference);
                    setListToAdapter();
                }
            }
        });
        spn_sizelist.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
            @Override
            public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {
                String selection = (String) item;

                for (int i = 0; i < selecteditemsDOImg.itemSizeList.size(); i++)
                {
                    if (selection.equalsIgnoreCase(selecteditemsDOImg.itemSizeList.get(i).ItemSize))
                    {
                        selecteditemsDOImg.itemSizeList.get(i).itemSelected = true;
                        getSelectedSize(selecteditemsDOImg.itemSizeList.get(i));


                    } else {
                        selecteditemsDOImg.itemSizeList.get(i).itemSelected = false;

                    }
                }
            }
        });

        spn_sort.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
            @Override
            public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {
                String selctedString = (String) item;

                sortactivation(selctedString);
                sortingbasedonselection();

                setListToAdapter();
                //Toast.makeText(ItemsActivity.this, s, Toast.LENGTH_SHORT).show();
            }
        });
        btn_applysearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*if (TextUtils.isEmpty(edt_search.getText()))
                {
                    showAlertDialog("please enter your query","ok");
                }else
                {*/
                applyingsearchfilter(edt_search.getText().toString());
               /* }*/
            }
        });
        lst_filterheadings.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                FilterHeadingDO model = (FilterHeadingDO) parent.getItemAtPosition(position);

                for (int i = 0; i < filterheadings.size(); i++) {
                    filterheadings.get(i).selected = false;
                }
                model.selected = true;
                getTheFilterMetadata(model.filterName, position);


                filterHeadingAdapter.notifyDataSetChanged();
            }
        });
    lst_filter_head.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                FilterHeadingDO model = (FilterHeadingDO) parent.getItemAtPosition(position);

                for (int i = 0; i < filterheadings.size(); i++) {
                    filterheadings.get(i).selected = false;
                }
                model.selected = true;
                getTheFilterMetadata(model.filterName, position);


                filterHeadingAdapter.notifyDataSetChanged();
            }
        });
        JSONObject jsonObject = new JSONObject();

        try
        {
            // catJSON.put("categoryId",CatgId);
            JSONObject colorobject = new JSONObject();
            colorobject.put("colorId", CatgId);
            catJSON.put(colorobject);
            colorobject.put("email", preferenceUtils.getUserDetails().get(AppConstants.KEY_USER_EMAIL));
            JSONObject and = new JSONObject();
            and.put("and", catJSON);
            jsonObject.put("where", and);
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }
        queryParams = jsonObject.toString();
        btn_clear_all.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(allSelectedFilternameAndIDDo!=null && allSelectedFilternameAndIDDo.size()>0){
                    allSelectedFilternameAndIDDo.clear();
                    filterSelectionAdater       = new FilterSelectionAdater(allSelectedFilternameAndIDDo, mContext);
                    grd_filter_selections.setAdapter(filterSelectionAdater);
                }
                getItems(queryParams);

            }
        });

        txt_actionbar.setText(UserShopName);
        txt_catgName.setText(CatgName);

        //   txt_cartCount.startAnimation(animRotate);
        tvCartCount.setText("" + CartDatabase.getCartlistCount());


        //List icon on the header to convert view to list layout
        img_lst.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                img_grd.setImageResource(R.drawable.gridunselected);
                img_lst.setImageResource(R.drawable.listselected);
//                img_grd.setChecked(false);
//                img_lst.setChecked(true);
                getListFunctonality();

            }
        });

        //Grid icon on the header to convert view to grid layout
        img_grd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                img_grd.setImageResource(R.drawable.gridselected);
                img_lst.setImageResource(R.drawable.listunselected);
//                img_grd.setChecked(true);
//                img_lst.setChecked(false);
                getGridFunctionality();

            }
        });

        //Side panel listview to show items on it
        lst_items.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                itemsDo = (ItemsDO) parent.getItemAtPosition(position);
                tv_itemName.setText(itemsDo.ItemName);


            }
        });

        //Click of the cart icon on the list view
        chk_addtocart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (selecteditemsDOImg.cartIconChecked) {
                    //removeItemsFromCartList();
                    showAlertDialog("This item already Added to Cart", "OK");
//                    chk_addtocart.setText("Add to cart");

                } else {
                    addItemsToCartList();
                    //chk_addtocart.setText("Remove from cart");
                    chk_addtocart.setText("Add to Cart");
                }

            }
        });
        btn_buy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (selecteditemsDOImg.cartIconChecked) {
                    gotoCartActivity();
                } else {
                    addItemsToCartListandproceedtoCart();

                }

            }
        });

       /*  itemCartsDOs=CartDatabase.getAllCartList();
        cartHomeAdapter.notifyDataSetChanged();.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (cartCount > 0) {
                    Intent intent = new Intent(mContext, CartActivity.class);
                    startActivity(intent);
                } else {
                    showAlertDialog("Please add some items to cart", "OK");
                }
            }
        });*/

        /*img_home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(mContext, Dashboard.class);
                startActivity(intent);
            }
        });*/

       /* img_favourite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(mContext, FavouriteActivity.class);
                startActivity(intent);
            }
        });*/

        //Click of the big image on the list view
        ll_img_item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                showDialogImage(mContext, 700, 450, selecteditemsDOImg);
                addItemsToReviewList();
            }
        });

        //For opening the filter selection view
        rl_filterOpen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openFilterLayout();

            }
        });

        //For closing the filter selection view
        rl_filterClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                closeFilterlayout();

            }
        });


        //For applying filter on the products
        btn_applyfilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SelectedSizeFilterList.size() > 0 || SelectedPatternFilterList.size() > 0
                        || SelectedMaterialFilterList.size() > 0 || SelectedColorFilterList.size() > 0
                        || SelectedSizeGroupFilterList.size() > 0 || SelectedPriceFilterList.size() > 0
                        || SelectedCategoryFilterList.size() > 0 || SelectedRunnerFilterList.size() > 0) {
                    getFilteredProducts(SelectedSizeFilterList, SelectedPatternFilterList, SelectedMaterialFilterList
                            , SelectedColorFilterList, SelectedSizeGroupFilterList, SelectedPriceFilterList, SelectedFeelerFilterList, SelectedCategoryFilterList,
                            SelectedRunnerFilterList);
                } else {
                    Toast.makeText(mContext, "Please select some filter", Toast.LENGTH_SHORT).show();
                }
            }
        });

        //to reset the filter on the products
        btn_resetfilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SelectedSizeFilterList.size() > 0 || SelectedPatternFilterList.size() > 0
                        || SelectedMaterialFilterList.size() > 0 || SelectedColorFilterList.size() > 0
                        || SelectedSizeGroupFilterList.size() > 0 || SelectedPriceFilterList.size() > 0
                        || SelectedCategoryFilterList.size() > 0 || SelectedRunnerFilterList.size() > 0) {


                    action = AppConstants.RESET_FILTER;

                    getItems(queryParams);
                } else {
                    Toast.makeText(mContext, "No filters has been selected", Toast.LENGTH_SHORT).show();
                }
            }
        });

        final DecimalFormat decimalFormat = new DecimalFormat("#.00");
        tvIncrese.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tvQuantity.setText(""+(StringUtils.getInt(tvQuantity.getText().toString())+1));

                tv_offer_price.setText("" +String.format("%.2f", StringUtils.getDouble(tv_offer_price.getText().toString())+ StringUtils.getDouble(itemSizeDO.BasePrice)));
                tv_original_price.setText("" +(StringUtils.getDouble(tv_original_price.getText().toString().replace("£", ""))+ StringUtils.getDouble(itemSizeDO.FinalPrice)));
                tv_original_price.setPaintFlags(tv_original_price.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);

            }
        });
        tvDecrese.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(StringUtils.getInt(tvQuantity.getText().toString())>1)
                {
                    tvQuantity.setText(""+(StringUtils.getInt(tvQuantity.getText().toString())-1));
                    tv_offer_price.setText("" +String.format("%.2f", StringUtils.getDouble(tv_offer_price.getText().toString())- StringUtils.getDouble(itemSizeDO.BasePrice)));
                    tv_original_price.setText("" +(StringUtils.getDouble(tv_original_price.getText().toString().replace("£", "")) - StringUtils.getDouble(itemSizeDO.FinalPrice)));
                    tv_original_price.setPaintFlags(tv_original_price.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                }
            }
        });


        //Get the latest reviewd items

       /* img_review.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(img_review.isChecked())
                {
                    ll_review.startAnimation(animLeft);
                    ll_review.setVisibility(View.VISIBLE);

                   getTheReviewList();
                }
                else
                {
                    ll_review.startAnimation(animRight);
                    ll_review.setVisibility(View.GONE);
                }

            }
        });*/

        /* grd_size.setOnItemClickListener(new AdapterView.OnItemClickListener() {
             @Override
             public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                 Toast.makeText(mContext, "Size Selected =", Toast.LENGTH_SHORT).show();
             }
         });*/


    }

    private void sortactivation(String selctedString) {
        if (selctedString.equalsIgnoreCase(AppConstants.Nopreference))
        {
            sortnopreff     = true;
            sortmostviewed  = false;
            sortPriceHL     = false;
            sortPriceLH     = false;
            sortsavingsHL   = false;
            sortsavingsLH   = false;
            sortPopularity  = false;
            sortNewest      = false;

            btn_price_hl.setImageResource(R.drawable.sort_arrows);
            btn_price_lh.setImageResource(R.drawable.sort_arrows);
            btn_savings_hl.setImageResource(R.drawable.sort_arrows);
            btn_savings_lh.setImageResource(R.drawable.sort_arrows);
            ibPopularity.setImageResource(R.drawable.sort_arrows);
            ibNewest.setImageResource(R.drawable.sort_arrows);

            tv_savings.setTextColor(ContextCompat.getColor(mContext, R.color.light_grey));
            tv_most_view.setTextColor(ContextCompat.getColor(mContext, R.color.light_grey));
            tv_price.setTextColor(ContextCompat.getColor(mContext, R.color.light_grey));
            tvPopularity.setTextColor(ContextCompat.getColor(mContext, R.color.light_grey));
            tvNewest.setTextColor(ContextCompat.getColor(mContext, R.color.light_grey));
        }
        else if (selctedString.equalsIgnoreCase(AppConstants.MostViewed))
        {

            sortnopreff     = true;
            sortmostviewed  = false;
            sortPriceHL     = false;
            sortPriceLH     = false;
            sortsavingsHL   = false;
            sortsavingsLH   = false;
            sortPopularity  = false;
            sortNewest      = false;

            btn_price_hl.setImageResource(R.drawable.sort_arrows);
            btn_price_lh.setImageResource(R.drawable.sort_arrows);
            btn_savings_hl.setImageResource(R.drawable.sort_arrows);
            btn_savings_lh.setImageResource(R.drawable.sort_arrows);
            ibPopularity.setImageResource(R.drawable.sort_arrows);
            ibNewest.setImageResource(R.drawable.sort_arrows);

            tv_savings.setTextColor(ContextCompat.getColor(mContext, R.color.light_grey));
            tv_most_view.setTextColor(ContextCompat.getColor(mContext, R.color.light_grey));
            tv_price.setTextColor(ContextCompat.getColor(mContext, R.color.light_grey));
            tvPopularity.setTextColor(ContextCompat.getColor(mContext, R.color.light_grey));
            tvNewest.setTextColor(ContextCompat.getColor(mContext, R.color.light_grey));

            sortnopreff = false;
            sortmostviewed = true;
            sortPriceHL = false;
            sortPriceLH = false;
            sortsavingsHL = false;
            sortsavingsLH = false;
            btn_price_hl.setImageResource(R.mipmap.price_high_disable);
            btn_price_lh.setImageResource(R.mipmap.price_low_disable);
            btn_savings_hl.setImageResource(R.mipmap.price_high_disable);
            btn_savings_lh.setImageResource(R.mipmap.price_low_disable);
            tv_savings.setTextColor(ContextCompat.getColor(mContext, R.color.light_grey));
            tv_most_view.setTextColor(ContextCompat.getColor(mContext, R.color.black_color));
            tv_price.setTextColor(ContextCompat.getColor(mContext, R.color.light_grey));
        }
        else if (selctedString.equalsIgnoreCase(AppConstants.SORTPRICEHL))
        {
            sortnopreff = false;
            sortmostviewed = false;
            sortPriceHL = true;
            sortPriceLH = false;
            sortsavingsHL = false;
            sortsavingsLH = false;
            btn_price_hl.setImageResource(R.mipmap.price_high_enable);
            btn_price_lh.setImageResource(R.mipmap.price_low_disable);
            btn_savings_hl.setImageResource(R.mipmap.price_high_disable);
            btn_savings_lh.setImageResource(R.mipmap.price_low_disable);
            tv_savings.setTextColor(ContextCompat.getColor(mContext, R.color.light_grey));
            tv_most_view.setTextColor(ContextCompat.getColor(mContext, R.color.light_grey));
            tv_price.setTextColor(ContextCompat.getColor(mContext, R.color.black_color));
        } else if (selctedString.equalsIgnoreCase(AppConstants.SORTPriceLH)) {
            sortnopreff = false;
            sortmostviewed = false;
            sortPriceHL = false;
            sortPriceLH = true;
            sortsavingsHL = false;
            sortsavingsLH = false;
            btn_price_hl.setImageResource(R.mipmap.price_high_disable);
            btn_price_lh.setImageResource(R.mipmap.price_low_enable);
            btn_savings_hl.setImageResource(R.mipmap.price_high_disable);
            btn_savings_lh.setImageResource(R.mipmap.price_low_disable);
            tv_savings.setTextColor(ContextCompat.getColor(mContext, R.color.light_grey));
            tv_most_view.setTextColor(ContextCompat.getColor(mContext, R.color.light_grey));
            tv_price.setTextColor(ContextCompat.getColor(mContext, R.color.black_color));
        } else if (selctedString.equalsIgnoreCase(AppConstants.SORTSAVEHL)) {
            sortnopreff = false;
            sortmostviewed = false;
            sortPriceHL = false;
            sortPriceLH = false;
            sortsavingsHL = true;
            sortsavingsLH = false;
            btn_price_hl.setImageResource(R.mipmap.price_high_enable);
            btn_price_lh.setImageResource(R.mipmap.price_low_disable);
            btn_savings_hl.setImageResource(R.mipmap.price_high_enable);
            btn_savings_lh.setImageResource(R.mipmap.price_low_disable);
            tv_savings.setTextColor(ContextCompat.getColor(mContext, R.color.black_color));
            tv_most_view.setTextColor(ContextCompat.getColor(mContext, R.color.light_grey));
            tv_price.setTextColor(ContextCompat.getColor(mContext, R.color.light_grey));
        }
        else if (selctedString.equalsIgnoreCase(AppConstants.SORTSAVELH)) {
            sortnopreff    = false;
            sortmostviewed = false;
            sortPriceHL    = false;
            sortPriceLH    = false;
            sortsavingsHL  = false;
            sortsavingsLH  = true;
            btn_price_hl.setImageResource(R.mipmap.price_high_enable);
            btn_price_lh.setImageResource(R.mipmap.price_low_disable);
            btn_savings_hl.setImageResource(R.mipmap.price_high_disable);
            btn_savings_lh.setImageResource(R.mipmap.price_low_enable);
            tv_savings.setTextColor(ContextCompat.getColor(mContext, R.color.black_color));
            tv_most_view.setTextColor(ContextCompat.getColor(mContext, R.color.light_grey));
            tv_price.setTextColor(ContextCompat.getColor(mContext, R.color.light_grey));
        }


    }


    private ColorFilterAdapter colorFilterAdapter;
    private void showColorFilters(View view){
        View popupView = inflater.inflate(R.layout.drop_down_list_view, null);
        if(popupWindow == null)
            popupWindow                         = new PopupWindow(popupView, ViewGroup.LayoutParams.WRAP_CONTENT, 500);
        RecyclerView rvFilterItems              = (RecyclerView) popupView.findViewById(R.id.rvFilterItems);
        TextView tvApply                        = (TextView)popupView.findViewById(R.id.tvApply);
        popupWindow.setOutsideTouchable(true);
        tvApply.setVisibility(View.GONE);
        rvFilterItems.setLayoutManager(new GridLayoutManager(mContext, 2, GridLayoutManager.VERTICAL, false));
        rvFilterItems.setAdapter(colorFilterAdapter = new ColorFilterAdapter(mContext, popupWindow));
        tvApply.setOnClickListener(new Button.OnClickListener(){

            @Override
            public void onClick(View v) {

//                if(isNetworkConnectionAvailable(ItemsActivity.this)){
//                    showLoaderNew();
//                    new CommonBL(ItemsActivity.this, new DataListener() {
//                        @Override
//                        public void dataRetreived(Response data) {
//
//                        }
//                    }).getFilteredProducts(selectedColors);
//                }
                popupWindow.dismiss();
            }});

        if(!popupWindow.isShowing())
            popupWindow.showAsDropDown(view);//, 50, -30);
        else
            popupWindow.dismiss();

    }

    private class ColorFilterAdapter extends RecyclerView.Adapter<ViewHolder>{

        private PopupWindow popupWindow;
        private Context context;
        public ColorFilterAdapter(Context context, PopupWindow popupWindow){
            this.context     = context;
            this.popupWindow = popupWindow;
        }
        private void refresh(ArrayList<ColorsDO> colorsDOs){
            notifyDataSetChanged();
        }
        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View convertView = LayoutInflater.from(context).inflate(R.layout.spinner_cell_layout, parent, false);
            return new ViewHolder(convertView);
        }

        @Override
        public void onBindViewHolder(final ViewHolder holder, final int position) {

            holder.tvItemCell.setText(AppConstants.colorsDOs.get(position).ColorName);
            if(AppConstants.colorsDOs.get(position).isSelected){
                holder.ivSelected.setVisibility(View.VISIBLE);
            }
            else {
                holder.ivSelected.setVisibility(View.INVISIBLE);
            }
            holder.ivSelected.setVisibility(View.GONE);
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(AppConstants.colorsDOs.get(position).isSelected){
                        AppConstants.colorsDOs.get(position).isSelected = false;
                    }
                    else {
                        AppConstants.colorsDOs.get(position).isSelected = true;
                    }
                    ArrayList<String> selectedColors = new ArrayList<String>();
                    SelectedColorFilterList.clear();
                    for(int i=0; i<AppConstants.colorsDOs.size(); i++){
                        if(AppConstants.colorsDOs.get(i).isSelected){
                            selectedColors.add(AppConstants.colorsDOs.get(i).ColorId);
                            FilternameAndIDDo filternameAndIDDo = new FilternameAndIDDo();
                            filternameAndIDDo.id                = AppConstants.colorsDOs.get(i).ColorId;
                            filternameAndIDDo.name              = AppConstants.colorsDOs.get(i).ColorName;
                            SelectedColorFilterList.add(filternameAndIDDo);
                        }
                    }
                    getFilteredProducts(SelectedSizeFilterList, SelectedPatternFilterList, SelectedMaterialFilterList
                            , SelectedColorFilterList, SelectedSizeGroupFilterList, SelectedPriceFilterList, SelectedFeelerFilterList, SelectedCategoryFilterList,
                            SelectedRunnerFilterList);
                    popupWindow.dismiss();
//                    notifyDataSetChanged();
                }
            });
        }

        @Override
        public int getItemCount() {
            return AppConstants.colorsDOs!=null?AppConstants.colorsDOs.size():0;
        }
    }
    private static class ViewHolder extends RecyclerView.ViewHolder{
        private TextView tvItemCell;
        private ImageView ivSelected;
        public ViewHolder(View itemView) {
            super(itemView);
            tvItemCell          = (TextView) itemView.findViewById(R.id.tvItemCell);
            ivSelected          = (ImageView) itemView.findViewById(R.id.ivSelected);
        }
    }

//    private void sortactivation(String selctedString) {
//        if (selctedString.equalsIgnoreCase(AppConstants.Nopreference))
//        {
//            sortnopreff     = true;
//            sortmostviewed  = false;
//            sortPriceHL     = false;
//            sortPriceLH     = false;
//            sortsavingsHL   = false;
//            sortsavingsLH   = false;
//            sortPopularity  = false;
//            sortNewest      = false;
//
//            btn_price_hl.setImageResource(R.drawable.sort_arrows);
//            btn_price_lh.setImageResource(R.drawable.sort_arrows);
//            btn_savings_hl.setImageResource(R.drawable.sort_arrows);
//            btn_savings_lh.setImageResource(R.drawable.sort_arrows);
//            ibPopularity.setImageResource(R.drawable.sort_arrows);
//            ibNewest.setImageResource(R.drawable.sort_arrows);
//
//            tv_savings.setTextColor(ContextCompat.getColor(mContext, R.color.light_grey));
//            tv_most_view.setTextColor(ContextCompat.getColor(mContext, R.color.light_grey));
//            tv_price.setTextColor(ContextCompat.getColor(mContext, R.color.light_grey));
//            tvPopularity.setTextColor(ContextCompat.getColor(mContext, R.color.light_grey));
//            tvNewest.setTextColor(ContextCompat.getColor(mContext, R.color.light_grey));
//
//        }
//        else if (selctedString.equalsIgnoreCase(AppConstants.MostViewed)) {
//            sortnopreff         = false;
//            sortmostviewed      = true;
//            sortPriceHL         = false;
//            sortPriceLH         = false;
//            sortsavingsHL       = false;
//            sortsavingsLH       = false;
//            sortPopularity      = false;
//            sortNewest          = false;
//
//            btn_price_hl.setImageResource(R.drawable.sort_arrows);
//            btn_price_lh.setImageResource(R.drawable.sort_arrows);
//            btn_savings_hl.setImageResource(R.drawable.sort_arrows);
//            btn_savings_lh.setImageResource(R.drawable.sort_arrows);
//            ibPopularity.setImageResource(R.drawable.sort_arrows);
//            ibNewest.setImageResource(R.drawable.sort_arrows);
//
//
//            tv_savings.setTextColor(ContextCompat.getColor(mContext, R.color.light_grey));
//            tv_most_view.setTextColor(ContextCompat.getColor(mContext, R.color.black_color));
//            tv_price.setTextColor(ContextCompat.getColor(mContext, R.color.light_grey));
//            tvPopularity.setTextColor(ContextCompat.getColor(mContext, R.color.light_grey));
//            tvNewest.setTextColor(ContextCompat.getColor(mContext, R.color.light_grey));
//        }
//        else if (selctedString.equalsIgnoreCase(AppConstants.SORTPRICEHL)) {
//            sortnopreff     = false;
//            sortmostviewed  = false;
//            sortPriceHL     = true;
//            sortPriceLH     = false;
//            sortsavingsHL   = false;
//            sortsavingsLH   = false;
//            sortPopularity  = false;
//            sortNewest      = false;
//
//            btn_price_hl.setImageResource(R.drawable.sort_arrows_active);
//            btn_price_lh.setImageResource(R.drawable.sort_arrows);
//            btn_savings_hl.setImageResource(R.drawable.sort_arrows);
//            btn_savings_lh.setImageResource(R.drawable.sort_arrows);
//            ibPopularity.setImageResource(R.drawable.sort_arrows);
//            ibNewest.setImageResource(R.drawable.sort_arrows);
//
//            tv_savings.setTextColor(ContextCompat.getColor(mContext, R.color.light_grey));
//            tv_most_view.setTextColor(ContextCompat.getColor(mContext, R.color.light_grey));
//            tv_price.setTextColor(ContextCompat.getColor(mContext, R.color.black_color));
//            tvPopularity.setTextColor(ContextCompat.getColor(mContext, R.color.light_grey));
//            tvNewest.setTextColor(ContextCompat.getColor(mContext, R.color.light_grey));
//        }
//        else if (selctedString.equalsIgnoreCase(AppConstants.SORTPRICELH))
//        {
//            sortnopreff = false;
//            sortmostviewed = false;
//            sortPriceHL = false;
//            sortPriceLH = true;
//            sortsavingsHL = false;
//            sortsavingsLH = false;
//            sortPopularity=false;
//            sortNewest      = false;
//
//            btn_price_hl.setImageResource(R.mipmap.sort_arrows);
//            btn_price_lh.setImageResource(R.mipmap.sort_arrows_active);
//            btn_savings_hl.setImageResource(R.mipmap.sort_arrows);
//            btn_savings_lh.setImageResource(R.mipmap.sort_arrows);
//            ibPopularity.setImageResource(R.drawable.sort_arrows);
//            ibNewest.setImageResource(R.drawable.sort_arrows);
//
//            tv_savings.setTextColor(ContextCompat.getColor(mContext, R.color.light_grey));
//            tv_most_view.setTextColor(ContextCompat.getColor(mContext, R.color.light_grey));
//            tv_price.setTextColor(ContextCompat.getColor(mContext, R.color.black_color));
//            tvPopularity.setTextColor(ContextCompat.getColor(mContext, R.color.light_grey));
//            tvNewest.setTextColor(ContextCompat.getColor(mContext, R.color.light_grey));
//        }
//        else if (selctedString.equalsIgnoreCase(AppConstants.SORTSAVEHL))
//        {
//            sortnopreff = false;
//            sortmostviewed = false;
//            sortPriceHL = false;
//            sortPriceLH = false;
//            sortsavingsHL = true;
//            sortsavingsLH = false;
//            sortPopularity=false;
//            sortNewest      = false;
//
//            btn_price_hl.setImageResource(R.mipmap.sort_arrows_active);
//            btn_price_lh.setImageResource(R.mipmap.sort_arrows);
//            btn_savings_hl.setImageResource(R.mipmap.sort_arrows_active);
//            btn_savings_lh.setImageResource(R.mipmap.sort_arrows);
//            ibPopularity.setImageResource(R.drawable.sort_arrows);
//            ibNewest.setImageResource(R.drawable.sort_arrows);
//
//            tv_savings.setTextColor(ContextCompat.getColor(mContext, R.color.black_color));
//            tv_most_view.setTextColor(ContextCompat.getColor(mContext, R.color.light_grey));
//            tv_price.setTextColor(ContextCompat.getColor(mContext, R.color.light_grey));
//            tvPopularity.setTextColor(ContextCompat.getColor(mContext, R.color.light_grey));
//            tvNewest.setTextColor(ContextCompat.getColor(mContext, R.color.light_grey));
//        }
//        else if (selctedString.equalsIgnoreCase(AppConstants.SORTSAVELH))
//        {
//            sortnopreff = false;
//            sortmostviewed = false;
//            sortPriceHL = false;
//            sortPriceLH = false;
//            sortsavingsHL = false;
//            sortsavingsLH = true;
//            sortPopularity=false;
//            sortNewest      = false;
//
//            btn_price_hl.setImageResource(R.mipmap.sort_arrows_active);
//            btn_price_lh.setImageResource(R.mipmap.sort_arrows);
//            btn_savings_hl.setImageResource(R.mipmap.sort_arrows);
//            btn_savings_lh.setImageResource(R.mipmap.price_low_enable);
//            ibPopularity.setImageResource(R.drawable.sort_arrows);
//            ibNewest.setImageResource(R.drawable.sort_arrows);
//
//            tv_savings.setTextColor(ContextCompat.getColor(mContext, R.color.black_color));
//            tv_most_view.setTextColor(ContextCompat.getColor(mContext, R.color.light_grey));
//            tv_price.setTextColor(ContextCompat.getColor(mContext, R.color.light_grey));
//            tvPopularity.setTextColor(ContextCompat.getColor(mContext, R.color.light_grey));
//            tvNewest.setTextColor(ContextCompat.getColor(mContext, R.color.light_grey));
//        }
//      else if (selctedString.equalsIgnoreCase(AppConstants.Popularity))
//        {
//            sortnopreff         = false;
//            sortmostviewed      = false;
//            sortPriceHL         = false;
//            sortPriceLH         = false;
//            sortsavingsHL       = false;
//            sortsavingsLH       = false;
//            sortPopularity      = true;
//            sortNewest          = false;
//
//            btn_price_hl.setImageResource(R.drawable.sort_arrows);
//            btn_price_lh.setImageResource(R.drawable.sort_arrows);
//            btn_savings_hl.setImageResource(R.drawable.sort_arrows);
//            btn_savings_lh.setImageResource(R.drawable.sort_arrows);
//            ibPopularity.setImageResource(R.drawable.sort_arrows_active);
//            ibNewest.setImageResource(R.drawable.sort_arrows);
//
//            tv_savings.setTextColor(ContextCompat.getColor(mContext, R.color.light_grey));
//            tv_most_view.setTextColor(ContextCompat.getColor(mContext, R.color.light_grey));
//            tv_price.setTextColor(ContextCompat.getColor(mContext, R.color.light_grey));
//            tvPopularity.setTextColor(ContextCompat.getColor(mContext, R.color.black_color));
//            tvNewest.setTextColor(ContextCompat.getColor(mContext, R.color.light_grey));
//        }
//       else if (selctedString.equalsIgnoreCase(AppConstants.Newest))
//        {
//            sortnopreff         = false;
//            sortmostviewed      = false;
//            sortPriceHL         = false;
//            sortPriceLH         = false;
//            sortsavingsHL       = false;
//            sortsavingsLH       = false;
//            sortPopularity      = false;
//            sortNewest          = true;
//
//            btn_price_hl.setImageResource(R.mipmap.sort_arrows);
//            btn_price_lh.setImageResource(R.mipmap.sort_arrows);
//            btn_savings_hl.setImageResource(R.mipmap.sort_arrows);
//            btn_savings_lh.setImageResource(R.mipmap.sort_arrows);
//            ibPopularity.setImageResource(R.drawable.sort_arrows);
//            ibNewest.setImageResource(R.drawable.sort_arrows_active);
//
//            tv_savings.setTextColor(ContextCompat.getColor(mContext, R.color.light_grey));
//            tv_most_view.setTextColor(ContextCompat.getColor(mContext, R.color.light_grey));
//            tv_price.setTextColor(ContextCompat.getColor(mContext, R.color.light_grey));
//            tvPopularity.setTextColor(ContextCompat.getColor(mContext, R.color.light_grey));
//            tvNewest.setTextColor(ContextCompat.getColor(mContext, R.color.black_color));
//        }
//
//
//    }

    private void sortingbasedonselection() {

    }

    private void applyingsearchfilter(String textToSearch) {

        StringBuilder searchQuiery = new StringBuilder();
        searchQuiery.append(".*");
        searchQuiery.append(textToSearch);
        searchQuiery.append(".*");

        JSONObject emailObject = new JSONObject();
        JSONObject quiery = new JSONObject();
        JSONObject skuId = new JSONObject();
        JSONObject name = new JSONObject();

        JSONObject orObject = new JSONObject();
        JSONObject andObject = new JSONObject();

        JSONArray orArray = new JSONArray();
        JSONArray andArray = new JSONArray();
        JSONObject whereObject = new JSONObject();

        try {
            emailObject.put("email", preferenceUtils.getUserDetails().get(AppConstants.KEY_USER_EMAIL));

            quiery.put("like", searchQuiery);
            name.put("name", quiery);
            orArray.put(name);

            skuId.put("skuId", quiery);
            orArray.put(skuId);

            orObject.put("or", orArray);
            andArray.put(orObject);
            andArray.put(emailObject);


            andObject.put("and", andArray);
            whereObject.put("where", andObject);

        } catch (JSONException e) {


        }

        getItems(whereObject.toString());
    }

    private void closeFilterlayout() {
        // ll_filters.startAnimation(animUp);
        //  ll_filters.setVisibility(View.GONE);
        //  rl_filterClose.setVisibility(View.GONE);
        //rl_filterOpen.setVisibility(View.VISIBLE);
    }

    private boolean isRefineFilterOpen;
    private void openFilterLayout() {

//        if(refine_filter.getVisibility() == View.VISIBLE){
//            isRefineFilterOpen = true;
//            refine_filter.setVisibility(View.GONE);
//        }
//        else {
//            refine_filter.setVisibility(View.VISIBLE);
//            isRefineFilterOpen = false;
//        }
        int diff = Float.compare(refine_filter.getTranslationY(), 120);
        refine_filter.setVisibility(View.VISIBLE);
        if (diff == 0) {
            refine_filter.animate().translationY(-6000).setDuration(300);

        } else {
//            refine_filter.setVisibility(View.GONE);
            refine_filter.animate().translationY(120).setDuration(300);
        }
//         ll_filters.startAnimation(animDown);
        //ll_filters.setVisibility(View.VISIBLE);
        //  rl_filterClose.setVisibility(View.VISIBLE);
        //  rl_filterOpen.setVisibility(View.GONE);
    }


    private void getTheFilterMetadata(String FilterName, int position) {

        FilternameAndIDDo filternameAndIDDo = new FilternameAndIDDo();
        filternameAndIDDo.type = "DUMMY";
        ArrayList<FilternameAndIDDo> filternameAndIDDos = new ArrayList<>();
        for (int i = 0; i < position; i++) {
            filternameAndIDDos.add(filternameAndIDDo);
        }

        if (FilterName.equals("Price")) {
            sizeGroupFilterAdapter = new SizeGroupFilterAdapter(mContext, PricesNamesList, filternameAndIDDos, "Price");
            sizeGroupFilterAdapter.registerSelectedGroupFilterListener(this);
            grd_filterdata.setAdapter(sizeGroupFilterAdapter);
        } else if (FilterName.equals("Colour")) {
            sizeGroupFilterAdapter = new SizeGroupFilterAdapter(mContext, ColorNamesList, filternameAndIDDos, "Colour");
            sizeGroupFilterAdapter.registerSelectedGroupFilterListener(this);
            grd_filterdata.setAdapter(sizeGroupFilterAdapter);
        } else if (FilterName.equals("Material")) {
            sizeGroupFilterAdapter = new SizeGroupFilterAdapter(mContext, MaterialNamesList, filternameAndIDDos, "Material");
            sizeGroupFilterAdapter.registerSelectedGroupFilterListener(this);
            grd_filterdata.setAdapter(sizeGroupFilterAdapter);
        } else if (FilterName.equals("Pattern")) {
            sizeGroupFilterAdapter = new SizeGroupFilterAdapter(mContext, PatternNamesList, filternameAndIDDos, "Pattern");
            sizeGroupFilterAdapter.registerSelectedGroupFilterListener(this);
            grd_filterdata.setAdapter(sizeGroupFilterAdapter);
        } else if (FilterName.equals("SizeGroup")) {
            sizeGroupFilterAdapter = new SizeGroupFilterAdapter(mContext, SizeGroupNamesList, filternameAndIDDos, "SizeGroup");
            sizeGroupFilterAdapter.registerSelectedGroupFilterListener(this);
            grd_filterdata.setAdapter(sizeGroupFilterAdapter);
        } else if (FilterName.equals("Size")) {
            sizeGroupFilterAdapter = new SizeGroupFilterAdapter(mContext, SizeNamesList, filternameAndIDDos, "Size");
            sizeGroupFilterAdapter.registerSelectedGroupFilterListener(this);
            grd_filterdata.setAdapter(sizeGroupFilterAdapter);
        } else if (FilterName.equals("Runners")) {
            sizeGroupFilterAdapter = new SizeGroupFilterAdapter(mContext, RunnerList, filternameAndIDDos, "Runners");
            sizeGroupFilterAdapter.registerSelectedGroupFilterListener(this);
            grd_filterdata.setAdapter(sizeGroupFilterAdapter);
        } else if (FilterName.equals("Categories")) {
            sizeGroupFilterAdapter = new SizeGroupFilterAdapter(mContext, CategoryList, filternameAndIDDos, "Categories");
            sizeGroupFilterAdapter.registerSelectedGroupFilterListener(this);
            grd_filterdata.setAdapter(sizeGroupFilterAdapter);
        }
        lst_filter_data.setAdapter(sizeGroupFilterAdapter);

    }

    private void getTheReviewList() {
        reviewList = CartDatabase.getAllReviewList();
        itemsReviewListAdapter = new ItemsReviewListAdapter(mContext, reviewList, itemCartList, itemFavouriteList);
        itemsReviewListAdapter.registerCartListener(this);
        itemsReviewListAdapter.registerFavouriteListener(this);
        itemsReviewListAdapter.registerSelectedListItemListener(this);
        lst_review.setAdapter(itemsReviewListAdapter);
        scrollToSelectedPosition();
    }

    public void showDialogImage(Context context, int x, int y, ItemsDO selecteditemsDOImg) {
        dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.cart_dialogbigimage);
        dialog.setCanceledOnTouchOutside(false);

        ll_IMG = (LinearLayout) dialog.findViewById(R.id.ll_IMG);
        img_crossColor = (ImageView) dialog.findViewById(R.id.img_crossColor);


        img_crossColor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        imgLoader.getInstance().displayImage(selecteditemsDOImg.ItemBigImgUrl, new BgViewAware(ll_IMG), dispImage);

        dialog.show();

    }

    @SuppressLint("SetTextI18n")
    @Override
    protected void onResume() {
        super.onResume();
        LocalCartList = CartDatabase.getAllCartList();
        favsList = CartDatabase.getAllFavouriteList();
        cartCount = CartDatabase.getCartlistCount();
        int PositionListCount = CartDatabase.getPositionlistCount();
        LogUtils.error(ItemsActivity.class.getSimpleName(), "PositionListCount =" + PositionListCount);
        scrollToSelectedPosition();
        tvCartCount.setText("" + cartCount);
        getItems(queryParams);
    }

    private void getListFunctonality() {
        if(arrItems!=null && arrItems.size()>0)
            selecteditemsDOImg = arrItems.get(0);
        selecteditemsDOImg.setPositionSelected(true);
        lst_layout.setVisibility(View.VISIBLE);
        grd_layout.setVisibility(View.GONE);


        itemsListAdapter = new ItemsListAdapter(mContext, arrItems, itemCartList, itemFavouriteList);
        itemsListAdapter.registerCartListener(this);
        itemsListAdapter.registerFavouriteListener(this);
        itemsListAdapter.registerSelectedListItemListener(this);
        lst_items.setAdapter(itemsListAdapter);
        scrollToSelectedPosition();


        tv_itemName.setText(arrItems.get(0).ItemName);
        //  txt_productdesc.setText(arrItems.get(0).ItemDesc);
        //  txt_productsecnddesc.setText(arrItems.get(0).ItemProDesc);
        tv_material.setText(arrItems.get(0).ItemMaterial);
        tv_product_id.setText(arrItems.get(0).ProductID);


        positionSizeList = arrItems.get(0).itemSizeList;
        positionSizeAdapter = new PositionSizeAdapter(mContext, positionSizeList);
        positionSizeAdapter.registerSizePriceListener(this);
        lst_SizePrice.setAdapter(positionSizeAdapter);


        if (arrItems.get(0).cartIconChecked) {
            //  chk_addtocart.setText("Remove from cart");
        } else {
            chk_addtocart.setText("Add to cart");
        }

        imgLoader.getInstance().displayImage(arrItems.get(0).ItemBigImgUrl, new BgViewAware(ll_img_item), dispImage);
        setAdaptertoHorizontalList(arrItems.get(0));
    }

    private void setAdaptertoHorizontalList(ItemsDO itemsDo) {
        listImagesAdapter = new ListImagesAdapter(itemsDo.urls, mContext, new GetStringOnClickListener() {
            @Override
            public void getStringOnClick(String url) {
                imgLoader.getInstance().displayImage(url, new BgViewAware(ll_img_item), dispImage);

            }
        });

        LinearLayoutManager mLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false);
        lst_images.setLayoutManager(mLayoutManager);
//        RecyclerDivider recyclerDivider = new RecyclerDivider(ContextCompat.getDrawable(mContext, R.drawable.transparent_drawable));
//        lst_images.addItemDecoration(recyclerDivider);
        ((RecyclerView)findViewById(R.id.lst_images)).removeAllViews();
        ((RecyclerView)findViewById(R.id.lst_images)).setAdapter(listImagesAdapter);
//        listImagesAdapter.refresh(itemsDo.urls);
    }

    private void scrollToSelectedPosition() {
        selectedPosition = CartDatabase.getPosition();
        LogUtils.error(ItemsActivity.class.getSimpleName(), "selectedPosition after scroll " + selectedPosition);
        lst_items.setSelection(selectedPosition);
    }

    private void getGridFunctionality() {
        lst_layout.setVisibility(View.GONE);
        grd_layout.setVisibility(View.VISIBLE);

        setListToAdapter();
        /*itemsGridAdapter = new ItemsGridAdapter(mContext, arrItems, itemCartList, itemFavouriteList);
        itemsGridAdapter.registerCartListener(this);
        itemsGridAdapter.registerFavouriteListener(this);
        itemsGridAdapter.registerSelectedGridItemListener(this);
        grd_items.setAdapter(itemsGridAdapter);*/
    }

    private void removeItemsFromCartList() {
        selecteditemsDOImg.cartIconChecked = false;
        String ItemId = selecteditemsDOImg.ItemId;
        CartDatabase.deletecartlistById(ItemId);
        getCartListNormal();
        itemsGridAdapter.notifyDataSetChanged();
        itemsListAdapter.notifyDataSetChanged();

        itemCartsDOs = CartDatabase.getAllCartList();

        cartHomeAdapter = new CartHomeAdapter(itemCartsDOs, ItemsActivity.this, this);

        lst_cart_items.setAdapter(cartHomeAdapter);
    }

    private void addItemsToCartList() {
        if (itemSizeDO.itemSelected) {
            selecteditemsDOImg.cartIconChecked = true;
            selecteditemsDOImg.ItemQuantity    = StringUtils.getInt(tvQuantity.getText().toString());
            itemsListAdapter.notifyDataSetChanged();
            itemsGridAdapter.notifyDataSetChanged();
            cartDatabase.addCartData(new ItemCartsDO(selecteditemsDOImg.ItemId, selecteditemsDOImg.ProductID, selecteditemsDOImg.ItemName, selecteditemsDOImg.ItemQuantity, itemSizeDO.BasePrice, itemSizeDO.DiscountedPrice, itemSizeDO.FinalPrice, itemSizeDO.ItemSize,
                    selecteditemsDOImg.itemColorList.get(0), selecteditemsDOImg.ItemPattern, selecteditemsDOImg.ItemMaterial, selecteditemsDOImg.ItemSmallImgUrl,
                    selecteditemsDOImg.ItemMediumImgUrl, selecteditemsDOImg.ItemBigImgUrl, 1, selecteditemsDOImg.ItemRating, selecteditemsDOImg.ItemPerSaved, selecteditemsDOImg.catogiryname));

            LogUtils.info(CartDatabase.class.getSimpleName(), "catogiry id" + selecteditemsDOImg.CategoryId);

            if (selecteditemsDOImg.itemColorList.size() > 0) {
                cartDatabase.addColorData(selecteditemsDOImg.ItemId, selecteditemsDOImg.itemColorList);
            }
            for (int i = 0; i < selecteditemsDOImg.itemSizeList.size(); i++) {
                cartDatabase.addSizeData(new SizeListDO(selecteditemsDOImg.ItemId, selecteditemsDOImg.itemSizeList.get(i).ItemSize, selecteditemsDOImg.itemSizeList.get(i).BasePrice, selecteditemsDOImg.itemSizeList.get(i).DiscountedPrice, selecteditemsDOImg.itemSizeList.get(i).FinalPrice));
            }
            getCartListNormal();
        } else {
            //chk_addtocart.setChecked(false);
            showAlertDialog("Please select a size", "OK");
        }

        itemCartsDOs = CartDatabase.getAllCartList();
        cartHomeAdapter.notifyDataSetChanged();
    }

    private void addItemsToCartListandproceedtoCart() {
        if (itemSizeDO.itemSelected) {
            selecteditemsDOImg.cartIconChecked = true;
            itemsListAdapter.notifyDataSetChanged();
            itemsGridAdapter.notifyDataSetChanged();
            cartDatabase.addCartData(new ItemCartsDO(selecteditemsDOImg.ItemId, selecteditemsDOImg.ProductID, selecteditemsDOImg.ItemName, 1, itemSizeDO.BasePrice, itemSizeDO.DiscountedPrice, itemSizeDO.FinalPrice, itemSizeDO.ItemSize,
                    selecteditemsDOImg.itemColorList.get(0), selecteditemsDOImg.ItemPattern, selecteditemsDOImg.ItemMaterial, selecteditemsDOImg.ItemSmallImgUrl,
                    selecteditemsDOImg.ItemMediumImgUrl, selecteditemsDOImg.ItemBigImgUrl, 1, selecteditemsDOImg.ItemRating, selecteditemsDOImg.ItemPerSaved, selecteditemsDOImg.catogiryname));

            LogUtils.info(CartDatabase.class.getSimpleName(), "catogiry id" + selecteditemsDOImg.CategoryId);

            if (selecteditemsDOImg.itemColorList.size() > 0) {
                cartDatabase.addColorData(selecteditemsDOImg.ItemId, selecteditemsDOImg.itemColorList);
            }
            for (int i = 0; i < selecteditemsDOImg.itemSizeList.size(); i++) {
                cartDatabase.addSizeData(new SizeListDO(selecteditemsDOImg.ItemId, selecteditemsDOImg.itemSizeList.get(i).ItemSize, selecteditemsDOImg.itemSizeList.get(i).BasePrice, selecteditemsDOImg.itemSizeList.get(i).DiscountedPrice, selecteditemsDOImg.itemSizeList.get(i).FinalPrice));
            }
            getCartListNormal();
            gotoCartActivity();

        } else {
            //chk_addtocart.setChecked(false);
            showAlertDialog("Please select a size", "OK");
        }
    }

    private void addItemsToReviewList() {
        selecteditemsDOImg.cartIconChecked = true;
        itemsListAdapter.notifyDataSetChanged();
        itemsGridAdapter.notifyDataSetChanged();
        cartDatabase.addReviewListData(new ItemsDO(selecteditemsDOImg.ItemId, selecteditemsDOImg.ProductID, selecteditemsDOImg.ItemName, 1, selecteditemsDOImg.itemSizeList.get(0).BasePrice, selecteditemsDOImg.itemSizeList.get(0).DiscountedPrice, selecteditemsDOImg.itemSizeList.get(0).FinalPrice, selecteditemsDOImg.itemSizeList.get(0).ItemSize,
                selecteditemsDOImg.itemColorList.get(0), selecteditemsDOImg.ItemPattern, selecteditemsDOImg.ItemMaterial, selecteditemsDOImg.ItemSmallImgUrl,
                selecteditemsDOImg.ItemMediumImgUrl, selecteditemsDOImg.ItemBigImgUrl, 1, selecteditemsDOImg.ItemRating, selecteditemsDOImg.ItemPerSaved));

        if (selecteditemsDOImg.itemColorList.size() > 0) {
            cartDatabase.addReviewColorData(selecteditemsDOImg.ItemId, selecteditemsDOImg.itemColorList);
        }
        for (int i = 0; i < selecteditemsDOImg.itemSizeList.size(); i++) {
            cartDatabase.addReviewSizeData(new ReviewSizeListDO(selecteditemsDOImg.ItemId, selecteditemsDOImg.itemSizeList.get(i).ItemSize, selecteditemsDOImg.itemSizeList.get(i).BasePrice, selecteditemsDOImg.itemSizeList.get(i).DiscountedPrice, selecteditemsDOImg.itemSizeList.get(i).FinalPrice));
        }
    }


    private void getItems(String queryParams) {
        showLoaderNew();
        if (new CommonBL(mContext, ItemsActivity.this).getItems(queryParams)) {
        } else {
            hideloader();
            showAlertDialog("NO INTERNET", "OK");
        }

    }

    private void getFilteredProducts(ArrayList<FilternameAndIDDo> selectedSizeFilterList,
                                     ArrayList<FilternameAndIDDo> selectedPatternFilterList,
                                     ArrayList<FilternameAndIDDo> selectedMaterialFilterList,
                                     ArrayList<FilternameAndIDDo> selectedColorFilterList,
                                     ArrayList<FilternameAndIDDo> selectedSizeGroupFilterList,
                                     ArrayList<FilternameAndIDDo> selectedPriceFilterList,
                                     ArrayList<FilternameAndIDDo> SelectedFeelerFilterList,
                                     ArrayList<FilternameAndIDDo> selectedCategoryFilterList,
                                     ArrayList<FilternameAndIDDo> selectedRunnerFilterList) {

        showLoaderNew();
        if (new CommonBL(mContext, ItemsActivity.this).getFilteredProducts(selectedSizeFilterList, selectedPatternFilterList,
                 selectedMaterialFilterList, selectedColorFilterList, selectedSizeGroupFilterList, selectedPriceFilterList,SelectedFeelerFilterList,
                 selectedCategoryFilterList, selectedRunnerFilterList, preferenceUtils.getUserDetails().get(AppConstants.KEY_USER_EMAIL))) {
        } else {
            hideloader();
            showAlertDialog("NO INTERNET", "OK");
        }

    }


    private void getFilterData(String queryParamsFilter) {

        if (new CommonBL(mContext, ItemsActivity.this).getFilterData(queryParamsFilter)) {
        } else {
            hideloader();
            showAlertDialog("NO INTERNET", "OK");
        }

    }

    private void initializeControls() {
        llPriceSort                     = (LinearLayout) findViewById(R.id.llItems);
        llMaxSavings                    = (LinearLayout) findViewById(R.id.llMaxSavings);
        llPopularity                    = (LinearLayout) findViewById(R.id.llPopularity);
        llNewest                        = (LinearLayout) findViewById(R.id.llNewest);
        llItems                         = (LinearLayout) findViewById(R.id.llItems);
        tvDecrese                       = (TextView) findViewById(R.id.btn_minus);
        tvIncrese                       = (TextView) findViewById(R.id.btn_plus);
        tvQuantity                      = (TextView) findViewById(R.id.txt_qty);
        tv_original_price               = (TextView) findViewById(R.id.tv_original_price);
        tv_offer_price                  = (TextView) findViewById(R.id.tv_offer_price);
        flFilterList                    = (FrameLayout) findViewById(R.id.flFilterList);

        btn_clear_all = (Button) findViewById(R.id.btn_clear_all);
        tv_selection                    = (TextView) findViewById(R.id.tv_selection);
        tv_code_label = (TextView) findViewById(R.id.tv_code_label);
        tv_material_label = (TextView) findViewById(R.id.tv_material_label);
        tv_size_label = (TextView) findViewById(R.id.tv_size_label);
        tv_qauntity_label = (TextView) findViewById(R.id.tv_qauntity_label);
        tv_price_label = (TextView) findViewById(R.id.tv_price_label);
        tv_desc_label = (TextView) findViewById(R.id.tv_desc_label);

        tvPopularity                    = (TextView) findViewById(R.id.tvPopularity);
        tvNewest                        = (TextView) findViewById(R.id.tvNewest);

        tv_disc = (TextView) findViewById(R.id.tv_disc);
        tv_product_id = (TextView) findViewById(R.id.tv_product_id);
        //base_filter.setVisibility(View.VISIBLE);
        lst_filter_head = (ListView) findViewById(R.id.lst_filter_head);
        refine_filter = (LinearLayout) findViewById(R.id.refine_filter);
        filterSelectionAdater       = new FilterSelectionAdater(allSelectedFilternameAndIDDo, mContext);
        grd_filter_selections       = (GridView) findViewById(R.id.grd_filter_selections);
        grd_filter_selections.setAdapter(filterSelectionAdater);
        filterSelectionAdater.registerListener(this);

        btn_savings_lh              = (ImageButton) findViewById(R.id.btn_savings_lh);
        btn_savings_hl              = (ImageButton) findViewById(R.id.btn_savings_hl);
        btn_price_lh                = (ImageButton) findViewById(R.id.btn_price_lh);
        btn_price_hl                = (ImageButton) findViewById(R.id.btn_price_hl);

        ibPopularity                = (ImageButton) findViewById(R.id.ibPopularity);
        ibNewest                    = (ImageButton) findViewById(R.id.ibNewest);
        tv_price                    = (TextView) findViewById(R.id.tv_price);
        tv_savings                  = (TextView) findViewById(R.id.tv_savings);


        tv_num_items_fou            = (TextView) findViewById(R.id.tv_num_items_fou);
        tv_most_view                = (TextView) findViewById(R.id.tv_most_view);
        spn_sizelist                = (MaterialSpinner) findViewById(R.id.spn_sizelist);
        spSizes                     = (Spinner) findViewById(R.id.spSizes);

        spn_sort = (MaterialSpinner) findViewById(R.id.spn_sort);

        List<String> strings = new ArrayList<>();
        strings.add(AppConstants.Nopreference);
        strings.add(AppConstants.MostViewed);
        strings.add(AppConstants.SORTPRICELH);
        strings.add(AppConstants.SORTPRICEHL);
        strings.add(AppConstants.SORTSAVEHL);
        strings.add(AppConstants.SORTSAVELH);

        spn_sort.setItems(strings);
        edt_search = (EditText) findViewById(R.id.edt_search);
        edt_search.setVisibility(View.VISIBLE);
        lst_filter_data             = (ListView) findViewById(R.id.lst_filter_data);
        grd_filterdata              = (GridView) findViewById(R.id.grd_filterdata);

        btn_applysearch = (ImageButton) findViewById(R.id.btn_applysearch);
        btn_applysearch.setVisibility(View.VISIBLE);
        btn_buy             = (TextView) findViewById(R.id.btn_buy);
        lst_items           = (ListView) findViewById(R.id.lst_items);
        lst_images          = (RecyclerView) findViewById(R.id.lst_images);
        lst_review          = (ListView) findViewById(R.id.lst_review);
        lst_filterheadings  = (ListView) findViewById(R.id.lst_filterheadings);
        grd_items           = (GridView) findViewById(R.id.grd_items);


        tvItemsNoData       = (TextView) findViewById(R.id.tvItemsNoData);
        txt_catgName        = (TextView) findViewById(R.id.txt_catgName);
        tv_itemName         = (TextView) findViewById(R.id.tv_itemName);

        txt_catg            = (TextView) findViewById(R.id.txt_catg);
        ll_filters          = (RelativeLayout) findViewById(R.id.ll_filters);
        ll_review           = (RelativeLayout) findViewById(R.id.ll_review);
        rl_filterClose      = (RelativeLayout) findViewById(R.id.rl_filterClose);
        rl_filterOpen       = (RelativeLayout) findViewById(R.id.rl_filterOpen);
        lst_layout          = (LinearLayout) findViewById(R.id.lst_layout);
        grd_layout          = (RelativeLayout) findViewById(R.id.grd_layout);
        img_lst     = (ImageView) findViewById(R.id.img_lst);
        img_grd = (ImageView) findViewById(R.id.img_grd);
        //  img_review=(CheckBox)findViewById(R.id.img_review);
        ll_img_item = (LinearLayout) findViewById(R.id.ll_img_item);
        ll_img_item.setVisibility(View.VISIBLE);
        //Views Of included list layouts
        tv_material = (TextView) findViewById(R.id.tv_material);

        lst_SizePrice = (ListView) findViewById(R.id.lst_SizePrice);
        chk_addtocart = (TextView) findViewById(R.id.chk_addtocart);

        btn_resetfilter = (Button) findViewById(R.id.btn_resetfilter);
        btn_applyfilter = (Button) findViewById(R.id.btn_applyfilter);
    }

    @Override
    public void dataRetreived(Response data) {
        if (data != null && data.method == ServiceMethods.WS_GET_ITEMS) {
            hideloader();
            if (!data.isError) {
                flFilterList.setVisibility(View.GONE);
                btn_clear_all.setVisibility(View.GONE);
                defaultlist     = (ArrayList<ItemsDO>) data.data;
                arrItems        = (ArrayList<ItemsDO>) data.data;
                LogUtils.info(ItemsActivity.class.getSimpleName(), "size of items lsit" + arrItems.size());
//                if (defaultlist != null && defaultlist.size() > 0)
//                {
//                    tvItemsNoData.setVisibility(View.GONE);
//                    llItems.setVisibility(View.VISIBLE);
                    for (int i = 0; i < arrItems.size(); i++)
                    {
                        arrItems.get(i).itemSizeList.get(0).itemSelected = true;
                        for (int j = 0; j < LocalCartList.size(); j++)
                        {
                            if (LocalCartList.get(j).ItemId.equalsIgnoreCase(arrItems.get(i).ItemId))
                            {
                                arrItems.get(i).cartIconChecked = true;
                            }

                        }
                        for (int j = 0; j < favsList.size(); j++)
                        {
                            if (favsList.get(j).ItemId.equalsIgnoreCase(arrItems.get(i).ItemId)) {
                                arrItems.get(i).favIconChecked = true;
                            }
                        }
                    }

                    setListToAdapter();
                    getFilterData(queryParamsFilter);
                    clearAllSelectedFilterList();
                    closeFilterlayout();

                    if (action.equalsIgnoreCase(AppConstants.RESET_FILTER)) {

                        resettingAction();
                    }
//                }
//                else {
//                    tvItemsNoData.setVisibility(View.VISIBLE);
//                    llItems.setVisibility(View.GONE);
//                }
            }
        } else if (data != null && data.method == ServiceMethods.WS_POST_FILTER_PRODUCTS) {
            hideloader();
            if (!data.isError) {
                arrItems = (ArrayList<ItemsDO>) data.data;
                LogUtils.info(ItemsActivity.class.getSimpleName(), "size of items list" + arrItems.size());
                if (arrItems != null && arrItems.size() > 0) {
                    tvItemsNoData.setVisibility(View.GONE);
                    grd_items.setVisibility(View.VISIBLE);

                    for (int i = 0; i < arrItems.size(); i++) {
                        for (int j = 0; j < LocalCartList.size(); j++) {
                            if (LocalCartList.get(j).ItemId.equalsIgnoreCase(arrItems.get(i).ItemId)) {
                                arrItems.get(i).cartIconChecked = true;
                            }
                        }
                    }
                    setListToAdapter();
                   /* itemsGridAdapter = new ItemsGridAdapter(mContext, arrItems, itemCartList, itemFavouriteList);
                    itemsGridAdapter.registerCartListener(this);
                    itemsGridAdapter.registerFavouriteListener(this);
                    itemsGridAdapter.registerSelectedGridItemListener(this);
                    grd_items.setAdapter(itemsGridAdapter);

                    itemsListAdapter = new ItemsListAdapter(mContext, arrItems, itemCartList, itemFavouriteList);
                    itemsListAdapter.registerCartListener(this);
                    itemsListAdapter.registerFavouriteListener(this);
                    itemsListAdapter.registerSelectedListItemListener(this);
                    lst_items.setAdapter(itemsListAdapter);*/

                    closeFilterlayout();
                } else {
                    tvItemsNoData.setVisibility(View.VISIBLE);
                    grd_items.setVisibility(View.GONE);
                }
            }
        } else if (data != null && data.method == ServiceMethods.WS_GET_FILTER_DATA) {
            hideloader();
            if (!data.isError) {
                arrfilterMetadataDO = (ArrayList<FilterMetadataDO>) data.data;
                if (arrfilterMetadataDO != null && arrfilterMetadataDO.size() > 0) {
                    for (int j = 0; j < arrfilterMetadataDO.size(); j++) {
                        ColorNamesList = arrfilterMetadataDO.get(j).ColorNamesList;
                        SizeNamesList = arrfilterMetadataDO.get(j).SizeNamesList;
                        SizeGroupNamesList = arrfilterMetadataDO.get(j).SizeGroupNamesList;
                        PatternNamesList = arrfilterMetadataDO.get(j).PatternNamesList;
                        MaterialNamesList = arrfilterMetadataDO.get(j).MaterialNamesList;
                        PricesNamesList = arrfilterMetadataDO.get(j).PricesNamesList;
                        CategoryList = arrfilterMetadataDO.get(j).CategoryList;
                        RunnerList = arrfilterMetadataDO.get(j).RunnerList;
                        FilternameAndIDDo filternameAndIDDo = new FilternameAndIDDo();
                        filternameAndIDDo.type = "DUMMY";
                        ArrayList<FilternameAndIDDo> filternameAndIDDos = new ArrayList<>();


                        sizeGroupFilterAdapter = new SizeGroupFilterAdapter(mContext, ColorNamesList, filternameAndIDDos, "Price");
                        sizeGroupFilterAdapter.registerSelectedGroupFilterListener(this);
                        grd_filterdata.setAdapter(sizeGroupFilterAdapter);
                        lst_filter_data.setAdapter(sizeGroupFilterAdapter);

                        LogUtils.error(ItemsActivity.class.getSimpleName(), "ColorNamesList=" + ColorNamesList.size());
                        LogUtils.error(ItemsActivity.class.getSimpleName(), "SizeNamesList=" + SizeNamesList.size());
                        LogUtils.error(ItemsActivity.class.getSimpleName(), "SizeGroupNamesList=" + SizeGroupNamesList.size());
                        LogUtils.error(ItemsActivity.class.getSimpleName(), "PatternNamesList=" + PatternNamesList.size());
                        LogUtils.error(ItemsActivity.class.getSimpleName(), "MaterialNamesList=" + MaterialNamesList.size());
                        LogUtils.error(ItemsActivity.class.getSimpleName(), "PricesNamesList=" + PricesNamesList.size());
                        LogUtils.error(ItemsActivity.class.getSimpleName(), "RunnerList=" + RunnerList.size());
                        LogUtils.error(ItemsActivity.class.getSimpleName(), "CategoryList=" + CategoryList.size());
                    }
                } else {
                }
            }
        }

        tv_num_items_fou.setText("Showing " + arrItems.size() + " Results");
    }

    private void setListToAdapter() {
        ArrayList<ItemsDO> itemsDOs = arrItems;
        if (!sortnopreff) {
            if (sortmostviewed)
            {
                Collections.sort(itemsDOs, new Comparator<ItemsDO>() {
                    @Override
                    public int compare(ItemsDO obj1, ItemsDO obj2) {
                        ItemsDO item1 = (ItemsDO) obj1;
                        ItemsDO item2 = (ItemsDO) obj2;
                        int val1 = item1.viewcount;
                        int val2 = item2.viewcount;
                        return val2 - val1;
                    }
                });

            }
            else if (sortPriceLH)
            {
                Collections.sort(itemsDOs, new Comparator<ItemsDO>() {
                    @Override
                    public int compare(ItemsDO obj1, ItemsDO obj2) {
                        ItemsDO item1 = (ItemsDO) obj1;
                        ItemsDO item2 = (ItemsDO) obj2;
                        int val1 = (int) (Float.valueOf(item1.itemSizeList.get(0).FinalPrice) * 100);
                        int val2 = (int) (Float.valueOf(item2.itemSizeList.get(0).FinalPrice) * 100);

                        return val1 - val2;
                    }
                });

            }
            else if (sortPriceHL)
            {
                Collections.sort(itemsDOs, new Comparator<ItemsDO>() {
                    @Override
                    public int compare(ItemsDO obj1, ItemsDO obj2) {
                        ItemsDO item1 = (ItemsDO) obj1;
                        ItemsDO item2 = (ItemsDO) obj2;
                        int val1 = (int) (Float.valueOf(item1.itemSizeList.get(0).FinalPrice) * 100);
                        int val2 = (int) (Float.valueOf(item2.itemSizeList.get(0).FinalPrice) * 100);
                        return val2 - val1;
                    }
                });
            }
            else if (sortsavingsLH)
            {
                Collections.sort(itemsDOs, new Comparator<ItemsDO>() {
                    @Override
                    public int compare(ItemsDO obj1, ItemsDO obj2) {
                        ItemsDO item1 = (ItemsDO) obj1;
                        ItemsDO item2 = (ItemsDO) obj2;
                        int val1 = (int) item1.itemSizeList.get(0).savings * 100;
                        int val2 = (int) item2.itemSizeList.get(0).savings * 100;
                        return val1 - val2;
                    }
                });
            }
            else if (sortsavingsHL)
            {
                Collections.sort(itemsDOs, new Comparator<ItemsDO>() {
                    @Override
                    public int compare(ItemsDO obj1, ItemsDO obj2) {
                        ItemsDO item1 = (ItemsDO) obj1;
                        ItemsDO item2 = (ItemsDO) obj2;
                        int val1 = (int) item1.itemSizeList.get(0).savings * 100;
                        int val2 = (int) item2.itemSizeList.get(0).savings * 100;
                        return val2 - val1;
                    }
                });
            }
            else if (sortPopularity)
            {
                Collections.sort(itemsDOs, new Comparator<ItemsDO>() {
                    @Override
                    public int compare(ItemsDO obj1, ItemsDO obj2) {
                        ItemsDO item1 = (ItemsDO) obj1;
                        ItemsDO item2 = (ItemsDO) obj2;
                        int val1 = (int) item1.itemSizeList.get(0).savings * 100;
                        int val2 = (int) item2.itemSizeList.get(0).savings * 100;
                        return val2 - val1;
                    }
                });


            }
            else if (sortNewest)
            {
                Collections.sort(itemsDOs, new Comparator<ItemsDO>() {
                    @Override
                    public int compare(ItemsDO obj1, ItemsDO obj2) {
                        ItemsDO item1 = (ItemsDO) obj1;
                        ItemsDO item2 = (ItemsDO) obj2;
                        int val1 = (int) item1.itemSizeList.get(0).savings * 100;
                        int val2 = (int) item2.itemSizeList.get(0).savings * 100;

                        return val2 - val1;
                    }
                });


            }
            else
            {
                itemsDOs = defaultlist;
            }
        }
        grd_items.setVisibility(View.VISIBLE);
        itemsGridAdapter = new ItemsGridAdapter(mContext, itemsDOs, itemCartList, itemFavouriteList);
        itemsGridAdapter.registerCartListener(this);
        itemsGridAdapter.registerFavouriteListener(this);
        itemsGridAdapter.registerSelectedGridItemListener(this);
        grd_items.setAdapter(itemsGridAdapter);

        itemsListAdapter = new ItemsListAdapter(mContext, itemsDOs, itemCartList, itemFavouriteList);
        itemsListAdapter.registerCartListener(this);
        itemsListAdapter.registerFavouriteListener(this);
        itemsListAdapter.registerSelectedListItemListener(this);
        lst_items.setAdapter(itemsListAdapter);
        scrollToSelectedPosition();

    }

    private void resettingAction() {
        SelectedSizeFilterList = new ArrayList<>();
        SelectedPatternFilterList = new ArrayList<>();
        SelectedMaterialFilterList = new ArrayList<>();
        SelectedColorFilterList = new ArrayList<>();
        SelectedSizeGroupFilterList = new ArrayList<>();
        SelectedPriceFilterList = new ArrayList<>();
        SelectedCategoryFilterList = new ArrayList<>();
        SelectedRunnerFilterList = new ArrayList<>();

        for (int i = 0; i < filterheadings.size(); i++) {
            filterheadings.get(i).Counter = 0;
        }

        filterHeadingAdapter.notifyDataSetChanged();

    }

    private void clearAllSelectedFilterList() {
        SelectedSizeFilterList.clear();
        SelectedPatternFilterList.clear();
        SelectedMaterialFilterList.clear();
        SelectedColorFilterList.clear();
        SelectedSizeGroupFilterList.clear();
        SelectedPriceFilterList.clear();
        SelectedCategoryFilterList.clear();
        SelectedRunnerFilterList.clear();
    }

    @Override
    public void getCartList(ItemsDO model) {
        cartCount = CartDatabase.getCartlistCount();
        tvCartCount.startAnimation(animRotate);
        tvCartCount.setText("" + cartCount);

        if (model.cartIconChecked) {
            //  chk_addtocart.setText("Remove cart");
        } else {
            chk_addtocart.setText("Add to cart");
        }

    }


    public void getCartListNormal() {
        cartCount = CartDatabase.getCartlistCount();
        itemCartsDOs = CartDatabase.getAllCartList();
        if(cartCount > 0){
            tvCartCount.setVisibility(View.VISIBLE);
            tvCartCount.setText("" + cartCount);
            tvCartCount.startAnimation(animRotate);
            cartHomeAdapter = new CartHomeAdapter(itemCartsDOs, ItemsActivity.this, this);
            lst_cart_items.setAdapter(cartHomeAdapter);
        }
        else {
            tvCartCount.setVisibility(View.GONE);
        }
    }

    @Override
    public void getLocalDatabaseCartList() {
        itemCartsDOs = CartDatabase.getAllCartList();
        cartCount = CartDatabase.getCartlistCount();
        tvCartCount.setText("" + cartCount);
        if(cartCount > 0){
            tvCartCount.setVisibility(View.VISIBLE);
            cartHomeAdapter.refresh(itemCartsDOs);
            tvCartCount.startAnimation(animRotate);
        }
        else {
            tvCartCount.setVisibility(View.GONE);
            view.animate().translationY(-view.getHeight()).setDuration(300);
        }
    }

    @Override
    public void getTheIncreasedSubtotal(Double itemFinalPrice, Double itemPriceVendor) {

    }

    @Override
    public void getTheDecreasedSubtotal(Double itemFinalPrice, Double itemPriceVendor) {

    }

    @Override
    public void getFavouriteList(ArrayList<ItemfavouriteDO> itemFavouriteList) {
        this.itemFavouriteList = itemFavouriteList;
        ArrayList<ItemsDO> itemsDOs = CartDatabase.getAllFavouriteList();
        if(itemsDOs!=null)
            favCount = itemsDOs.size();
        tvFavouriteCount.setText(""+favCount);
        if(favCount > 0){
         tvFavouriteCount.setVisibility(View.VISIBLE);
            ivFavourite.setImageResource(R.mipmap.fav_disabled);
        }
        else {
            tvFavouriteCount.setVisibility(View.GONE);
        }
        Toast.makeText(mContext, "Your favourite items are '" + favCount+"'", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void getSelectedSizeFilterListener(FilternameAndIDDo model) {
        allSelectedFilternameAndIDDo.add(model);
        filterSelectionAdater.notifyDataSetChanged();
        SelectedSizeFilterList.add(model);
        for (int i = 0; i < filterheadings.size(); i++) {
            if (filterheadings.get(i).filterName.equals("Size")) {
                filterheadings.get(i).Counter = SelectedSizeFilterList.size();
            }
        }
        filterHeadingAdapter.notifyDataSetChanged();
        getFilteredProducts(SelectedSizeFilterList, SelectedPatternFilterList, SelectedMaterialFilterList
                , SelectedColorFilterList, SelectedSizeGroupFilterList, SelectedPriceFilterList, SelectedFeelerFilterList, SelectedCategoryFilterList,
                SelectedRunnerFilterList);
        LogUtils.error(ItemsActivity.class.getSimpleName(), "Sizes Are " + model.id);
        LogUtils.error(ItemsActivity.class.getSimpleName(), "SelectedSizeFilterList Size is=" + SelectedSizeFilterList.size());
        openFilterLayout();
    }

    @Override
    public void getSelectedColorFilterListener(FilternameAndIDDo model) {
        allSelectedFilternameAndIDDo.add(model);
        filterSelectionAdater.notifyDataSetChanged();
        SelectedColorFilterList.add(model);

        flFilterList.setVisibility(View.VISIBLE);
        btn_clear_all.setVisibility(View.VISIBLE);
        for (int i = 0; i < filterheadings.size(); i++) {
            if (filterheadings.get(i).filterName.equals("Colour")) {
                filterheadings.get(i).Counter = SelectedColorFilterList.size();
            }
        }
        filterHeadingAdapter.notifyDataSetChanged();
        getFilteredProducts(SelectedSizeFilterList, SelectedPatternFilterList, SelectedMaterialFilterList
                , SelectedColorFilterList, SelectedSizeGroupFilterList, SelectedPriceFilterList, SelectedFeelerFilterList, SelectedCategoryFilterList,
                SelectedRunnerFilterList);
        LogUtils.error(ItemsActivity.class.getSimpleName(), "Colors Are " + model.id);
        LogUtils.error(ItemsActivity.class.getSimpleName(), "SelectedColorFilterList Size is=" + SelectedColorFilterList.size());

        openFilterLayout();
    }

    @Override
    public void getSelectedPatternFilterListener(FilternameAndIDDo model) {
        allSelectedFilternameAndIDDo.add(model);
        filterSelectionAdater.notifyDataSetChanged();
        SelectedPatternFilterList.add(model);
        flFilterList.setVisibility(View.VISIBLE);
        btn_clear_all.setVisibility(View.VISIBLE);

        for (int i = 0; i < filterheadings.size(); i++) {
            if (filterheadings.get(i).filterName.equals("Pattern")) {
                filterheadings.get(i).Counter = SelectedPatternFilterList.size();
            }
        }
        filterHeadingAdapter.notifyDataSetChanged();
        getFilteredProducts(SelectedSizeFilterList, SelectedPatternFilterList, SelectedMaterialFilterList
                , SelectedColorFilterList, SelectedSizeGroupFilterList, SelectedPriceFilterList, SelectedFeelerFilterList, SelectedCategoryFilterList,
                SelectedRunnerFilterList);
        LogUtils.error(ItemsActivity.class.getSimpleName(), "Patterns Are " + model.id);
        LogUtils.error(ItemsActivity.class.getSimpleName(), "SelectedPatternFilterList Size is=" + SelectedPatternFilterList.size());

        openFilterLayout();
    }

    @Override
    public void getSelectedMaterialFilterListener(FilternameAndIDDo model) {
        allSelectedFilternameAndIDDo.add(model);
        filterSelectionAdater.notifyDataSetChanged();
        SelectedMaterialFilterList.add(model);
        flFilterList.setVisibility(View.VISIBLE);
        btn_clear_all.setVisibility(View.VISIBLE);

        for (int i = 0; i < filterheadings.size(); i++) {
            if (filterheadings.get(i).filterName.equals("Material")) {
                filterheadings.get(i).Counter = SelectedMaterialFilterList.size();
            }
        }
        filterHeadingAdapter.notifyDataSetChanged();
        getFilteredProducts(SelectedSizeFilterList, SelectedPatternFilterList, SelectedMaterialFilterList
                , SelectedColorFilterList, SelectedSizeGroupFilterList, SelectedPriceFilterList, SelectedFeelerFilterList, SelectedCategoryFilterList,
                SelectedRunnerFilterList);
        LogUtils.error(ItemsActivity.class.getSimpleName(), "Materials Are " + model.id);
        LogUtils.error(ItemsActivity.class.getSimpleName(), "SelectedMaterialFilterList Size is=" + SelectedMaterialFilterList.size());

        openFilterLayout();
    }

    @Override
    public void getSelectedSizegroupFilterListener(FilternameAndIDDo model) {
        allSelectedFilternameAndIDDo.add(model);
        filterSelectionAdater.notifyDataSetChanged();
        SelectedSizeGroupFilterList.add(model);
        flFilterList.setVisibility(View.VISIBLE);
        btn_clear_all.setVisibility(View.VISIBLE);

        for (int i = 0; i < filterheadings.size(); i++) {
            if (filterheadings.get(i).filterName.equals("SizeGroup")) {
                filterheadings.get(i).Counter = SelectedSizeGroupFilterList.size();
            }
        }
        filterHeadingAdapter.notifyDataSetChanged();
        getFilteredProducts(SelectedSizeFilterList, SelectedPatternFilterList, SelectedMaterialFilterList
                , SelectedColorFilterList, SelectedSizeGroupFilterList, SelectedPriceFilterList, SelectedFeelerFilterList, SelectedCategoryFilterList,
                SelectedRunnerFilterList);
        LogUtils.error(ItemsActivity.class.getSimpleName(), "Size Group Are " + model.id);
        LogUtils.error(ItemsActivity.class.getSimpleName(), "SelectedSizeGroupFilterList Size is=" + SelectedSizeGroupFilterList.size());

        openFilterLayout();
    }

    @Override
    public void getSelectedCategoryFilterListener(FilternameAndIDDo model) {
        allSelectedFilternameAndIDDo.add(model);
        filterSelectionAdater.notifyDataSetChanged();
        SelectedCategoryFilterList.add(model);
        flFilterList.setVisibility(View.VISIBLE);
        btn_clear_all.setVisibility(View.VISIBLE);


        for (int i = 0; i < filterheadings.size(); i++) {
            if (filterheadings.get(i).filterName.equals("Categories")) {
                filterheadings.get(i).Counter = SelectedCategoryFilterList.size();
            }
        }
        filterHeadingAdapter.notifyDataSetChanged();
        getFilteredProducts(SelectedSizeFilterList, SelectedPatternFilterList, SelectedMaterialFilterList
                , SelectedColorFilterList, SelectedSizeGroupFilterList, SelectedPriceFilterList, SelectedFeelerFilterList, SelectedCategoryFilterList,
                SelectedRunnerFilterList);
        LogUtils.error(ItemsActivity.class.getSimpleName(), "Categories Are " + model.id);
        LogUtils.error(ItemsActivity.class.getSimpleName(), "SelectedCategoryFilterList Size is=" + SelectedCategoryFilterList.size());
        openFilterLayout();
    }


    @Override
    public void getSelectedRunnerFilterListener(FilternameAndIDDo model) {
        allSelectedFilternameAndIDDo.add(model);
        filterSelectionAdater.notifyDataSetChanged();
        SelectedRunnerFilterList.add(model);
        flFilterList.setVisibility(View.VISIBLE);
        btn_clear_all.setVisibility(View.VISIBLE);

        for (int i = 0; i < filterheadings.size(); i++) {
            if (filterheadings.get(i).filterName.equals("Runners")) {
                filterheadings.get(i).Counter = SelectedRunnerFilterList.size();
            }
        }
        filterHeadingAdapter.notifyDataSetChanged();
        getFilteredProducts(SelectedSizeFilterList, SelectedPatternFilterList, SelectedMaterialFilterList
                , SelectedColorFilterList, SelectedSizeGroupFilterList, SelectedPriceFilterList, SelectedFeelerFilterList, SelectedCategoryFilterList,
                SelectedRunnerFilterList);
        LogUtils.error(ItemsActivity.class.getSimpleName(), "Runners Are " + model.id);
        LogUtils.error(ItemsActivity.class.getSimpleName(), "SelectedRunnerFilterList Size is=" + SelectedRunnerFilterList.size());

        openFilterLayout();
    }

    @Override
    public void getSelectedPriceFilterListener(FilternameAndIDDo model) {
        allSelectedFilternameAndIDDo.add(model);
        filterSelectionAdater.notifyDataSetChanged();
        flFilterList.setVisibility(View.VISIBLE);
        btn_clear_all.setVisibility(View.VISIBLE);

        SelectedPriceFilterList.add(model);
        for (int i = 0; i < filterheadings.size(); i++) {
            if (filterheadings.get(i).filterName.equals("Price")) {
                filterheadings.get(i).Counter = SelectedPriceFilterList.size();
            }
        }
        filterHeadingAdapter.notifyDataSetChanged();
        getFilteredProducts(SelectedSizeFilterList, SelectedPatternFilterList, SelectedMaterialFilterList
                , SelectedColorFilterList, SelectedSizeGroupFilterList, SelectedPriceFilterList, SelectedFeelerFilterList, SelectedCategoryFilterList,
                SelectedRunnerFilterList);
        LogUtils.error(ItemsActivity.class.getSimpleName(), "Prices Are " + model.name);
        LogUtils.error(ItemsActivity.class.getSimpleName(), "SelectedPriceFilterList Size is=" + SelectedPriceFilterList.size());
        openFilterLayout();
    }

    @Override
    public void removeSelectedColorFilterListener(FilternameAndIDDo model) {

        allSelectedFilternameAndIDDo.remove(model);
        filterSelectionAdater.notifyDataSetChanged();


        if (SelectedColorFilterList.size() > 0) {
            for (int i = 0; i < SelectedColorFilterList.size(); i++) {
                if (SelectedColorFilterList.get(i).id.equals(model.id)) {
                    SelectedColorFilterList.remove(model);
                }
            }
        }

        for (int i = 0; i < filterheadings.size(); i++) {
            if (filterheadings.get(i).filterName.equals("Colour")) {
                filterheadings.get(i).Counter = SelectedColorFilterList.size();
            }
        }
        filterHeadingAdapter.notifyDataSetChanged();
        getFilteredProducts(SelectedSizeFilterList, SelectedPatternFilterList, SelectedMaterialFilterList
                , SelectedColorFilterList, SelectedSizeGroupFilterList, SelectedPriceFilterList, SelectedFeelerFilterList, SelectedCategoryFilterList,
                SelectedRunnerFilterList);
        LogUtils.error(ItemsActivity.class.getSimpleName(), "SelectedColorFilterList Size is=" + SelectedColorFilterList.size());

        openFilterLayout();
    }

    @Override
    public void removeSelectedMaterialFilterListener(FilternameAndIDDo model) {

        allSelectedFilternameAndIDDo.remove(model);
        filterSelectionAdater.notifyDataSetChanged();


        if (SelectedMaterialFilterList.size() > 0) {
            for (int i = 0; i < SelectedMaterialFilterList.size(); i++) {
                if (SelectedMaterialFilterList.get(i).id.equals(model.id)) {
                    SelectedMaterialFilterList.remove(model);
                }
            }
        }

        for (int i = 0; i < filterheadings.size(); i++) {
            if (filterheadings.get(i).filterName.equals("Material")) {
                filterheadings.get(i).Counter = SelectedMaterialFilterList.size();
            }
        }
        filterHeadingAdapter.notifyDataSetChanged();
        getFilteredProducts(SelectedSizeFilterList, SelectedPatternFilterList, SelectedMaterialFilterList
                , SelectedColorFilterList, SelectedSizeGroupFilterList, SelectedPriceFilterList, SelectedFeelerFilterList, SelectedCategoryFilterList,
                SelectedRunnerFilterList);
        LogUtils.error(ItemsActivity.class.getSimpleName(), "SelectedMaterialFilterList Size is=" + SelectedMaterialFilterList.size());
        openFilterLayout();

    }

    @Override
    public void removeSelectedPatternFilterListener(FilternameAndIDDo model) {

        allSelectedFilternameAndIDDo.remove(model);
        filterSelectionAdater.notifyDataSetChanged();


        if (SelectedPatternFilterList.size() > 0) {
            for (int i = 0; i < SelectedPatternFilterList.size(); i++) {
                if (SelectedPatternFilterList.get(i).id.equals(model.id)) {
                    SelectedPatternFilterList.remove(model);
                }
            }
        }

        for (int i = 0; i < filterheadings.size(); i++) {
            if (filterheadings.get(i).filterName.equals("Pattern")) {
                filterheadings.get(i).Counter = SelectedPatternFilterList.size();
            }
        }
        filterHeadingAdapter.notifyDataSetChanged();
        getFilteredProducts(SelectedSizeFilterList, SelectedPatternFilterList, SelectedMaterialFilterList
                , SelectedColorFilterList, SelectedSizeGroupFilterList, SelectedPriceFilterList, SelectedFeelerFilterList, SelectedCategoryFilterList,
                SelectedRunnerFilterList);
        LogUtils.error(ItemsActivity.class.getSimpleName(), "SelectedPatternFilterList Size is=" + SelectedPatternFilterList.size());

        openFilterLayout();
    }

    @Override
    public void removeSelectedPriceFilterListener(FilternameAndIDDo model) {

        allSelectedFilternameAndIDDo.remove(model);
        filterSelectionAdater.notifyDataSetChanged();


        if (SelectedPriceFilterList.size() > 0) {
            for (int i = 0; i < SelectedPriceFilterList.size(); i++) {
                if (SelectedPriceFilterList.get(i).id.equals(model.id)) {
                    SelectedPriceFilterList.remove(model);
                }
            }
        }

        for (int i = 0; i < filterheadings.size(); i++) {
            if (filterheadings.get(i).filterName.equals("Price")) {
                filterheadings.get(i).Counter = SelectedPriceFilterList.size();
            }
        }
        filterHeadingAdapter.notifyDataSetChanged();
        getFilteredProducts(SelectedSizeFilterList, SelectedPatternFilterList, SelectedMaterialFilterList
                , SelectedColorFilterList, SelectedSizeGroupFilterList, SelectedPriceFilterList, SelectedFeelerFilterList, SelectedCategoryFilterList,
                SelectedRunnerFilterList);
        LogUtils.error(ItemsActivity.class.getSimpleName(), "SelectedPriceFilterList Size is=" + SelectedPriceFilterList.size());
        openFilterLayout();
    }

    @Override
    public void removeSelectedSizeFilterListener(FilternameAndIDDo model) {
        allSelectedFilternameAndIDDo.remove(model);
        filterSelectionAdater.notifyDataSetChanged();


        if (SelectedSizeFilterList.size() > 0) {
            for (int i = 0; i < SelectedSizeFilterList.size(); i++) {
                if (SelectedSizeFilterList.get(i).id.equals(model.id)) {
                    SelectedSizeFilterList.remove(model);
                }
            }
        }

        for (int i = 0; i < filterheadings.size(); i++) {
            if (filterheadings.get(i).filterName.equals("Size")) {
                filterheadings.get(i).Counter = SelectedSizeFilterList.size();
            }
        }
        filterHeadingAdapter.notifyDataSetChanged();
        getFilteredProducts(SelectedSizeFilterList, SelectedPatternFilterList, SelectedMaterialFilterList
                , SelectedColorFilterList, SelectedSizeGroupFilterList, SelectedPriceFilterList, SelectedFeelerFilterList, SelectedCategoryFilterList,
                SelectedRunnerFilterList);
        LogUtils.error(ItemsActivity.class.getSimpleName(), "SelectedSizeFilterList Size is=" + SelectedSizeFilterList.size());
        openFilterLayout();
    }

    @Override
    public void removeSelectedSizegroupFilterListener(FilternameAndIDDo model) {
        allSelectedFilternameAndIDDo.remove(model);
        filterSelectionAdater.notifyDataSetChanged();


        if (SelectedSizeGroupFilterList.size() > 0) {
            for (int i = 0; i < SelectedSizeGroupFilterList.size(); i++) {
                if (SelectedSizeGroupFilterList.get(i).id.equals(model.id)) {
                    SelectedSizeGroupFilterList.remove(model);
                }
            }
        }

        for (int i = 0; i < filterheadings.size(); i++) {
            if (filterheadings.get(i).filterName.equals("SizeGroup")) {
                filterheadings.get(i).Counter = SelectedSizeGroupFilterList.size();
            }
        }
        filterHeadingAdapter.notifyDataSetChanged();
        getFilteredProducts(SelectedSizeFilterList, SelectedPatternFilterList, SelectedMaterialFilterList
                , SelectedColorFilterList, SelectedSizeGroupFilterList, SelectedPriceFilterList, SelectedFeelerFilterList, SelectedCategoryFilterList,
                SelectedRunnerFilterList);
        LogUtils.error(ItemsActivity.class.getSimpleName(), "SelectedSizeGroupFilterList Size is=" + SelectedSizeGroupFilterList.size());
        openFilterLayout();
    }

    @Override
    public void removeSelectedCategoryFilterListener(FilternameAndIDDo model) {
        allSelectedFilternameAndIDDo.remove(model);
        filterSelectionAdater.notifyDataSetChanged();


        if (SelectedCategoryFilterList.size() > 0) {
            for (int i = 0; i < SelectedCategoryFilterList.size(); i++) {
                if (SelectedCategoryFilterList.get(i).id.equals(model.id)) {
                    SelectedCategoryFilterList.remove(model);
                }
            }
        }

        for (int i = 0; i < filterheadings.size(); i++) {
            if (filterheadings.get(i).filterName.equals("Categories")) {
                filterheadings.get(i).Counter = SelectedCategoryFilterList.size();
            }
        }
        filterHeadingAdapter.notifyDataSetChanged();
        LogUtils.error(ItemsActivity.class.getSimpleName(), "SelectedCategoryFilterList Size is=" + SelectedCategoryFilterList.size());
        openFilterLayout();
    }

    @Override
    public void removeSelectedRunnerFilterListener(FilternameAndIDDo model) {

        allSelectedFilternameAndIDDo.remove(model);
        filterSelectionAdater.notifyDataSetChanged();
        if (SelectedRunnerFilterList.size() > 0) {
            for (int i = 0; i < SelectedRunnerFilterList.size(); i++) {
                if (SelectedRunnerFilterList.get(i).id.equals(model.id)) {
                    SelectedRunnerFilterList.remove(model);
                }
            }
        }

        for (int i = 0; i < filterheadings.size(); i++) {
            if (filterheadings.get(i).filterName.equals("Runners")) {
                filterheadings.get(i).Counter = SelectedRunnerFilterList.size();
            }
        }
        filterHeadingAdapter.notifyDataSetChanged();
        allSelectedFilternameAndIDDo.remove(model);
        grd_filterdata.notify();
        LogUtils.error(ItemsActivity.class.getSimpleName(), "SelectedRunnerFilterList Size is=" + SelectedRunnerFilterList.size());

        openFilterLayout();
    }


    @Override
    public void getSelectedGridItemListener(final ItemsDO itemsDO) {
        selecteditemsDOImg = itemsDO;
        this.itemSizeDO = itemsDO.itemSizeList.get(0);

        final ArrayList<String> sizeList = new ArrayList<>();
        for (int i = 0; i < itemsDO.itemSizeList.size(); i++) {
            sizeList.add(itemsDO.itemSizeList.get(i).ItemSize);
        }
        this.itemSizeDO = itemsDO.itemSizeList.get(0);


        spn_sizelist.setItems(sizeList);



        if(sizeList!=null && sizeList.size()>0){
            ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(ItemsActivity.this, R.layout.spinner_text_view, sizeList);
            dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spSizes.setAdapter(dataAdapter);
        }
        spSizes.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                tv_offer_price.setText("" + String.format("%.2f", Double.parseDouble(itemsDO.itemSizeList.get(position).FinalPrice)));
                tv_original_price.setText("£" + String.format("%.2f", Double.parseDouble(itemSizeDO.BasePrice)));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });



        if (itemSizeDO.savingstate) {
            tv_offer_price.setVisibility(View.VISIBLE);
            tv_original_price.setVisibility(View.VISIBLE);
            tv_offer_price.setText("" + String.format("%.2f", Double.parseDouble(itemSizeDO.FinalPrice)));
            tv_original_price.setText("£" + String.format("%.2f", Double.parseDouble(itemSizeDO.BasePrice)));
            tv_original_price.setPaintFlags(tv_original_price.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        } else {
            tv_offer_price.setVisibility(View.GONE);
            tv_original_price.setVisibility(View.VISIBLE);
            tv_offer_price.setText("" + String.format("%.2f", Double.parseDouble(itemSizeDO.FinalPrice)));
            tv_original_price.setText("£" + itemSizeDO.BasePrice);
            tv_original_price.setPaintFlags(tv_original_price.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        }
        img_grd.setImageResource(R.drawable.gridunselected);
        img_lst.setImageResource(R.drawable.listselected);
        int SelectedIndex = arrItems.indexOf(itemsDO);
        Collections.swap(arrItems, SelectedIndex, 0);
        itemsListAdapter = new ItemsListAdapter(mContext, arrItems, itemCartList, itemFavouriteList);

        itemsListAdapter.registerSelectedListItemListener(this);
        itemsListAdapter.registerCartListener(this);
        itemsListAdapter.registerFavouriteListener(this);

        lst_items.setAdapter(itemsListAdapter);
        lst_layout.setVisibility(View.VISIBLE);
        grd_layout.setVisibility(View.GONE);


        tv_itemName.setText(arrItems.get(0).ItemName);
        tv_disc.setText(arrItems.get(0).ItemProDesc);
        new FontType(mContext).applyfonttoView(tv_disc, FontType.ROBOTOLIGHT);
        new FontType(mContext).applyfonttoView(tv_material, FontType.ROBOTOLIGHT);
        new FontType(mContext).applyfonttoView(tv_product_id, FontType.ROBOTOLIGHT);

        //    txt_productdesc.setText(arrItems.get(0).ItemDesc);
        //   txt_productsecnddesc.setText(arrItems.get(0).ItemProDesc);
        tv_material.setText(arrItems.get(0).ItemMaterial);
        tv_product_id.setText(itemsDO.ProductID);


        positionColorList = arrItems.get(0).itemColorList;
        positionColorAdapter = new PositionColorAdapter(mContext, positionColorList);

        positionSizeList = arrItems.get(0).itemSizeList;
        positionSizeAdapter = new PositionSizeAdapter(mContext, positionSizeList);
        positionSizeAdapter.registerSizePriceListener(this);
        lst_SizePrice.setAdapter(positionSizeAdapter);

        if (arrItems.get(0).cartIconChecked) {
            //chk_addtocart.setText("Remove From cart");
        } else {
            chk_addtocart.setText("Add to cart");
        }

        if (itemsDO.cartIconChecked) {
//            showAlertDialog("This item already Added to Cart", "OK");
        }
        else {
            chk_addtocart.setText("Add to Cart");
        }
        imgLoader.getInstance().displayImage(arrItems.get(0).ItemBigImgUrl, new BgViewAware(ll_img_item), dispImage);
        setAdaptertoHorizontalList(arrItems.get(0));
        //  ll_img_item.setBackground(ContextCompat.getDrawable(mContext, R.drawable.edt_radius));

    }

    @Override
    public void getSelectedListItemListener(final ItemsDO itemsDO) {
        selecteditemsDOImg = itemsDO;

        ArrayList<String> sizeList = new ArrayList<>();
        for (int i = 0; i < itemsDO.itemSizeList.size(); i++) {
            sizeList.add(itemsDO.itemSizeList.get(i).ItemSize);
        }
        this.itemSizeDO = itemsDO.itemSizeList.get(0);

        spn_sizelist.setItems(sizeList);

        if (itemSizeDO.savingstate) {
            tv_offer_price.setVisibility(View.VISIBLE);
            tv_original_price.setVisibility(View.VISIBLE);
            tv_offer_price.setText("" + String.format("%.2f", Double.parseDouble(itemSizeDO.BasePrice)));
            tv_original_price.setText("£" + String.format("%.2f", Double.parseDouble(itemSizeDO.FinalPrice)));
            tv_original_price.setPaintFlags(tv_original_price.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        } else {
            tv_offer_price.setVisibility(View.VISIBLE);
            tv_original_price.setVisibility(View.VISIBLE);
            tv_offer_price.setText("" + String.format("%.2f", Double.parseDouble(itemSizeDO.BasePrice)));
            tv_original_price.setText("£" + itemSizeDO.FinalPrice);
            tv_original_price.setPaintFlags(tv_original_price.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);

        }

        tv_itemName.setText(itemsDO.ItemName);
        //  txt_productdesc.setText(itemsDO.ItemDesc);
        //  txt_productsecnddesc.setText(itemsDO.ItemProDesc);
        tv_material.setText(itemsDO.ItemMaterial);
        tv_product_id.setText(itemsDO.ProductID);


        positionSizeList = itemsDO.itemSizeList;
        positionSizeAdapter = new PositionSizeAdapter(mContext, positionSizeList);
        positionSizeAdapter.registerSizePriceListener(this);
        lst_SizePrice.setAdapter(positionSizeAdapter);

        if (itemsDO.cartIconChecked) {
            //chk_addtocart.setText("Remove from Cart");
        } else {
            chk_addtocart.setText("Add to cart");
        }

        if (itemsDO.cartIconChecked) {
//            showAlertDialog("This item already Added to Cart", "OK");
        }
        else {
            chk_addtocart.setText("Add to Cart");
        }

        // need to read qty from DB and bind with item Id
        new Thread(new Runnable() {
            @Override
            public void run() {
               final int cartQty =  CartDatabase.getCartQuantity(itemsDO.ItemId);
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if(cartQty>1){
                            tvQuantity.setText(""+cartQty);
                        }
                        else {
                            tvQuantity.setText("1");
                        }
                    }
                });
            }
        }).start();


        imgLoader.getInstance().displayImage(itemsDO.ItemBigImgUrl, new BgViewAware(ll_img_item), dispImage);
        setAdaptertoHorizontalList(itemsDO);
        //  ll_img_item.setBackground(ContextCompat.getDrawable(mContext, R.drawable.edt_radius));

    }

    @Override
    public void getClickBuynow(ItemsDO itemsDO) {


        addItemsToCartList(itemsDO);
    }

    private void addItemsToCartList(ItemsDO itemsDO) {

    }

    @Override
    public void getSelectedSize(ItemsSizeDO itemSizeDO) {
        this.itemSizeDO = itemSizeDO;
        if (itemSizeDO.savingstate) {
            tv_offer_price.setVisibility(View.VISIBLE);
            tv_original_price.setVisibility(View.VISIBLE);
            tv_offer_price.setText("" + df.format(Double.parseDouble(itemSizeDO.BasePrice)));
            tv_original_price.setText("£" + df.format(Double.parseDouble(itemSizeDO.FinalPrice)));
            tv_original_price.setPaintFlags(tv_original_price.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        } else {
            tv_offer_price.setVisibility(View.VISIBLE);
            tv_original_price.setVisibility(View.VISIBLE);
            tv_offer_price.setText("" + df.format(Double.parseDouble(itemSizeDO.BasePrice)));
            tv_original_price.setText("£" + itemSizeDO.FinalPrice);
            tv_original_price.setPaintFlags(tv_original_price.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        }
    }

    @Override
    public void clearFilter(FilterHeadingDO filterHeadingDO) {
        clearAllSelectedParticularFIlter(filterHeadingDO);
    }

    private void clearAllSelectedParticularFIlter(FilterHeadingDO filterHeadingDO) {

        if (filterHeadingDO.filterName.equalsIgnoreCase("Price")) {
            SelectedPriceFilterList.clear();

            for (int i = 0; i < PricesNamesList.size(); i++) {
                PricesNamesList.get(i).checkState = false;
            }
        } else if (filterHeadingDO.filterName.equalsIgnoreCase("Colour")) {
            SelectedColorFilterList.clear();
            for (int i = 0; i < ColorNamesList.size(); i++) {
                ColorNamesList.get(i).checkState = false;
            }
        } else if (filterHeadingDO.filterName.equalsIgnoreCase("Material")) {
            SelectedMaterialFilterList.clear();

            for (int i = 0; i < MaterialNamesList.size(); i++) {
                MaterialNamesList.get(i).checkState = false;
            }
        } else if (filterHeadingDO.filterName.equalsIgnoreCase("Pattern")) {
            SelectedPatternFilterList.clear();

            for (int i = 0; i < PatternNamesList.size(); i++) {
                PatternNamesList.get(i).checkState = false;
            }
        } else if (filterHeadingDO.filterName.equalsIgnoreCase("SizeGroup")) {
            SelectedSizeGroupFilterList.clear();

            for (int i = 0; i < SizeGroupNamesList.size(); i++) {
                SizeGroupNamesList.get(i).checkState = false;
            }
        } else if (filterHeadingDO.filterName.equalsIgnoreCase("Size")) {
            SelectedSizeFilterList.clear();

            for (int i = 0; i < SizeNamesList.size(); i++) {
                SizeNamesList.get(i).checkState = false;
            }
        } else if (filterHeadingDO.filterName.equalsIgnoreCase("Categories")) {
            SelectedCategoryFilterList.clear();

            for (int i = 0; i < CategoryList.size(); i++) {
                CategoryList.get(i).checkState = false;
            }
        } else if (filterHeadingDO.filterName.equalsIgnoreCase("Runners")) {
            SelectedRunnerFilterList.clear();

            for (int i = 0; i < RunnerList.size(); i++) {
                RunnerList.get(i).checkState = false;
            }
        }

        sizeGroupFilterAdapter.notifyDataSetChanged();

        for (int i = 0; i < filterheadings.size(); i++) {
            if (filterheadings.get(i).filterName.equalsIgnoreCase(filterHeadingDO.filterName)) {
                filterheadings.get(i).Counter = 0;
            }
        }
        filterHeadingAdapter.notifyDataSetChanged();
    }

    @Override
    public void filterselctionclose(FilternameAndIDDo filternameAndIDDo) {

        allSelectedFilternameAndIDDo.remove(filternameAndIDDo);
        filterSelectionAdater.notifyDataSetChanged();
        if (filternameAndIDDo.type.equalsIgnoreCase("Price")) {
            SelectedPriceFilterList.remove(filternameAndIDDo);
        } else if (filternameAndIDDo.type.equalsIgnoreCase("Size")) {
            SelectedSizeFilterList.remove(filternameAndIDDo);
        } else if (filternameAndIDDo.type.equalsIgnoreCase("Pattern")) {
            SelectedPatternFilterList.remove(filternameAndIDDo);
        } else if (filternameAndIDDo.type.equalsIgnoreCase("Material")) {
            SelectedMaterialFilterList.remove(filternameAndIDDo);
        } else if (filternameAndIDDo.type.equalsIgnoreCase("SizeGroup")) {
            SelectedSizeGroupFilterList.remove(filternameAndIDDo);
        } else if (filternameAndIDDo.type.equalsIgnoreCase("Colour")) {
            SelectedColorFilterList.remove(filternameAndIDDo);
        }
        filterHeadingAdapter.notifyDataSetChanged();
        sizeGroupFilterAdapter.notifyDataSetChanged();
        getFilteredProducts(SelectedSizeFilterList, SelectedPatternFilterList, SelectedMaterialFilterList
                , SelectedColorFilterList, SelectedSizeGroupFilterList, SelectedPriceFilterList, SelectedFeelerFilterList, SelectedCategoryFilterList,
                SelectedRunnerFilterList);
    }
}
