package com.yits.rugshop.listener;

import com.yits.rugshop.objects.ItemsDO;

/**
 * Created by sai on 6/1/2016.
 */
public interface UpdateListView {
    public void update();
    void addTocart(ItemsDO itemsDO);
}
