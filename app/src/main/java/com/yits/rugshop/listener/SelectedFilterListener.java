package com.yits.rugshop.listener;

import com.yits.rugshop.objects.FilternameAndIDDo;


public interface SelectedFilterListener {

    public void getSelectedColorFilterListener  (FilternameAndIDDo model);

    public void getSelectedMaterialFilterListener(FilternameAndIDDo model) ;

    public void getSelectedPatternFilterListener(FilternameAndIDDo model) ;

    public void getSelectedPriceFilterListener(FilternameAndIDDo model);

    public void getSelectedSizeFilterListener(FilternameAndIDDo model);

    public void getSelectedSizegroupFilterListener(FilternameAndIDDo model);

    public void getSelectedCategoryFilterListener(FilternameAndIDDo model);

    public void getSelectedRunnerFilterListener(FilternameAndIDDo model);



    public void removeSelectedColorFilterListener  (FilternameAndIDDo model);

    public void removeSelectedMaterialFilterListener(FilternameAndIDDo model) ;

    public void removeSelectedPatternFilterListener(FilternameAndIDDo model) ;

    public void removeSelectedPriceFilterListener(FilternameAndIDDo model);

    public void removeSelectedSizeFilterListener(FilternameAndIDDo model);

    public void removeSelectedSizegroupFilterListener(FilternameAndIDDo model);

    public void removeSelectedCategoryFilterListener(FilternameAndIDDo model);

    public void removeSelectedRunnerFilterListener(FilternameAndIDDo model);




}
