package com.yits.rugshop.listener;

import com.yits.rugshop.objects.ItemsDO;

public interface SelectedItemListener {

    public void getSelectedGridItemListener(ItemsDO itemsDO);

    public void getSelectedListItemListener(ItemsDO itemsDO);

    public void getClickBuynow(ItemsDO itemsDO);
}
