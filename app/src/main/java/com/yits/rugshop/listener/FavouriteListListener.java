package com.yits.rugshop.listener;

import com.yits.rugshop.objects.ItemfavouriteDO;

import java.util.ArrayList;

/**
 * Created by win8 on 14-01-2016.
 */
public interface FavouriteListListener {


    public void getFavouriteList(ArrayList<ItemfavouriteDO> itemFavouriteList);
}
