package com.yits.rugshop.listener;

/**
 * Created by sai on 8/25/2016.
 */
public interface GetStringOnClickListener {

    public void getStringOnClick(String url);
}
