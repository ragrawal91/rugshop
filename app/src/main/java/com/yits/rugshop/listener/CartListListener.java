package com.yits.rugshop.listener;

import com.yits.rugshop.objects.ItemsDO;

/**
 * Created by win8 on 14-01-2016.
 */
public interface CartListListener {


    public void getCartList(ItemsDO model);

    public void getLocalDatabaseCartList();

    public void getTheIncreasedSubtotal(Double itemFinalPrice,Double itemVendorPrice);

    public void getTheDecreasedSubtotal(Double itemFinalPrice,Double itemVendorPrice);
}
