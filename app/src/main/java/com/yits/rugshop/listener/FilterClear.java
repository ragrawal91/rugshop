package com.yits.rugshop.listener;

import com.yits.rugshop.objects.FilterHeadingDO;

/**
 * Created by sai on 6/6/2016.
 */
public interface FilterClear {
    public void clearFilter(FilterHeadingDO filterHeadingDO);
}
