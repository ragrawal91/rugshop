package com.yits.rugshop.listener;

import com.yits.rugshop.objects.ItemsSizeDO;

/**
 * Created by win8 on 14-01-2016.
 */
public interface GetSelectedSizeListener {


    public void getSelectedSize(ItemsSizeDO itemSizeDO);


}
