package com.yits.rugshop.common;

/**
 * Created by Kishore.G on 5/6/2016.
 */
public interface SingleSelectedItemListener {

    public abstract void setSingleSelectedItem(Object selectedObject);
}
